export default {
    tier: {
        name: 'Please fill the tier name. This is the name for loyalty app\'s tier.',
        groupId: 'Bepoz group id that will be linked with the app\'s tier',
        selectCustomField: 'The custom field that is setup in bepoz for the pre-created account that the app will use for its new users signup.',
        allowVenue: 'Once a user has been created then this user can only view and select venues that have this venue TAG associated to them via the preferred venue option within the App.'
    },
    legals: {
        termWebviewEnable: 'This option allows you to use current legals that you may have on a website via a URL as opposed to typing your own Legals.'
    },
    venue: {
        yourorderIntegration: 'Enable this option to setup your order url and location'
    }

}
