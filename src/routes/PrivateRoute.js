
import React, {useEffect} from 'react';
import {connect, useSelector} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';

function PrivateRoute({ children, ...rest }) {
  const loggedIn = useSelector(state => state.user.loggedIn)

  return (
    <Route
      {...rest}
      render={({ location }) =>
        loggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute
