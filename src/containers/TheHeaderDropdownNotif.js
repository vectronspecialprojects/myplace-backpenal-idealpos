import React from 'react'
import {
  CBadge,
  CButton,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CTooltip,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons'
import {postClearNotification} from '../utilities/ApiManage'
import {useHistory} from 'react-router-dom'
import ROUTES from '../constants/routes'

const TheHeaderDropdownNotif = () => {
  const history = useHistory()
  return (
    <CDropdown inNav className="c-header-nav-item mx-2">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <CIcon name="cil-bell" />
        <CBadge shape="pill" color="danger">
          0
        </CBadge>
      </CDropdownToggle>
      <CDropdownMenu placement="bottom-end" className="pt-0">
        <CDropdownItem header tag="div" className="text-center d-flex align-items-center" color="light">
          <h5 className="m-0 pr-5">Notification</h5>
          <CButton onClick={() => history.push(ROUTES.BEPOZ_REPORT)}>
            <CTooltip content="Bepoz Log">
              <CIcon content={freeSet.cilListRich} />
            </CTooltip>
          </CButton>
          <CButton onClick={() => history.push(ROUTES.GAMING_REPORT)}>
            <CTooltip content="Gaming Log">
              <CIcon content={freeSet.cilListRich} />
            </CTooltip>
          </CButton>
          <CButton onClick={() => history.push(ROUTES.SYSTEM_REPORT)}>
            <CTooltip content="System Log">
              <CIcon content={freeSet.cilListRich} />
            </CTooltip>
          </CButton>
          <CButton onClick={postClearNotification}>
            <CTooltip content="Clear all notification">
              <CIcon content={freeSet.cilTrash} />
            </CTooltip>
          </CButton>
        </CDropdownItem>
        <CDropdownItem>No Notification</CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdownNotif
