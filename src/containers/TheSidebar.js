import React, {useEffect, useMemo, useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CSidebarFooter,
  CCol,
} from '@coreui/react'

// sidebar nav config
import navigation, {LISTINGS, SETTINGS, SIDE_BAR} from './_nav'
import {setShowNav} from '../redux/actions/app'
import Images from '../themes/Images'
import {getVersion, getSsoVersion} from '../utilities/ApiManage'
import {isTrue} from '../helpers'
import ROUTES from '../constants/routes'

const TheSidebar = () => {
  const dispatch = useDispatch()
  const [version, setVersion] = useState({})
  const show = useSelector((state) => state.app.sidebarShow)
  const listing = useSelector((state) => state.app.listing)
  const setting = useSelector((state) => state.app.appSettings)
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const sideNav = setting?.sidenavigationbar
  const yourl = setting?.your_order_api
  const [minimize, setMinimize] = useState(false)
  const [versionB, setVersionB] = useState(bepozVersion)
  const [versionS, setVersionS] = useState('')

  useEffect(() => {
    setVersionB(bepozVersion)
  }, [bepozVersion])

  useEffect(() => {
    ;(async () => {
      const res = await getVersion()
      if (yourl.value.replace('v1/sso', 'version')) {
        const ssoVersion = await getSsoVersion(yourl.value.replace('v1/sso', 'version'))
        if (ssoVersion.ok) {
          setVersionS(ssoVersion)
        }
      }
      if (res.ok) {
        setVersion(res)
      }
    })()
  }, [])

  const navList = useMemo(() => {
    try {
      let listData = [...LISTINGS]
      listing?.map((item) => {
        if (item.id === 8) return
        if (!item?.key?.hide_this) {
          listData.push({
            _tag: 'CSidebarNavItem',
            name: item.name,
            to: `/listing/${item.id}`,
            icon: 'cil-star',
          })
        }
      })
      const data = JSON.parse(sideNav?.extended_value)
      listData.push({
        _tag: 'CSidebarNavTitle',
        _children: ['Settings'],
      })
      data?.map((item) => {
        if (item?.key === 'dashboard') {
          return
        }
        if (item?.visible) {
          listData.push({
            _tag: 'CSidebarNavItem',
            name: item.label,
            to: item.path,
            icon: SIDE_BAR[item.key]?.icon,
          })
        }
      })
      listData = [...listData, ...SETTINGS]
      if (isTrue(setting.gaming_system_enable?.value)) {
        listData = [
          ...listData,
          {
            _tag: 'CSidebarNavItem',
            name: 'Gaming Test',
            to: ROUTES.IGT,
            icon: 'cil-settings',
          },
        ]
      }
      if (data?.[0]?.key === 'dashboard' && data?.[0]?.visible) {
        return [...navigation, ...listData]
      } else {
        return listData
      }
    } catch (e) {
      console.log(e)
    }
  }, [listing, sideNav])

  return (
    <CSidebar
      className="bg-gradient-dark"
      show={show}
      onShowChange={(val) => dispatch(setShowNav(val))}
      minimize={minimize}
      onMinimizeChange={() => setMinimize(!minimize)}>
      <CSidebarBrand className="d-md-down-none" to="/">
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <img src={Images.logo} width="100%" height="100%" className="img-fluid" />
          <span style={{fontSize: 10, color: '#fff', alignSelf: 'flex-end', marginRight: 20}}>
            v.{versionB?.Data1}
          </span>
        </div>
      </CSidebarBrand>
      <CSidebarNav className="side-bar">
        <CCreateElement
          items={navList}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none">
        {!minimize && (
          <div style={{display: 'flex', flexDirection: 'column', position: 'absolute', left: 50, bottom: 5}}>
            <span style={{fontSize: 9, color: '#fff', margin: 0, padding: 0}}>BP: v.4.0.0</span>
            <span style={{fontSize: 9, color: '#fff'}}>BE: v.{version.version || ''}</span>
            <span style={{fontSize: 9, color: '#fff'}}>SSO v.{versionS?.data}</span>
          </div>
        )}
      </CSidebarMinimizer>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
