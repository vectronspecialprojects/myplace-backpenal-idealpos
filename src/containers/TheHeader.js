import React, {useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {
  CBreadcrumbRouter,
  CButton,
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CToggler,
  CTooltip,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

// routes config
import routes from '../routes'

import {TheHeaderDropdownNotif} from './index'
import {setShowNav} from '../redux/actions/app'
import {freeSet} from '@coreui/icons'
import PopupSendEmail from './PopupSendEmail'
import {API_URI} from '../constants/constants'
import {getAccessToken} from '../utilities/NetworkingAuth'
import {setModal} from '../reusable/Modal'
import PopupChangePassword from './PopupChangePassword'
import {userLogout} from '../redux/actions/user'
import ROUTES from '../constants/routes'

const TheHeader = () => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector((state) => state.app.sidebarShow)
  const [showSendEmail, setShowSendEmail] = useState(false)
  const [showChangePassword, setShowChangePassword] = useState(false)

  const downloadManual = () => {
    setModal({
      show: true,
      title: 'DOWNLOAD MANUAL',
      message: 'DO YOU WANT TO PROCEED?',
      primaryBtnClick: () => {
        window.open(`${API_URI}admin/manual?token=${getAccessToken()}`, '_blank')
      },
    })
  }

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch(setShowNav(val))
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch(setShowNav(val))
  }

  const logout = () => {
    dispatch(userLogout())
    window.location.replace(ROUTES.LOGIN)
  }

  return (
    <CHeader withSubheader>
      <CToggler inHeader className="ml-md-3 d-lg-none" onClick={toggleSidebarMobile} />
      <CToggler inHeader className="ml-3 d-md-down-none" onClick={toggleSidebar} />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink to="/dashboard">Dashboard</CHeaderNavLink>
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        <CButton onClick={() => setShowSendEmail(true)}>
          <CTooltip content="Send HelpDesk email">
            <CIcon content={freeSet.cilEnvelopeClosed} />
          </CTooltip>
        </CButton>
        <CButton onClick={downloadManual}>
          <CTooltip content="Download Manual">
            <CIcon content={freeSet.cilCloudDownload} />
          </CTooltip>
        </CButton>
        <CButton onClick={() => setShowChangePassword(true)}>
          <CTooltip content="Change password">
            <CIcon content={freeSet.cilBarcode} />
          </CTooltip>
        </CButton>
        <TheHeaderDropdownNotif />
        <CButton onClick={logout}>
          <CTooltip content="Log out">
            <CIcon content={freeSet.cilAccountLogout} />
          </CTooltip>
        </CButton>
      </CHeaderNav>

      <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter className="border-0 c-subheader-nav m-0 px-0 px-md-3" routes={routes} />
      </CSubheader>
      <PopupSendEmail show={showSendEmail} closeModal={() => setShowSendEmail(false)} />
      <PopupChangePassword show={showChangePassword} closeModal={() => setShowChangePassword(false)} />
    </CHeader>
  )
}

export default TheHeader
