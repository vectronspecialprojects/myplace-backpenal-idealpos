import React from 'react'
import CIcon from '@coreui/icons-react'
import {cilFastfood, cilCash} from '@coreui/icons/js/free'
import ROUTES from '../constants/routes'
import {LISTING} from '../views/Listing/constant'
import {cilGift} from '@coreui/icons';

const _nav = [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: ROUTES.DASHBOARD,
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />,
  },
]
export const LISTINGS = [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Listing'],
  },
]

export const SETTINGS = [
  {
    _tag: 'CSidebarNavItem',
    name: 'Settings',
    to: ROUTES.SETTING,
    icon: 'cil-settings',
  },
]

export const SIDE_BAR = {
  tickets: {
    _tag: 'CSidebarNavItem',
    name: 'Tickets',
    to: '/product',
    icon: <CIcon content={cilFastfood} customClasses="c-sidebar-nav-icon" />,
  },
  vouchers: {
    _tag: 'CSidebarNavItem',
    name: 'Vouchers',
    to: ROUTES.VOUCHER,
    icon: 'cil-puzzle',
  },
  members: {
    _tag: 'CSidebarNavItem',
    name: 'Members',
    to: ROUTES.MEMBER,
    icon: 'cil-cursor',
  },
  staff: {
    _tag: 'CSidebarNavItem',
    name: 'Staff',
    to: ROUTES.STAFF,
    icon: 'cil-chart-pie',
  },
  notifications: {
    _tag: 'CSidebarNavItem',
    name: 'Notifications',
    to: ROUTES.SYSTEM_NOTIFICATION,
    icon: 'cil-bell',
  },
  enquiries: {
    _tag: 'CSidebarNavItem',
    name: 'Enquiries',
    to: ROUTES.ENQUIRY,
    icon: 'cil-envelope-letter',
  },
  faqs: {
    _tag: 'CSidebarNavItem',
    name: 'FAQs',
    to: ROUTES.FAQ,
    icon: 'cil-list-rich',
  },
  friendreferrals: {
    _tag: 'CSidebarNavItem',
    name: 'Friend Referrals',
    to: ROUTES.FRIEND_REFERRAL,
    icon: 'cil-user-follow',
  },
  transactions: {
    _tag: 'CSidebarNavItem',
    name: 'Transactions',
    to: ROUTES.TRANSACTION,
    icon: <CIcon content={cilCash} customClasses="c-sidebar-nav-icon" />,
  },
  survey: {
    _tag: 'CSidebarNavItem',
    name: 'Survey',
    to: ROUTES.SURVEY,
    icon: 'cil-list-numbered',
  },
  specialProduct: {
    _tag: 'CSidebarNavItem',
    name: 'Products',
    to: ROUTES.PRODUCT,
    icon: <CIcon content={cilGift} customClasses="c-sidebar-nav-icon" />,
  },
}

export default _nav
