import React, {useEffect} from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'
import {useDispatch} from 'react-redux';
import {appGetData, getBepozVersion} from '../redux/actions/app';

const TheLayout = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(appGetData())
    dispatch(getBepozVersion())
  }, [])

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <TheContent/>
        </div>
        {/*<TheFooter/>*/}
      </div>
    </div>
  )
}

export default TheLayout
