import React, {useEffect, useState} from 'react'
import {CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'
import FormInput from '../common/FormInput'
import {postSendHelpDesk} from '../utilities/ApiManage'
import {updateForm} from '../helpers'
import FormSelect from '../common/FormSelect'
import FormTextarea from '../common/FormTextarea'

function PopupSendEmail({show, closeModal}) {
  const [form, setForm] = useState({})

  const changeForm = (event) => setForm((oldState) => updateForm(event, oldState))

  useEffect(() => {
    setForm({cc: '', from: '', message: '', subject: '', to: ''})
  }, [show])

  const saveAndClose = async () => {
    const res = await postSendHelpDesk(form)
    closeModal()
  }

  return (
    <>
      <CModal show={show} onClose={closeModal}>
        <CModalHeader closeButton>
          <CModalTitle>Email Send</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <FormSelect label="To" id="to" options={EMAIL} value={form.to} onChange={changeForm} />
          <FormInput label="CC" id="cc" value={form.cc} onChange={changeForm} />
          <FormInput label="From" id="from" value={form.from} onChange={changeForm} />
          <FormInput label="Subject" id="subject" value={form.subject} onChange={changeForm} />
          <FormTextarea label="Message" id="message" value={form.message} onChange={changeForm} />
        </CModalBody>
        <CModalFooter>
          <CButton
            color="primary"
            onClick={saveAndClose}
            disabled={!form.from || !form.message || !form.subject || !form.to}>
            SEND
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupSendEmail

const EMAIL = [
  {label: 'New South Wales', value: 'support@vectron.com.au'},
  {label: 'Victoria', value: 'support@vectron.com.au'},
  {label: 'Queensland', value: 'support@bepoz.com.au'},
  {label: 'Western Australia', value: 'support@vectron.com.au'},
  {label: 'South Australia', value: 'support@vectron.com.au'},
  {label: 'Tasmania', value: 'support@vectron.com.au'},
  {label: 'Australian Capital Territory', value: 'support@bepoz.com.au'},
  {label: 'Northern Territory', value: 'support@bepoz.com.au'},
]
