import React, {useEffect, useState} from 'react'
import {CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'
import FormInput from '../common/FormInput'
import {postSendHelpDesk} from '../utilities/ApiManage'
import {updateForm} from '../helpers'

function PopupChangePassword({show, closeModal}) {
  const [form, setForm] = useState({})

  const changeForm = (event) => setForm((oldState) => updateForm(event, oldState))

  useEffect(() => {
    setForm({new_password: '', new_password_confirmation: '', old_password: ''})
  }, [show])

  const saveAndClose = async () => {
    const res = await postSendHelpDesk(form)
    if (res.ok) {
      closeModal()
    }
  }

  return (
    <>
      <CModal show={show} onClose={closeModal}>
        <CModalHeader closeButton>
          <CModalTitle>Change Password</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <FormInput
            label="Old Password"
            required
            id="old_password"
            value={form.old_password}
            onChange={changeForm}
            type="password"
            maxLength={60}
          />
          <FormInput
            label="New Password"
            required
            id="new_password"
            value={form.new_password}
            onChange={changeForm}
            type="password"
            maxLength={60}
          />
          <FormInput
            label="Confirm New Password"
            required
            id="new_password_confirmation"
            value={form.new_password_confirmation}
            onChange={changeForm}
            type="password"
            maxLength={60}
          />
        </CModalBody>
        <CModalFooter>
          <CButton
            color="primary"
            onClick={saveAndClose}
            disabled={!form.new_password || !form.old_password || !form.new_password_confirmation}>
            CHANGE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupChangePassword
