import React from 'react'
import ROUTES from './constants/routes'
import DisplayOrder from './views/Listing/DisplayOrder'

const Dashboard = React.lazy(() => import('./views/Dashboard'))
const ListingPage = React.lazy(() => import('./views/Listing/ListingPage'))
const UpdatePage = React.lazy(() => import('./views/Listing/UpdatePage'))
const CreatePage = React.lazy(() => import('./views/Listing/CreatePage'))

const TicketListPage = React.lazy(() => import('./views/Ticket/ListPage'))
const TicketUpdatePage = React.lazy(() => import('./views/Ticket/UpdatePage'))
const TicketCreatePage = React.lazy(() => import('./views/Ticket/CreatePage'))

const VouchersPage = React.lazy(() => import('./views/Voucher/VouchersPage'))
const VouchersUpdatePage = React.lazy(() => import('./views/Voucher/VouchersUpdatePage'))
const VouchersAddPage = React.lazy(() => import('./views/Voucher/VouchersAddPage'))

const MemberListPage = React.lazy(() => import('./views/Member/ListPage'))
const MemberUpdatePage = React.lazy(() => import('./views/Member/UpdatePage'))
const MemberCreatePage = React.lazy(() => import('./views/Member/CreatePage'))

const StaffListPage = React.lazy(() => import('./views/Staff/ListPage'))

const SystemNotificationListPage = React.lazy(() => import('./views/SystemNotification/ListPage'))
const SystemNotificationUpdatePage = React.lazy(() => import('./views/SystemNotification/UpdatePage'))
const SystemNotificationCreatePage = React.lazy(() => import('./views/SystemNotification/CreatePage'))

const EnquiryListPage = React.lazy(() => import('./views/Enquiry/ListPage'))

const FAQListPage = React.lazy(() => import('./views/FAQ/ListPage'))

const FriendReferralListPage = React.lazy(() => import('./views/FriendReferral/ListPage'))

const SurveyListPage = React.lazy(() => import('./views/Survey/ListPage'))
const SurveyCreatePage = React.lazy(() => import('./views/Survey/CreatePage'))
const SurveyUpdatePage = React.lazy(() => import('./views/Survey/UpdatePage'))
const SurveyReportPage = React.lazy(() => import('./views/Survey/ReportPage'))
const SurveyGenerateReportPage = React.lazy(() => import('./views/Survey/GenerateReportPage'))

const QuestionListPage = React.lazy(() => import('./views/Question/ListPage'))
const QuestionCreatePage = React.lazy(() => import('./views/Question/CreatePage'))
const QuestionUpdatePage = React.lazy(() => import('./views/Question/UpdatePage'))

const TransactionListPage = React.lazy(() => import('./views/Transactions/ListPage'))
const TransactionDetailPage = React.lazy(() => import('./views/Transactions/DetailPage'))

const AnswerListPage = React.lazy(() => import('./views/Answer/ListPage'))
const ProductListPage = React.lazy(() => import('./views/Product/ListPage'))
const ProductCreatePage = React.lazy(() => import('./views/Product/CreatePage'))
const ProductUpdatePage = React.lazy(() => import('./views/Product/UpdatePage'))

const SettingPage = React.lazy(() => import('./views/Setting'))

const IGTPage = React.lazy(() => import('./views/IGTTest'))

const MembersReportPage = React.lazy(() => import('./views/Report/MembersReport'))
const BepozReportPage = React.lazy(() => import('./views/Report/BepozLog'))
const SystemReportPage = React.lazy(() => import('./views/Report/SystemLog'))
const GamingReportPage = React.lazy(() => import('./views/Report/GamingReport'))

const routes = [
  {path: '/', exact: true, name: 'Home'},
  {path: ROUTES.DASHBOARD, name: 'Dashboard', component: Dashboard},

  {path: ROUTES.LISTING_UPDATE, name: 'Update', component: UpdatePage, exact: true},
  {path: ROUTES.LISTING_CREATE, name: 'Create', component: CreatePage, exact: true},
  {path: ROUTES.LISTING_DISPLAY_ORDER, name: 'DisplayOrder', component: DisplayOrder, exact: true},
  {path: ROUTES.LISTING, name: 'Listing', component: ListingPage, exact: true},

  {path: ROUTES.TICKET_UPDATE, name: 'Update Ticket', component: TicketUpdatePage, exact: true},
  {path: ROUTES.TICKET_CREATE, name: 'Create Ticket', component: TicketCreatePage, exact: true},
  {path: ROUTES.TICKET, name: 'Ticket List', component: TicketListPage, exact: true},

  {path: ROUTES.VOUCHER_UPDATE, name: 'Update Voucher', component: VouchersUpdatePage, exact: true},
  {path: ROUTES.VOUCHER_CREATE, name: 'Create  Voucher', component: VouchersAddPage, exact: true},
  {path: ROUTES.VOUCHER, name: 'Vouchers', component: VouchersPage, exact: true},

  {path: ROUTES.MEMBER_UPDATE, name: 'Update Member', component: MemberUpdatePage, exact: true},
  {path: ROUTES.MEMBER_CREATE, name: 'Create Member', component: MemberCreatePage, exact: true},
  {path: ROUTES.MEMBER, name: 'Member List', component: MemberListPage, exact: true},

  {path: ROUTES.STAFF, name: 'Staff List', component: StaffListPage, exact: true},

  {
    path: ROUTES.SYSTEM_NOTIFICATION_UPDATE,
    name: 'Update System Notification',
    component: SystemNotificationUpdatePage,
    exact: true,
  },
  {
    path: ROUTES.SYSTEM_NOTIFICATION_CREATE,
    name: 'Create System Notification',
    component: SystemNotificationCreatePage,
    exact: true,
  },
  {
    path: ROUTES.SYSTEM_NOTIFICATION,
    name: 'System Notification List',
    component: SystemNotificationListPage,
    exact: true,
  },

  {path: ROUTES.ENQUIRY, name: 'Enquiry List', component: EnquiryListPage, exact: true},

  {path: ROUTES.FAQ, name: 'FAQs List', component: FAQListPage, exact: true},

  {
    path: ROUTES.FRIEND_REFERRAL,
    name: 'Friend Referral List',
    component: FriendReferralListPage,
    exact: true,
  },

  {path: ROUTES.SURVEY, name: 'Survey List', component: SurveyListPage, exact: true},
  {path: ROUTES.SURVEY_CREATE, name: 'Create Survey', component: SurveyCreatePage, exact: true},
  {path: ROUTES.SURVEY_UPDATE, name: 'Update Survey', component: SurveyUpdatePage, exact: true},
  {path: ROUTES.SURVEY_REPORT, name: 'Survey Detail', component: SurveyReportPage, exact: true},
  {
    path: ROUTES.SURVEY_REPORT_GENERATE,
    name: 'Survey Generate Report',
    component: SurveyGenerateReportPage,
    exact: true,
  },

  {path: ROUTES.QUESTION, name: 'Question List', component: QuestionListPage, exact: true},
  {path: ROUTES.QUESTION_CREATE, name: 'Create Question', component: QuestionCreatePage, exact: true},
  {path: ROUTES.QUESTION_UPDATE, name: 'Update Question', component: QuestionUpdatePage, exact: true},

  {path: ROUTES.ANSWER, name: 'Answer List', component: AnswerListPage, exact: true},
  {path: ROUTES.PRODUCT, name: 'Product List', component: ProductListPage, exact: true},
  {path: ROUTES.PRODUCT_CREATE, name: 'Create Product', component: ProductCreatePage, exact: true},
  {path: ROUTES.PRODUCT_UPDATE, name: 'Update Product', component: ProductUpdatePage, exact: true},

  {path: ROUTES.TRANSACTION, name: 'Transaction List', component: TransactionListPage, exact: true},
  {
    path: ROUTES.TRANSACTION_DETAIL,
    name: 'Transaction Detail',
    component: TransactionDetailPage,
    exact: true,
  },

  {
    path: ROUTES.TRANSACTION_DETAIL,
    name: 'Transaction Detail',
    component: TransactionDetailPage,
    exact: true,
  },

  
  {path: ROUTES.SETTING, name: 'Setting', component: SettingPage, exact: true},

  {path: ROUTES.IGT, name: 'IGT Test', component: IGTPage, exact: true},

  {path: ROUTES.MEMBER_REPORT, name: 'Members Report', component: MembersReportPage, exact: true},
  {path: ROUTES.BEPOZ_REPORT, name: 'Bepoz Report', component: BepozReportPage, exact: true},
  {path: ROUTES.SYSTEM_REPORT, name: 'System Report', component: SystemReportPage, exact: true},
  {path: ROUTES.GAMING_REPORT, name: 'Gaming Report', component: GamingReportPage, exact: true},
]

export default routes
