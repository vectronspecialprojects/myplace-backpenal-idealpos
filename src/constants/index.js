export const MODE = {
  NEW: 'new',
  NOT_MODIFIED: 'not_modified',
  DELETED: 'deleted',
}
