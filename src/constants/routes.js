const ROUTES = {
  DASHBOARD: '/dashboard',
  LOGIN: '/login',

  LISTING: '/listing/:id',
  LISTING_CREATE: '/listing/:id/create',
  LISTING_UPDATE: '/listing/:type_id/update/:id',
  LISTING_DISPLAY_ORDER: '/listing/:id/displayorder',

  TICKET: '/product',
  TICKET_CREATE: '/product/create',
  TICKET_UPDATE: '/product/update/:id',

  VOUCHER: '/voucher',
  VOUCHER_CREATE: '/voucher/create',
  VOUCHER_UPDATE: '/voucher/update/:id',

  MEMBER: '/member',
  MEMBER_CREATE: '/member/create',
  MEMBER_UPDATE: '/member/update/:id',

  STAFF: '/staff',

  SYSTEM_NOTIFICATION: '/systemNotification',
  SYSTEM_NOTIFICATION_CREATE: '/systemNotification/create',
  SYSTEM_NOTIFICATION_UPDATE: '/systemNotification/update/:id',

  ENQUIRY: '/enquiry',

  FAQ: '/faq',

  FRIEND_REFERRAL: '/friendReferral',

  SURVEY: '/survey',
  SURVEY_CREATE: '/survey/create',
  SURVEY_UPDATE: '/survey/update/:id',
  SURVEY_REPORT: '/survey/report/:id',
  SURVEY_REPORT_GENERATE: '/survey/report/:id/generate',

  QUESTION: '/question',
  QUESTION_CREATE: '/question/create',
  QUESTION_UPDATE: '/question/update/:id',

  ANSWER: '/answer',

  PRODUCT: '/specialProduct',
  PRODUCT_CREATE: '/specialProduct/create',
  PRODUCT_UPDATE: '/specialProduct/:id',

  TRANSACTION: '/transaction',
  TRANSACTION_DETAIL: '/transaction/:id',

  SETTING: '/setting',

  IGT: '/igt',

  MEMBER_REPORT: '/report/membersReport',
  SYSTEM_REPORT: '/report/systemLog',
  GAMING_REPORT: '/report/gamingLog',
  BEPOZ_REPORT: '/report/bepozLog',
}
export default ROUTES
