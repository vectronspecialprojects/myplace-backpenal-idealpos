import {call, put, takeLatest, takeEvery, select, delay} from 'redux-saga/effects'
import {
  getAdminVenue,
  getListingType,
  getSetting,
  getSystemNotification,
  getTier,
  postSetting,
  postTierSetting,
  getVenueTag,
  getVersionBepoz
} from '../utilities/ApiManage'
import {
  APP_GET_DATA,
  GET_VENUES,
  GET_APP_SETTINGS,
  getAppSettingsSuccess,
  getListingTypeSuccess,
  getSystemNotificationSuccess,
  getBepozVersionSuccess,
  getTierSuccess,
  getVenueSuccess,
  getTagsSuccess,
  UPDATE_APP_SETTINGS,
  UPDATE_TIER_SETTINGS,
  GET_TAGS,
  GET_BEPOZ_VERSION
} from '../redux/actions/app'
import {toast} from 'react-toastify'

function* appGetData(action) {
  try {
    //Get settings
    const settings = yield call(getSetting)
    if (settings.ok) {
      yield put(getAppSettingsSuccess(settings.data?.reduce((acc, item) => ({...acc, [item.key]: item}), {})))
    }
    //Get Tier
    const resTier = yield call(getTier)
    if (resTier.ok) yield put(getTierSuccess(resTier.data))

    //Get system notification
    const resSystemNotification = yield call(getSystemNotification)
    if (resSystemNotification.ok) yield put(getSystemNotificationSuccess(resSystemNotification.data))

    //Get admin Venue
    const resVenue = yield call(getAdminVenue)
    if (resVenue.ok) yield put(getVenueSuccess(resVenue.data))

    //Get Tag
    const resTags = yield call(getVenueTag)
    if (resTags.ok) yield put(getTagsSuccess(resTags.data))

    //Get BepozVersion
    const resVer = yield call(getVersionBepoz)
    if (resVer.ok) yield put(getBepozVersionSuccess(resVer.data))

    //Get listing type
    const listingType = yield call(getListingType)
    if (listingType.ok) yield put(getListingTypeSuccess(listingType.data))
  } catch (e) {
    console.log(e)
  }
}

function* getVenueList(action) {
  try {
    const resVenue = yield call(getAdminVenue)
    if (resVenue.ok) {
      yield put(getVenueSuccess(resVenue.data))
    }
  } catch (e) {
    console.log(e)
  }
}

function* getTagsList() {
  try {
    const res = yield call(getVenueTag)
    if (res.ok) {  
      yield put(getTagsSuccess(res.data))
    }
  } catch (e) {
    console.log(e)
  }
}

function* getVersionB() {
  try {
    const res = yield call(getVersionBepoz)
    if (res.ok) {  
      yield put(getBepozVersionSuccess(res.data))
    }
  } catch (e) {
    console.log(e)
  }
}

function* getSettingSaga() {
  try {
    const settings = yield call(getSetting)
    if (settings.ok) {
      yield put(getAppSettingsSuccess(settings.data?.reduce((acc, item) => ({...acc, [item.key]: item}), {})))
    }
  } catch (e) {}
}

function* updateAppSettings(action) {
  try {
    const res = yield call(postSetting, action.payload)
    if (res.ok) {
      toast('Updating setting is successful', {type: 'success'})
      yield call(getSettingSaga)
    } else {
      toast(res.message, {type: 'error'})
    }
  } catch (e) {
    console.log(e)
  }
}

function* getTierSaga() {
  const resTier = yield call(getTier)
  if (resTier.ok) yield put(getTierSuccess(resTier.data))
}

function* updateTierSettings(action) {
  try {
    const res = yield call(postTierSetting, action.payload)
    if (res.ok) {
      toast('Updating tiers is successful', {type: 'success'})
      yield call(getTierSaga)
    } else {
      toast(res.message, {type: 'error'})
    }
  } catch (e) {
    console.log(e)
  }
}

export default [
  takeLatest(APP_GET_DATA, appGetData),
  takeLatest(GET_VENUES, getVenueList),
  takeLatest(GET_TAGS, getTagsList),
  takeLatest(GET_APP_SETTINGS, getSettingSaga),
  takeLatest(UPDATE_APP_SETTINGS, updateAppSettings),
  takeLatest(UPDATE_TIER_SETTINGS, updateTierSettings),
  takeLatest(GET_BEPOZ_VERSION, getVersionB),
]
