import { call, put, takeLatest, select, delay } from "redux-saga/effects";
import {USER_LOGIN, userLoginSuccess} from '../redux/actions/user';
import { push } from "connected-react-router";
import {userLogin} from '../utilities/ApiManage';
import {setAccessToken} from '../utilities/NetworkingAuth';
function* sessionLoginSaga(action) {
  try {
    const res = yield call(userLogin, action.email, action.password)
    if (!res.ok) throw new Error(res.message)
    yield put(userLoginSuccess({...res.user, token: res.token}))
    setAccessToken(res.token)

    yield put(push("/dashboard"));
  } catch (e) {
    console.log(e)
  }
}

export default [takeLatest(USER_LOGIN, sessionLoginSaga)];
