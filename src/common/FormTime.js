import {CInput} from '@coreui/react'
import React from 'react'
import moment from 'moment'

const FormTime = ({showToday = true, isoString = false, ...props}) => {
  const handleChange = (e) => {
    if (isoString) {
      return props.onChange?.({
        target: {
          id: props.id,
          value: new Date(`${moment().format('MM-DD-YYYY')} ${e.target.value}`).toISOString(),
        },
      })
    }
    props.onChange?.(e)
  }

  return (
    <>
      <CInput
        format="HH:mm:ss"
        step="1"
        {...props}
        onChange={handleChange}
        type="time"
        value={isoString ? moment(props.value).format('HH:mm:ss') : props.value}
      />
    </>
  )
}

export default FormTime
