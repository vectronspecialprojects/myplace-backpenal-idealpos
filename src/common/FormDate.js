import {CButton, CInput} from '@coreui/react'
import React from 'react'
import FormItem from './FormItem'
import moment from 'moment'

const FormDate = ({showToday = false, label, required, onValueChange, ...props}) => {
  const handleChange = (e) => {
    props?.onChange?.(e)
    onValueChange?.(e.target.value)
  }

  return (
    <>
      <FormItem required={required} label={label} id={props.id}>
        <CInput
          {...props}
          onChange={handleChange}
          type="date"
          value={typeof props.value === 'string' ? props.value : moment(props.value).format('YYYY-MM-DD')}
          format="mm/dd/yyyy"
        />
      </FormItem>
      {showToday && (
        <CButton
          disabled={props.disabled}
          onClick={() => props.onChange({target: {id: props.id, value: new Date()}})}>
          TODAY
        </CButton>
      )}
    </>
  )
}

export default FormDate
