import React, {useEffect, useState} from 'react'
import {CSwitch} from '@coreui/react'

const StatusToggle = ({active, onChange, color = '', getEvent = false, autoSwitch = true, ...props}) => {
  const [checked, setChecked] = useState(active)

  useEffect(() => {
    setChecked(active)
  }, [active])

  const handleChange = (e) => {
    if (!!autoSwitch) {
      setChecked(!checked)
    }
    if (getEvent) {
      onChange?.(e)
    } else {
      onChange?.(e.target.checked)
    }
  }

  return (
    <CSwitch
      className="mx-1"
      shape="pill"
      color={color}
      variant="opposite"
      checked={autoSwitch?checked:active}
      onChange={handleChange}
      {...props}
    />
  )
}

export default StatusToggle
