import {
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CInput,
  CInputCheckbox,
  CInputRadio,
  CLabel,
  CRow,
  CSelect,
  CSwitch,
} from '@coreui/react'
import {DEFAULT_NOTIFY_CONTENT, NOTIFICATION} from '../views/Listing/constant'
import {CCard} from '@coreui/react/es'
import React, {useEffect, useState} from 'react'
import {getSystemNotification} from '../utilities/ApiManage'
import {useSelector} from 'react-redux'
import {isTrue} from '../helpers'

const ID = {
  triggerNotif: 'triggerNotif',
  notifContent: 'notifContent',
  notifType: 'notifType',
  notifTierAll: 'notifTierAll',
  notifTier: 'notifTierItem',
  notifContentType: 'notifContentType',
}

const NotificationSetting = ({
  id,
  setting,
  onUpdate,
  label = 'Trigger a notification when this voucher is issued',
  targetText = 'Target',
  targetTitle = '',
  disabled,
}) => {
  const tiers = useSelector((state) => state?.app?.tier || [])
  const [systemNotification, setSystemNotification] = useState([])
  const [notificationCustom, setNotificationCustom] = useState(setting?.notifContentType === 'custom')
  const [notificationContent, setNotificationContent] = useState('')
  const notifTier = (setting.notifTier || []).map((_) => +_)

  useEffect(() => {
    const isCustom = setting?.notifContentType === 'custom'
    setNotificationCustom(isCustom)
    if (isCustom) setNotificationContent(setting?.notifContent)
  }, [setting?.notifContentType, setting?.notifContent])

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getSystemNotification()
        if (!res.ok) throw new Error(res.message)
        setSystemNotification(res.data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  useEffect(() => {
    let content = DEFAULT_NOTIFY_CONTENT
    if (setting.notifType === 'system' && systemNotification?.length) content = systemNotification[0].id
    else if (notificationCustom) content = notificationContent
    onUpdate({
      target: {
        id,
        value: {
          ...setting,
          notifContent: content,
          notifContentType: notificationCustom ? 'custom' : 'default',
        },
      },
    })
  }, [setting.notifType, notificationCustom, notificationContent])

  const updateData = (e) => {
    let value = e.target.value
    if (e.target.id === ID.triggerNotif) value = {...setting, triggerNotif: e.target.checked}
    if (e.target.id.includes(ID.notifType)) value = {...setting, notifType: value}
    if (e.target.id === ID.notifContent) value = {...setting, notifContent: value}

    if (e.target.id === ID.notifTierAll)
      value = {...setting, notifTier: notifTier.length ? [] : tiers.map((_) => +_.id)}
    if (e.target.id.includes(ID.notifTier))
      value = {
        ...setting,
        notifTier: notifTier.includes(+value)
          ? notifTier.filter((_) => _ !== +value)
          : [...notifTier, +value],
      }
    onUpdate({target: {id, value}})
  }

  return (
    <>
      <div className="d-flex align-items-center mb-2">
        <CLabel className="m-0">{label}</CLabel>
        <CSwitch
          disabled={disabled}
          className="mx-1"
          id={ID.triggerNotif}
          shape="pill"
          color="info"
          variant="opposite"
          checked={isTrue(setting.triggerNotif)}
          onChange={updateData}
        />
      </div>

      {isTrue(setting.triggerNotif) && (
        <>
          {NOTIFICATION.map((_) => (
            <CFormGroup variant="checkbox" id={_.value} key={_.value}>
              <CInputRadio
                className="form-check-input"
                id={`${ID.notifType}${_.value}`}
                checked={setting.notifType === _.value}
                value={_.value}
                onChange={updateData}
              />
              <CLabel variant="checkbox" htmlFor={`${ID.notifType}${_.value}`}>
                {_.label}
              </CLabel>
            </CFormGroup>
          ))}
          {setting.notifType === NOTIFICATION[0].value ? (
            <>
              <CLabel className="mt-3">Notification Content</CLabel>
              <CFormGroup variant="checkbox">
                <CInputRadio
                  className="form-check-input"
                  id="notification-default"
                  checked={notificationCustom === false}
                  onChange={() => setNotificationCustom(false)}
                />
                <CLabel variant="checkbox" htmlFor="notification-default">
                  Default
                </CLabel>
              </CFormGroup>
              <CFormGroup variant="checkbox" className="d-flex">
                <CInputRadio
                  id="notification-custom"
                  className="form-check-input"
                  checked={notificationCustom}
                  onChange={() => setNotificationCustom(true)}
                />
                <CLabel variant="checkbox" htmlFor="notification-custom">
                  Custom
                </CLabel>
                <CInput
                  size="sm"
                  className="ml-2"
                  value={notificationContent}
                  onChange={(e) => setNotificationContent(e.target.value)}
                />
              </CFormGroup>
            </>
          ) : (
            <>
              <CLabel className="mt-3">System Notification Content</CLabel>
              <CFormGroup>
                <CLabel htmlFor="point_member_get">Notification</CLabel>
                <CSelect
                  custom
                  name="select"
                  id={ID.notifContent}
                  value={setting.notifContent}
                  onChange={updateData}>
                  {systemNotification.map((_) => (
                    <option key={_.id} value={_.id}>
                      {_.title}
                    </option>
                  ))}
                </CSelect>
              </CFormGroup>
            </>
          )}

          <CLabel className="form-check-label mt-5 ">{targetTitle}</CLabel>
          <CCard className="mt-3">
            <CCardHeader>
              <div className="px-2">
                <CInputCheckbox
                  id={ID.notifTierAll}
                  checked={notifTier.length === tiers.length}
                  onChange={updateData}
                />
                <CLabel variant="checkbox" className="form-check-label m-0" htmlFor={ID.notifTierAll}>
                  {targetText}
                </CLabel>
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow>
                {tiers.map((_) => (
                  <CCol xs="6" key={_.id}>
                    <CFormGroup className="px-2">
                      <CInputCheckbox
                        id={`${ID.notifTier}${_.id}`}
                        value={_.id}
                        checked={notifTier.includes(_.id)}
                        onChange={updateData}
                      />
                      <CLabel
                        variant="checkbox"
                        className="form-check-label"
                        htmlFor={`${ID.notifTier}${_.id}`}>
                        {_.name}
                      </CLabel>
                    </CFormGroup>
                  </CCol>
                ))}
              </CRow>
            </CCardBody>
          </CCard>
        </>
      )}
    </>
  )
}
export default NotificationSetting
