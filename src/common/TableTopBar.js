import React from 'react'
import ItemPerPage from './ItemPerPage'
import {CLabel, CRow, CCol, CInputGroup, CInputGroupText, CInput, CButton} from '@coreui/react'

const TableTopBar = ({
  pageNumber,
  totalItems,
  pageTitle,
  itemPerPage,
  setItemPerPage,
  isSearchable,
  searchInfo,
  setSearchInfo,
  onButtonPress
}) => {
  const testInput = (event) => {
    console.log(event.target.value)

  }
  return (
    <CRow style={{paddingLeft: 20, alignItems: 'center'}}>
      <CLabel style={{marginRight: 10, fontSize: 20}}>{pageTitle}</CLabel>
      {isSearchable && <CCol>
        <CInputGroup className="flex-nowrap">
          <CLabel style={{marginRight: 10}}>Filter:</CLabel>
          <CInput
            style={{marginTop: -8}}
            id="search"
            placeholder="Search"
            value={searchInfo}
            onChange={setSearchInfo}
          />
        </CInputGroup>
      </CCol>}

      <CCol>
        <ItemPerPage title={'Number per Page: '} currentValue={itemPerPage} onSelect={setItemPerPage} />
      </CCol>

      <CCol>
        <CLabel>Total:</CLabel>
        <CLabel style={{paddingLeft: 20}}>{totalItems}</CLabel>
      </CCol>

      <CCol>
        <CLabel>Page Number:</CLabel>
        <CLabel style={{paddingLeft: 20}}>{pageNumber}</CLabel>
      </CCol>
      <CButton color={'primary'} onClick={onButtonPress}>Add</CButton>
    </CRow>
  )
}

export default TableTopBar
