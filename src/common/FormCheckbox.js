import {CFormGroup, CInputCheckbox, CLabel} from '@coreui/react'
import React from 'react'
import {isTrue} from '../helpers'

const FormCheckbox = ({
  className,
  onValueChange,
  checkValue = true,
  uncheckValue = false,
  onChange,
  label,
  desc,
  disabled = false,
  ...props
}) => {
  const handleChange = (e) => {
    const checked = e?.target?.checked || !props.checked
    const newValue = props.value || (checked ? checkValue : uncheckValue)
    onChange?.({target: {id: props.id || '', checked, value: newValue}})
    onValueChange?.(newValue)
  }
  return (
    <CFormGroup variant="checkbox" className="checkbox">
      <CInputCheckbox
        {...props}
        disabled={disabled}
        className={`${className || ''}`}
        onChange={handleChange}
        checked={isTrue(props.checked)}
      />
      {label && (
        <CLabel
          variant="checkbox"
          onClick={() => {
            if (!disabled) handleChange()
          }}>
          {label}
        </CLabel>
      )}

      {desc && (
        <div>
          <small>
            <em>{desc}</em>
          </small>
        </div>
      )}
    </CFormGroup>
  )
}

export default FormCheckbox
