import React, {useMemo} from 'react'
import CIcon from '@coreui/icons-react'
import {CButton, CInput} from '@coreui/react'

const TagInput = ({onValueChange, value = [], int, disabled = false}) => {
  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      if (
        e.target.value &&
        !value.find((_) => _.toString() === e.target.value.toString()) &&
        (int ? !isNaN(+e.target.value) && Number.isInteger(+e.target.value) : true)
      )
        onValueChange?.([...value, e.target.value])
      e.preventDefault()
      e.currentTarget.value = ''
    }
  }

  const handleRemove = (index) => {
    value.splice(index, 1)
    onValueChange(value)
  }

  const sortArray = useMemo(() => {
    return value.sort((a, b) => a - b)
  }, [value])

  return (
    <div className="d-flex flex-wrap">
    
      {sortArray.map((item, i) => (
        <div key={i} className="d-flex rounded-pill bg-secondary mr-1 mb-1 py-1 px-2 align-items-center">
          <span>{item}</span>
          <CButton className="ml-1 p-0" onClick={() => handleRemove(i)} disabled={disabled}>
            <CIcon name="cil-x" />
          </CButton>
        </div>
      ))}
      
      <CInput
        className="border-0 shadow-none"
        placeholder={disabled?'This field is not in use':"click here add new"}
        onKeyDown={handleKeyDown}
        style={{width: 'unset'}}
        disabled={disabled}
      />
    </div>
  )
}

export default TagInput
