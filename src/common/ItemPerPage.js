import React from 'react'
import {
  CDropdown,
  CDropdownDivider,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CLabel,
} from '@coreui/react'

const Range = [5, 10, 20, 30, 40, 50]

function ItemPerPage({title, onSelect, currentValue}) {
  return (
    <div className="row-center-margin">
      <CLabel className="m-0">{title}</CLabel>
      <CDropdown className="m-1">
        <CDropdownToggle color="secondary">{currentValue}</CDropdownToggle>
        <CDropdownMenu>
          {Range.map((item, index) => {
            return (
              <div key={item}>
                <CDropdownItem onClick={() => onSelect(item)}>{item}</CDropdownItem>
                {index !== Range.length - 1 && <CDropdownDivider />}
              </div>
            )
          })}
        </CDropdownMenu>
      </CDropdown>
    </div>
  )
}

export default ItemPerPage
