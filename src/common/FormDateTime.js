import {CButton, CInput, CLabel} from '@coreui/react'
import React from 'react'
import moment from 'moment'

const FormDateTime = ({label, required, showToday = true, onValueChange, ...props}) => {
  const handleChange = (e) => {
    onValueChange?.(e.target.value)
    props.onChange?.(e)
  }

  const clickToday = () => {
    props.onChange?.({target: {id: props.id, value: new Date()}})
    onValueChange?.(new Date())
  }

  return (
    <>
      {label && (
        <CLabel htmlFor={props.id}>
          {label}
          {required && '*'}
        </CLabel>
      )}
      <CInput
        {...props}
        type="datetime-local"
        value={moment(props.value).format('YYYY-MM-DDTkk:mm')}
        onChange={handleChange}
      />
      {showToday && (
        <CButton disabled={props.disabled} onClick={clickToday}>
          TODAY
        </CButton>
      )}
    </>
  )
}

export default FormDateTime
