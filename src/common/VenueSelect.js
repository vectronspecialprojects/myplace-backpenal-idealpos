import React, {useMemo} from 'react'
import {CDropdown, CDropdownDivider, CDropdownItem, CDropdownMenu, CDropdownToggle} from '@coreui/react'
import {useSelector} from 'react-redux'

function VenueSelect({onSelect, value, onChange, onVenueChange, idZeroName ,defaultText = 'All', disabled, ...props}) {
  const venues = useSelector((state) => [{id: 0, name: idZeroName?idZeroName:'All'}, ...state.app.venues])
  const currentValue = useMemo(
    () => value?.name || (value && venues.find((_) => _.id === value)?.name),
    [value, venues],
  )

  const handleChange = (item) => {
    onSelect?.(item.id)
    onVenueChange?.(item)
    onChange?.({target: {id: props.id || '', value: item.id}})
  }

  return (
    <CDropdown {...props}>
      <CDropdownToggle
        color="secondary"
        className="w-100 d-flex align-items-center justify-content-between"
        disabled={disabled}>
        {currentValue || defaultText}
      </CDropdownToggle>
      <CDropdownMenu style={{minWidth: '100%'}}>
        <div style={{height: 300, overflow: 'auto'}}>
          <CDropdownDivider />
          {venues?.map((item) => (
            <div key={item.id}>
              <CDropdownItem onClick={() => handleChange(item)}>{item.name}</CDropdownItem>
              <CDropdownDivider />
            </div>
          ))}
        </div>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default VenueSelect
