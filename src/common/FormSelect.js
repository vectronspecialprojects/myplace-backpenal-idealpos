import {CFormGroup, CInvalidFeedback, CLabel, CSelect} from '@coreui/react'
import React from 'react'
import Tooltip from '../common/Tooltip'

const FormSelect = ({
  required,
  label,
  value,
  error,
  desc,
  options,
  getValue,
  onValueChange,
  className,
  toolTip = false,
  tipContent,
  getLabel = (_) => _.label,
  isGetObject = false,
  ...props
}) => {
  const handleChang = (e) => {
    const {id, value} = e.target
    if (getValue) {
      const v = getValue(options.find((_) => getValue(_) === value))
      onValueChange?.(v)
      return props.onChange?.({target: {id, value: v}})
    }
    if (isGetObject) {
      const obj = options.find((item) => item.value == value)
      onValueChange?.(obj)
      return props.onChange?.({target: {id, value: obj}})
    }
    props.onChange?.(e)
    onValueChange?.(value)
  }

  return (
    <CFormGroup className={`mb-0 ${className}`}>
      <CLabel htmlFor={props.id}>
        {label}
        {required && '*'}
      </CLabel>
      {toolTip && <Tooltip tipContent={tipContent} />}
      <CSelect custom {...props} invalid={!!error} value={value || ''} onChange={handleChang}>
        <option value="" disabled>
          Please select...
        </option>
        {options.map((_, i) => {
          const value = getValue?.(_) || _.value
          return (
            <option key={`${i}-${value}`} value={value}>
              {getLabel(_)}
            </option>
          )
        })}
      </CSelect>
      {error && <CInvalidFeedback>{error}</CInvalidFeedback>}
    </CFormGroup>
  )
}

export default FormSelect
