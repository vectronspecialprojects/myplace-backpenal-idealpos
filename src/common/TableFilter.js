import {
  CButton,
  CDropdown,
  CDropdownDivider,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CForm,
  CInput,
  CLabel,
  CTooltip,
} from '@coreui/react'
import ItemPerPage from './ItemPerPage'
import VenueSelect from './VenueSelect'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import React, {useEffect, useState} from 'react'
import FormCheckbox from './FormCheckbox'

const FILTER = {
  null: 'None',
  ticket: 'Ticket',
  gift_certificate: 'Gift Certificate',
  promotion: 'Promotion',
  friend_referral: 'Friend Referral',
  signup_reward: 'Signup Reward',
}
const FILTERS = Object.entries(FILTER).map(([value, label]) => ({value, label}))

const TableFilter = ({
  title,
  onClickDisplayOrder = null,
  onClickCreate = null,
  total,
  onParamChange = () => {},
  params = {},
  actions = null,
  disabled = false
}) => {
  const [itemPerPage, setItemPerPage] = useState(params.itemPerPage || 10)
  const [venueSelected, setVenueSelected] = useState(params.venueSelected || 0)
  const [filter, setFilter] = useState(params.filter || null)
  const [showInactive, setShowInactive] = useState(params.showInactive || 0)
  const [showDone, setShowDone] = useState(params.showDone || false)
  const [showSearch, setShowSearch] = useState(false)
  const [searchText, setSearchText] = useState(params.searchText || '')

  useEffect(() => {
    onParamChange({searchText, showInactive, venueSelected, itemPerPage, showDone, filter})
  }, [searchText, showInactive, venueSelected, itemPerPage, showDone, filter])

  return (
    <div className="header-container">
      <h5 className="m-0 mr-3">{title}</h5>
      {params.showInactive !== undefined && (
        <div className="row-center">
          <FormCheckbox
            checkValue={1}
            uncheckValue={0}
            checked={showInactive === 1}
            onValueChange={setShowInactive}
            label="Show inactive?"
          />
        </div>
      )}
      {params.showDone !== undefined && (
        <div className="row-center">
          <FormCheckbox label="Show Done?" checked={showDone} onValueChange={setShowDone} />
        </div>
      )}
      {params.itemPerPage !== undefined && (
        <ItemPerPage title={'Show: '} currentValue={itemPerPage} onSelect={setItemPerPage} />
      )}

      {params?.venueSelected !== undefined && (
        <>
          <CLabel className="m-0 ml-3">Venue:</CLabel>
          <VenueSelect value={venueSelected} onSelect={setVenueSelected} className="ml-1" />
        </>
      )}
      {params?.filter !== undefined && (
        <>
          <CLabel className="m-0 ml-3 mr-1">Filter:</CLabel>
          <CDropdown>
            <CDropdownToggle
              color="secondary"
              className="w-100 d-flex align-items-center justify-content-between">
              {FILTER[filter] || 'None'}
            </CDropdownToggle>
            <CDropdownMenu style={{minWidth: '100%'}}>
              <div style={{height: 300, overflow: 'auto'}}>
                <CDropdownDivider />
                {FILTERS?.map((item) => (
                  <div key={item.value}>
                    <CDropdownItem onClick={() => setFilter(item.value)}>{item.label}</CDropdownItem>
                    <CDropdownDivider />
                  </div>
                ))}
              </div>
            </CDropdownMenu>
          </CDropdown>
        </>
      )}
      {total !== undefined && <div style={{marginLeft: 50, alignSelf: 'center'}}>Total: {total}</div>}
      <div className="header-flex-end">
        {actions}
        {onClickDisplayOrder && (
          <CIcon
            content={freeSet.cilListNumbered}
            height="25"
            color={'red'}
            alt={'order'}
            onClick={onClickDisplayOrder}
            style={{marginRight: 10}}
          />
        )}
        {params?.searchText !== undefined && (
          <CForm inline>
            <CButton onClick={() => setShowSearch(true)}>
              <CTooltip content="Search">
                <CIcon content={freeSet.cilSearch} style={{width: 25, height: 25}} />
              </CTooltip>
            </CButton>
            {showSearch && (
              <CInput
                className="mr-sm-2"
                placeholder="Search"
                size="sm"
                value={searchText}
                onChange={(event) => setSearchText(event.target.value)}
                autoFocus
                onBlur={() => {
                  if (!searchText) setShowSearch(false)
                }}
              />
            )}
          </CForm>
        )}
        {onClickCreate && (
          <CButton disabled={disabled} className="button-add mx-2" onClick={onClickCreate}>
            <CTooltip content="Create">
              <CIcon content={freeSet.cilPlus} height="25" color={'red'} alt={'Edit'} />
            </CTooltip>
          </CButton>
        )}
      </div>
    </div>
  )
}

export default TableFilter
