import React, {useEffect, useState} from 'react'

const Image = ({blob, ...props}) => {
  const [path, setPath] = useState('')

  useEffect(() => {
    if (blob instanceof Blob) {
      const url = window.URL.createObjectURL(blob)
      setPath(url)
    } else {
      setPath(blob)
    }
  }, [blob])

  return <img {...props} src={path} />
}

export default Image
