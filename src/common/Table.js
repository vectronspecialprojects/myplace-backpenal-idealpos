import {CDataTable, CPagination} from '@coreui/react'
import React from 'react'

const MyTable = ({data, fields, params, onSort, scopedSlots, onPageChange, clickableRows=true, ...props}) => {
  return (
    <>
      <CDataTable
        items={data || []}
        fields={fields}
        striped
        itemsPerPage={params?.itemPerPage}
        onSorterValueChange={onSort}
        sorter={!!onSort}
        scopedSlots={scopedSlots}
        clickableRows={clickableRows}
        hover={true}
        {...props}
      />
      <CPagination
        activePage={params?.page}
        pages={Math.ceil(params?.total / params?.itemPerPage)}
        onActivePageChange={onPageChange}
      />
    </>
  )
}
export default MyTable
