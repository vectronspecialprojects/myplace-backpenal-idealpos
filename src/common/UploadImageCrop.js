import React, {PureComponent} from 'react'
import ReactCrop from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css'
import {CButton, CLabel, CTooltip, CRow, CCol} from '@coreui/react'

class ImageUploadCrop extends PureComponent {
  fileInput = null
  state = {
    src: null,
    crop: {
      unit: '%',
      width: this.props.cropWidth || 125,
      aspect: this.props.aspect || 16 / 9,
    },
    fileType: 'image/jpeg',
    ext: 'jpg',
    dimensions: {dimensions: {}},
  }

  onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      let fileNameArr = e.target.files?.[0]?.name?.split('.')
      this.setState({
        fileType: e.target.files?.[0]?.type || 'image/jpeg',
        ext: fileNameArr[fileNameArr?.length - 1],
      })
      const reader = new FileReader()
      reader.addEventListener('load', () => this.setState({src: reader.result}))
      reader.readAsDataURL(e.target.files[0])
    }
  }

  // If you setState the crop in here you should return false.
  onImageLoaded = (image) => {
    console.log('widht', image.width, 'height', image.height)
    this.setState({dimensions: {height: image.height, width: image.width}})
    this.imageRef = image
  }

  onCropComplete = (crop) => {
    this.makeClientCrop(crop)
  }

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({crop})
  }

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(this.imageRef, crop, `newFile.${this.state.ext}`)
      this.setState({croppedImageUrl})
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas')
    const scaleX = image.naturalWidth / image.width
    const scaleY = image.naturalHeight / image.height
    canvas.width = crop.width
    canvas.height = crop.height
    const ctx = canvas.getContext('2d')
    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height,
    )

    return new Promise((resolve, reject) => {
      canvas.toBlob((blob) => {
        if (!blob) {
          return
        }
        blob.name = fileName
        // window.URL.revokeObjectURL(this.fileUrl)
        // this.fileUrl = window.URL.createObjectURL(blob)
        // resolve(this.fileUrl)
        resolve(blob)
      }, this.state.fileType)
    })
  }

  handleOpenSelectFile = () => {
    if (this.fileInput) this.fileInput.click()
  }

  clickCancel = () => {
    this.setState({src: null})
  }

  clickUndo = () => {
    this.props.onConfirm({target: {id: this.props.id, value: this.props.defaultValue}})
    this.setState({croppedImageUrl: null})
    this.clickCancel()
  }

  clickConfirm = () => {
    this.props.onConfirm({target: {id: this.props.id, value: this.state.croppedImageUrl}})
    this.clickCancel()
  }

  render() {
    const {crop, croppedImageUrl, src, dimensions} = this.state
    return (
      <>
        <CTooltip content="Click here to change">
          <CLabel
            style={!!this.props.disabled ? {opacity: 0.2} : null}
            onClick={() => {
              if (this.props.disabled !== true) {
                this.handleOpenSelectFile()
              }
            }}
            className={this.props.className || ''}>
            {this.props.children}
          </CLabel>
        </CTooltip>
        {src ? (
          <div>
            <ReactCrop
              src={src}
              crop={crop}
              ruleOfThirds
              onImageLoaded={this.onImageLoaded}
              onComplete={this.onCropComplete}
              onChange={this.onCropChange}
              className={'mw-100'}
            />

            <CRow className="ml-0">
              {(!this.props.sizeLimited ||
                (dimensions.width === this.props.imgWidth && dimensions.height === this.props.imgHeight)) && (
                <CButton
                  type="submit"
                  color="secondary"
                  disabled={!croppedImageUrl}
                  onClick={this.clickConfirm}>
                  Confirm
                </CButton>
              )}
              {this.props.sizeLimited &&
                (dimensions.width !== this.props.imgWidth || dimensions.height !== this.props.imgHeight) && (
                  <p style={{color: 'red'}}>
                    Image size must be {this.props.imgWidth}w x {this.props.imgHeight}h
                  </p>
                )}

              <CButton style={{borderColor: '#ced2d8'}} color="error" onClick={this.clickCancel}>
                Cancel
              </CButton>
            </CRow>
          </div>
        ) : (
          <div>
            <input
              ref={(ref) => (this.fileInput = ref)}
              type="file"
              accept="image/*"
              onChange={this.onSelectFile}
              className="d-none"
            />
            {!this.props.hideUndo && croppedImageUrl && (
              <CButton type="submit" color="secondary" onClick={this.clickUndo}>
                UNDO IMAGE CHANGE
              </CButton>
            )}
          </div>
        )}
      </>
    )
  }
}

export default ImageUploadCrop
