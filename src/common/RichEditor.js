import React, {useEffect, useState} from 'react'
import {Editor} from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import {EditorState, convertToRaw, ContentState, convertFromHTML} from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import '../scss/_custom.scss'

let timer = null

const RichEditor = ({id, initValue, onChange}, ref) => {
  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(initValue))),
  )
  const isEmpty = !initValue

  useEffect(() => {
    setEditorState(
      EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(initValue))),
    )
  }, [isEmpty])

  useEffect(() => {
    if (onChange) {
      if (timer) clearTimeout(timer)

      timer = setTimeout(() => {
        onChange({target: {id, value: draftToHtml(convertToRaw(editorState.getCurrentContent()))}})
      }, 500)
    }
  }, [editorState])

  return (
    <Editor
      editorState={editorState}
      editorClassName="editor-wrapper"
      onEditorStateChange={(state) => setEditorState(state)}
      variant="outlined"
    />
  )
}

export default React.forwardRef(RichEditor)
