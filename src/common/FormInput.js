import {CFormGroup, CInput} from '@coreui/react'
import React from 'react'
import FormItem from './FormItem'

const FormInput = ({
  required,
  label,
  error,
  desc,
  onChangeText,
  maxLength,
  value,
  message,
  formClassName,
  className,
  toolTip,
  tipContent,
  ...props
}) => {
  const handleChange = (e) => {
    const event = {...e}
    if (props.type === 'number') {
      event.target.value = +event.target.value
      props.onChange && props.onChange({target: {id: props.id, value: +e.target.value}})
    } else {
      props.onChange && props.onChange(event)
    }
    onChangeText?.(event.target.value)
  }
  return (
    <CFormGroup className={`mb-0 ${className}`}>
      <FormItem
        toolTip={toolTip}
        tipContent={tipContent}
        error={error}
        label={label}
        required={required}
        desc={desc}
        maxLength={maxLength}
        id={props.id}
        className={formClassName}
        value={value}>
        <CInput {...props} invalid={!!error} onChange={handleChange} value={value || ''} />
        {!!message && <span style={{fontSize: 10}}>{message}</span>}
      </FormItem>
    </CFormGroup>
  )
}

export default FormInput
