import {CLabel} from '@coreui/react'
import React from 'react'

const StatusText = ({status = 'inactive'}) => {
  return (
    <CLabel className={`${status === 'active' ? 'text-success' : 'text-danger'} font-weight-bold m-0`}>
      {status.toUpperCase()}
    </CLabel>
  )
}
export default StatusText
