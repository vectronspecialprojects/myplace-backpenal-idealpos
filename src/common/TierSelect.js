import {CSelect} from '@coreui/react'
import React, {useMemo} from 'react'
import {useSelector} from 'react-redux'
import FormItem from './FormItem'

const TierSelect = ({
  label,
  required = false,
  error,
  placeholder = '',
  value,
  onUpdate,
  onValueChange,
  ...props
}) => {
  const tiers = useSelector((state) => state?.app?.tier || [])

  const currentValue = useMemo(() => value?.id, [value])
  const handleChange = (e) => {
    const v = tiers.find((_) => _.id === +e.target.value)
    onUpdate && onUpdate({target: {id: props.id, value: v}})
    onValueChange?.(v)
  }

  return (
    <FormItem label={label} error={error} required={required} id={props.id}>
      <CSelect custom {...props} value={currentValue || ''} invalid={!!error} onChange={handleChange}>
        {placeholder && (
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        {tiers.map((_) => (
          <option key={_.id} value={_.id}>
            {_.name}
          </option>
        ))}
      </CSelect>
    </FormItem>
  )
}

export default TierSelect
