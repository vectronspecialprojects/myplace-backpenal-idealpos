import {CFormGroup, CTextarea} from '@coreui/react'
import React from 'react'
import FormItem from './FormItem'

const FormTextarea = ({required, label, error, desc, onChangeText, value, ...props}) => {
  const handleChange = (e) => {
    onChangeText?.(e.target.value || '')
    props.onChange?.(e)
  }

  return (
    <CFormGroup>
      <FormItem error={error} label={label} required={required} desc={desc} value={value} id={props.id}>
        <CTextarea {...props} invalid={!!error} value={value || ''} onChange={handleChange} />
      </FormItem>
    </CFormGroup>
  )
}

export default FormTextarea
