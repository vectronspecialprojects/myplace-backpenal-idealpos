import {CCardBody, CCardHeader, CCol, CFormGroup, CRow} from '@coreui/react'
import {CCard} from '@coreui/react/es'
import React, {useMemo} from 'react'
import {useSelector} from 'react-redux'
import FormCheckbox from './FormCheckbox'

const TierCheckbox = ({
  id,
  onChange,
  targetText = 'Target',
  value = [],
  getValue = (_, tiers) => tiers.find((tier) => tier.id === _),
}) => {
  const tiers = useSelector((state) => state?.app?.tier || [])
  const valueId = useMemo(() => value.map((_) => _?.id || _), [value])

  const onCheckAll = (e) => {
    onChange?.({
      target: {id, value: valueId.length === tiers.length ? [] : tiers.map((_) => getValue(_.id, tiers))},
    })
  }

  const onCheckItem = (e) => {
    onChange?.({
      target: {
        id,
        value: (valueId.includes(+e.target.value)
          ? valueId.filter((_) => _ !== +e.target.value)
          : [...valueId, +e.target.value]
        ).map((_) => getValue(_, tiers)),
      },
    })
  }

  return (
    <CCard className="mt-3">
      <CCardHeader>
        <div className="px-2">
          <FormCheckbox label={targetText} checked={value.length === tiers.length} onChange={onCheckAll} />
        </div>
      </CCardHeader>
      <CCardBody>
        <CRow>
          {tiers.map((_) => (
            <CCol xs="6" key={_.id}>
              <CFormGroup className="px-2">
                <FormCheckbox
                  label={_.name}
                  value={_.id}
                  checked={valueId.includes(_.id)}
                  onChange={onCheckItem}
                />
              </CFormGroup>
            </CCol>
          ))}
        </CRow>
      </CCardBody>
    </CCard>
  )
}
export default TierCheckbox
