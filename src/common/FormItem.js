import {CFormGroup, CInvalidFeedback, CLabel} from '@coreui/react'
import React from 'react'
import Tooltip from '../common/Tooltip'

const FormItem = ({
  required,
  label,
  error,
  desc,
  children,
  maxLength,
  value,
  id,
  toolTip = false,
  tipContent,
  ...props
}) => {
  return (
    <CFormGroup {...props}>
      {label && (
        <>
          <CLabel htmlFor={id}>
            {label}
            {required && '*'}
          </CLabel>
          {toolTip && <Tooltip tipContent={tipContent} />}
        </>
      )}
      {children}
      {error && <CInvalidFeedback>{error}</CInvalidFeedback>}
      <small>
        {desc && (
          <small>
            <em>{desc}</em>
          </small>
        )}
        {maxLength && (
          <small className="float-right">
            {value?.length || 0}/{maxLength}
          </small>
        )}
      </small>
    </CFormGroup>
  )
}

export default FormItem
