import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faQuestionCircle} from '@fortawesome/pro-light-svg-icons'
import {CTooltip} from '@coreui/react'

const Tooltip = ({tipContent, ...props}) => (
  <CTooltip {...props} content={tipContent} hover='hover'>
    <FontAwesomeIcon style={{marginLeft: 5}} icon={faQuestionCircle} color="blue" />
  </CTooltip>
)

export default Tooltip
