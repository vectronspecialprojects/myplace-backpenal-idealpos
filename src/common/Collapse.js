import React, {useEffect, useState} from 'react'
import {CCard, CCardBody, CCardHeader, CCollapse} from '@coreui/react'

const Collapse = ({title, children, open = false, onOpen}) => {
  const [collapse, setCollapse] = useState(open)

  useEffect(() => {
    setCollapse(open)
  }, [open])

  const toggle = (e) => {
    setCollapse(!collapse)
    e.preventDefault()
  }

  return (
    <CCard className="mb-1">
      <CCardHeader onClick={toggle} className="bg-gradient-dark font-weight-bold">{title}</CCardHeader>
      <CCollapse show={collapse}>
        <CCardBody>{children}</CCardBody>
      </CCollapse>
    </CCard>
  )
}

export default Collapse
