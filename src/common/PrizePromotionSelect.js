import React, {useEffect, useMemo, useState} from 'react'
import {
  CDropdown,
  CDropdownDivider,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFormGroup,
} from '@coreui/react'
import {getPrizePromotions} from '../utilities/ApiManage'
import {useSelector} from 'react-redux'
import FormInput from './FormInput'

function PrizePromotionSelect({id = '', value, onChange, ...props}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const [prizes, setPrizes] = useState([])
  const currentValue = useMemo(() => prizes.find((_) => _.id === +value), [value, prizes])

  useEffect(() => {
    ;(async () => {
      const res = await getPrizePromotions()
      if (res.data?.length) {
        setPrizes(res.data)
      }
    })()
  }, [])

  const handleChange = (item) => {
    onChange && onChange({target: {id, value: item.id}})
  }

  return (
    <>
      <CFormGroup>
        <CDropdown {...props}>
          <CDropdownToggle
            disabled={!bepozVersion}
            color="secondary"
            className="w-100 d-flex align-items-center justify-content-between">
            {currentValue?.name || 'Select the Prize Promotion'}
          </CDropdownToggle>
          <CDropdownMenu style={{minWidth: '100%'}}>
            <div style={{height: 300, overflow: 'auto'}}>
              <CDropdownDivider />
              {prizes?.map((item) => (
                <div key={item.id}>
                  <CDropdownItem onClick={() => handleChange(item)}>{item.name}</CDropdownItem>
                  <CDropdownDivider />
                </div>
              ))}
            </div>
          </CDropdownMenu>
        </CDropdown>
        <small>
          <em>Prize Promotion must be created in Bepoz Back- office first to be linked here</em>
        </small>
      </CFormGroup>

      <FormInput
        disabled
        label="Prize Promotion Required Quantity to get free voucher"
        value={currentValue?.needed || ''}
      />
    </>
  )
}

export default PrizePromotionSelect
