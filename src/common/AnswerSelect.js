import {CLabel} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getAnswer} from '../utilities/ApiManage'
import Select from 'react-select'
import FormItem from './FormItem'

const AnswerSelect = ({id, label, required, placeholder = '', ...props}) => {
  const [answers, setAnswers] = useState([])

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getAnswer()
        if (!res.ok) throw new Error(res.message)
        setAnswers(res.data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  const formatOptionLabel = ({answer, id}) => {
    return (
      <CLabel className="m-0">
        <CLabel className="font-weight-bold m-0">{answer}</CLabel>(ID: {id})
      </CLabel>
    )
  }

  const onChange = (e) => {
    props.onChange({target: {id, value: e}})
  }

  return (
    <FormItem label={label} required={required}>
      <Select
        placeholder={props.placeholder}
        getOptionValue={(item) => item.id}
        value={props.value || undefined}
        onChange={onChange}
        options={answers}
        closeMenuOnSelect={false}
        formatOptionLabel={formatOptionLabel}
        isMulti={true}
      />
    </FormItem>
  )
}

export default AnswerSelect
