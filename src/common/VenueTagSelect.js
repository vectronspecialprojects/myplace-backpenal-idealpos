import React, {useEffect, useMemo, useState} from 'react'
import {CDropdown, CDropdownDivider, CDropdownItem, CDropdownMenu, CDropdownToggle} from '@coreui/react'
import {getPaginateVenueTag} from '../utilities/ApiManage'

function VenueTagSelect({onSelect, value, onChange, ...props}) {
  const [venueTags, setVenueTags] = useState([])
  const currentValue = useMemo(() => value && venueTags.find((_) => _.id === value)?.name, [value, venueTags])

  const fetchData = () => {
    try {
      const getValueTags = async (page = 1) => {
        const res = await getPaginateVenueTag(page)
        const {last_page, data} = res.data
        if (res.ok) {
          setVenueTags((prev) => [...prev, ...data])
          if (page < last_page) {
            await getValueTags(page + 1)
          }
        }
      }
      return getValueTags()
    } catch (err) {
      console.log(err)
    }
  }
  useEffect(() => {
    fetchData()
  }, [])

  const handleChange = (item) => {
    onSelect && onSelect(item.id)
    onChange && onChange({target: {id: props.id || '', value: item.id}})
  }

  return (
    <CDropdown {...props}>
      <CDropdownToggle color="secondary" className="w-100 d-flex align-items-center justify-content-between">
        {currentValue || 'Please select the venue tag'}
      </CDropdownToggle>
      <CDropdownMenu style={{minWidth: '100%'}}>
        <div style={{height: 300, overflow: 'auto'}}>
          <CDropdownDivider />
          {venueTags?.map((item) => (
            <div key={item.id}>
              <CDropdownItem onClick={() => handleChange(item)}>{item.name}</CDropdownItem>
              <CDropdownDivider />
            </div>
          ))}
        </div>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default VenueTagSelect
