import {CInvalidFeedback, CLabel, CSelect} from '@coreui/react'
import React from 'react'
import {useSelector} from 'react-redux'

const SystemNotificationSelect = ({label, required = false, error, placeholder = '', value, ...props}) => {
  const systemNotifications = useSelector((state) => state?.app?.systemNotification || [])

  return (
    <>
      {label && (
        <CLabel htmlFor={props.id}>
          {label}
          {required && '*'}
        </CLabel>
      )}
      <CSelect custom {...props} value={value || ''} invalid={!!error}>
        {placeholder && (
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        {systemNotifications.map((_) => (
          <option key={_.id} value={_.id}>
            {_.title}
          </option>
        ))}
      </CSelect>
      {error && <CInvalidFeedback>{error}</CInvalidFeedback>}
    </>
  )
}

export default SystemNotificationSelect
