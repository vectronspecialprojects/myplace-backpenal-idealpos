import React from 'react'
import {arrayMove, SortableContainer, SortableElement} from 'react-sortable-hoc'

const SortableItem = SortableElement(({value, indexItem, renderItem}) => {
  return (
  <li style={{listStyle: 'none'}}>{renderItem(value, indexItem)}</li>
)})

const SortableList = SortableContainer(({items, renderItem}) => {
  return (
    <ul className="p-0">
      {items.map((value, index) => (
        <SortableItem key={`item-${index}`} index={index} indexItem={index} value={value} renderItem={renderItem} />
      ))}
    </ul>
  )
})

const SortableComponent = ({items, onChange, renderItem}) => {
  const onSortEnd = ({oldIndex, newIndex}) => {
    onChange(arrayMove(items, oldIndex, newIndex))
  }
  return <SortableList  helperClass="sortableHelper" items={items} onSortEnd={onSortEnd} renderItem={renderItem} />
}

export default SortableComponent
