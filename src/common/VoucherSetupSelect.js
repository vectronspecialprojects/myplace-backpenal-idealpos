import {CSelect} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getVoucherSetup} from '../utilities/ApiManage'

const VoucherSetupSelect = ({placeholder = '', value, onValueChange, noVoucher, disabled, ...props}) => {
  const [voucherSetups, setVoucherSetups] = useState([])

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getVoucherSetup()
        if (!res.ok) throw new Error(res.message)
        setVoucherSetups(res.data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  const handleChange = (e) => {
    props.onChange?.(e)
    onValueChange?.(e.target.value)
  }

  return (
    <CSelect disabled={disabled} custom {...props} onChange={handleChange} value={value || ''}>
      <option value="" disabled>
        Select voucher...
      </option>
      {noVoucher && <option value="0">No Voucher</option>}
      {voucherSetups.map((_) => (
        <option key={_.id} value={_.id}>
          {_.name}
        </option>
      ))}
    </CSelect>
  )
}

export default VoucherSetupSelect
