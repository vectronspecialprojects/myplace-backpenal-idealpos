import {applyMiddleware, createStore, compose} from 'redux'
import rootSaga from '../saga/sagas'
import createSagaMiddleware from 'redux-saga'
import {routerMiddleware} from 'connected-react-router'
import {createBrowserHistory} from 'history'
import rootReducer from './reducers'
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
const persistConfig = {
  key: 'primaryData',
  blacklist: ['router'],
  storage
}
export const history = createBrowserHistory()

const persistedReducer = persistReducer(persistConfig, rootReducer(history))

const sagaMiddleware = createSagaMiddleware()
const middlewares = [sagaMiddleware, routerMiddleware(history)]

const store = createStore(persistedReducer, {}, compose(applyMiddleware(...middlewares)))

export const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)

export default store
