import {combineReducers} from 'redux';
import appReducer from './reducers/app';
import userReducer from './reducers/user';
import {connectRouter} from 'connected-react-router';

const rootReducer = (history) => combineReducers({
  app: appReducer,
  user: userReducer,
  router: connectRouter(history),
})

export default rootReducer;
