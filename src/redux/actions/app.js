import moment from 'moment'

export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE'
export const SET_SETTING_UNLOCK_EXPIRE_TIME = 'SET_SETTING_UNLOCK_EXPIRE_TIME'

export function unlockSetting(isSave = false) {
  return {
    type: SET_SETTING_UNLOCK_EXPIRE_TIME,
    payload: {time: isSave ? moment().endOf('day').toISOString() : null, isUnlock: true},
  }
}

export function removeUnlockSetting() {
  return {type: SET_SETTING_UNLOCK_EXPIRE_TIME, payload: {time: null, isUnlock: false}}
}

export function changeLanguage(selectedLanguage) {
  return {
    type: CHANGE_LANGUAGE,
    language: selectedLanguage,
  }
}

export const APP_GET_DATA = 'APP_GET_DATA'
export function appGetData() {
  return {
    type: APP_GET_DATA,
  }
}

export const GET_VENUES = 'GET_VENUES'
export function getVenueList() {
  return {
    type: GET_VENUES,
  }
}

export const GET_VENUE_SUCCESS = 'GET_VENUE_SUCCESS'
export function getVenueSuccess(payload) {
  return {
    type: GET_VENUE_SUCCESS,
    payload,
  }
}

export const GET_TAGS = 'GET_TAGS'
export function getTags() {
  return {
    type: GET_TAGS,
  }
}

export const GET_TAGS_SUCCESS = 'GET_TAGS_SUCCESS'
export function getTagsSuccess(payload) {
  return {
    type: GET_TAGS_SUCCESS,
    payload,
  }
}

export const SET_SHOW_NAV = 'SET_SHOW_NAV'
export function setShowNav(val) {
  return {
    type: SET_SHOW_NAV,
    payload: val,
  }
}

export const GET_BEPOZ_VERSION = 'GET_BEPOZ_VERSION'
export function getBepozVersion() {
  return {
    type: GET_BEPOZ_VERSION,
  }
}

export const GET_BEPOZ_VERSION_SUCCESS = 'GET_BEPOZ_VERSION_SUCCESS'
export function getBepozVersionSuccess(payload) {
  return {
    type: GET_BEPOZ_VERSION_SUCCESS,
    payload
  }
}

export const GET_TIER_SUCCESS = 'GET_TIER_SUCCESS'
export function getTierSuccess(payload) {
  return {
    type: GET_TIER_SUCCESS,
    payload,
  }
}
export const GET_SYSTEM_NOTIFICATION_SUCCESS = 'GET_SYSTEM_NOTIFICATION_SUCCESS'
export function getSystemNotificationSuccess(payload) {
  return {type: GET_SYSTEM_NOTIFICATION_SUCCESS, payload}
}

export const GET_LISTING_TYPE = 'GET_LISTING_TYPE'
export function getListingType() {
  return {
    type: GET_LISTING_TYPE,
  }
}

export const GET_LISTING_TYPE_SUCCESS = 'GET_LISTING_TYPE_SUCCESS'
export function getListingTypeSuccess(payload) {
  return {
    type: GET_LISTING_TYPE_SUCCESS,
    payload,
  }
}

export const UPDATE_LISTING_TYPE = 'UPDATE_LISTING_TYPE'
export function updateListingType(payload) {
  return {
    type: UPDATE_LISTING_TYPE,
    payload,
  }
}

export const GET_APP_SETTINGS = 'GET_APP_SETTINGS'
export function getAppSettings() {
  return {
    type: GET_APP_SETTINGS,
  }
}

export const GET_APP_SETTINGS_SUCCESS = 'GET_APP_SETTINGS_SUCCESS'
export function getAppSettingsSuccess(payload) {
  return {
    type: GET_APP_SETTINGS_SUCCESS,
    payload,
  }
}

export const UPDATE_APP_SETTINGS = 'UPDATE_APP_SETTINGS'
export function updateAppSettings(payload) {
  return {
    type: UPDATE_APP_SETTINGS,
    payload,
  }
}

export const UPDATE_TIER_SETTINGS = 'UPDATE_TIER_SETTINGS'
export function updateTierSettings(payload) {
  return {
    type: UPDATE_TIER_SETTINGS,
    payload,
  }
}
