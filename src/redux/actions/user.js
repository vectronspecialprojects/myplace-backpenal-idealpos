
export const USER_LOGIN = 'USER_LOGIN';
export function userLogin(email, password) {
  return {
    type: USER_LOGIN,
    email,
    password,
  };
}

export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export function userLoginSuccess(payload) {
  return {
    type: USER_LOGIN_SUCCESS,
    payload,
  };
}

export const GET_USER_INFO = 'GET_USER_INFO';
export function getUserInfo() {
  return {
    type: GET_USER_INFO,
  };
}

export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';
export function getUserInfoSuccess(payload) {
  return {
    type: GET_USER_INFO_SUCCESS,
    payload
  };
}


export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';
export function updateUserInfo(payload, avatar) {
  return {
    type: UPDATE_USER_INFO,
    payload,
    avatar,
  };
}

export const UPDATE_USER_INFO_SUCCESS = 'UPDATE_USER_INFO_SUCCESS';

export function updateUserInfoSuccess(payload) {
  return {
    type: UPDATE_USER_INFO_SUCCESS,
    payload,
  };
}


export const USER_LOGOUT = 'USER_LOGOUT';
export function userLogout() {
  return {
    type: USER_LOGOUT,
  };
}
