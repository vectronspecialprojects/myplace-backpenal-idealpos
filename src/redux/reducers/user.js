import {
  GET_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_SUCCESS,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
} from '../actions/user'
import {REHYDRATE} from 'redux-persist'
import {setAccessToken} from '../../utilities/NetworkingAuth'
export default function user(state, action) {
  switch (action.type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        loggedIn: true,
        userInfo: action.payload,
      }
    case USER_LOGOUT:
      return {
        ...state,
        loggedIn: false,
        userInfo: {},
      }
    case UPDATE_USER_INFO_SUCCESS:
      return {
        ...state,
        userInfo: action.payload,
      }
    case GET_USER_INFO_SUCCESS:
      return {
        ...state,
        userInfo: action.payload,
      }
    case REHYDRATE:
      setAccessToken(action.payload?.user?.userInfo?.token)
      return {
        ...state,
        ...(action?.payload?.user || {}),
      }
    default:
      return {
        ...state,
      }
  }
}
