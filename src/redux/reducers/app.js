import {
  CHANGE_LANGUAGE,
  GET_SYSTEM_NOTIFICATION_SUCCESS,
  GET_TIER_SUCCESS,
  GET_VENUE_SUCCESS,
  SET_SHOW_NAV,
  SET_SETTING_UNLOCK_EXPIRE_TIME,
  GET_LISTING_TYPE_SUCCESS,
  UPDATE_LISTING_TYPE,
  GET_APP_SETTINGS_SUCCESS,
  GET_TAGS_SUCCESS,
  GET_BEPOZ_VERSION_SUCCESS
} from '../actions/app'

export default function app(
  state = {
    sidebarShow: false,
  },
  action,
) {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.language,
      }
    case SET_SETTING_UNLOCK_EXPIRE_TIME:
      return {
        ...state,
        settingUnlockExpiredTime: action.payload.time || null,
        settingUnlock: action.payload.isUnlock || false,
      }
    case SET_SHOW_NAV:
      return {
        ...state,
        sidebarShow: action.payload,
      }
    case GET_TIER_SUCCESS:
      return {
        ...state,
        tier: action.payload,
      }
    case GET_SYSTEM_NOTIFICATION_SUCCESS:
      return {
        ...state,
        systemNotification: action.payload,
      }
    case GET_VENUE_SUCCESS:
      return {
        ...state,
        venues: action.payload,
      }
    case GET_BEPOZ_VERSION_SUCCESS:
      return {
        ...state,
        // bepozVersion: false,
        bepozVersion: action.payload || false,
      }
    case GET_TAGS_SUCCESS:
      return {
        ...state,
        tags: action.payload,
      }
    case GET_LISTING_TYPE_SUCCESS:
      return {
        ...state,
        listing: action.payload,
      }
    case UPDATE_LISTING_TYPE:
      let newList = [...state.listing]
      let index = newList.findIndex((item) => item.id === action.payload?.id)
      if (index > -1) {
        newList[index] = action.payload
      }
      return {
        ...state,
        listing: newList,
      }
    case GET_APP_SETTINGS_SUCCESS:
      return {
        ...state,
        appSettings: action.payload,
      }
    default:
      return state
  }
}
