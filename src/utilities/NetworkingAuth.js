import {
  getWithTimeout,
  postWithTimeout,
  deleteWithTimeout,
  putWithTimeout,
  patchWithTimeout,
} from './networking'
import {app_secret, app_token} from '../constants/constants'

const getFormData = (params) => {
  const formData = new FormData()
  Object.keys(params).forEach((item) => {
    if (typeof params[item] === 'object' && !(params[item] instanceof Blob)) {
      formData.append(`${item}`, JSON.stringify(params[item]))
    } else {
      formData.append(`${item}`, params[item])
    }
  })
  return formData
}

let accessToken = null

export function setAccessToken(token) {
  accessToken = token
}
export function getAccessToken() {
  return accessToken
}

export function clearToken() {
  accessToken = ''
}

export function getWithCheckingToken(api, headers) {
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return getWithTimeout(api, headers)
}

export function patchWithCheckingToken(api, headers, body) {
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return patchWithTimeout(api, headers, body)
}

export function postWithCheckingToken(api, headers, body) {
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return postWithTimeout(api, headers, {...body, app_secret, app_token})
}

export function postFormDataWithCheckingToken(api, headers, body) {
  const params = {...body, app_secret, app_token}
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return postWithTimeout(api, headers, getFormData(params))
}

export function putWithCheckingToken(api, headers, body) {
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return putWithTimeout(api, headers, {...body, app_secret, app_token})
}

export function deleteWithCheckingToken(api, headers, body) {
  if (!headers || !headers['token'])
    headers = {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    }
  return deleteWithTimeout(api, headers, {...body, app_secret, app_token})
}
