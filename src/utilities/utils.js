let timeoutDebounce
export function debounce(callback, time = 500) {
  clearTimeout(timeoutDebounce)
  timeoutDebounce = setTimeout(() => {
    callback()
  }, time)
}
