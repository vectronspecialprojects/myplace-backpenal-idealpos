import {
  deleteWithCheckingToken,
  getAccessToken,
  getWithCheckingToken,
  postFormDataWithCheckingToken,
  postWithCheckingToken,
  putWithCheckingToken,
} from './NetworkingAuth'
import {getWithTimeout} from './networking'
import {API_URI, app_secret, app_token, APP_URI} from '../constants/constants'
import {GIFT_CERTIFICATE_MODE} from '../views/Listing/constant'
import {toast} from 'react-toastify'

export function userLogin(email, password) {
  return postWithCheckingToken(
    `${API_URI}login`,
    {},
    {
      email,
      password,
    },
  )
}

export function getSsoVersion(url) {
  return getWithTimeout(url, {})
}

export function getListingType() {
  return getWithCheckingToken(`${API_URI}admin/listingType`, {})
}

export function putListingType(id = '', body) {
  return putWithCheckingToken(`${API_URI}admin/listingType/${id}`, {}, body)
}

export function getAdminVenue() {
  return getWithCheckingToken(`${API_URI}admin/venue`)
}

export function getCheckEmail(email) {
  return getWithCheckingToken(`${API_URI}email/check?email=${email}`)
}

export function retrievePromotions() {
  return getWithCheckingToken(`${API_URI}admin/retrievePromotions`)
}

export function getGiftCertificate() {
  return getWithCheckingToken(`${API_URI}admin/setting/gift_certificate`)
}

export function getPagingVouchers(
  page = 1,
  limit = 10,
  keyword = '',
  showAll = 0,
  sortBy = 'desc',
  orderBy = 'id',
  show_hidden = 0,
) {
  return getWithCheckingToken(`${API_URI}admin/paginateProduct?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &showAll=${showAll}&show_hidden=${show_hidden}&sortBy=${sortBy}&keyword=${keyword}&filter=voucher`)
}

export function getDataVoucher(id) {
  return getWithCheckingToken(
    `${API_URI}admin/product/${id}?token=${getAccessToken()} &app_secret=${app_secret}&app_token=${app_token}`,
  )
}

export function getPagingListing(
  type_id,
  venue_id,
  page = 1,
  limit = 10,
  showAll = 0,
  keyword = '',
  sortBy = 'asc',
  orderBy = 'status',
) {
  return getWithCheckingToken(`${API_URI}admin/paginateListing?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &showAll=${showAll}&sortBy=${sortBy}&type_id=${type_id}&venue_id=${venue_id}&keyword=${keyword}`)
}

export function getActiveListing(type_id) {
  return getWithCheckingToken(`${API_URI}admin/activeListing?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&type_id=${type_id}`)
}

export function duplicateListing(listing_id) {
  return postWithCheckingToken(`${API_URI}admin/duplicateListing`, {}, {listing_id})
}

export function displayOrder(display_order) {
  return postWithCheckingToken(
    `${API_URI}admin/displayOrder`,
    {},
    {display_order: JSON.stringify(display_order)},
  )
}

export function changeListingStatus(id, status) {
  return postWithCheckingToken(
    `${API_URI}admin/changeListingStatus`,
    {},
    {
      id,
      status,
    },
  )
}

export async function getDataListing(id) {
  const res = await getWithCheckingToken(
    `${API_URI}admin/listing/${id}?token=${getAccessToken()} &app_secret=${app_secret}&app_token=${app_token}`,
  )
  if (typeof res?.data?.extra_settings === 'string') {
    res.data.extra_settings = JSON.parse(res.data.extra_settings)
  }
  if (typeof res?.data?.payload === 'string') {
    res.data.payload = JSON.parse(res.data.payload)
  }
  res.data.products = res.data.products.map((_) => ({
    ..._,
    ..._.product,
    product: undefined,
    mode: GIFT_CERTIFICATE_MODE.NOT_MODIFIED,
  }))
  res.data.schedules = res.data.schedules.map((_) => ({
    ..._,
    mode: GIFT_CERTIFICATE_MODE.NOT_MODIFIED,
  }))
  return res
}

export function postListingData(id = '', body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/listing/${id}`, {}, body)
}

export function getTier(showAll = 0) {
  return getWithCheckingToken(`${API_URI}admin/tier?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&show_all=${showAll}`)
}

export async function createVouchers(body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/product?token=${getAccessToken()}`, {}, body)
}

export async function updateVouchers(id, body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/product/${id}?token=${getAccessToken()}`, {}, body)
}

export function getVoucherSetup() {
  return getWithCheckingToken(
    `${API_URI}bepoz/voucherSetups/?token=${getAccessToken()} &app_secret=${app_secret}&app_token=${app_token}`,
  )
}

export async function postVoucherSetup() {
  const res = await postWithCheckingToken(
    `${API_URI}bepoz/voucherSetups/?token=${getAccessToken()} &app_secret=${app_secret}&app_token=${app_token}`,
  )
  if (res.ok) {
    toast('Updating voucher setup is successful.', {type: 'success'})
  }
  return res
}

export function getVouchers() {
  return getWithCheckingToken(`${API_URI}admin/product?token=${getAccessToken()}`)
}

export function postMarkItAsDone(body) {
  return postWithCheckingToken(`${API_URI}admin/markItAsDone`, {}, body)
}

export function postSaveComment(body) {
  return postWithCheckingToken(`${API_URI}admin/saveComment`, {}, body)
}

export function getPrizePromotions() {
  return getWithCheckingToken(`${API_URI}bepoz/prizePromotions?token=${getAccessToken()}`)
}

export function getBepozGroups() {
  return getWithCheckingToken(`${API_URI}bepoz/bepozGroups`)
}

export function getOperatorId() {
  return getWithCheckingToken(`${API_URI}bepoz/bepozOperatorList`)
}
export function getTillId() {
  return getWithCheckingToken(`${API_URI}bepoz/bepozWorkstationList`)
}

// PRODUCT API
export function getPagingProduct(
  page = 1,
  limit = 10,
  showAll = 0,
  keyword = '',
  sortBy = 'asc',
  orderBy = 'id',
  filter = 'ticket',
) {
  return getWithCheckingToken(`${API_URI}admin/paginateProduct?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &showAll=${showAll}&sortBy=${sortBy}&keyword=${keyword}&filter=${filter}`)
}

export function changeProductStatus(id, status) {
  return postWithCheckingToken(
    `${API_URI}admin/changeProductStatus`,
    {},
    {
      id,
      status,
    },
  )
}

export function getProduct(product_id) {
  return getWithCheckingToken(`${API_URI}admin/product/${product_id}`, {})
}

export function duplicateProduct(product_id) {
  return postWithCheckingToken(`${API_URI}admin/duplicateProduct`, {}, {product_id})
}

export function postProductData(id = '', body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/product/${id}`, {}, body)
}

export function deleteProduct(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/product/${id}`, {})
}

// MEMBER API
export function getPagingMember(
  page = 1,
  limit = 10,
  showAll = 0,
  keyword = '',
  sortBy = 'asc',
  orderBy = 'id',
) {
  return getWithCheckingToken(`${API_URI}admin/member?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &showAll=${showAll}&sortBy=${sortBy}&keyword=${keyword}`)
}

export function postMember(id = '', body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/member/${id}`, {}, body)
}

export function getMember(id = '') {
  return getWithCheckingToken(`${API_URI}admin/member/${id}`, {})
}

export function getMemberLog(id = '', page = 1) {
  return getWithCheckingToken(`${API_URI}admin/memberLogs?member_id=${id}&page=${page}`, {})
}

export function postResendConfirmation() {
  return postFormDataWithCheckingToken(`${API_URI}admin/resendConfirmationEmails`, {})
}

export function postResendConfirmationMember(body) {
  return postWithCheckingToken(`${API_URI}admin/resendConfirmationMember`, {}, body)
}

export function postBroadcastGroupNotification(body) {
  return postWithCheckingToken(`${API_URI}admin/broadcastGroupNotification`, {}, body)
}

export function postSendBroadcastSystemNotification(body) {
  return postWithCheckingToken(`${API_URI}admin/sendBroadcastSystemNotification`, {}, body)
}

export function postSendSystemNotification(body) {
  return postWithCheckingToken(`${API_URI}admin/sendSystemNotification`, {}, body)
}

export function postPushNotification(body) {
  return postWithCheckingToken(`${API_URI}admin/pushNotification`, {}, body)
}

// STAFF API
export function getPagingStaff(page = 1, limit = 10, showAll = 0, keyword='') {
  return getWithCheckingToken(`${API_URI}admin/paginateStaff?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&page=${page}&paginate_number=${limit}
  &showAll=${showAll}&keyword=${keyword}`)
}

export function postStaff(id = '', body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/staff/${id}`, {}, body)
}

export function changeStaffStatus(id, active) {
  return postWithCheckingToken(
    `${API_URI}admin/changeStaffStatus`,
    {},
    {
      id,
      active,
    },
  )
}

// SYSTEM NOTIFICATION API
export function getPagingSystemNotification(
  page = 1,
  limit = 10,
  keyword = '',
  sortBy = 'asc',
  orderBy = 'id',
) {
  return getWithCheckingToken(`${API_URI}admin/paginateSystemNotification?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&page=${page}&paginate_number=${limit}
  &orderBy=${orderBy}&sortBy=${sortBy}&keyword=${keyword}`)
}

export async function getSystemNotification(id = '') {
  if (id) {
    const res = await getWithCheckingToken(
      `${API_URI}admin/systemNotification/${id}?token=${getAccessToken()}`,
    )
    res.data.payload = JSON.parse(res.data.payload)
    return res
  }
  return getWithCheckingToken(`${API_URI}admin/systemNotification?token=${getAccessToken()}`)
}

export function postSystemNotification(id = '', data) {
  const body = {...data}
  body.payload = JSON.stringify(body.payload)
  if (id) {
    return putWithCheckingToken(`${API_URI}admin/systemNotification/${id}`, {}, body)
  } else {
    return postWithCheckingToken(`${API_URI}admin/systemNotification`, {}, body)
  }
}

export function deleteSystemNotification(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/systemNotification/${id}`, {})
}

// ENQUIRY API
export function getPagingEnquiry(page = 1, limit = 10, showDone = false) {
  return getWithCheckingToken(`${API_URI}admin/listingEnquiry?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&page=${page}&paginate_number=${limit}&show_done=${
    showDone ? 1 : 0
  }`)
}
export function deleteListingEnquiry(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/listingEnquiry/${id}`, {})
}

// FAQ API
export function getPagingFAQ(page = 1, limit = 10, showAll = 0) {
  return getWithCheckingToken(`${API_URI}admin/faq?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&page=${page}&paginate_number=${limit}&showAll=${showAll}`)
}
export function changeFAQStatus(id, status) {
  return postWithCheckingToken(`${API_URI}admin/changeFaqStatus`, {}, {id, status})
}

export function postFAQ(id = '', body) {
  if (id) {
    return putWithCheckingToken(`${API_URI}admin/faq/${id}`, {}, body)
  } else {
    return postWithCheckingToken(`${API_URI}admin/faq`, {}, body)
  }
}

// FRIEND REFERRAL API
export function getPagingFriendReferral(page = 1, limit = 10) {
  return getWithCheckingToken(`${API_URI}admin/paginateFriendReferrals?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&expired=0&page=${page}&paginate_number=${limit}`)
}
export function postShowFriendReferral(body) {
  return postWithCheckingToken(`${API_URI}admin/showFriendReferrals`, {}, body)
}

// SURVEY API
export function getPagingSurvey(page = 1, limit = 10, keyword = '') {
  return getWithCheckingToken(`${API_URI}admin/paginateSurvey?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}&keyword=${keyword}`)
}
export function postCheckSurveyQuestion(body) {
  return postWithCheckingToken(`${API_URI}admin/checkSurveyQuestion`, {}, body)
}
export function postSurveyGenerateReport(body) {
  return postWithCheckingToken(`${API_URI}admin/generateSurveyReport`, {}, body)
}
export function getSurvey(id) {
  return getWithCheckingToken(`${API_URI}admin/survey/${id}`)
}
export function getSurveyDetail(id) {
  return getWithCheckingToken(`${API_URI}admin/surveyDetail/${id}`)
}
export function deleteSurvey(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/survey/${id}`, {})
}
export function postSurvey(id = '', body) {
  const params = {...body}
  if (id) {
    params.tiers = JSON.stringify(params.tiers)
    params.questions = JSON.stringify(params.questions)
    params.date_expiry = new Date(params.date_expiry).toISOString()
    return putWithCheckingToken(`${API_URI}admin/survey/${id}`, {}, params)
  } else {
    params.tiers = encodeURI(JSON.stringify(params.tiers))
    params.questions = encodeURI(JSON.stringify(params.questions))
    return postWithCheckingToken(
      `${API_URI}admin/survey?${Object.entries(params)
        .filter(([key, value]) => value !== undefined)
        .map(([key, value]) => `${key}=${value}`)
        .join('&')}`,
      {},
    )
  }
}

// QUESTION API
export function getQuestion(id = '') {
  return getWithCheckingToken(`${API_URI}admin/question${id ? `/${id}` : ''}?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}
export function postQuestion(id = '', body) {
  if (id) {
    const params = {...body}
    params.answers = JSON.stringify(params.answers)
    params.sequence = JSON.stringify(params.sequence)
    return putWithCheckingToken(`${API_URI}admin/question/${id}`, {}, params)
  } else {
    return postFormDataWithCheckingToken(`${API_URI}admin/question`, {}, body)
  }
}
export function deleteQuestion(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/question/${id}`, {})
}

// ANSWER API
export function getAnswer() {
  return getWithCheckingToken(`${API_URI}admin/answer?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}
export function postAnswer(id = '', body) {
  if (id) {
    return putWithCheckingToken(`${API_URI}admin/answer/${id}`, {}, body)
  } else {
    return postWithCheckingToken(`${API_URI}admin/answer`, {}, body)
  }
}
export function deleteAnswer(id = '') {
  return deleteWithCheckingToken(`${API_URI}admin/answer/${id}`, {})
}

// TRANSACTION API
export function getPagingTransaction(page = 1, limit = 10, venue_id = 0, filter = 'none') {
  return getWithCheckingToken(`${API_URI}admin/transaction?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}&venue_id=${venue_id}&filter=${filter}`)
}
export function getTransaction(id = '') {
  return getWithCheckingToken(`${API_URI}admin/transaction/${id}`)
}
export function getInvoice(id = '') {
  const url = `${API_URI}admin/invoice/${id}?token=${getAccessToken()}&app_secret=${app_secret}&app_token=${app_token}`
  const oReq = new XMLHttpRequest()
  oReq.open('GET', url, true)
  oReq.responseType = 'arraybuffer'
  oReq.onload = function (oEvent) {
    const blob = new Blob([oReq.response], {type: 'application/pdf'})
    const URL = window.URL || window.webkitURL
    const dataUrl = URL.createObjectURL(blob)
    window.open(dataUrl, '_blank')
  }
  oReq.send()
}
export function getDailySales() {
  return getWithCheckingToken(`${API_URI}admin/getDailySales`)
}
export function getWeeklySales() {
  return getWithCheckingToken(`${API_URI}admin/getWeeklySales`)
}
export function getMonthlySales() {
  return getWithCheckingToken(`${API_URI}admin/getMonthlySales`)
}
export function getQuarterlySales() {
  return getWithCheckingToken(`${API_URI}admin/getQuarterlySales`)
}
export function getYearlySales() {
  return getWithCheckingToken(`${API_URI}admin/getYearlySales`)
}
export function getQuarterlyYearlySales() {
  return getWithCheckingToken(`${API_URI}admin/getQuarterlyYearlySales`)
}

// SETTING API
export function getSetting() {
  return getWithCheckingToken(`${API_URI}admin/setting`)
}

export function postSetting(body) {
  return postWithCheckingToken(`${API_URI}admin/setting`, {}, body)
}
export async function postTierSetting(body) {
  return postWithCheckingToken(`${API_URI}admin/tierSetting`, {}, body)
  // const res = await postWithCheckingToken(`${API_URI}admin/tierSetting`, {}, body)
  // if (res.ok) {
  //   toast('Updating tier setting is successful', {type: 'success'})
  // }
  // return res
}

export function postUploadGalleryPhoto(body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/uploadGalleryPhoto`, {}, body)
}

export function putRole(id, body) {
  return putWithCheckingToken(`${API_URI}admin/role/${id}`, {}, body)
}

export function getWebStore() {
  return getWithCheckingToken(`${APP_URI}/webstore`)
}

export function postUnlockKey(body) {
  return postFormDataWithCheckingToken(`${API_URI}admin/unlockKey`, {}, body)
}
// VENUE API
export function getPagingVenue(page = 1, limit = 10, showAll, keyword='') {
  return getWithCheckingToken(`${API_URI}admin/paginateVenue?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}&showAll=${showAll}&keyword=${keyword}`)
}
export function getPaginateRole(page = 1, limit = 10) {
  return getWithCheckingToken(`${API_URI}admin/paginateRole?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}`)
}
export function getEmail(id = '') {
  return getWithCheckingToken(`${API_URI}admin/email/${id}`)
}
export function putEmail(id = '', body) {
  return putWithCheckingToken(`${API_URI}admin/email/${id}`, {}, body)
}
export async function postVenue(body) {
  const queryString = Object.entries(body)
    .map(([key, value]) => `${key}=${value}`)
    .join('&')
    const res = await postWithCheckingToken(`${API_URI}admin/venue?${queryString}`)
    if (res.ok) {
      toast('Venues added successfully', {type: 'success'})
    }
  return res
}

export async function updateVenue(id, payload) {
  const res = await putWithCheckingToken(`${API_URI}admin/venue/${id}`, {}, payload)
  if (res.ok) {
    toast('Venues setting updated successfully', {type: 'success'})
  }
  return res
}

export function getPaginateVenueTag(page = 1, limit = 10, keyword = '') {
  return getWithCheckingToken(`${API_URI}admin/paginateVenueTag?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}&keyword=${keyword}`)
}

export function getVenueTag(id = '') {
  return getWithCheckingToken(`${API_URI}admin/venueTag/${id}`)
}

export async function postVenueTag(body) {
  const params = {...body, venuesPivot: encodeURIComponent(JSON.stringify(body.venuesPivot))}
  const queryString = Object.entries(params)
    .map(([key, value]) => `${key}=${value}`)
    .join('&')
  const res = await postWithCheckingToken(`${API_URI}admin/venueTag?${queryString}`)
  if (res.ok) {
    toast('a venue added successfully', {type: 'success'})
  }
  return res
}

export async function putVenueTag(id, body) {
  const res = await putWithCheckingToken(`${API_URI}admin/venueTag/${id}`, {}, body)
  if (res.ok) {
    toast('Venue added successfully', {type: 'success'})
  }
  return res
}

export async function postChangeVenueTagStatus(body) {
  const res = await postWithCheckingToken(`${API_URI}admin/changeVenueTagStatus`, {}, body)
  if (res.ok) {
    toast('Venue tag status updates successfully', {type: 'success'})
  }
  return res
}

export async function postChangeVenueStatus(body) {
  const res = await postWithCheckingToken(`${API_URI}admin/changeVenueStatus`, {}, body)
  if (res.ok) {
    toast('Venue status updates successfully', {type: 'success'})
  }
  return res
}

export async function postChangeVenueTagHideInApp(body) {
  const res = await postWithCheckingToken(`${API_URI}admin/changeVenueTagHideInApp`, {}, body)
  if (res.ok) {
    toast('Vanue tag updates successfully', {type: 'success'})
  }
  return res
}

// IGT API
export function postTestIGT(body) {
  return postWithCheckingToken(`${API_URI}testerigt`, {}, body)
}
export function postTestWSDL(body) {
  return postWithCheckingToken(`${API_URI}testerigtwsdl`, {}, body)
}

// DASHBOARD API
export function postNewDashboard(body) {
  return postWithCheckingToken(`${API_URI}admin/newDashboard`, {}, body)
}
export function getRetrievePromotions() {
  return getWithCheckingToken(`${API_URI}admin/retrievePromotions`)
}
export function getJoinedMembers(body) {
  return postWithCheckingToken(`${API_URI}admin/getJoinedMembers`, {}, body)
}
export function getListingCounts(body) {
  return postWithCheckingToken(`${API_URI}admin/getListingCounts`, {}, body)
}
export function postRegisteredMembersReport(body) {
  return postWithCheckingToken(`${API_URI}admin/registeredMembersReport`, {}, body)
}

export async function postSendHelpDesk(body) {
  const res = await postWithCheckingToken(`${API_URI}admin/sendHelpDesk`, {}, body)
  toast('Sending email is successful', {type: 'success'})
  return res
}
export async function postClearNotification() {
  const res = await postWithCheckingToken(`${API_URI}admin/clearNotification`)
  if (res.ok) {
    toast('Notification container is clear', {type: 'success'})
  }
  return res
}

export async function putChangePassword(body) {
  const res = await postWithCheckingToken(`${API_URI}admin/password`, {}, body)
  if (res.ok) {
    toast('Changing password is successful', {type: 'success'})
  }
  return res
}

export function getPagingBepozLogs(
  page = 1,
  limit = 10,
  keyword = '',
  startDate,
  endDate,
  logFilter = '*',
  orderBy = 'id',
  sortBy = 'asc',
) {
  return getWithCheckingToken(`${API_URI}admin/getBepozLogs?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &logFilter=${logFilter}&endDate=${endDate}&startDate=${startDate}&sortBy=${sortBy}&keyword=${keyword}`)
}

export function getPagingSystemLogs(page = 1, limit = 10) {
  return getWithCheckingToken(`${API_URI}admin/systemLogs?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&page=${page}&paginate_number=${limit}`)
}

export function getVersion() {
  return getWithCheckingToken(`${API_URI}version?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}

export function getVersionBepoz() {
  return getWithCheckingToken(`${API_URI}bepoz/versionGet`)
}

export function getBepozVenueName() {
  return getWithCheckingToken(`${API_URI}bepoz/bepozVenueNames`)
}

export function getBepozCustomFieldsMetaGet() {
  return getWithCheckingToken(`${API_URI}bepoz/bepozCustomFieldsMetaGet`)
}

export function bepozAccountGet(AccNumber, CardNumber) {
  return postWithCheckingToken(
    `${API_URI}bepoz/bepozAccountGet`,
    {},
    {
      AccNumber,
      CardNumber,
      timestamp: Date.now(),
    },
  )
}

export function listingTypeHide(id, isHide) {
  let endPoint = isHide ? 'listingTypeUnHide' : 'listingTypeHide'
  return postWithCheckingToken(
    `${API_URI}admin/${endPoint}`,
    {},
    {
      id,
    },
  )
}

export function getGamingLog(
  page = 1,
  limit = 10,
  keyword = '',
  startDate,
  endDate,
  logFilter = '*',
  orderBy = 'id',
  sortBy = 'asc',
) {
  return getWithCheckingToken(`${API_URI}admin/getGamingLogs?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &logFilter=${logFilter}&endDate=${endDate}&startDate=${startDate}&sortBy=${sortBy}&keyword=${keyword}`)
}

export function restoreGaming() {
  return getWithCheckingToken(`${API_URI}admin/restoreGamingDefault?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}
//admin/refreshGamingSettingOdyssey
export function refreshGamingSettingOdy() {
  return getWithCheckingToken(`${API_URI}admin/refreshGamingSettingOdyssey?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}

//refreshGamingSettingIGT
export function refreshGamingSettingIGT() {
  return getWithCheckingToken(`${API_URI}admin/refreshGamingSettingIGT?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}
//callCustomFieldOdyssey
export function testCustomFieldOdy() {
  return getWithCheckingToken(`${API_URI}admin/callCustomFieldOdyssey?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}`)
}

export function importMember(body) {
  return postFormDataWithCheckingToken(`${API_URI}import`, {}, body)
}

export function getProducts(page = 1, limit = 10, show_hidden = 0, orderBy = 'id', sortBy = 'desc') {
  return getWithCheckingToken(`${API_URI}admin/paginateProduct?token=${getAccessToken()}
  &app_secret=${app_secret}&app_token=${app_token}&orderBy=${orderBy}&page=${page}&paginate_number=${limit}
  &sortBy=${sortBy}&filter=product&show_hidden=${show_hidden}`)
}

export function createProduct(data) {
  return postWithCheckingToken(`${API_URI}admin/product`, {}, data)
}

export function getProductById(id) {
  return getWithCheckingToken(`${API_URI}admin/product/${id}`)
}

export function updateProduct(id, data) {
  return postFormDataWithCheckingToken(`${API_URI}admin/product/${id}`, {}, data)
}

export function getDropdownProduct() {
  return getWithCheckingToken(`${API_URI}admin/product`, {})
}

export function getTriggerAllMemberCustomField() {
  return getWithCheckingToken(`${API_URI}admin/triggerAllMemberCustomField`, {})
}
