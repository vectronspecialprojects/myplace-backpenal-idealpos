export const isTrue = (value) => value === 'true' || value === true || value === 1 || value === '1' || (typeof value === 'string' && value.length > 0 && value !== 'false')

export const updateForm = (event, form) => {
  let value = event.target.value
  const newForm = {...form}
  const updateDataRecursive = (data, keys) => {
    if (keys.length === 1) return (data[keys[0]] = value)
    const newData = data[keys[0]]
    keys.shift()
    return updateDataRecursive(newData, keys)
  }
  updateDataRecursive(newForm, event.target.id.split('.'))
  return newForm
}

export const changeValueLoop = (key, value, state) => {
  const newState = {...state}
  const updateDataRecursive = (data, keys) => {
    if (keys.length === 1) return (data[keys[0]] = value)
    const newData = data[keys[0]]
    keys.shift()
    return updateDataRecursive(newData, keys)
  }
  updateDataRecursive(newState, key.split('.'))
  return newState
}

export const getPluralString = (number, string) => {
  return string + (+number === 1 ? '' : 's')
}

export const allSettled = function (promiseList) {
  let results = new Array(promiseList.length)

  return new Promise((ok, rej) => {
    let fillAndCheck = function (i) {
      return function (ret) {
        results[i] = ret
        for (let j = 0; j < results.length; j++) {
          if (results[j] == null) return
        }
        ok(results)
      }
    }

    for (let i = 0; i < promiseList.length; i++) {
      promiseList[i].then(fillAndCheck(i), fillAndCheck(i))
    }
  })
}
