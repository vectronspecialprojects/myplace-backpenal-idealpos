import ReactDOM from 'react-dom'
import React from 'react'
import {CButton, CLabel, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'

const MODAL_EMPTY_ID = 'modal-empty-id'

export const MODAL_EMPTY_COMPONENT = <div id={MODAL_EMPTY_ID} />

export const setModal = ({
  show = true,
  title = '',
  message = '',
  primaryBtnText = 'OK',
  primaryBtnClick = () => {},
  secondaryBtnText = 'Cancel',
  secondaryBtnClick = () => setModal({show: false}),
  isShowSecondaryBtn = true,
  size = 'lg',
  centered = true,
  closeButton= true
}) => {
  const parent = document.createElement('div')
  parent.setAttribute('id', MODAL_EMPTY_ID)
  ReactDOM.render(
    <CModal show={show} onClose={() => setModal({show: false})} size={size} centered={centered}>
      <CModalHeader closeButton = {closeButton}>
        <CModalTitle>{title}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CLabel>{message}</CLabel>
      </CModalBody>
      <CModalFooter>
        <CButton color="primary" onClick={primaryBtnClick}>
          {primaryBtnText}
        </CButton>
        {isShowSecondaryBtn && (
          <CButton color="secondary" onClick={secondaryBtnClick}>
            {secondaryBtnText}
          </CButton>
        )}
      </CModalFooter>
    </CModal>,
    document.getElementById(MODAL_EMPTY_ID),
  )
}
