import React, {useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CLabel, CRow} from '@coreui/react'
import FormInput from '../../common/FormInput'
import FormSelect from '../../common/FormSelect'
import FormTextarea from '../../common/FormTextarea'
import {postTestIGT, postTestWSDL} from '../../utilities/ApiManage'

const METHOD = [
  {label: 'GET', value: 'get'},
  {label: 'POST', value: 'post'},
  {label: 'PUT', value: 'put'},
  {label: 'DELETE', value: 'delete'},
]

function IGT() {
  const [method, setMethod] = useState(METHOD[0].value)
  const [url, setUrl] = useState('')
  const [request, setRequest] = useState('')
  const [response, setResponse] = useState('')

  const wsdl = async () => {
    const res = await postTestWSDL({method, url, xml: request})
    if (res.ok) {
      setResponse(res.wsdl)
    }
  }

  const load = async () => {
    const res = await postTestIGT({method, url, xml: request})
    if (res.ok) {
      setResponse(res.response)
    }
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader>
              <h5 className="m-0 mr-3">API Test</h5>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs={12} md={4} />
                <CCol xs={12} md={8}>
                  <FormSelect options={METHOD} value={method} onValueChange={setMethod} />
                  <CButton className="btn btn-secondary mr-2 mb-3" onClick={load}>
                    LOAD
                  </CButton>
                  <CButton className="btn btn-secondary mb-3" onClick={wsdl}>
                    WSDL
                  </CButton>
                </CCol>
                <CCol xs={12} md={4}>
                  <CLabel>URL</CLabel>
                </CCol>
                <CCol xs={12} md={8}>
                  <FormInput value={url} onChangeText={setUrl} />
                </CCol>
                <CCol xs={12} md={4}>
                  <CLabel>Request</CLabel>
                </CCol>
                <CCol xs={12} md={8}>
                  <FormTextarea value={request} onChangeText={setRequest} />
                </CCol>
                <CCol xs={12} md={4}>
                  <CLabel>Response</CLabel>
                </CCol>
                <CCol xs={12} md={8}>
                  <FormTextarea value={response} onChangeText={setResponse} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default IGT
