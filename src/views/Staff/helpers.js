import {validateEmail, validatePhoneNumber} from '../../helpers/validation'

const helpers = {
  validateForm: (form, isEdit = false) => {
    const error = {__checked: true}
    if (form.first_name.length === 0) error.first_name = 'The first name is required'
    if (form.last_name.length === 0) error.last_name = 'The last name is required'
    if (!isEdit && form.password.length === 0) error.password = 'The password is required'
    if (form.email.length === 0) error.email = 'The email is required'
    if (validateEmail(form.email)) error.email = 'The email is not valid'
    if (form.phone.length === 0) error.phone = 'The mobile is required'
    if (validatePhoneNumber(form.phone)) error.phone = 'The mobile is not valid'
    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
