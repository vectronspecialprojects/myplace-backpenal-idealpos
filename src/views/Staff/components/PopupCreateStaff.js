import React, {useEffect, useMemo, useState} from 'react'
import {
  CAlert,
  CButton,
  CCol,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CSelect,
} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import {updateForm} from '../../../helpers'
import {DEFAULT_STAFF} from '../constant'
import FormDate from '../../../common/FormDate'
import FormCheckbox from '../../../common/FormCheckbox'
import helpers from '../../Staff/helpers'
import {postStaff} from '../../../utilities/ApiManage'

function PopupCreateStaff({show, closeModal, selectedStaff, onSubmit}) {
  const [staff, setStaff] = useState({...DEFAULT_STAFF})
  const [error, setError] = useState({__checked: false})

  const isEdit = useMemo(() => !!selectedStaff, [selectedStaff])

  useEffect(() => {
    if (show && selectedStaff)
      setStaff({...selectedStaff, email: selectedStaff.user.email, role: selectedStaff.user.roles?.[0]?.id})
    else {
      setStaff({...DEFAULT_STAFF})
      setError({__checked: false})
    }
  }, [show])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(staff, isEdit)
      setError(validator.error)
    }
  }, [staff, isEdit])

  const onChange = (event) => setStaff((oldState) => updateForm(event, oldState))

  const saveAndClose = async () => {
    const validator = helpers.validateForm(staff, isEdit)
    if (validator.valid) {
      const res = await postStaff(selectedStaff?.id, staff)
      if (res.ok) {
        onSubmit()
        closeModal()
      }
    }
    setError(validator.error)
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Create Staff</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CAlert color="info">Caution: it will create a member profile at the same time.</CAlert>
          <CRow>
            <CCol xs={12} md={6}>
              <FormInput
                id="email"
                value={staff.email}
                error={error.email}
                onChange={onChange}
                autocomplete="chrome-off"
                label="Email"
                required
                maxLength={255}
                desc="If you(member) have registered before, we will grant an access for you"
              />
            </CCol>
            <CCol xs={12} md={6}>
              <FormInput
                label="Password"
                autocomplete="new-password"
                error={error.password}
                required={!isEdit}
                id="password"
                onChange={onChange}
                type="password"
                desc={isEdit && 'Fill the blank to change password'}
                value={staff.password}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <FormInput
                label="First Name"
                error={error.first_name}
                required
                maxLength={255}
                id="first_name"
                onChange={onChange}
                value={staff.first_name}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <FormInput
                label="Last Name"
                required
                maxLength={255}
                id="last_name"
                onChange={onChange}
                error={error.last_name}
                value={staff.last_name}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <FormInput
                label="Mobile"
                required
                id="phone"
                onChange={onChange}
                value={staff.phone}
                error={error.phone}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <FormDate label="DOB" id="dob" onChange={onChange} value={staff.dob} />
            </CCol>
            <CCol xs={12} md={6}>
              <FormCheckbox
                label="Active"
                checkValue={1}
                uncheckValue={0}
                onChange={onChange}
                checked={staff.active === 1}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <CLabel>Role</CLabel>
              <CSelect id="role" custom value={staff.role} onChange={onChange}>
                <option value={1}>Admin</option>
                <option value={2}>Staff</option>
                <option value={4} disabled>
                  Bepoz admin
                </option>
              </CSelect>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & EXIT
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupCreateStaff
