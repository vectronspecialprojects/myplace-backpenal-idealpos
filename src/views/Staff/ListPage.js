import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CTooltip} from '@coreui/react'
import {changeStaffStatus, getPagingStaff} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import PopupCreateStaff from './components/PopupCreateStaff'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#', sorder: false},
  {key: 'first_name', label: 'First Name', sorder: false},
  'status',
  'email',
  'role',
  'option',
]

function ListPage() {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [searchText, setSearchText] = useState('')
  const [showPopupCreate, setShowPopupCreate] = useState(false)
  const [selectedStaff, setSelectedStaff] = useState(null)

  async function handleGetData() {
    const res = await getPagingStaff(currentPage, itemPerPage, showInactive, searchText)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showInactive, searchText])

  const handleChangeStatus = (item, checked) => changeStaffStatus(item.id, checked ? 1 : 0)

  async function handleCreate() {
    setShowPopupCreate(true)
    setSelectedStaff(null)
  }

  function handleNavigateUpdate(item) {
    setShowPopupCreate(true)
    setSelectedStaff(item)
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <>
      <CCard>
        <CCardHeader>
          <TableFilter
            params={{searchText, showInactive, itemPerPage}}
            title="Staff"
            onClickCreate={handleCreate}
            total={data.total}
            size={'small'}
            onParamChange={handleChangeParam}
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fields}
            size={'sm'}
            onPageChange={setCurrentPage}
            onRowClick={(item) => {
              handleNavigateUpdate(item)
            }}
            scopedSlots={{
              status: (item) => (
                <td>
                  <StatusToggle
                    size={'sm'}
                    active={item.active === 1}
                    onChange={(checked) => handleChangeStatus(item, checked)}
                  />
                </td>
              ),
              email: (item) => <td>{item.user.email || ''}</td>,
              role: (item) => (
                <td className="font-weight-bold">
                  {(item.user.roles?.[0].name || '').split('_').join(' ')}
                </td>
              ),
              option: (item) => (
                <td>
                  <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                    <CTooltip content="Edit">
                      <CIcon content={freeSet.cilPen} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>

      <PopupCreateStaff
        show={showPopupCreate}
        closeModal={() => setShowPopupCreate(false)}
        onSubmit={handleGetData}
        selectedStaff={selectedStaff}
      />
    </>
  )
}

export default ListPage
