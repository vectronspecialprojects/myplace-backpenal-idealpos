export const DEFAULT_STAFF = {
  active: 1,
  role: 2,
  email: '',
  password: '',
  first_name: '',
  last_name: '',
  phone: '',
  dob: undefined,
}
