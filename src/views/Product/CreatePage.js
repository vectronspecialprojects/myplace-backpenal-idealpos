import {CButton, CCard, CCardBody, CCardHeader, CCol, CInputCheckbox, CLabel, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../common/FormInput'
import Image from '../../common/image'
import UploadImageCrop from '../../common/UploadImageCrop'
import DefaultImage500x500 from '../../assets/images/500x500.png'
import {PRICE_OPTION, PRODUCT_TYPE} from '../Setting/constant'
import FormSelect from '../../common/FormSelect'
import VoucherSetupSelect from '../../common/VoucherSetupSelect'
import Schedule from '../Listing/components/Schedule'
import helpers from '../Listing/helpers'
import {createProduct} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import {toast} from 'react-toastify'
import {useSelector} from 'react-redux'

function CreatePage({history, location}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const [form, setForm] = useState({
    image: 'http://placehold.it/500x500',
    system_point_ratio: 0,
    point_get: 0,
    unit_price: 0,
    point_price: 0,
    bepoz_voucher_setup_id: '',
    is_hidden: 0,
    product_type_id: 'product',
    name: '',
    desc_short: '',
    status: 'active',
    product_price_option: 'dollar_price',
    is_free: 0,
  })

  const [error, setError] = useState({})
  const [systemCalc, setSystemCalc] = useState(false)
  const [point, setPoint] = useState('')

  const updateForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  const handleCreate = async () => {
    try {
      const res = await createProduct(form)
      if (res.ok) {
        history.push({pathname: ROUTES.PRODUCT})
        toast('Product created successfully!', {type: 'success'})
      }
    } catch (e) {}
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Create Product</CCardHeader>
        </CCard>
        <CCard className="create-listing-container">
          <CCardBody>
            <CRow>
              <CCol xs="12" md="8">
                <CRow>
                  <CCol xs="12" md="6">
                    <FormInput
                      label="Product name"
                      id="name"
                      placeholder="Name"
                      value={form?.name}
                      onChange={updateForm}
                      maxLength={255}
                      required
                    />
                  </CCol>
                  <CCol xs="12" md="6">
                    <FormSelect
                      disabled={!bepozVersion}
                      label={'Type'}
                      value={form.product_type_id}
                      options={PRODUCT_TYPE}
                      onChange={updateForm}
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <FormInput
                      label="Description"
                      error={error?.desc_short}
                      id="desc_short"
                      placeholder="Description"
                      desc="Summarized information displayed in the app"
                      value={form?.desc_short}
                      onChange={updateForm}
                      maxLength={255}
                      required
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <div className="d-flex justify-content-between">
                      <div>
                        <CInputCheckbox
                          id="active"
                          checked={form?.status === 'active'}
                          type="checkbox"
                          className="ml-0"
                          onChange={updateForm}
                        />
                        <CLabel
                          variant="checkbox"
                          className="form-check-label ml-3"
                          htmlFor="sellStartSameAsAbove">
                          Activate this item now?
                        </CLabel>
                      </div>
                    </div>
                  </CCol>
                  <CCol xs="12" md="12" className="mt-2">
                    <FormSelect
                      label={'Type'}
                      id={'product_price_option'}
                      value={form.product_price_option}
                      options={PRICE_OPTION}
                      onChange={updateForm}
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <FormInput
                      label="Price (AUD$)"
                      id="unit_price"
                      value={form?.unit_price}
                      onChange={updateForm}
                      type="number"
                      required
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <FormInput
                      label="Point Price"
                      id="point_price"
                      desc={'Amount of points needed to buy this ticket'}
                      value={form?.point_price}
                      onChange={updateForm}
                      type="number"
                      required
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <div className="d-flex justify-content-between">
                      <CInputCheckbox
                        checked={form.system_point_ratio}
                        type="checkbox"
                        className="ml-0"
                        onChange={(value) =>
                          setForm({...form, system_point_ratio: value.target?.checked ? 1 : 0})
                        }
                      />
                      <CLabel
                        variant="checkbox"
                        className="form-check-label ml-3"
                        htmlFor="sellStartSameAsAbove">
                        To calculate the reward, Do you want to use System's Point Ratio?
                      </CLabel>
                    </div>
                  </CCol>
                  <CCol xs="12" md="12" className="mt-2">
                    <FormInput
                      label="How many points will a member get after they buy your item?"
                      id="point_get"
                      desc={
                        systemCalc
                          ? "Use system's point ratio. every AUD$ 1,00 spent = 1 pts."
                          : `every AUD$0.00 spent = ${form.point_get} pts`
                      }
                      value={form?.system_point_ratio || form.point_get}
                      onChange={updateForm}
                      type="number"
                      disabled={!!form?.system_point_ratio}
                    />
                  </CCol>
                  <CCol xs="12" md="12">
                    <VoucherSetupSelect
                      disabled={!bepozVersion}
                      noVoucher
                      id={'bepoz_voucher_setup_id'}
                      value={form.bepoz_voucher_setup_id}
                      onChange={updateForm}
                      placeholder={'Click here to select voucher'}
                    />
                  </CCol>
                </CRow>
              </CCol>
              <CCol xs="12" md="4">
                <UploadImageCrop aspect={1} id="image" onConfirm={updateForm} className="w-100">
                  <Image blob={form?.image || DefaultImage500x500} className="mt-2 w-100" />
                </UploadImageCrop>
              </CCol>
            </CRow>
            <Schedule disabled={!bepozVersion} form={form} updateForm={updateForm} />
            <CButton
              disabled={!bepozVersion}
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={() => handleCreate()}>
              SAVE AND EXIT
            </CButton>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
