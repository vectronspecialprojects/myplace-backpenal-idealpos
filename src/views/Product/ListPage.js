import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CTooltip} from '@coreui/react'
import {
  changeStaffStatus,
  deleteAnswer,
  deleteProduct,
  duplicateProduct,
  getPagingStaff,
  getProducts,
} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'
import ROUTES from '../../constants/routes'
import {setModal} from '../../reusable/Modal'

const fields = [
  {key: 'id', label: '#', sorder: false},
  {key: 'name', label: 'Name', sorder: false},
  'status',
  'type',
  {key: 'price', label: 'Price | Point'},
  {key: 'point_get', label: 'Point Get'},
  'option',
]

function ListPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [showPopupCreate, setShowPopupCreate] = useState(false)
  const [selectedStaff, setSelectedStaff] = useState(null)

  async function handleGetData() {
    const res = await getProducts(currentPage, itemPerPage, showInactive)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showInactive])

  const handleChangeStatus = (item, checked) => changeStaffStatus(item.id, checked ? 1 : 0)

  async function handleCreate() {
    history.push({
      pathname: ROUTES.PRODUCT_CREATE,
    })
  }

  function handleNavigateUpdate(item) {
    history.push({pathname: ROUTES.PRODUCT_UPDATE.replace(':id', item.id)})
  }

  const handleChangeParam = (params) => {
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
  }

  const clickDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected product (#${item.id})`,
      message: 'Caution: deleting this ticket is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteProduct(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  function handleDuplicate(item) {
    setModal({
      title: `Would you like to duplicate the selected voucher (#${item.id})`,
      message: 'This action may take a few minutes.',
      primaryBtnClick: async () => {
        const res = await duplicateProduct(item.id)
        if (res?.data?.id) {
          history.push({pathname: ROUTES.PRODUCT_UPDATE.replace(':id', res.data.id)})
          setModal({show: false})
        }
      },
    })
  }

  return (
    <>
      <CCard>
        <CCardHeader>
          <TableFilter
            params={{showInactive, itemPerPage}}
            title="Staff"
            onClickCreate={handleCreate}
            total={data.total}
            onParamChange={handleChangeParam}
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fields}
            size={'sm'}
            onPageChange={setCurrentPage}
            onRowClick={(item) => {
              handleNavigateUpdate(item)
            }}
            scopedSlots={{
              status: (item) => (
                <td>
                  <StatusToggle
                    size={'sm'}
                    active={item.active === 'active'}
                    onChange={(checked) => handleChangeStatus(item, checked)}
                  />
                </td>
              ),
              type: (item) => <td>{item.product_type?.name || ''}</td>,
              price: (item) => (
                <td>
                  <span style={{fontWeight: 'bold'}}>AUD$ {item.unit_price}</span>
                  <span> | {item.point_price} pts</span>
                </td>
              ),
              point_get: (item) => <td>{item.point_get} pts</td>,
              option: (item) => (
                <td>
                  <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                    <CTooltip content="Edit">
                      <CIcon content={freeSet.cilPen} />
                    </CTooltip>
                  </CButton>
                  <CButton
                    onClick={(event) => {
                      event.stopPropagation()
                      clickDelete(item)
                    }}
                    className="p-0 pr-2">
                    <CTooltip content="Delete">
                      <CIcon name="cil-trash" />
                    </CTooltip>
                  </CButton>
                  <CButton
                    onClick={(event) => {
                      event.stopPropagation()
                      handleDuplicate(item)
                    }}
                    className="p-0 pr-2">
                    <CTooltip content="Duplicate">
                      <CIcon content={freeSet.cilCopy} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>
    </>
  )
}

export default ListPage
