import {CCol, CFormGroup, CRow} from '@coreui/react'
import React, {useEffect} from 'react'
import FormInput from '../../../common/FormInput'
import FormCheckbox from '../../../common/FormCheckbox'

const PriceRewards = ({form, error, updateForm}) => {
  useEffect(() => {
    if (!form.system_point_ratio) updateForm({target: {id: 'point_get', value: form.unit_price}})
  }, [form.system_point_ratio, form.unit_price])

  return (
    <>
      <CRow>
        <CCol xs="4">
          <FormInput
            label="Price (AUD$)"
            id="unit_price"
            value={form?.unit_price}
            onChange={updateForm}
            error={error?.unit_price}
            type="number"
            required
          />
        </CCol>
        <CCol xs="4">
          <FormInput
            label="Point Price"
            id="point_price"
            value={form?.point_price}
            onChange={updateForm}
            error={error?.point_price}
            type="number"
            desc="Amount of points needed to buy this ticket"
            required
          />
        </CCol>
        <CCol xs="4">
          <FormCheckbox
            id="is_free"
            checkValue={1}
            uncheckValue={0}
            label="Free"
            onChange={updateForm}
            checked={form.is_free === 1}
          />
        </CCol>
        <CCol xs="12">
          <CFormGroup>
            <FormCheckbox
              label="To calculate the reward. Do you want to use System's Point Ratio?"
              id="system_point_ratio"
              checked={form.system_point_ratio === 1}
              checkValue={1}
              uncheckValue={0}
              onChange={updateForm}
            />
          </CFormGroup>
        </CCol>
        <CCol xs="12">
          <FormInput
            label="How many points will a member get after they buy your item?"
            id="point_get"
            value={form?.point_get}
            onChange={updateForm}
            error={error?.point_get}
            type="number"
            disabled={form.system_point_ratio}
            desc={
              form.system_point_ratio
                ? `Use system's point ratio. every AUD$ 1,00 spent = 1 pts.`
                : `every AUD$${(+form?.unit_price || 0).toFixed(2)} spent = ${form?.point_get || 0} pts`
            }
            required
          />
        </CCol>
      </CRow>
    </>
  )
}

export default PriceRewards
