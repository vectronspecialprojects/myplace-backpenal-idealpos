import React from 'react'
import {CCol, CRow} from '@coreui/react'
import MainInformation from './MainInformation'
import PriceRewards from './PriceReward'
import VoucherSetupSelect from '../../../common/VoucherSetupSelect'
import FormInput from '../../../common/FormInput'

const StepReview = ({form, error, updateForm, disabled = false}) => {
  return (
    <>
      {/*<h1>Review</h1>*/}
      <CRow>
        <CCol xs={12} md={12}>
          <VoucherSetupSelect
            disabled={disabled}
            id="bepoz_voucher_setup_id"
            value={form.bepoz_voucher_setup_id}
            onChange={updateForm}
            className="mb-2"
          />
          <FormInput
            disabled={disabled}
            label="Product ID"
            id="productId"
            value={form?.productId}
            onChange={updateForm}
          />
          <MainInformation form={form} error={error} updateForm={updateForm} />
          <PriceRewards form={form} error={error} updateForm={updateForm} />
        </CCol>
      </CRow>
    </>
  )
}

export default StepReview
