import {CCol, CFormGroup, CLabel, CRow, CSelect} from '@coreui/react'
import React from 'react'
import FormInput from '../../../common/FormInput'
import FormCheckbox from '../../../common/FormCheckbox'

const MainInformation = ({form, error, updateForm}) => {
  return (
    <>
      <CRow>
        <CCol xs="12" md="6">
          <FormInput
            label="Ticket name"
            id="name"
            placeholder="Name"
            value={form?.name}
            onChange={updateForm}
            error={error?.name}
            maxLength={255}
            required
          />
        </CCol>

        <CCol xs="12" md="6">
          <FormInput
            label="Description"
            id="desc_short"
            value={form?.desc_short}
            onChange={updateForm}
            maxLength={255}
            error={error?.desc_short}
            required
          />
        </CCol>
        {/*<CCol xs="12" md="6">*/}
        {/*  <FormCheckbox*/}
        {/*    id="is_hidden"*/}
        {/*    checkValue={1}*/}
        {/*    uncheckValue={0}*/}
        {/*    label="Hide this"*/}
        {/*    onChange={updateForm}*/}
        {/*    checked={form.is_hidden === 1}*/}
        {/*  />*/}
        {/*</CCol>*/}
        <CCol xs="12">
          <CFormGroup>
            <FormCheckbox
              label="Activate this item now?"
              checkValue="active"
              uncheckValue="inactive"
              onChange={updateForm}
              id="status"
              checked={form.status === 'active'}
            />
          </CFormGroup>
        </CCol>
      </CRow>
    </>
  )
}

export default MainInformation
