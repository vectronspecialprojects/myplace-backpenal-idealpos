import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CLabel, CRow, CTooltip} from '@coreui/react'
import {
  changeProductStatus,
  deleteProduct,
  duplicateProduct,
  getPagingProduct,
} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import {setModal} from '../../reusable/Modal'
import ROUTES from '../../constants/routes'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#'},
  {key: 'name'},
  {key: 'status'},
  {key: 'unit_price', label: 'Price|Point'},
  {key: 'point_get', label: 'Point Get'},
  {key: 'option', sorder: false},
]

function ListPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [searchText, setSearchText] = useState('')
  const [sort, setSort] = useState({column: 'status', asc: true})

  const handleGetData = async () => {
    const res = await getPagingProduct(
      currentPage,
      itemPerPage,
      showInactive,
      searchText,
      sort?.asc ? 'asc' : 'desc',
      sort.column,
      'ticket',
    )
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showInactive, searchText, sort])

  const handleCreate = async () => history.push({pathname: ROUTES.TICKET_CREATE})

  const handleDuplicate = (item) => {
    setModal({
      title: `Would you like to duplicate the selected product (#${item.id})`,
      message: 'This action may take a few minutes.',
      primaryBtnClick: async () => {
        const res = await duplicateProduct(item.id)
        if (res?.data?.id) {
          history.push({pathname: ROUTES.TICKET_UPDATE.replace(':id', res.data.id)})
          setModal({show: false})
        }
      },
    })
  }

  const handleDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected product (#${item.id})`,
      message: 'Caution: deleting this ticket is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteProduct(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  const handleNavigateUpdate = (item) =>
    history.push({pathname: ROUTES.TICKET_UPDATE.replace(':id', item.id)})

  const handleUpdateStatus = (item, value) => changeProductStatus(item.id, value ? 'active' : 'inactive')

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <CCard>
      <CCardHeader>
        <TableFilter
          params={{searchText, showInactive, itemPerPage}}
          title="Tickets"
          onClickCreate={handleCreate}
          total={data.total}
          onParamChange={handleChangeParam}
        />
      </CCardHeader>
      <CCardBody>
        <MyTable
          params={{itemPerPage, page: currentPage, total: data?.total}}
          data={data?.data}
          fields={fields}
          onPageChange={setCurrentPage}
          onSort={setSort}
          size={'sm'}
          onRowClick={(item) => {
            handleNavigateUpdate(item)
          }}
          scopedSlots={{
            status: (item) => (
              <td>
                <StatusToggle
                  active={item.status === 'active'}
                  onChange={(value) => handleUpdateStatus(item, value)}
                  size={'sm'}
                />
              </td>
            ),
            point_get: (item) => <td>{item.point_get || 0} pst</td>,
            option: (item) => (
              <td>
                <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                  <CTooltip content="Edit">
                    <CIcon content={freeSet.cilPen} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => handleDelete(item)} className="p-0 pr-2">
                  <CTooltip content="Delete">
                    <CIcon content={freeSet.cilTrash} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => handleDuplicate(item)} className="p-0 pr-2">
                  <CTooltip content="Duplicate">
                    <CIcon content={freeSet.cilCopy} />
                  </CTooltip>
                </CButton>
              </td>
            ),
          }}
        />
      </CCardBody>
    </CCard>
  )
}

export default ListPage
