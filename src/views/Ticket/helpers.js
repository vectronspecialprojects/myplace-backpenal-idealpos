const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.name.length === 0) error.name = 'The name is required'
    if (form.desc_short.length === 0) error.desc_short = 'The description is required'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
