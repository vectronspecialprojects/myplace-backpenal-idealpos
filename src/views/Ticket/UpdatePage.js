import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import helpers from './helpers'
import {getProduct, postProductData} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import {DEFAULT_TICKET} from './constant'
import MainInformation from './components/MainInformation'
import PriceRewards from './components/PriceReward'
import VoucherSetupSelect from '../../common/VoucherSetupSelect'
import {updateForm} from '../../helpers'
import FormInput from '../../common/FormInput'
import {useSelector} from 'react-redux'

const UpdatePage = ({location, history}) => {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])
  const [form, setForm] = useState({...DEFAULT_TICKET})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getProduct(id)
        if (!res.ok) throw new Error(res.message)
        const data = {...form, ...res.data}
        setForm(data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const handleUpdate = (event) => setForm((oldState) => updateForm(event, oldState))

  const clickUpdate = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      const res = await postProductData(id, form)
      if (res.ok) history.push({pathname: ROUTES.TICKET})
    }
  }

  return (
    <CCol xs="12">
      <CCard>
        <CCardHeader>Edit Ticket</CCardHeader>
      </CCard>

      <CCard className="create-listing-container">
        <CCardBody>
          <CRow>
            <CCol xs={12} md={8}>
              <VoucherSetupSelect
                id="bepoz_voucher_setup_id"
                value={form.bepoz_voucher_setup_id}
                onChange={handleUpdate}
                className="mb-2"
              />
              <FormInput label="Product ID" id="productId" value={form?.productId} onChange={updateForm} />
              <MainInformation form={form} error={error} updateForm={handleUpdate} />
              <PriceRewards form={form} error={error} updateForm={handleUpdate} />
            </CCol>
          </CRow>
          <CButton
            disabled={!bepozVersion}
            variant="outline"
            color="secondary"
            className="text-dark font-weight-bold mt-3"
            onClick={clickUpdate}>
            UPDATE AND EXIT
          </CButton>
        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default UpdatePage
