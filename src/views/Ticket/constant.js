export const DEFAULT_TICKET = {
  system_point_ratio: 1,
  point_get: 0,
  unit_price: 0,
  point_price: 0,
  bepoz_voucher_setup_id: 0,
  is_hidden: 0,
  product_type_id: 'ticket',
  name: '',
  status: 'active',
  desc_short: '',
  is_free: 0,
  image_square: 'http://placehold.it/500x500',
}
