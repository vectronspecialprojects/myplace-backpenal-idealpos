import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import StepReview from './components/StepReview'
import {postProductData} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import helpers from './helpers'
import {DEFAULT_TICKET} from './constant'
import {updateForm} from '../../helpers'
import UploadImageCrop from '../../common/UploadImageCrop'
import Image from '../../common/image'
import {useSelector} from 'react-redux'

function CreatePage({history, location}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const [step, setStep] = useState(0)
  const [form, setForm] = useState({...DEFAULT_TICKET})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const handleFormChange = (event) => setForm((oldState) => updateForm(event, oldState))

  const isReview = true

  const clickNext = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      if (isReview) {
        const res = await postProductData('', form)
        if (res.ok) {
          history.push({pathname: ROUTES.TICKET})
        }
      } else {
        setStep(Math.min(step + 1, 5))
      }
    }
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Add Ticket</CCardHeader>
        </CCard>
        <CCard className="create-listing-container">
          <CRow>
            <CCol xs={12} md={8}>
              <StepReview disabled={!bepozVersion} form={form} error={error} updateForm={handleFormChange} />
            </CCol>
            <CCol xs={12} md={4}>
              <span>Image</span>
              <UploadImageCrop aspect={1} id="image" onConfirm={handleFormChange} className="w-100">
                <Image blob={form?.image_square} className="w-100" />
              </UploadImageCrop>
            </CCol>
          </CRow>
          <CCol>
            <CButton
              disabled={!bepozVersion}
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={clickNext}>
              {isReview ? 'SAVE AND EXIT' : 'NEXT STEP'}
            </CButton>
          </CCol>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
