import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CTooltip} from '@coreui/react'
import {changeListingStatus, duplicateListing, getPagingListing} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import {setModal} from '../../reusable/Modal'
import ROUTES from '../../constants/routes'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'
import {useSelector} from 'react-redux'

const fields = [
  {key: 'id', label: '#', _style: {width: '1%'}},
  'name',
  'heading',
  'venue',
  {key: 'Sold/Total', sorter: false},
  {key: 'status', sorter: false},
  {key: 'display_order', label: 'Display Order'},
  {key: 'option', sorter: false},
]

function ListingPage({location, history}) {
  const typeId = useMemo(() => +location.pathname?.split('/').pop(), [location.pathname])
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [venueSelected, setVenueSelected] = useState(0)
  const [showInactive, setShowInactive] = useState(0)
  const [searchText, setSearchText] = useState('')
  const [sort, setSort] = useState({column: 'status', asc: true})
  const listingInfo = useSelector((state) => state.app.listing?.find((item) => item.id === typeId))

  useEffect(() => {
    handleGetData(
      typeId,
      venueSelected?.id || venueSelected,
      currentPage,
      itemPerPage,
      showInactive ? 1 : 0,
      searchText,
      sort,
    )
  }, [typeId, currentPage, itemPerPage, venueSelected, showInactive, searchText, sort])

  async function handleGetData(id, venue, page, limit, showAll, keyword, srt) {
    try {
      const res = await getPagingListing(
        id,
        venue,
        page,
        limit,
        showAll,
        keyword,
        srt?.asc ? 'asc' : 'desc',
        srt.column,
      )
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      setData([])
    }
  }

  async function handleCreate() {
    history.push({
      pathname: ROUTES.LISTING_CREATE.replace(':id', typeId),
    })
  }

  function handleDuplicate(item) {
    setModal({
      title: `Would you like to duplicate the selected listing (#${item.id})`,
      message: 'This action may take a few minutes.',
      primaryBtnClick: async () => {
        const res = await duplicateListing(item.id)
        if (res?.data?.id) {
          history.push({pathname: ROUTES.LISTING_UPDATE.replace(':id', typeId).replace(':id', res.data.id)})
          setModal({show: false})
        }
      },
    })
  }

  function handleNavigateUpdate(item) {
    history.push({pathname: ROUTES.LISTING_UPDATE.replace(':type_id', typeId).replace(':id', item.id)})
  }

  function handleNavigateDisplayOrder() {
    history.push({pathname: ROUTES.LISTING_DISPLAY_ORDER.replace(':id', typeId)})
  }

  async function handleUpdateStatus(item, value) {
    await changeListingStatus(item.id, value ? 'active' : 'inactive')
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
    setVenueSelected(params.venueSelected)
  }

  return (
    <CCard>
      <CCardHeader>
        <TableFilter
          params={{searchText, showInactive, venueSelected, itemPerPage}}
          title={listingInfo?.name}
          onClickCreate={handleCreate}
          total={data.total}
          onClickDisplayOrder={handleNavigateDisplayOrder}
          onParamChange={handleChangeParam}
        />
      </CCardHeader>
      <CCardBody>
        <MyTable
          data={data?.data}
          params={{itemPerPage, page: currentPage, total: data?.total}}
          fields={fields}
          size={'sm'}
          onPageChange={setCurrentPage}
          onSort={setSort}
          onRowClick={(item) => {
            handleNavigateUpdate(item)
          }}
          scopedSlots={{
            venue: (item) => <td>{item.venue_name}</td>,
            'Sold/Total': (item) => (
              <td>
                {item.order_details_count} / {item?.max_limit || '∞'}
              </td>
            ),
            status: (item) => (
              <td>
                <StatusToggle
                  size={'sm'}
                  active={item.status === 'active'}
                  onChange={(value) => handleUpdateStatus(item, value)}
                />
              </td>
            ),
            option: (item) => (
              <td>
                <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                  <CTooltip content="Edit">
                    <CIcon content={freeSet.cilPen} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => handleDuplicate(item)} className="p-0 pr-2">
                  <CTooltip content="Duplicate">
                    <CIcon content={freeSet.cilCopy} />
                  </CTooltip>
                </CButton>
              </td>
            ),
          }}
        />
      </CCardBody>
    </CCard>
  )
}

export default ListingPage
