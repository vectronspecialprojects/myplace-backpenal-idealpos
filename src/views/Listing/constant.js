export const LISTING = {
  SPECIAL_EVENT: 1,
  REGULAR_EVENT: 2,
  PROMOTION: 4,
  GIFT_CERTIFICATE: 5,
  OUR_TEAM: 6,
  STAMP_CARD: 7,
  STAMP_CARD_WON: 8,
  SHOPS: 9,
  MEMBERSHIP_TIER: 10
}
export const LISTING_INFO = {
  [LISTING.SPECIAL_EVENT]: {label: 'Special Event', category: 'information'},
  [LISTING.REGULAR_EVENT]: {label: 'Regular Event', category: 'information'},
  [LISTING.PROMOTION]: {label: 'Promotion', category: 'promotion'},
  [LISTING.GIFT_CERTIFICATE]: {label: 'Gift Certificate', category: 'information'},
  [LISTING.OUR_TEAM]: {label: 'Our Team', category: 'information'},
  [LISTING.STAMP_CARD]: {label: 'Stamp Card', category: 'information'},
  [LISTING.STAMP_CARD_WON]: {label: 'Stamp Card Won', category: 'information'},
  [LISTING.MEMBERSHIP_TIER]: {label: 'Membership Tier', category: 'information'},
  [LISTING.SHOPS]: {label: 'Shops', category: 'information'},
}
export const VOUCHER_CATEGORIES = [
  {label: 'Voucher', value: 'voucher'},
  {label: 'F&B Voucher', value: 'fb'},
]
export const TICKET_CATEGORIES = [{label: 'Ticket', value: 'ticket'}]
export const NOTIFICATION = [
  {label: 'Push Notification', value: 'push'},
  {label: 'System Notification', value: 'system'},
]

export const DEFAULT_NOTIFY_CONTENT = 'Your voucher is ready'

export const DEFAULT_CREATE_VOUCHER = {
  image: 'http://placehold.it/500x500',
  system_point_ratio: 1,
  bepoz_voucher_setup_id: 0,
  point_get: 0,
  unit_price: 0,
  point_price: 0,
  is_hidden: 0,
  product_type_id: VOUCHER_CATEGORIES[0].value,
  gift_value: 0,
  category: 'voucher',
  name: '',
  desc_short: '',
  is_free: 0,
  status: 'inactive',
  payload: [{triggerNotif: false, notifType: 'push', notifContent: DEFAULT_NOTIFY_CONTENT, notifTier: []}],
}

export const DEFAULT_CREATE_SCHEDULE = {
  status: 'inactive',
  date_start: new Date().toISOString(),
  date_end: new Date().toISOString(),
  types: [],
  venues: [],
  tags: [],
  never_expired: false,
  mode: 'new',
}

export const GIFT_CERTIFICATE_MODE = {
  NEW: 'new',
  NOT_MODIFIED: 'not_modified',
  MODIFIED: 'modified',
  DELETED: 'deleted',
}

export const DEFAULT_LISTING = {
  image_square: 'http://placehold.it/500x500',
  image_banner: 'http://placehold.it/500x160',
  max_limit: 0,
  max_limit_per_day: 0,
  max_limit_per_account: 0,
  max_limit_per_day_per_account: 0,
  add_enquiry: false,
  status: 'active', // active || inactive
  extra_settings: {
    add_enquiry: false,
    add_booking: true,
    triggerNotif: false,
    notifType: 'push',
    notifContentType: 'default',
    notifBroadcastDone: false,
    reminder_ticket_enable: false,
    notification_id: false,
    notifContentDefault: 'We have a new Gift Certificate Available',
    notifContent: 'We have a new Gift Certificate Available',
  },
  venue_id: 0,
  display_order: 0,
  type_id: 5,
  name: '',
  heading: '',
  desc_short: '',
  desc_long: '',
  comment: '',
  listing_type_id: 5,
  date_timezone: 'Asia/Saigon',
  date_start: new Date(),
  date_end: new Date(),
  datetime_start: new Date(),
  datetime_end: new Date(),
  sell_datetime_start: new Date(),
  sell_datetime_end: new Date(),
  // time_end: '09:29:14 am',
  // time_start: '09:29:14 am',
  payload: {occurrence: []},
}
