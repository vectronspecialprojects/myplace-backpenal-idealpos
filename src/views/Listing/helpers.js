
const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.name.length === 0) error.name = 'The name is required'
    if (form.heading.length === 0) error.heading = 'The heading is required'
    if (form.desc_short.length === 0) error.desc_short = 'The short description is required'

    return {valid: Object.keys(error).length === 1, error}
  },
  updateForm: (event, form) => {
    let value = event.target.value
    if (event.target.type === 'checkbox') value = event.target.checked
    if (event.target.id === 'status') value = event.target.checked ? 'active' : 'inactive'
    if (event.target.id === 'products') value = [value]
    const newForm = {...form}

    const updateDataRecursive = (data, keys) => {
      if (keys.length === 1) return (data[keys[0]] = value)
      const newData = data[keys[0]]
      keys.shift()
      return updateDataRecursive(newData, keys)
    }
    updateDataRecursive(newForm, event.target.id.split('.'))
    return newForm
  },
}
export default helpers
