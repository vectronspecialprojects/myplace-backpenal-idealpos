import {CCardBody, CCardHeader, CCol, CFormGroup, CRow} from '@coreui/react'
import {CCard} from '@coreui/react/es'
import React from 'react'
import FormCheckbox from '../../../common/FormCheckbox'

const DAYS = [
  {name: 'Monday', value: 'monday', order: 1},
  {name: 'Tuesday', value: 'tuesday', order: 2},
  {name: 'Wednesday', value: 'wednesday', order: 3},
  {name: 'Thursday', value: 'thursday', order: 4},
  {name: 'Friday', value: 'friday', order: 5},
  {name: 'Saturday', value: 'saturday', order: 6},
  {name: 'Sunday', value: 'sunday', order: 0},
]

const WeekdaysSelect = ({id, value = [], onChange, ...props}) => {
  const keySelected = (value || []).map((_) => _?.value)
  const handleChange = (e) => {
    if (e.target.id === 'all') {
      onChange({target: {id, value: [...DAYS]}})
    } else {
      const item = e.target.value
      onChange({
        target: {
          id,
          value: keySelected.includes(item.value)
            ? value.filter((_) => _.value !== item.value)
            : [...value, item],
        },
      })
    }
  }

  return (
    <CCard {...props}>
      <CCardHeader>
        <div>
          <FormCheckbox
            id="all"
            label="Select All"
            onChange={handleChange}
            checked={value.length === DAYS.length}
          />
        </div>
      </CCardHeader>
      <CCardBody>
        <CRow>
          {DAYS.map((_) => (
            <CCol xs="6" md="4" key={_.value}>
              <CFormGroup>
                <FormCheckbox
                  id="item"
                  label={_.name}
                  value={_}
                  onChange={handleChange}
                  checked={keySelected.includes(_.value)}
                />
              </CFormGroup>
            </CCol>
          ))}
        </CRow>
      </CCardBody>
    </CCard>
  )
}
export default WeekdaysSelect
