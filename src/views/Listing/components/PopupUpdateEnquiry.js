import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCol,
  CFormGroup,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CTextarea,
} from '@coreui/react'
import FormCheckbox from '../../../common/FormCheckbox'
import {postMarkItAsDone, postSaveComment} from '../../../utilities/ApiManage'

function PopupUpdateEnquiry({show, closeModal, selectedEnquiry}) {
  const [enquiry, setEnquiry] = useState({})

  useEffect(() => {
    if (show) {
      setEnquiry(selectedEnquiry)
    }
  }, [show])

  const handleSaveComment = async () => {
    await postSaveComment({comment: enquiry.comment, listing_id: enquiry.id})
    selectedEnquiry.comment = enquiry.comment
    closeModal()
  }

  const handleCheckItDone = async (e) => {
    setEnquiry({...enquiry, is_answered: e.target.value})
    await postMarkItAsDone({is_answered: e.target.value, listing_id: enquiry.id})
    selectedEnquiry.is_answered = e.target.value
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Enquiry</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs="12" md="4">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Member:</CLabel>
                    <CLabel>{`${enquiry?.member?.first_name || ''} ${enquiry?.member?.last_name || ''} (${
                      enquiry?.member?.id || ''
                    })`}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Subject:</CLabel>
                    <CLabel>{enquiry?.subject}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Message:</CLabel>
                    <CLabel>{enquiry?.message}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Member Requests Contact:</CLabel>
                    <CLabel>{enquiry?.contact_me}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Rating:</CLabel>
                    <CLabel>{enquiry?.rating}</CLabel>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>

            <CCol xs="12" md="4">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Email:</CLabel>
                    <CLabel>{}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold d-block">Mobile:</CLabel>
                    <CLabel>{enquiry?.member?.phone || ''}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold">Comment:</CLabel>
                    <CTextarea
                      value={enquiry.comment}
                      onChange={(e) => setEnquiry({...enquiry, comment: e.target.value})}
                    />
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>
            <CCol xs="12" md="4">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold">Listing Information:</CLabel>
                    <img src={enquiry?.listing?.image_banner} className="w-100" />
                    <CLabel className="font-weight-bold mr-1">Heading:</CLabel>
                    <CLabel>{enquiry?.listing?.heading}</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <FormCheckbox
                      label="Mark it as DONE"
                      checked={enquiry?.is_answered}
                      checkValue={1}
                      uncheckValue={0}
                      onChange={handleCheckItDone}
                    />
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel className="font-weight-bold mr-1">Commented by Staff:</CLabel>
                    <CLabel>{enquiry?.listing?.comment || 'Not Signed Yet'}</CLabel>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleSaveComment}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupUpdateEnquiry
