import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import {CCard} from '@coreui/react/es'
import FormDateTime from '../../../common/FormDateTime'
import {DEFAULT_CREATE_SCHEDULE} from '../constant'
import {useSelector} from 'react-redux'
import FormCheckbox from '../../../common/FormCheckbox'
import helpers from '../helpers'

function PopSelectVoucher({show, setModal, onSubmit, selectedSchedule, typeId}) {
  const tiers = useSelector((state) => state?.app?.tier || [])
  const [newSchedule, setNewSchedule] = useState({...DEFAULT_CREATE_SCHEDULE})
  const listingInfo = useSelector((state) => state.app.listing?.find((item) => item.id === typeId))
  useEffect(() => {
    if (show) {
      setNewSchedule(selectedSchedule || {...DEFAULT_CREATE_SCHEDULE})
    }
  }, [show])

  const updateData = (e) => {
    let value = e.target.value,
      id = e.target.id
    if (id === 'types') {
      const types = newSchedule.types
      value =
        types.findIndex((_) => _.tier_id === +value) > -1
          ? types.filter((_) => _.tier_id !== +value)
          : [...types, {type: {...listingInfo, tiers}, tier_id: +value}]
    }
    if (id === 'all-tiers') {
      id = 'types'
      value =
        newSchedule.types.length === tiers.length
          ? []
          : tiers.map((_) => ({type: {...listingInfo, tiers}, tier_id: +_.id}))
    }
    setNewSchedule((oldData) => {
      const newData = {...oldData}
      const updateDataRecursive = (data, keys) => {
        if (keys.length === 1) return (data[keys[0]] = value)
        const deepData = data[keys[0]]
        keys.shift()
        return updateDataRecursive(deepData, keys)
      }

      updateDataRecursive(newData, id.split('.'))
      return newData
    })
  }

  const submitSchedule = () => {
    onSubmit(newSchedule)
    setModal(false)
  }

  return (
    <>
      <CModal show={show} onClose={setModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Create Schedule</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs="12" md="6">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel>Start Date</CLabel>
                    <div className="d-flex">
                      <FormDateTime id="date_start" value={newSchedule.date_start} onChange={updateData} />
                    </div>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CLabel>Status</CLabel>
                  <CFormGroup>
                    <FormCheckbox
                      id="status"
                      checked={newSchedule.status === 'active'}
                      checkValue={'active'}
                      uncheckValue={'inactive'}
                      onChange={updateData}
                      label="Active"
                    />
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel htmlFor="tag">Tag(s)</CLabel>
                    <CInput id="tag" disabled />
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCol>

            <CCol xs="12" md="6">
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel>End Date</CLabel>
                    <CFormGroup>
                      <FormCheckbox
                        id="never_expired"
                        checked={newSchedule.never_expired}
                        onChange={updateData}
                        label="Never Expired"
                      />
                    </CFormGroup>
                    <div className="d-flex">
                      <FormDateTime
                        disabled={newSchedule.never_expired}
                        type="date"
                        id="date_end"
                        value={newSchedule.date_end}
                        onChange={updateData}
                      />
                    </div>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel>Display</CLabel>
                  </CFormGroup>
                </CCol>
                <CCol xs="12">
                  <CCard className="mt-3">
                    <CCardHeader>
                      <FormCheckbox
                        id="all-tiers"
                        checked={newSchedule.types.length === tiers.length}
                        onChange={updateData}
                        label="Gift Certificate: (Select all tiers)"
                      />
                    </CCardHeader>
                    <CCardBody>
                      <CRow>
                        {tiers.map((_) => (
                          <CCol xs="6" key={_.id}>
                            <CFormGroup>
                              <FormCheckbox
                                id="types"
                                value={_.id}
                                checked={newSchedule.types.findIndex((type) => type.tier_id === _.id) > -1}
                                onChange={updateData}
                                label={_.name}
                              />
                            </CFormGroup>
                          </CCol>
                        ))}
                      </CRow>
                    </CCardBody>
                  </CCard>
                </CCol>
              </CRow>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={submitSchedule}>
            SUBMIT
          </CButton>
          <CButton color="secondary" onClick={() => setModal(false)}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopSelectVoucher
