import React from 'react'
import {CCol, CFormGroup, CInput, CLabel, CRow, CSelect} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import FormTime from '../../../common/FormTime'
import {isTrue} from '../../../helpers'

const Reminder = ({form, updateForm}) => {
  return (
    <CRow>
      <CCol xs={12}>
        <CLabel>Send Email for reminding before Special Event Date to whom bought ticket</CLabel>
        <CSelect
          id="extra_settings.reminder_ticket_enable"
          onChange={updateForm}
          value={isTrue(form.extra_settings?.reminder_ticket_enable)}>
          <option value={true}>On</option>
          <option value={false}>Off</option>
        </CSelect>
      </CCol>
      <CCol xs={12}>
        <FormInput
          label="Email Reminder Interval(days) before the Event"
          desc="Schedules for reminding before Special Event Date to whom bought ticket"
          placeholder="click here add new"
        />
      </CCol>
      <CCol xs={12}>
        <CLabel>At what time reminder should be sent?</CLabel>
        <CFormGroup className="d-flex justify-content-between">
          <CLabel>Select time here</CLabel>
          <div>
            <FormTime
              id="reminder_ticket_time_select"
              value={form.reminder_ticket_time_select}
              isoString
              onChange={updateForm}
            />
          </div>
        </CFormGroup>
        <CInput placeholder="click here add new" />
      </CCol>
    </CRow>
  )
}

export default Reminder
