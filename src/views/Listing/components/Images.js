import React from 'react'
import DefaultImage500x160 from '../../../assets/images/500x160.png'
import DefaultImage500x500 from '../../../assets/images/500x500.png'
import UploadImageCrop from '../../../common/UploadImageCrop'
import Image from '../../../common/image'
import {LISTING} from '../constant'

const Images = ({typeId, form, updateForm}) => {
  return (
    <>
      {[
        LISTING.SPECIAL_EVENT,
        LISTING.REGULAR_EVENT,
        LISTING.GIFT_CERTIFICATE,
        LISTING.OUR_TEAM,
        LISTING.STAMP_CARD,
        LISTING.MEMBERSHIP_TIER
      ].includes(typeId) && (
        <UploadImageCrop aspect={500 / 160} id="image_banner" onConfirm={updateForm} className="w-100">
          <Image blob={form?.image_banner || DefaultImage500x160} className="w-100" />
        </UploadImageCrop>
      )}
      {typeId !== LISTING.STAMP_CARD && (
        <UploadImageCrop aspect={1} id="image_square" onConfirm={updateForm} className="w-100">
          <Image blob={form?.image_square || DefaultImage500x500} className="mt-2 w-100" />
        </UploadImageCrop>
      )}
    </>
  )
}

export default Images
