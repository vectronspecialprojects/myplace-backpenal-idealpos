import React from 'react'
import NotificationSetting from '../../../common/NotificationSettting'

const Notification = ({form, updateForm, disabled}) => {
  const extra_settings = form.extra_settings || {}
  return (
    <NotificationSetting
      disabled={disabled}
      id="extra_settings"
      setting={extra_settings}
      onUpdate={updateForm}
      label="The notification will be sent on the Start Display Date"
      targetText="Target all"
      targetTitle="Please select all tier if you want broadcast to all member"
    />
  )
}

export default Notification
