import {CButton, CDataTable, CLabel} from '@coreui/react'
import React, {useState} from 'react'
import PopSelectVoucher from './PopupSelectVoucher'
import CIcon from '@coreui/icons-react'
import StatusText from '../../../common/StatusText'
import {GIFT_CERTIFICATE_MODE, LISTING} from '../constant'
import {cilActionRedo} from '@coreui/icons'

const fields = ['Name', 'Type', 'Price|Points', 'Options']

const TicketVoucher = ({form, updateForm, typeId, disabled}) => {
  const [showAddVoucher, setShowAddVoucher] = useState(false)
  const products = form?.products || []

  const handleSelectVoucher = (value) => {
    updateForm({target: {id: 'products', value}})
  }

  const undoVoucher = (item) => {
    const product = products.find((_) => _.id === item.id)
    product.mode = GIFT_CERTIFICATE_MODE.NOT_MODIFIED
    updateForm({target: {id: 'products', value: [...products]}})
  }

  const removeVoucher = (item) => {
    if (item.mode === GIFT_CERTIFICATE_MODE.NOT_MODIFIED) {
      const product = products.find((_) => _.id === item.id)
      product.mode = GIFT_CERTIFICATE_MODE.DELETED
      updateForm({target: {id: 'products', value: [...products]}})
    } else {
      updateForm({target: {id: 'products', value: products.filter((_) => _.id !== item.id)}})
    }
  }

  return (
    <>
      {typeId === LISTING.SPECIAL_EVENT ? (
        <>
          <h1 className="display-3">Tickets</h1>
          <CButton
            disabled={disabled}
            color="secondary"
            className="text-dark"
            onClick={() => setShowAddVoucher(true)}>
            ADD TICKET
          </CButton>
        </>
      ) : (
        <>
          <h1 className="display-3">Vouchers</h1>
          <CButton
            disabled={disabled}
            color="secondary"
            className="text-dark"
            onClick={() => setShowAddVoucher(true)}>
            ADD VOUCHER
          </CButton>
        </>
      )}
      {products.length > 0 && (
        <CDataTable
          items={products}
          fields={fields}
          striped
          itemsPerPage={5}
          pagination
          scopedSlots={{
            Name: (item) => (
              <td>
                {item.name}
                <CLabel className="font-weight-bold m-0">(#{item.id})</CLabel> -
                <StatusText status={item.status} />
              </td>
            ),
            Type: (item) => <td>{item?.category || item?.product_type?.name || ''}</td>,
            ['Price|Points']: (item) => (
              <td>
                <CLabel className="m-0">
                  <CLabel className="font-weight-bold m-0">AUD$ ${+(item.unit_price || 0)} </CLabel> |
                  {` ${+(item.point_price || 0)} pts`}
                </CLabel>
              </td>
            ),
            Options: (item) => (
              <td>
                {item.mode === GIFT_CERTIFICATE_MODE.DELETED ? (
                  <CButton disabled={disabled} onClick={() => undoVoucher(item)}>
                    <CIcon content={cilActionRedo} />
                  </CButton>
                ) : (
                  <CButton disabled={disabled} onClick={() => removeVoucher(item)}>
                    <CIcon name="cil-trash" />
                  </CButton>
                )}
              </td>
            ),
          }}
        />
      )}
      <PopSelectVoucher
        show={showAddVoucher}
        setModal={setShowAddVoucher}
        onSelectVoucher={handleSelectVoucher}
        isTicket={typeId === 1}
        currentVouchers={products}
      />
    </>
  )
}

export default TicketVoucher
