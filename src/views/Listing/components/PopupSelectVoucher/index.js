import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CModal, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'
import {createVouchers, getVouchers, getVoucherSetup} from '../../../../utilities/ApiManage'
import SearchVoucher from './Search'
import CreateVoucher from './Create'
import {DEFAULT_CREATE_VOUCHER, TICKET_CATEGORIES, VOUCHER_CATEGORIES} from '../../constant'

const SCREEN = [
  {value: 1, label: (type) => 'SEARCH'},
  {value: 2, label: (type) => `CREATE A NEW ${type.toUpperCase()}`},
]

function PopSelectVoucher({show, setModal, onSelectVoucher, currentVouchers, isTicket = false}) {
  const [vouchers, setVouchers] = useState([])
  const [voucherSetups, setVoucherSetups] = useState([])
  const [selectedVouchers, setSelectedVoucher] = useState([])
  const [screen, setScreen] = useState(SCREEN[0].value)
  const popupInfo = useMemo(
    () =>
      isTicket
        ? {title: 'ticket', categories: TICKET_CATEGORIES}
        : {title: 'voucher', categories: VOUCHER_CATEGORIES},
    [isTicket],
  )
  const [newVoucher, setNewVoucher] = useState({
    ...DEFAULT_CREATE_VOUCHER,
    product_type_id: popupInfo.categories[0].value,
  })
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (show) {
      setSelectedVoucher(currentVouchers)
      handleGetData()
    }
  }, [show])

  useEffect(() => {
    if (error?.__checked) validateForm()
  }, [newVoucher])

  async function handleGetData() {
    try {
      const res = await getVouchers()
      if (!res.ok) throw new Error(res.message)
      setVouchers(res.data)
    } catch (e) {
      console.log(e)
    }
    try {
      const res = await getVoucherSetup()
      if (!res.ok) throw new Error(res.message)
      setVoucherSetups(res.data)
      setError({__checked: false})
    } catch (e) {
      console.log(e)
    }
  }

  const validateForm = () => {
    const error = {__checked: true}
    if (newVoucher.name.length === 0) error.name = 'The name is required'
    if (newVoucher.desc_short.length === 0) error.desc_short = 'The short description is required'
    if (newVoucher.unit_price < 0) error.unit_price = 'The unit price must be greater than or equal to 0'
    if (newVoucher.point_price < 0) error.point_price = 'The point price must be greater than or equal to 0'
    setError(error)
    return Object.keys(error).length === 1
  }

  const createNewVoucher = async () => {
    if (validateForm()) {
      const res = await createVouchers(newVoucher)
      if (res.ok) {
        setScreen(SCREEN[0].value)
        setNewVoucher({
          ...DEFAULT_CREATE_VOUCHER,
          product_type_id: popupInfo.categories[0].value,
        })
        handleGetData()
      }
    }
  }

  const submit = () => {
    if (screen === SCREEN[0].value) {
      onSelectVoucher(selectedVouchers)
      setModal(false)
    } else {
      createNewVoucher()
    }
  }

  return (
    <>
      <CModal show={show} onClose={setModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Add {popupInfo.title}(s)</CModalTitle>
        </CModalHeader>
        <div className="text-center">
          {SCREEN.map((item) => (
            <CButton
              variant="outline"
              className={`${item.value === screen ? 'btn-secondary' : 'btn-outline-secondary'} m-2`}
              key={item.value}
              onClick={() => setScreen(item.value)}>
              {item.label(popupInfo.title)}
            </CButton>
          ))}
        </div>
        {
          {
            1: (
              <SearchVoucher
                selectedVouchers={selectedVouchers}
                setSelectedVoucher={setSelectedVoucher}
                vouchers={vouchers}
                popupInfo={popupInfo}
              />
            ),
            2: (
              <CreateVoucher
                error={error}
                voucherSetups={voucherSetups}
                data={newVoucher}
                setData={setNewVoucher}
                popupInfo={popupInfo}
              />
            ),
          }[screen]
        }
        <CModalFooter>
          <CButton color="primary" onClick={submit}>
            SUBMIT
          </CButton>
          <CButton color="secondary" onClick={() => setModal(false)}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopSelectVoucher
