import React, {useMemo, useState} from 'react'
import {
  CCol,
  CInput,
  CInputCheckbox,
  CLabel,
  CListGroup,
  CListGroupItem,
  CModalBody,
  CRow,
} from '@coreui/react'
import StatusText from '../../../../common/StatusText'
import {GIFT_CERTIFICATE_MODE} from '../../constant'

let timer = null

function SearchVoucher({vouchers, selectedVouchers, setSelectedVoucher, popupInfo}) {
  const [search, setSearch] = useState('')
  const [showHidden, setShowHidden] = useState(false)

  const selectVoucher = (item) => {
    const newSelectedVouchers = [...selectedVouchers]
    const i = newSelectedVouchers.findIndex((_) => _.id === item.id)
    if (i >= 0) newSelectedVouchers.splice(i, 1)
    else newSelectedVouchers.push({...item, checked: true, mode: GIFT_CERTIFICATE_MODE.NEW})
    setSelectedVoucher(newSelectedVouchers)
  }

  const handleSearch = (e) => {
    clearTimeout(timer)
    const value = e.target.value
    timer = setTimeout(() => {
      setSearch(value)
    }, 1000)
  }

  const list = useMemo(() => {
    let result = vouchers
    if (popupInfo?.title)
      result = result.filter((_) => (_.product_type?.name || '').toLowerCase() === popupInfo?.title)
    if (search) result = result.filter((_) => _.name.toLowerCase().includes(search.toLowerCase()))
    if (!showHidden) result = result.filter((_) => !_.is_hidden)
    return result
  }, [vouchers, search, showHidden, popupInfo])

  const idChecked = selectedVouchers.reduce((acc, _) => ({...acc, [_.id]: true}), {})
  const idDisabled = selectedVouchers.reduce((acc, _) => {
    if (_.mode === GIFT_CERTIFICATE_MODE.NEW) return acc
    return {...acc, [_.id]: true}
  }, {})

  return (
    <CModalBody>
      <CRow>
        <CCol xs="8" md="10">
          <CLabel htmlFor="search">Search</CLabel>
        </CCol>
        <CCol xs="4" md="2" className=" text-right" />
        <CCol xs="8" md="10">
          <CInput id="search" onChange={handleSearch} />
        </CCol>
        <CCol xs="4" md="2" className=" text-right">
          <CInputCheckbox
            id="show-hidden"
            checked={showHidden}
            onChange={(e) => setShowHidden(e.target.checked)}
          />
          <CLabel variant="checkbox" className="form-check-label" htmlFor="show-hidden">
            Show hidden?
          </CLabel>
        </CCol>
      </CRow>

      <CLabel className="pt-4 pb-2">List of {popupInfo.title}s</CLabel>
      <CListGroup>
        {list?.map((item) => {
          return (
            <CListGroupItem className="d-flex btn" onClick={() => selectVoucher(item)}>
              <div className="header-container flex-fill">
                <CInputCheckbox checked={idChecked[item.id]} disabled={idDisabled[item.id]} />
                <div>
                  {item.name}
                  <CLabel className="font-weight-bold m-0">(#{item.id})</CLabel> -
                  <StatusText status={item.status} />
                </div>
              </div>

              <CLabel className="m-0">
                <CLabel className="font-weight-bold m-0">AUD$ ${+(item.unit_price || 0)} </CLabel> |
                {` ${+(item.point_price || 0)} pts`}
              </CLabel>
            </CListGroupItem>
          )
        })}
      </CListGroup>
    </CModalBody>
  )
}

export default SearchVoucher
