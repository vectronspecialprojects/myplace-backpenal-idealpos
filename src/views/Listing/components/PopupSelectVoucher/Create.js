import React from 'react'
import {CCol, CFormGroup, CInput, CLabel, CModalBody, CRow, CSelect} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import NotificationSetting from '../../../../common/NotificationSettting'
import FormCheckbox from '../../../../common/FormCheckbox'

function CreateVoucher({error, voucherSetups, data, setData, popupInfo}) {
  const updateData = (e) => {
    const {value, id} = e.target
    setData((oldData) => {
      const newData = {...oldData}
      const updateDataRecursive = (data, keys) => {
        if (keys.length === 1) return (data[keys[0]] = value)
        const deepData = data[keys[0]]
        keys.shift()
        return updateDataRecursive(deepData, keys)
      }
      updateDataRecursive(newData, id.split('.'))
      return newData
    })
  }

  return (
    <>
      <CModalBody>
        <CRow>
          <CCol xs="12" md="6">
            <CRow>
              <CCol xs="6" md="3">
                <FormInput
                  label="Name"
                  id="name"
                  required
                  value={data.name}
                  onChange={updateData}
                  error={error.name}
                />
              </CCol>
              <CCol xs="6" md="3">
                <CFormGroup>
                  <CLabel htmlFor="product_type_id">Type</CLabel>
                  <CSelect custom id="product_type_id" value={data.category} onChange={updateData}>
                    {popupInfo.categories.map((_) => (
                      <option value={_.value} key={_.value}>
                        {_.label}
                      </option>
                    ))}
                  </CSelect>
                </CFormGroup>
              </CCol>
              <CCol xs="6" md="3">
                <FormCheckbox
                  id="is_hidden"
                  label="Hide this"
                  checked={data.is_hidden === 1}
                  checkValue={1}
                  uncheckValue={0}
                  onChange={updateData}
                />
              </CCol>
              <CCol xs="6" md="3">
                <FormCheckbox
                  id="status"
                  label="Active"
                  checked={data.status === 'active'}
                  checkValue="active"
                  uncheckValue="inactive"
                  onChange={updateData}
                />
              </CCol>
              <CCol xs="12" md="12">
                <FormInput
                  label="Short Description"
                  id="desc_short"
                  value={data.desc_short}
                  onChange={updateData}
                  error={error.desc_short}
                  required
                />
              </CCol>
              <CCol xs="4" md="3">
                <CLabel>Image</CLabel>

                <UploadImageCrop id="image" aspect={1} onConfirm={updateData} className="w-100">
                  <Image blob={data?.image} className="w-100" />
                </UploadImageCrop>
              </CCol>
              <CCol xs="8" md="9">
                <CLabel className="h6 font-weight-bold d-block">Notification</CLabel>
                <NotificationSetting setting={data.payload?.[0]} id="payload.0" onUpdate={updateData} />
              </CCol>
            </CRow>
          </CCol>

          <CCol xs="12" md="6">
            <CRow>
              <CCol xs="12" md="6">
                <FormInput
                  label="Unit Price($AUD)"
                  id="unit_price"
                  value={data.unit_price}
                  onChange={updateData}
                  type="number"
                  error={error.unit_price}
                  required
                />
              </CCol>
              <CCol xs="12" md="6">
                <FormInput
                  label="Point Price"
                  id="point_price"
                  value={data.point_price}
                  onChange={updateData}
                  type="number"
                  error={error.point_price}
                  required
                />
              </CCol>

              <CCol xs="12" md="12">
                <FormCheckbox
                  id="is_free"
                  label="Free"
                  checked={data.is_free === 1}
                  checkValue={1}
                  uncheckValue={0}
                  onChange={updateData}
                />
              </CCol>

              <CCol xs="12" md="12">
                <FormCheckbox
                  id="system_point_ratio"
                  label="To calculate the reward, Do you want to use System's Point Ratio?"
                  checked={data.system_point_ratio}
                  onChange={updateData}
                />
              </CCol>

              <CCol xs="6" md="12">
                <CFormGroup>
                  <CLabel htmlFor="point_member_get">
                    How many points will a member get after they buy your item?
                  </CLabel>
                  <CInput
                    id="point_get"
                    disabled={data.system_point_ratio}
                    value={data.system_point_ratio ? data.unit_price : data.point_get}
                    onChange={updateData}
                  />
                  <CLabel>
                    {data.system_point_ratio
                      ? "Use system's point ratio every AUD$ 1,00 spent = 1pts."
                      : `every AUD$${parseFloat((+data.unit_price).toFixed(2)).toLocaleString()} spent = ${
                          data.point_get
                        } pts`}
                  </CLabel>
                </CFormGroup>
              </CCol>

              <CCol xs="6" md="12">
                <CFormGroup>
                  <CLabel htmlFor="point_member_get">
                    Voucher must be created in Bepoz Back office first to be linked here
                  </CLabel>
                  <CSelect custom name="select" id="voucher_setup">
                    <option key={''} value="" />
                    {voucherSetups.map((_) => (
                      <option key={_.id} value={_.id}>
                        {_.name}
                      </option>
                    ))}
                  </CSelect>
                </CFormGroup>
              </CCol>

              <CCol xs="6" md="12">
                <CFormGroup>
                  <CLabel htmlFor="product_number">Please provide Bepoz Product Number</CLabel>
                  <CInput id="product_number" disabled />
                  <CLabel>A product must be created in Bepoz Back office first to be linked here</CLabel>
                </CFormGroup>
              </CCol>
            </CRow>
          </CCol>
        </CRow>
      </CModalBody>
    </>
  )
}

export default CreateVoucher
