import {CButton, CCol, CFormGroup, CInputCheckbox, CLabel, CRow, CTextarea} from '@coreui/react'
import RichEditor from '../../../common/RichEditor'
import React from 'react'
import FormInput from '../../../common/FormInput'
import FormDateTime from '../../../common/FormDateTime'
import VenueSelect from '../../../common/VenueSelect'
import FormTime from '../../../common/FormTime'
import Occurrence from './Occurrence'
import FormCheckbox from '../../../common/FormCheckbox'
import PrizePromotionSelect from '../../../common/PrizePromotionSelect'
import {LISTING} from '../constant'
import {useSelector} from 'react-redux'
import ProductSelect from './ProductSelect'
import TierSelect from '../../../common/TierSelect'

const MainInformation = ({typeId, form, error, updateForm, showRestock = false, showSaleDate = false}) => {
  const setting = useSelector((state) => state.app.appSettings)

  return (
    <>
      <CRow>
        {[LISTING.STAMP_CARD].includes(typeId) && (
          <CCol xs="12" md="12">
            <CLabel>Prize Promotion</CLabel>
            <PrizePromotionSelect
              id="prize_promotion_id"
              value={form?.prize_promotion_id}
              onChange={updateForm}
            />
          </CCol>
        )}
        <CCol xs="12" md="6">
          <FormInput
            label="Internal Name"
            id="name"
            placeholder="Name"
            value={form?.name}
            onChange={updateForm}
            error={error?.name}
            maxLength={255}
            desc="The title will only be seen by the BE users"
            required
          />
        </CCol>
        <CCol xs="12" md="6">
          {setting?.venue_number.value === 'multiple' && ![LISTING.OUR_TEAM].includes(typeId) && (
            <CFormGroup>
              <CLabel>Venue</CLabel>
              <div className="d-flex">
                <VenueSelect
                  value={form.venue_id || 0}
                  id="venue_id"
                  onChange={updateForm}
                  className="flex-fill"
                />
              </div>
            </CFormGroup>
          )}
        </CCol>

        <CCol xs="12" md="6">
          <FormInput
            label="Display Heading"
            id="heading"
            placeholder="Heading"
            value={form?.heading}
            onChange={updateForm}
            maxLength={40}
            desc="Heading displayed in the app"
            error={error?.heading}
            required
          />
        </CCol>
        <CCol xs="12" md="6">
          <CFormGroup variant="checkbox" className="checkbox">
            <CInputCheckbox id="status" checked={form?.status === 'active'} onChange={updateForm} />
            <CLabel variant="checkbox" className="form-check-label" htmlFor="status">
              Active
            </CLabel>
          </CFormGroup>
        </CCol>

        <CCol xs="12" md="12">
          <FormInput
            label="Short Description"
            error={error?.desc_short}
            id="desc_short"
            placeholder="Short Description"
            desc="Summarized information displayed in the app"
            value={form?.desc_short}
            onChange={updateForm}
            maxLength={255}
            required
          />
        </CCol>

        {[LISTING.REGULAR_EVENT, LISTING.GIFT_CERTIFICATE].includes(typeId) && (
          <CCol xs="12" md="12">
            <CFormGroup>
              <CLabel htmlFor="desc_long">Long Description</CLabel>
              <RichEditor id="desc_long" initValue={form?.desc_long || ''} onChange={updateForm} />
              <small>
                <em>Additional information displayed in the app</em>
              </small>
            </CFormGroup>
          </CCol>
        )}

        {[LISTING.REGULAR_EVENT].includes(typeId) && (
          <>
            <CCol xs="12" md="6">
              <CFormGroup>
                <CLabel htmlFor="time_start">Event Start Date Time</CLabel>
                <FormTime id="time_start" value={form?.time_start} onChange={updateForm} />
              </CFormGroup>
            </CCol>
            <CCol xs="12" md="6">
              <CFormGroup>
                <CLabel htmlFor="time_end">Event End Date Time</CLabel>
                <FormTime id="time_end" value={form?.time_end} onChange={updateForm} />
              </CFormGroup>
            </CCol>
            <CCol xs="12">
              <CLabel>Select the days of the week this event happens on:</CLabel>
              <Occurrence id={'payload.occurrence'} onChange={updateForm} value={form?.payload?.occurrence} />
            </CCol>
          </>
        )}

        {[LISTING.SPECIAL_EVENT].includes(typeId) && (
          <CCol xs="12">
            <CRow>
              <CCol xs="12" md="6">
                <CFormGroup>
                  <CLabel htmlFor="datetime_start">Event Start Date Time</CLabel>
                  <FormDateTime
                    showToday={false}
                    id="datetime_start"
                    value={form?.datetime_start}
                    onChange={updateForm}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="12" md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between">
                    <CLabel htmlFor="datetime_end">Event End Date Time</CLabel>
                    <div>
                      <CInputCheckbox
                        id="neverEnds"
                        checked={form?.neverEnds}
                        type="checkbox"
                        className="ml-0"
                        onChange={updateForm}
                      />
                      <CLabel variant="checkbox" className="form-check-label ml-3" htmlFor="neverEnds">
                        Never Ends
                      </CLabel>
                    </div>
                  </div>
                  <FormDateTime
                    showToday={false}
                    id="datetime_end"
                    value={form?.datetime_end}
                    onChange={updateForm}
                  />
                </CFormGroup>
              </CCol>
              {showSaleDate && (
                <>
                  <CCol xs="12" md="6">
                    <CFormGroup>
                      <div className="d-flex justify-content-between">
                        <CLabel htmlFor="sell_datetime_start">Sell Start Date Time</CLabel>
                        <div>
                          <CInputCheckbox
                            id="sellStartSameAsAbove"
                            checked={form?.sellStartSameAsAbove}
                            type="checkbox"
                            className="ml-0"
                            onChange={updateForm}
                          />
                          <CLabel
                            variant="checkbox"
                            className="form-check-label ml-3"
                            htmlFor="sellStartSameAsAbove">
                            Same as above
                          </CLabel>
                        </div>
                      </div>
                      <FormDateTime
                        showToday={false}
                        id="sell_datetime_start"
                        value={form?.sell_datetime_start}
                        onChange={updateForm}
                      />
                    </CFormGroup>
                  </CCol>
                  <CCol xs="12" md="6">
                    <CFormGroup>
                      <div className="d-flex justify-content-between">
                        <CLabel htmlFor="sell_datetime_end">Sell End Date Time</CLabel>
                        <div>
                          <CInputCheckbox
                            id="sellEndSameAsAbove"
                            type="checkbox"
                            checked={form?.sellEndSameAsAbove}
                            className="ml-0"
                            onChange={updateForm}
                          />
                          <CLabel
                            variant="checkbox"
                            className="form-check-label ml-3"
                            htmlFor="sellEndSameAsAbove">
                            Same as above
                          </CLabel>
                        </div>
                      </div>
                      <FormDateTime
                        showToday={false}
                        id="sell_datetime_end"
                        value={form?.sell_datetime_end}
                        onChange={updateForm}
                      />
                    </CFormGroup>
                  </CCol>
                </>
              )}
            </CRow>
          </CCol>
        )}

        {[LISTING.SPECIAL_EVENT, LISTING.PROMOTION].includes(typeId) && (
          <>
            <CCol xs="12" md="12">
              <FormInput
                label="Item Available About"
                id="max_limit"
                desc="Available amount of items to be sold or redeemed. '0' means unlimited"
                value={form?.max_limit}
                onChange={updateForm}
                type="number"
              />
            </CCol>
            <CCol xs="12" md="12">
              <FormInput
                label="Max Item Available Amount Per Day"
                id="max_limit_per_day"
                desc="Available amount of items to be sold or redeemed per day. '0' means unlimited"
                value={form?.max_limit_per_day}
                onChange={updateForm}
                type="number"
              />
            </CCol>
          </>
        )}

        {showRestock &&
          [LISTING.SPECIAL_EVENT, LISTING.PROMOTION, LISTING.GIFT_CERTIFICATE].includes(typeId) && (
            <CCol xs="12" md="12">
              <CRow>
                <CCol xs="12" md="5">
                  <FormInput
                    label="Restock Quantity"
                    id="restock_qty"
                    value={+form?.restock_qty || 0}
                    onChange={updateForm}
                    type="number"
                    maxLength={255}
                    desc="Resets available amount of items on restock. '0' means unlimited"
                  />
                </CCol>
                <CCol xs="12" md="2">
                  <CButton
                    color="secondary"
                    className="text-dark mt-2"
                    onClick={() =>
                      updateForm({
                        target: {
                          id: 'item_available_amount',
                          value: (+form?.item_available_amount || 0) + (+form?.restock_qty || 0),
                        },
                      })
                    }>
                    RESTOCK ->
                  </CButton>
                </CCol>
                <CCol xs="12" md="5">
                  <FormInput
                    label="Item Available Amount Unlimited"
                    id="item_available_amount"
                    value={+form?.item_available_amount || 'Unlimited'}
                    onChange={updateForm}
                    disabled
                    desc="Available amount of items"
                  />
                </CCol>
              </CRow>
            </CCol>
          )}

        {[LISTING.SPECIAL_EVENT, LISTING.PROMOTION].includes(typeId) && (
          <>
            <CCol xs="12" md="12">
              <FormInput
                label="Max Item Available Amount Per Account"
                id="max_limit_per_account"
                desc="Available amount of items to be sold or redeemed per day. '0' means unlimited"
                value={form?.max_limit_per_account}
                onChange={updateForm}
                type="number"
              />
            </CCol>
            <CCol xs="12" md="12">
              <FormInput
                label="Max Item Available Amount Per Day Per Account"
                id="max_limit_per_day_per_account"
                desc="Available amount of items to be sold or redeemed per day. '0' means unlimited"
                value={form?.max_limit_per_day_per_account}
                onChange={updateForm}
                type="number"
              />
            </CCol>
          </>
        )}
        <CCol xs="12" md="12">
          <CRow>
            {[LISTING.SPECIAL_EVENT, LISTING.MEMBERSHIP_TIER].includes(typeId) && (
              <CCol xs="12" md="6">
                <FormCheckbox
                  checkValue={true}
                  uncheckValue={false}
                  id="extra_settings.add_booking"
                  checked={form?.extra_settings?.add_booking}
                  type="checkbox"
                  label="Display Booking button/ Purchase button?"
                />
              </CCol>
            )}

            {[LISTING.SPECIAL_EVENT, LISTING.REGULAR_EVENT, LISTING.MEMBERSHIP_TIER].includes(typeId) && (
              <CCol xs="12" md="6">
                <FormCheckbox
                  checkValue={true}
                  uncheckValue={false}
                  id="extra_settings.add_enquiry"
                  checked={form?.extra_settings?.add_enquiry}
                  label="Display Enquiry Button?"
                  onChange={updateForm}
                />
              </CCol>
            )}
          </CRow>
        </CCol>
        {[LISTING.MEMBERSHIP_TIER].includes(typeId) && (
          <>
            <CCol xs="12" md="12" className="mt-3">
              <CLabel className="form-check-label mb-2" htmlFor="sellEndSameAsAbove">
                Please select product
              </CLabel>
              <ProductSelect onChange={updateForm} id={'products'} value={form.products?.[0]} />
            </CCol>
            <CCol xs="12" md="12" className="mt-3">
              <TierSelect
                id="tier_selected"
                value={form.tier_selected}
                onUpdate={updateForm}
                label="Membership Tier"
                placeholder="Please select"
                required
                error={error.tier_selected}
              />
            </CCol>
          </>
        )}
      </CRow>
    </>
  )
}

export default MainInformation
