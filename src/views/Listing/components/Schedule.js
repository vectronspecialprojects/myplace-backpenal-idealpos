import React, {useState} from 'react'
import {CButton, CDataTable} from '@coreui/react'
import PopAddSchedule from './PopAddSchedule'
import CIcon from '@coreui/icons-react'
import StatusText from '../../../common/StatusText'
import moment from 'moment'
import {useSelector} from 'react-redux'
import {GIFT_CERTIFICATE_MODE} from '../constant'
import {cilActionRedo} from '@coreui/icons'
import Notification from './Notification'

const fields = ['', 'Start', 'End', 'Display(s)', 'Tag(s)']

const Schedule = ({typeId, form, updateForm, disabled}) => {
  const tiers = useSelector((state) => state?.app?.tier || [])
  const schedules = form?.schedules || []
  const [showAddSchedule, setShowAddSchedule] = useState(false)
  const [selectedSchedule, setSelectedSchedule] = useState(null)

  const addSchedule = (value) => {
    let newSchedules = []
    if (selectedSchedule !== null) {
      newSchedules = [...schedules]

      if (newSchedules[selectedSchedule]?.mode === GIFT_CERTIFICATE_MODE.NEW) {
        newSchedules[selectedSchedule] = value
      } else {
        newSchedules[selectedSchedule] = {
          ...value,
          mode: GIFT_CERTIFICATE_MODE.MODIFIED,
          original: {...newSchedules[selectedSchedule]},
        }
      }

      setSelectedSchedule(null)
    } else {
      newSchedules = [...schedules, value]
    }
    updateForm({target: {id: 'schedules', value: newSchedules}})
  }

  const undoSchedule = (index) => {
    if (schedules[index].original) {
      schedules[index] = schedules[index].original
    } else {
      schedules[index].mode = GIFT_CERTIFICATE_MODE.NOT_MODIFIED
    }
    updateForm({target: {id: 'schedules', value: [...schedules]}})
  }

  const ediSchedule = (index) => {
    setSelectedSchedule(index)
    setShowAddSchedule(true)
  }

  const removeSchedule = (index) => {
    if (schedules[index].mode === GIFT_CERTIFICATE_MODE.NOT_MODIFIED) {
      schedules[index].mode = GIFT_CERTIFICATE_MODE.DELETED
      updateForm({target: {id: 'schedules', value: [...schedules]}})
    } else {
      updateForm({target: {id: 'schedules', value: schedules.filter((_, i) => i !== index)}})
    }
  }

  return (
    <>
      <h1 className="display-3">Schedules</h1>
      <Notification disabled={disabled} form={form} updateForm={updateForm} />
      <CButton
        disabled={disabled}
        color="secondary"
        className="text-dark"
        onClick={() => setShowAddSchedule(true)}>
        ADD DISPLAY SCHEDULE
      </CButton>
      {schedules.length > 0 && (
        <CDataTable
          items={schedules}
          fields={fields}
          striped
          itemsPerPage={5}
          pagination
          scopedSlots={{
            '': (item, index) => (
              <td>
                <CButton disabled={disabled} onClick={() => ediSchedule(index)}>
                  <CIcon name="cil-pencil" />
                </CButton>
                {(item.original || item.mode === GIFT_CERTIFICATE_MODE.DELETED) && (
                  <CButton disabled={disabled} onClick={() => undoSchedule(index)}>
                    <CIcon content={cilActionRedo} />
                  </CButton>
                )}

                {item.mode !== GIFT_CERTIFICATE_MODE.DELETED && (
                  <CButton disabled={disabled} onClick={() => removeSchedule(index)}>
                    <CIcon name="cil-trash" />
                  </CButton>
                )}
                <StatusText status={item.status} />
              </td>
            ),
            Start: (item) => <td>{moment(item?.date_start).format('LLL')}</td>,
            End: (item) => <td>{moment(item?.date_end).format('LLL')}</td>,
            ['Display(s)']: (item) => (
              <td>
                {item.types.length
                  ? item.types.map((type) => (
                      <span
                        key={type.tier_id}
                        className="badge badge-pill badge-secondary py-2 px-3 mr-1 mb-1">
                        {`${type.type.name} (${
                          (type.type?.tiers || tiers)?.find((_) => _.id === type.tier_id).name || ''
                        })`}
                      </span>
                    ))
                  : 'None'}
              </td>
            ),
            ['Tag(s)']: (item) => <td>None</td>,
          }}
        />
      )}
      <PopAddSchedule
        show={showAddSchedule}
        setModal={setShowAddSchedule}
        onSubmit={addSchedule}
        selectedSchedule={schedules[selectedSchedule]}
        typeId={typeId}
      />
    </>
  )
}

export default Schedule
