
import React, {useEffect, useMemo, useState} from 'react';
import {CDropdown, CDropdownDivider, CDropdownItem, CDropdownMenu, CDropdownToggle} from '@coreui/react';
import {getDropdownProduct} from '../../../utilities/ApiManage';

function ProductSelect({onSelect, value, onChange, defaultText = 'Please select product', disabled, ...props}) {
  const [data, setData] = useState([])

  useEffect(() => {
    getDropdownProduct().then(res => {
      if (res.ok) {
        setData(res.data)
      }
    })
  }, [])

  const currentValue = useMemo(
    () => value?.name || (value && data.find((_) => _.id === value)?.name),
    [value, data],
  )

  const handleChange = (item) => {
    onSelect?.(item.id)
    onChange?.({target: {id: props.id || '', value: item}})
  }

  return (
    <CDropdown {...props}>
      <CDropdownToggle
        className="w-100 d-flex align-items-center justify-content-between border"
        disabled={disabled}>
        {currentValue || defaultText}
      </CDropdownToggle>
      <CDropdownMenu style={{minWidth: '100%'}}>
        <div style={{height: 300, overflow: 'auto'}}>
          <CDropdownDivider />
          {data?.map((item) => (
            <div key={item.id}>
              <CDropdownItem onClick={() => handleChange(item)}>{item.name}</CDropdownItem>
              <CDropdownDivider />
            </div>
          ))}
        </div>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default ProductSelect
