import React, {useState} from 'react'
import {CButton, CDataTable} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import PopupUpdateEnquiry from './PopupUpdateEnquiry'
import {setModal} from '../../../reusable/Modal'
import {deleteListingEnquiry} from '../../../utilities/ApiManage'

const fields = ['#', 'Member', 'Subject', 'Message', 'Options']

const Enquiries = ({form, updateForm}) => {
  const [selectedEnquiry, setSelectedEnquiry] = useState(null)
  const {enquiries = []} = form

  const handleRemove = (item) => {
    setModal({
      show: true,
      title: `Would you like to delete the selected enquiry (#${item.id})`,
      message: 'Caution: deleting this item is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteListingEnquiry(item.id)
        if (res.status === 'ok') {
          updateForm({target: {id: 'enquiries', value: enquiries.filter((_) => _.id !== item.id)}})
          setModal({show: false})
        }
      },
    })
  }

  return (
    <>
      <h1 className="display-3">Enquiries</h1>
      {enquiries.length > 0 && (
        <CDataTable
          items={enquiries}
          fields={fields}
          striped
          scopedSlots={{
            '#': (item) => <td>{item.id}</td>,
            Member: (item) => (
              <td>{`${item.member?.first_name || ''} ${item.member?.last_name || ''} (${
                item.member?.id
              })`}</td>
            ),
            Subject: (item) => <td>{item.subject}</td>,
            Message: (item) => <td>{item.message}</td>,
            Options: (item, index) => (
              <td>
                <CButton onClick={() => setSelectedEnquiry(index)}>
                  <CIcon name="cil-pencil" />
                </CButton>
                <CButton onClick={() => handleRemove(item)}>
                  <CIcon name="cil-trash" />
                </CButton>
              </td>
            ),
          }}
        />
      )}
      <PopupUpdateEnquiry
        show={enquiries[selectedEnquiry]}
        closeModal={() => setSelectedEnquiry(null)}
        selectedEnquiry={enquiries[selectedEnquiry]}
      />
    </>
  )
}

export default Enquiries
