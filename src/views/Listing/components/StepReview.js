import React from 'react'
import {CCol, CNav, CNavItem, CNavLink, CRow, CTabContent, CTabPane, CTabs} from '@coreui/react'
import MainInformation from './MainInformation'
import Images from './Images'
import TicketVoucher from './TicketVoucher'
import Schedule from './Schedule'
import Notification from './Notification'
import Reminder from './Reminder'
import {LISTING} from '../constant'

const StepReview = ({typeId, form, error, updateForm, disabled = false}) => {
  const tabs = [
    [
      LISTING.SPECIAL_EVENT,
      LISTING.PROMOTION,
      LISTING.GIFT_CERTIFICATE,
      LISTING.STAMP_CARD,
      LISTING.STAMP_CARD_WON,
    ].includes(typeId) && {
      label: typeId === LISTING.SPECIAL_EVENT ? 'TICKETS' : 'VOUCHERS',
      component: <TicketVoucher disabled={disabled} form={form} updateForm={updateForm} typeId={typeId} />,
    },
    {label: 'SCHEDULES', component: <Schedule disabled={disabled} form={form} updateForm={updateForm} />},
    [LISTING.SPECIAL_EVENT, LISTING.MEMBERSHIP_TIER].includes(typeId) && {
      label: 'REMINDER',
      component: <Reminder form={form} updateForm={updateForm} />,
    },
  ].filter((_) => _)

  return (
    <>
      <CRow>
        <CCol xs="12" md="8">
          <div className="mb-3">
            <MainInformation typeId={typeId} form={form} error={error} updateForm={updateForm} />
          </div>
        </CCol>

        <CCol xs="12" md="4">
          <Images typeId={typeId} form={form} updateForm={updateForm} />
        </CCol>

        <CCol xs="12 mt-5">
          <CTabs>
            <CNav variant="tabs">
              {tabs.map((tab) => (
                <CNavItem key={tab.label}>
                  <CNavLink>{tab.label}</CNavLink>
                </CNavItem>
              ))}
            </CNav>
            <CTabContent>
              {tabs.map((tab) => (
                <CTabPane key={tab.label}>{tab.component}</CTabPane>
              ))}
            </CTabContent>
          </CTabs>
        </CCol>
      </CRow>
    </>
  )
}

export default StepReview
