import React, {useEffect, useMemo, useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CLabel,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTabs,
} from '@coreui/react'
import MainInformation from './components/MainInformation'
import Images from './components/Images'
import TicketVoucher from './components/TicketVoucher'
import Schedule from './components/Schedule'
import Notification from './components/Notification'
import {DEFAULT_LISTING, LISTING} from './constant'
import helpers from './helpers'
import {getDataListing, postListingData} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import Enquiries from './components/Enquiries'
import Reminder from './components/Reminder'
import {useSelector} from 'react-redux'

const UpdatePage = ({location, history}) => {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const {typeId, id} = useMemo(() => {
    const pathSplit = location.pathname.split('/')
    return {typeId: +pathSplit[2], id: +pathSplit[4]}
  }, [location.pathname])
  const [form, setForm] = useState({...DEFAULT_LISTING})
  const [error, setError] = useState({__checked: false})
  const listingInfo = useSelector((state) => state.app.listing?.find((item) => item.id === typeId))
  useEffect(() => {
    ;(async () => {
      try {
        const res = await getDataListing(id)
        if (!res.ok) throw new Error(res.message)
        const data = {...form, ...res.data, type_id: res.data.type.id, listing_type_id: res.data.type.id}
        setForm(data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const updateForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  const clickUpdate = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      const res = await postListingData(id, form)
      if (res.ok) history.push({pathname: ROUTES.LISTING.replace(':id', typeId)})
    }
  }

  const tabs = [
    [
      LISTING.SPECIAL_EVENT,
      LISTING.PROMOTION,
      LISTING.GIFT_CERTIFICATE,
      LISTING.STAMP_CARD,
      LISTING.STAMP_CARD_WON,
    ].includes(typeId) && {
      label: typeId === LISTING.SPECIAL_EVENT ? 'TICKETS' : 'VOUCHERS',
      component: <TicketVoucher disabled={!bepozVersion} form={form} updateForm={updateForm} typeId={typeId} />,
    },
    {label: 'SCHEDULES', component: <Schedule disabled={!bepozVersion} form={form} updateForm={updateForm} typeId={typeId} />},
    [LISTING.SPECIAL_EVENT, LISTING.REGULAR_EVENT].includes(typeId) && {
      label: 'ENQUIRIES',
      component: <Enquiries form={form} updateForm={updateForm} />,
    },
    {
      label: 'NOTIFICATION',
      component: (
        <>
          <div className="d-flex my-3">
            <CLabel className="h6 font-weight-bold d-block flex-fill">Notification</CLabel>
            <CButton
              color="secondary"
              className="text-dark"
              onClick={() =>
                updateForm({
                  target: {
                    id: 'item_available_amount',
                    value: (+form?.item_available_amount || 0) + (+form?.restock_qty || 0),
                  },
                })
              }>
              RESEND NOTIFICATION
            </CButton>
          </div>
          <CLabel className="h6 text-danger font-weight-bold d-block">
            Make sure change schedule before resend notification
          </CLabel>
          <Notification disabled={!bepozVersion} form={form} updateForm={updateForm} />
        </>
      ),
    },

    [LISTING.SPECIAL_EVENT, LISTING.REGULAR_EVENT].includes(typeId) && {
      label: 'REMINDER',
      component: (
        <CRow>
          <CCol xs={12} md={8}>
            <Reminder form={form} updateForm={updateForm} />
          </CCol>
        </CRow>
      ),
    },
  ].filter((_) => _)

  const sales = [
    {label: 'Sold', value: `${form.total_order || 0}`},
    {label: 'Order Gross', value: `$${(+(form.total_gross || 0)).toFixed(2)}`},
    {label: 'Total Discount', value: `$${(+(form.total_discount || 0)).toFixed(2)}`},
    {label: 'Order Net', value: `$${(+(form.total_net || 0)).toFixed(2)}`},
    {label: 'Total GST', value: `$${(+(form.total_gst || 0)).toFixed(2)}`},
  ]

  return (
    <CCol xs="12">
      <CCard>
        <CCardHeader>Edit {listingInfo?.name}</CCardHeader>
      </CCard>

      <CCard className="create-listing-container">
        <CCardBody>
          <h1>Review</h1>
          <CRow>
            <CCol xs="12" md="8">
              <MainInformation
                typeId={typeId}
                form={form}
                error={error}
                updateForm={updateForm}
                showRestock
              />
            </CCol>

            <CCol xs="12" md="4">
              <Images typeId={typeId} form={form} updateForm={updateForm} />
              <CCard className="mt-3">
                <CCardHeader>
                  <CLabel className="form-check-label m-0 h5">Sales</CLabel>
                </CCardHeader>
                <CCardBody>
                  {sales.map((sale) => (
                    <div key={sale.label} className="d-flex">
                      <CLabel className="form-check-label m-0 flex-fill">{sale.label}</CLabel>
                      <CLabel className="form-check-label m-0 ">{sale.value}</CLabel>
                    </div>
                  ))}
                  <small>Values are gross values minus discount amount</small>
                </CCardBody>
              </CCard>
            </CCol>

            <CCol xs="12 mt-5">
              <CTabs>
                <CNav variant="tabs">
                  {tabs.map((tab) => (
                    <CNavItem key={tab.label}>
                      <CNavLink>{tab.label}</CNavLink>
                    </CNavItem>
                  ))}
                </CNav>
                <CTabContent>
                  {tabs.map((tab) => (
                    <CTabPane key={tab.label}>{tab.component}</CTabPane>
                  ))}
                </CTabContent>
              </CTabs>
            </CCol>
          </CRow>
          <CButton
            disabled={!bepozVersion}
            variant="outline"
            color="secondary"
            className="text-dark font-weight-bold mt-3"
            onClick={clickUpdate}>
            UPDATE AND EXIT
          </CButton>
        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default UpdatePage
