import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow, CTabs} from '@coreui/react'
import StepReview from './components/StepReview'
import {DEFAULT_LISTING, LISTING} from './constant'
import {postListingData} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import helpers from './helpers'
import {useSelector} from 'react-redux'

function CreatePage({history, location}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const typeId = useMemo(() => +location.pathname.split('/')[2], [location.pathname])
  const [step, setStep] = useState(0)
  const [form, setForm] = useState({...DEFAULT_LISTING, type_id: typeId, listing_type_id: typeId})
  const [error, setError] = useState({__checked: false})
  const listingInfo = useSelector((state) => state.app.listing?.find((item) => item.id === typeId))

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const updateForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  const isReview = true

  const clickNext = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      if (isReview) {
        const res = await postListingData('', form)
        if (res.ok) {
          history.push({pathname: ROUTES.LISTING.replace(':id', typeId)})
        }
      } else {
        setStep(Math.min(step + 1, 5))
      }
    }
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Add {listingInfo?.name || ''}</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <CTabs activeTab={step} onActiveTabChange={setStep}>
              <StepReview disabled={!bepozVersion} typeId={typeId} form={form} error={error} updateForm={updateForm} />
            </CTabs>
            <CButton
              disabled={!bepozVersion}
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={clickNext}>
              {isReview ? 'SAVE AND EXIT' : 'NEXT STEP'}
            </CButton>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
