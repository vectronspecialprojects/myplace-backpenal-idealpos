import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import {displayOrder, getActiveListing} from '../../utilities/ApiManage'
import '../../scss/_custom.scss'
import SortableComponent from '../../common/SortableList'
import {CLabel} from '@coreui/react/es'
import ROUTES from '../../constants/routes'
import {useSelector} from 'react-redux'

function DisplayOrder({location, history}) {
  const typeId = useMemo(() => +location.pathname.split('/')[2], [location.pathname])
  const [data, setData] = useState([])
  const listingInfo = useSelector((state) => state.app.listing?.find((item) => item.id === typeId))
  useEffect(() => {
    handleGetData()
  }, [])

  const handleSave = async () => {
    try {
      const res = await displayOrder(data)
      if (!res.ok) throw new Error(res.message)
      history.push({pathname: ROUTES.LISTING.replace(':id', typeId)})
    } catch (e) {
      console.log(e)
    }
  }

  console.log('data', data)

  async function handleGetData() {
    try {
      const res = await getActiveListing(typeId)
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader>
              <CRow className="px-3">
                <CLabel className="flex-fill m-0 h4">{listingInfo?.name}(s)</CLabel>
                <CButton className="btn btn-secondary" onClick={handleSave}>
                  SAVE AND EXIT
                </CButton>
              </CRow>
            </CCardHeader>
            <CCardBody>
              <SortableComponent
                onChange={(e) => {
                  console.log('e', e)
                  setData(e)
                }}
                items={data}
                renderItem={(_) => (
                  <CCard className="my-3 p-3 ">
                    <CLabel>{_.name}</CLabel>
                  </CCard>
                )}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default DisplayOrder
