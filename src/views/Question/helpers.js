const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.question.length === 0) error.question = 'The question is required'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
