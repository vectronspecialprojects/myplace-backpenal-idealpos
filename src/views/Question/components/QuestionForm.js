import {CAlert, CButton, CCard, CCol, CLabel, CRow} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import React, {useEffect, useState} from 'react'
import {updateForm} from '../../../helpers'
import {getQuestion, postQuestion} from '../../../utilities/ApiManage'
import ROUTES from '../../../constants/routes'
import helpers from '../helpers'
import {DEFAULT_QUESTION} from '../constant'
import {useHistory} from 'react-router-dom'
import FormCheckbox from '../../../common/FormCheckbox'
import AnswerSelect from '../../../common/AnswerSelect'
import SortableComponent from '../../../common/SortableList'

const QuestionForm = ({questionId, question, showSaveButton = true, onFormChange}) => {
  const history = useHistory()
  const [form, setForm] = useState({...DEFAULT_QUESTION})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (error?.__checked || onFormChange) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
      onFormChange({form, validator})
    }
  }, [form, onFormChange])

  useEffect(() => {
    if (questionId) {
      ;(async () => {
        try {
          const res = await getQuestion(questionId)
          if (!res.ok) throw new Error(res.message)
          const data = {...form, ...res.data}
          setForm(data)
        } catch (e) {
          console.log(e)
        }
      })()
    }
    if (question) {
      setForm(question)
    }
  }, [questionId, question])

  const onChange = (event) => setForm((oldState) => updateForm(event, oldState))

  const handleOrderAnswer = (items) => {
    onChange({target: {id: 'answers', value: items}})
    onChange({target: {id: 'sequence', value: items}})
  }

  const clickSave = async () => {
    const validator = helpers.validateForm(form)
    if (validator.valid) {
      const res = await postQuestion(questionId, form)
      if (res.ok) {
        history.push({pathname: ROUTES.QUESTION})
      }
    }
    setError(validator.error)
  }

  return (
    <CRow>
      <CCol xs={12} md={8}>
        <FormInput
          id="question"
          value={form.question}
          onChange={onChange}
          error={error.question}
          label="Question"
          required
        />
      </CCol>
      <CCol xs={12} md={4}>
        <FormCheckbox
          id="multiple_choice"
          onChange={onChange}
          label="Multiple Choice"
          checked={form.multiple_choice === 1}
          checkValue={1}
          uncheckValue={0}
        />
      </CCol>
      <CCol xs={12}>
        <AnswerSelect
          onChange={onChange}
          id="answers"
          value={form.answers}
          label="Answer"
          placeholder="Search an answer"
        />
      </CCol>

      {questionId && (
        <>
          <CAlert color="warning">WARNING. Adding/Removing an answer will reset the sequence</CAlert>
          <CCol xs={12}>
            <CLabel>
              <CLabel className="font-weight-bold">THe sequence of Answer.</CLabel> Just drag and drop the
              answers below.
            </CLabel>

            <SortableComponent
              onChange={handleOrderAnswer}
              items={form.answers}
              renderItem={(_) => (
                <CCard className="my-3 p-3 ">
                  <CLabel>{_.answer}</CLabel>
                </CCard>
              )}
            />
          </CCol>
        </>
      )}

      {showSaveButton && (
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          SAVE AND EXIT
        </CButton>
      )}
    </CRow>
  )
}

export default QuestionForm
