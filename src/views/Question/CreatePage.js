import React from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import QuestionForm from './components/QuestionForm'

function CreatePage() {
  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Add Question</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <QuestionForm />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
