import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow, CTooltip} from '@coreui/react'
import {deleteQuestion, getQuestion} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TableFilter from '../../common/TableFilter'
import {useHistory} from 'react-router-dom'
import ROUTES from '../../constants/routes'
import {setModal} from '../../reusable/Modal'

const fields = [{key: 'id', label: '#'}, 'question', 'multiple_choice', {key: 'option', label: ''}]

function ListPage() {
  const history = useHistory()
  const [data, setData] = useState([])

  async function handleGetData() {
    try {
      const res = await getQuestion()
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    handleGetData()
  }, [])

  const clickUpdate = (item) => {
    history.push(ROUTES.QUESTION_UPDATE.replace(':id', item.id))
  }

  const clickDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected question (#${item.id})`,
      message: 'Caution: deleting this ticket is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteQuestion(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  const clickCreate = () => {
    history.push(ROUTES.QUESTION_CREATE)
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader>
              <TableFilter
                title="Questions"
                total={data.total}
                onClickCreate={clickCreate}
                actions={
                  <>
                    <CButton onClick={() => history.push(ROUTES.SURVEY)}>
                      <CTooltip content="Back">
                        <CIcon content={freeSet.cilArrowLeft} style={{width: 25, height: 25}} />
                      </CTooltip>
                    </CButton>
                  </>
                }
              />
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={data || []}
                fields={fields}
                striped
                scopedSlots={{
                  multiple_choice: (item) => <td>{item.multiple_choice === 1 ? 'Yes' : 'No'}</td>,
                  option: (item) => (
                    <td>
                      <CRow>
                        <CButton onClick={() => clickUpdate(item)}>
                          <CTooltip content="Edit">
                            <CIcon content={freeSet.cilPen} />
                          </CTooltip>
                        </CButton>
                        <CButton onClick={() => clickDelete(item)}>
                          <CTooltip content="Delete">
                            <CIcon name="cil-trash" />
                          </CTooltip>
                        </CButton>
                      </CRow>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default ListPage
