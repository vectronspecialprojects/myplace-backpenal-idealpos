import React, {useMemo} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import QuestionForm from './components/QuestionForm'

function UpdatePage({location}) {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Update Question</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <QuestionForm questionId={id} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default UpdatePage
