import React, {useEffect, useMemo, useState} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CLabel, CRow} from '@coreui/react'
import {getTransaction} from '../../utilities/ApiManage'
import moment from 'moment'
import helpers from './helpers'

const UpdatePage = ({location}) => {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])
  const [data, setData] = useState({})

  useEffect(() => {
    ;(async () => {
      const res = await getTransaction(id)
      if (res.ok) {
        setData({...res.data, payload: JSON.parse(res.data.payload)})
      }
    })()
  }, [])

  const getListingDetail = () => {
    let title = 'EVENT',
      des = (
        <h6 className="text-muted">
          Date:{' '}
          <CLabel className="font-weight-light">{moment(data?.listing.date_start).format('LLLL')}</CLabel>
        </h6>
      )
    if (data?.payload?.transaction_type === 'gift_certificate') {
      title = 'GIFT_CERTIFICATE'
      des = (
        <h6 className="text-muted">
          Description: <CLabel className="font-weight-light">{data?.listing?.desc_short}</CLabel>
        </h6>
      )
    }

    return (
      <>
        <h5>
          {title}: <CLabel className="font-weight-light m-0">{data?.listing?.name}</CLabel>
        </h5>
        {des}
      </>
    )
  }

  return (
    <CCol xs="12">
      <CCard>
        <CCardHeader>Transaction Detail</CCardHeader>
      </CCard>

      <CCard className="create-listing-container">
        <CCardBody>
          <CRow>
            <CCol xs={12} md={6}>
              <div className="b-b-1 py-2">
                <h6 className="text-muted">Transaction #{id}</h6>
              </div>
              <div className="b-b-1 py-2">
                <h5>
                  Token: <CLabel className="font-weight-light">{data?.token || ''}</CLabel>
                </h5>
                <h6 className="text-muted">
                  Created at:{' '}
                  <CLabel className="font-weight-light">{moment(data?.created_at).format('LLLL')}</CLabel>
                </h6>
              </div>
              <div className="b-b-1 py-2">
                <div className="mb-2">
                  <h5>
                    Payment Status:{' '}
                    <CLabel className="font-weight-light">{data?.payment_status || ''}</CLabel>
                  </h5>
                  <h6 className="text-muted">
                    Payment Type: <CLabel className="font-weight-light">{data?.type || ''}</CLabel>
                  </h6>
                  <h6 className="text-muted">
                    Paid at:{' '}
                    <CLabel className="font-weight-light">
                      {data?.paid_at ? moment(data.paid_at).format('LLLL') : ''}
                    </CLabel>
                  </h6>
                </div>
                <div className="mb-2">
                  <h5>
                    Total Paid:{' '}
                    <CLabel className="font-weight-light">AUD${data?.paid_total_price || ''}</CLabel>
                  </h5>
                  <h6 className="text-muted">
                    Point Rewards:{' '}
                    <CLabel className="font-weight-light">
                      {data?.claimable_point_reward || 0} pst (claimable) of {data?.point_reward || 0} pst
                    </CLabel>
                  </h6>
                  <h6 className="text-muted">
                    Paid at:{' '}
                    <CLabel className="font-weight-light">
                      {data?.paid_at ? moment(data.paid_at).format('LLLL') : ''}
                    </CLabel>
                  </h6>
                </div>
                <div>
                  <h5>
                    Redeem Point Ratio:{' '}
                    <CLabel className="font-weight-light">{data?.redeem_point_ratio || ''} pts / $1</CLabel>
                  </h5>
                  <h6 className="text-muted">
                    Point Used:{' '}
                    <CLabel className="font-weight-light">{data?.paid_total_point || 0} pts</CLabel>
                  </h6>
                </div>

                <div className="b-b-1 py-2">
                  <h6 className="text-muted">Order Details</h6>
                </div>
                {data?.order_details?.map((order) => (
                  <div key={order.id} className="py-2">
                    <h5>
                      Product: <CLabel className="font-weight-light">{order?.name || ''}</CLabel>
                    </h5>
                    <h6 className="text-muted">
                      Qty:{' '}
                      <CLabel className="font-weight-light">
                        {order?.qyi} - ${order.point_price} per item
                      </CLabel>
                    </h6>
                    <h6 className="text-muted">
                      Subtotal:{' '}
                      <CLabel className="font-weight-light">AUD$ {order?.subtotal_point_price}</CLabel>
                    </h6>
                    <h6>
                      Reward Point Ratio:{' '}
                      <CLabel className="font-weight-light">{order?.reward_point_ratio} per $1</CLabel>
                    </h6>
                    <h6>
                      Reward: <CLabel className="font-weight-light">{order?.point_reward} pst</CLabel>
                    </h6>
                    <h6 className="text-muted">
                      Lookup:{' '}
                      <CLabel className="font-weight-light">
                        {order?.member_vouchers?.map((voucher) => voucher.lookup).join(',')}
                      </CLabel>
                    </h6>
                  </div>
                ))}
              </div>
            </CCol>
            <CCol xs={12} md={6} className="b-b-1">
              <div className="b-b-1 py-2 d-flex">
                {data?.member?.profile_img && (
                  <img src={data.member.profile_img} width={40} height={40} className="rounded-circle mr-2" />
                )}
                <div>
                  <h5>
                    Member ID:{' '}
                    <CLabel className="font-weight-light">{data?.member?.bepoz_account_id || ''}</CLabel>
                  </h5>
                  <h6 className="text-muted">
                    Name:{' '}
                    <CLabel className="font-weight-light">
                      {data?.member?.first_name || ''} {data?.member?.last_name || ''} (#{data?.member?.id})
                    </CLabel>
                  </h6>
                  <h6 className="text-muted">
                    Contract Number:{' '}
                    <CLabel className="font-weight-light">{data?.member?.phone || ''}</CLabel>
                  </h6>
                </div>
              </div>

              <div className="b-b-1 py-2">
                <h6 className="text-muted">More Details</h6>
              </div>

              {data?.listing && (
                <div className="d-flex py-2">
                  <img src={data.listing.image_square} width={40} height={40} className="rounded-circle" />
                  <div className="d-flex flex-column justify-content-center ml-2">{getListingDetail()}</div>
                </div>
              )}

              <div className=" py-2">
                <h6>
                  Transaction Type:{' '}
                  <CLabel className="font-weight-light">
                    {helpers.getTransactionType(data?.payload?.transaction_type)}
                  </CLabel>
                </h6>
                <h6>
                  Venue: <CLabel className="font-weight-light">{data?.venue?.name || ''}</CLabel>
                </h6>
                {data?.payload?.api_request?.stripe_token && (
                  <>
                    <h6>
                      Paid using: <CLabel className="font-weight-light">Stripe</CLabel>
                    </h6>

                    <div className="py-2">
                      <h5>
                        Stripe Transaction ID:{' '}
                        <CLabel className="font-weight-light">{data?.payment_log?.response?.id}</CLabel>
                      </h5>
                      <h6>
                        Outcome:{' '}
                        <CLabel className="font-weight-light">
                          {data?.response?.outcome?.seller_message || ''}
                        </CLabel>
                      </h6>
                      <h6 className="text-muted">
                        Card: <CLabel className="font-weight-light">Visa</CLabel>
                      </h6>
                    </div>
                    <div className="py-2">
                      <h5>
                        Application ID:{' '}
                        <CLabel className="font-weight-light">
                          {data?.payment_log?.response?.application}
                        </CLabel>
                      </h5>
                      <h6>
                        Fee ID:{' '}
                        <CLabel className="font-weight-light">
                          {data?.payment_log?.response?.application_fee}
                        </CLabel>
                      </h6>
                      <h6 className="text-muted">
                        Balance Transaction:{' '}
                        <CLabel className="font-weight-light">
                          {data?.payment_log?.response?.balance_transaction}
                        </CLabel>
                      </h6>
                    </div>
                  </>
                )}
              </div>
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default UpdatePage
