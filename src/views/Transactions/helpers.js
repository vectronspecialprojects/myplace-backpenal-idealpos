const helpers = {
  getTransactionType: (transactionType) => {
    const TYPE = {
      place_order: 'TICKET',
      claim_promotion: 'PROMOTION',
      gift_certificate: 'GIFT CERTIFICATE',
      friend_referral: 'FRIEND REFERRAL',
      signup_reward: 'SIGNUP REWARD',
    }
    return TYPE[transactionType] || ''
  },
}
export default helpers
