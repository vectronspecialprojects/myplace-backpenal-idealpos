import React from 'react'
import {CChartBar} from '@coreui/react-chartjs'

const BACKGROUND_COLOR = ['#f87979', '#00D8FF']

const TransactionChart = ({value}) => {
  if (!value) return null
  const datasets = value.map((_, i) => ({
    label: _.key,
    backgroundColor: BACKGROUND_COLOR[i],
    data: _.values.map((value) => value.y1),
  }))
  const labels = value?.[0]?.values?.map((_) => _.x)
  return (
    <CChartBar
      datasets={datasets}
      labels={labels}
      options={{
        tooltips: {enabled: true},
        scales: {
          yAxes: [{ticks: {min: 0, beginAtZero: true}, scaleLabel: {display: true, labelString: '$'}}],
        },
        animation: {duration: 1},
      }}
    />
  )
}
export default TransactionChart
