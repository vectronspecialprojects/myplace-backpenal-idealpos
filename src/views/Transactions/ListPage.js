import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CLabel, CRow, CTooltip} from '@coreui/react'
import {
  getDailySales,
  getInvoice,
  getMonthlySales,
  getPagingTransaction,
  getQuarterlySales,
  getQuarterlyYearlySales,
  getWeeklySales,
  getYearlySales,
} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TableFilter from '../../common/TableFilter'
import moment from 'moment'
import ROUTES from '../../constants/routes'
import helpers from './helpers'
import TransactionChart from './components/TransactionChart'
import {allSettled} from '../../helpers'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#'},
  'invoice',
  {key: 'price', label: 'Price|Point'},
  {key: 'option', label: ''},
]

function ListPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [venueSelected, setVenueSelected] = useState(0)
  const [filter, setFilter] = useState(null)
  const [sales, setSales] = useState([])

  const handleGetData = async () => {
    const res = await getPagingTransaction(currentPage, itemPerPage, venueSelected, filter)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    // ;(async () => {
    //   const listSale = [
    //     'Daily Sales',
    //     'Weekly Sales',
    //     'Monthly Sales',
    //     'Quarterly Sales',
    //     'Yearly Sales',
    //     'Quarterly/Yearly Sales',
    //   ]
    //   const res = await allSettled([
    //     getDailySales(),
    //     getWeeklySales(),
    //     getMonthlySales(),
    //     getQuarterlySales(),
    //     getYearlySales(),
    //     getQuarterlyYearlySales(),
    //   ])
    //   setSales(
    //     listSale.map((sale, index) => ({name: sale, value: res?.[index]?.ok ? res[index].data : null})),
    //   )
    // })()
  }, [])

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, venueSelected, filter])

  const clickQuickLook = (item) => history.push(ROUTES.TRANSACTION_DETAIL.replace(':id', item.id))

  const clickDownloadInvoice = (item) => getInvoice(item.id)

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
    setVenueSelected(params.venueSelected)
    setFilter(params.filter)
  }

  const getTransactionType = (item) => helpers.getTransactionType(JSON.parse(item.payload)?.transaction_type)

  return (
    <CRow>
      <CCol xs={12} md={12}>
        <CCard>
          <CCardHeader>
            <TableFilter
              params={{venueSelected, itemPerPage, filter}}
              title="Transactions"
              total={data.total}
              onParamChange={handleChangeParam}
            />
          </CCardHeader>
          <CCardBody>
            <MyTable
              data={data?.data}
              params={{itemPerPage, page: currentPage, total: data?.total}}
              fields={fields}
              size={'sm'}
              onPageChange={setCurrentPage}
              onRowClick={(item) => {
                clickQuickLook(item)
              }}
              scopedSlots={{
                invoice: (item) => (
                  <td>
                    <CLabel className="d-block">{moment(item.created_at).format('LLLL')}</CLabel>
                    <CLabel className="d-block">
                      {item.member?.first_name || ''} {item.member?.last_name || ''} ---{' '}
                      {getTransactionType(item)}
                    </CLabel>
                    <CLabel className="d-block">Status: {item.voucher_status} </CLabel>
                  </td>
                ),
                price: (item) => (
                  <td>
                    <CLabel className="font-weight-bold">AUD${item.actual_total_price}</CLabel>|
                    {item.actual_total_point}
                  </td>
                ),
                option: (item) => (
                  <td>
                    <CButton onClick={() => clickQuickLook(item)} className="p-0 pr-2">
                      <CTooltip content="Click here to quick look">
                        <CIcon content={freeSet.cilFindInPage} />
                      </CTooltip>
                    </CButton>
                    <CButton onClick={() => clickDownloadInvoice(item)} className="p-0 pr-2">
                      <CTooltip content="Download invoice">
                        <CIcon content={freeSet.cilCloudDownload} />
                      </CTooltip>
                    </CButton>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>
      {/*<CCol xs={12} md={4}>*/}
      {/*  {sales.map((sale) => (*/}
      {/*    <CCard key={sale.name}>*/}
      {/*      <CCardHeader>{sale.name}</CCardHeader>*/}
      {/*      <CCardBody>*/}
      {/*        <TransactionChart value={sale.value} />*/}
      {/*      </CCardBody>*/}
      {/*    </CCard>*/}
      {/*  ))}*/}
      {/*</CCol>*/}
    </CRow>
  )
}

export default ListPage
