import React, {useEffect, useState} from 'react'
import {
  CAlert,
  CButton,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import {postAnswer} from '../../../utilities/ApiManage'

function PopupCreateAnswer({show, closeModal, selectedAnswer, onSubmit}) {
  const [answer, setAnswer] = useState('')

  useEffect(() => {
    setAnswer(selectedAnswer?.answer || '')
  }, [selectedAnswer, show])

  const saveAndClose = async () => {
    const res = await postAnswer(selectedAnswer?.id, {answer})
    if (res.ok) {
      onSubmit()
      closeModal()
    }
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>{selectedAnswer ? 'Update' : 'Create'} Answer</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12} md={12}>
              <FormInput value={answer} onChangeText={setAnswer} label="Answer" required />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose} disabled={!answer}>
            SAVE & EXIT
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupCreateAnswer
