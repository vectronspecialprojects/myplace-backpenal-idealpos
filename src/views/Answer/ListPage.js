import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow, CTooltip} from '@coreui/react'
import {deleteAnswer, getAnswer} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TableFilter from '../../common/TableFilter'
import {useHistory} from 'react-router-dom'
import ROUTES from '../../constants/routes'
import {setModal} from '../../reusable/Modal'
import PopupCreateAnswer from './components/PopupCreateAnswer'

const fields = [{key: 'id', label: '#'}, 'answer', {key: 'option', label: ''}]

function ListPage() {
  const history = useHistory()
  const [data, setData] = useState([])
  const [selectedAnswer, setSelectedAnswer] = useState(null)
  const [modalCreateVisible, setModalCreateVisible] = useState(false)

  async function handleGetData() {
    try {
      const res = await getAnswer()
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    handleGetData()
  }, [])

  const clickUpdate = (item) => {
    setSelectedAnswer(item)
    setModalCreateVisible(true)
  }

  const clickDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected answer (#${item.id})`,
      message: 'Caution: deleting this ticket is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteAnswer(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  const clickCreate = () => {
    setSelectedAnswer(null)
    setModalCreateVisible(true)
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader>
              <TableFilter
                title="Answers"
                total={data.total}
                onClickCreate={clickCreate}
                actions={
                  <>
                    <CButton onClick={() => history.push(ROUTES.SURVEY)}>
                      <CTooltip content="Back">
                        <CIcon content={freeSet.cilArrowLeft} style={{width: 25, height: 25}} />
                      </CTooltip>
                    </CButton>
                  </>
                }
              />
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={data || []}
                fields={fields}
                striped
                scopedSlots={{
                  option: (item) => (
                    <td>
                      <CRow>
                        <CButton onClick={() => clickUpdate(item)}>
                          <CTooltip content="Edit">
                            <CIcon content={freeSet.cilPen} />
                          </CTooltip>
                        </CButton>
                        <CButton onClick={() => clickDelete(item)}>
                          <CTooltip content="Delete">
                            <CIcon name="cil-trash" />
                          </CTooltip>
                        </CButton>
                      </CRow>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <PopupCreateAnswer
        show={modalCreateVisible}
        closeModal={() => setModalCreateVisible(false)}
        onSubmit={handleGetData}
        selectedAnswer={selectedAnswer}
      />
    </>
  )
}

export default ListPage
