import React, {useEffect, useState} from 'react'
import {CButton, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import {postFAQ} from '../../../utilities/ApiManage'
import {DEFAULT_FAQ} from '../constant'
import {toast} from 'react-toastify'
import helpers from '../helpers'
import {updateForm} from '../../../helpers'
import FormTextarea from '../../../common/FormTextarea'
import FormCheckbox from '../../../common/FormCheckbox'

function PopupCreateFAQ({show, closeModal, selectedFAQ, onOk, viewOnly = false}) {
  const [form, setForm] = useState({...DEFAULT_FAQ})
  const [error, setError] = useState({__checked: false})

  const changeForm = (event) => setForm((oldState) => updateForm(event, oldState))

  useEffect(() => {
    setForm(selectedFAQ || {...DEFAULT_FAQ})
  }, [show, selectedFAQ])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const saveAndClose = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      const res = await postFAQ(selectedFAQ?.id, form)
      if (res.ok) {
        toast(res.message, {type: 'success'})
        closeModal()
        onOk()
      }
    }
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>FAQs</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12} md={9}>
              <FormInput
                disabled={viewOnly}
                label="Question"
                error={error.question}
                required
                id="question"
                value={form.question}
                onChange={changeForm}
                maxLength={255}
              />
            </CCol>
            <CCol xs={12} md={3}>
              <FormCheckbox
                disabled={viewOnly}
                label="Active"
                id="status"
                checkValue="active"
                uncheckValue="inactive"
                checked={form.status === 'active'}
                onChange={updateForm}
              />
            </CCol>
            <CCol xs={12} md={12}>
              <FormTextarea
                disabled={viewOnly}
                label="Answer"
                required
                id="answer"
                value={form.answer}
                onChange={changeForm}
                maxLength={99999}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          {viewOnly ? (
            <CButton color="primary" onClick={closeModal}>
              CLOSE
            </CButton>
          ) : (
            <CButton color="primary" onClick={saveAndClose}>
              SAVE & CLOSE
            </CButton>
          )}
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupCreateFAQ
