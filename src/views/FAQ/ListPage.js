import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CRow, CTooltip} from '@coreui/react'
import {changeFAQStatus, getPagingFAQ} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import PopupCreateFAQ from './components/PopupCreateFAQ'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'

const fields = [{key: 'id', label: '#'}, 'question', 'status', 'option']

function ListPage() {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [selectedFAQ, setSelectedFAQ] = useState(null)
  const [showModalCreate, setShowModalCreate] = useState(false)
  const [viewModal, setViewModal] = useState(false)

  const handleGetData = async () => {
    const res = await getPagingFAQ(currentPage, itemPerPage, showInactive)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showInactive])

  const handleUpdateStatus = (item, value) => changeFAQStatus(item.id, value ? 'active' : 'inactive')

  const clickUpdate = (item) => {
    setShowModalCreate(true)
    setViewModal(false)
    setSelectedFAQ(item)
  }

  const clickCreate = () => {
    setShowModalCreate(true)
    setViewModal(false)
    setSelectedFAQ(null)
  }

  function clickShow(item) {
    setShowModalCreate(true)
    setViewModal(true)
    setSelectedFAQ(item)
  }

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
    setShowInactive(params.showInactive)
  }

  return (
    <>
      <CCard>
        <CCardHeader>
          <TableFilter
            params={{itemPerPage, showInactive}}
            title="FAQs"
            total={data.total}
            onClickCreate={clickCreate}
            onParamChange={handleChangeParam}
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fields}
            size={'sm'}
            onPageChange={setCurrentPage}
            onRowClick={(item) => {
              clickShow(item)
            }}
            scopedSlots={{
              status: (item) => (
                <td>
                  <StatusToggle
                    size={'sm'}
                    active={item.status === 'active'}
                    onChange={(value) => handleUpdateStatus(item, value)}
                  />
                </td>
              ),
              option: (item) => (
                <td>
                  <CButton onClick={() => clickShow(item)} className="p-0 pr-2">
                    <CTooltip content="Show">
                      <CIcon content={freeSet.cilFindInPage} />
                    </CTooltip>
                  </CButton>
                  <CButton onClick={() => clickUpdate(item)} className="p-0 pr-2">
                    <CTooltip content="Edit">
                      <CIcon content={freeSet.cilPen} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>

      <PopupCreateFAQ
        show={showModalCreate}
        closeModal={() => setShowModalCreate(false)}
        selectedFAQ={selectedFAQ}
        viewOnly={viewModal}
        onOk={handleGetData}
      />
    </>
  )
}

export default ListPage
