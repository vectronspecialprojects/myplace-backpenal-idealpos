import React from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import SystemNotificationForm from './components/SystemNotificationForm'

function CreatePage() {
  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Create System Notification</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <SystemNotificationForm />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
