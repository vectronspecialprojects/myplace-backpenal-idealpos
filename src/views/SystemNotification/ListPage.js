import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CRow, CTooltip} from '@coreui/react'
import {deleteSystemNotification, getPagingSystemNotification} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import {setModal} from '../../reusable/Modal'
import ROUTES from '../../constants/routes'
import TableFilter from '../../common/TableFilter'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#', sorder: false},
  'title',
  {key: 'payload', label: 'Page Redirect'},
  {key: 'option', label: '', sorder: false, filter: false},
]

function ListPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [searchText, setSearchText] = useState('')
  const [sort, setSort] = useState({})

  const handleGetData = async () => {
    const res = await getPagingSystemNotification(
      currentPage,
      itemPerPage,
      searchText,
      sort.asc === false ? 'desc' : 'asc',
      sort.column,
    )
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, searchText, sort])

  const handleCreate = () => history.push(ROUTES.SYSTEM_NOTIFICATION_CREATE)

  const handleNavigateUpdate = (item) =>
    history.push(ROUTES.SYSTEM_NOTIFICATION_UPDATE.replace(':id', item.id))

  const handleDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected notification (#${item.id})`,
      message: 'Caution: deleting this item is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteSystemNotification(item.id)
        if (res?.status === 'ok') {
          handleGetData(currentPage, itemPerPage, searchText, sort)
          setModal({show: false})
        }
      },
    })
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <CCard>
      <CCardHeader>
        <TableFilter
          params={{searchText, itemPerPage}}
          title="Notifications"
          onClickCreate={handleCreate}
          total={data.total}
          onParamChange={handleChangeParam}
        />
      </CCardHeader>
      <CCardBody>
        <MyTable
          data={data?.data}
          params={{itemPerPage, page: currentPage, total: data?.total}}
          fields={fields}
          onPageChange={setCurrentPage}
          onSort={setSort}
          size={'sm'}
          onRowClick={(item) => {
            handleNavigateUpdate(item)
          }}
          scopedSlots={{
            option: (item) => (
              <td>
                <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                  <CTooltip content="Edit">
                    <CIcon content={freeSet.cilPen} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => handleDelete(item)} className="p-0 pr-2">
                  <CTooltip content="Delete">
                    <CIcon content={freeSet.cilTrash} />
                  </CTooltip>
                </CButton>
              </td>
            ),
            payload: (item) => {
              let data = {}
              try {
                data = JSON.parse(item.payload)
              } catch (e) {}
              return (
                <td>
                  <span>{data?.page}</span>
                </td>
              )
            },
          }}
        />
      </CCardBody>
    </CCard>
  )
}

export default ListPage
