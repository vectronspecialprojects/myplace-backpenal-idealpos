import React, {useMemo} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import SystemNotificationForm from './components/SystemNotificationForm'

function UpdatePage({location}) {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])
  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Update System Notification</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <SystemNotificationForm id={id} isEdit />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default UpdatePage
