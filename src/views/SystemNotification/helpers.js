const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.title.length === 0) error.title = 'The title is required'
    if (form.type.length === 0) error.type = 'The type is required'
    if (form.message.length === 0) error.message = 'The message is required'
    if (form.action.length === 0) error.action = 'The action is required'
    if (!form.payload) error.payload = 'The page is required'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
