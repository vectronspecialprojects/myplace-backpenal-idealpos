import {CButton, CCol, CLabel, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'
import {
  DEFAULT_SYSTEM_NOTIFICATION,
  SYSTEM_NOTIFICATION_ACTION,
  SYSTEM_NOTIFICATION_LETTER,
  SYSTEM_NOTIFICATION_PAGE,
  SYSTEM_NOTIFICATION_TYPE,
} from '../constant'
import FormInput from '../../../common/FormInput'
import helpers from '../helpers'
import {updateForm} from '../../../helpers'
import {getSystemNotification, postSystemNotification} from '../../../utilities/ApiManage'
import ROUTES from '../../../constants/routes'
import FormSelect from '../../../common/FormSelect'
import FormTextarea from '../../../common/FormTextarea'

const SystemNotificationForm = ({id}) => {
  const history = useHistory()
  const [form, setForm] = useState({...DEFAULT_SYSTEM_NOTIFICATION})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getSystemNotification(id)
        if (!res.ok) throw new Error(res.message)
        const data = {...form, ...res.data}
        setForm(data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [id])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const onChange = (event) => setForm((oldState) => updateForm(event, oldState))

  const clickSave = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      const res = await postSystemNotification(id , form)
      if (res.ok) {
        history.push(ROUTES.SYSTEM_NOTIFICATION)
      }
    }
  }

  return (
    <>
      <CRow>
        <CCol xs={12} md={6}>
          <CRow>
            <CCol xs={12}>
              <FormInput
                label="Title"
                required
                id="title"
                value={form.title}
                error={error.title}
                onChange={onChange}
              />
            </CCol>
            <CCol xs={12}>
              <FormTextarea
                label="Message"
                required
                maxLength={9999}
                id="message"
                value={form.message}
                onChange={onChange}
                error={error.message}
              />
            </CCol>
            <CCol xs={12}>
              <FormSelect
                label="Page"
                required
                id="payload"
                value={form.payload}
                getValue={(v) => `${v.page}${v.react}`}
                onChange={onChange}
                error={error.payload}
                options={SYSTEM_NOTIFICATION_PAGE}
              />
            </CCol>
          </CRow>
        </CCol>
        <CCol xs={12} md={6}>
          <CLabel style={{whiteSpace: 'pre-wrap'}}>{SYSTEM_NOTIFICATION_LETTER}</CLabel>
        </CCol>
      </CRow>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        SAVE AND EXIT
      </CButton>
    </>
  )
}

export default SystemNotificationForm
