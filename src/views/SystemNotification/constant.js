export const DEFAULT_SYSTEM_NOTIFICATION = {
  action: 'redirect',
  message: '',
  title: '',
  type: 'alert',
}

export const SYSTEM_NOTIFICATION_TYPE = [
  {value: 'alert', label: 'Alert'},
  {value: 'success', label: 'Success'},
  {value: 'error', label: 'Error'},
  {value: 'marketing', label: 'Marketing'},
]
export const SYSTEM_NOTIFICATION_ACTION = [{value: 'redirect', label: 'Set link to app page'}]
export const SYSTEM_NOTIFICATION_PAGE = [
  {page: 'Home', id: false, react: 'tabMenu1'},
  {page: "What's On", id: false, react: 'tabMenu2'},
  {page: 'Order Online', id: false, react: 'tabMenu3'},
  {page: 'Stamp Card', id: false, react: 'tabMenu4'},
  {page: 'Website', id: false, react: 'tabMenu5'},
  {page: 'Saved', id: false, react: 'homeMenu1'},
  {page: 'Feedback', id: false, react: 'homeMenu2'},
  {page: 'Offers', id: false, react: 'homeMenu3'},
  {page: 'Vouchers', id: false, react: 'homeMenu4'},
  {page: 'Order Now', id: false, react: 'homeMenu5'},
  {page: 'Stamp Cards', id: false, react: 'homeMenu6'},
  {page: 'About', id: false, react: 'sideMenu1'},
  {page: 'Legal Notices', id: false, react: 'sideMenu2'},
  {page: 'Vouchers', id: false, react: 'sideMenu3'},
  {page: 'Locations', id: false, react: 'sideMenu4'},
  {page: 'FAQ', id: false, react: 'sideMenu5'},
  {page: 'Gift Certificates', id: false, react: 'sideMenu6'},
  {page: 'Surveys', id: false, react: 'sideMenu7'},
  {page: "What's On", id: false, react: 'sideMenu8'},
  {page: 'Ticket', id: false, react: 'sideMenu9'},
  {page: 'Our Team', id: false, react: 'sideMenu10'},
  {page: 'Refer a Friend', id: false, react: 'sideMenu11'},
].map((_) => ({value: _, label: _.page}))

export const SYSTEM_NOTIFICATION_LETTER = `
Steps:
1. Set the notification title on the title box, e.g: PROMOTION or Check our new event!
2. Put your message inside the message box.
3. (if there is) Select the page that you want to go after you click that notification.
- - - - - - - - - - - - - - - - -
Steps to send:
1. Go to member section.
2. if you want to send to all members, click broadcast icon on toolbar, choose a notification and send it.
3. if you want to send a specific member, find a member and click broadcast icon next to edit button.
- - - - - - - - - - - - - - - - -
System Notification (S.N.) V.S Push Notification (P.N.)
1. S.N. will store in DB (100% delivered), P.N. does not.
2. P.N. only contains 'message', whereas S.N. can open page.
3. S.N does NOT have 'native' notification.
`
