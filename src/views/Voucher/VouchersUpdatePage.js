import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCol, CRow} from '@coreui/react'
import {DEFAULT_CREATE_VOUCHER} from './constant'
import {updateForm} from '../../helpers'
import helpers from './helpers'
import Review from './components/Review'
import {getProduct, updateVouchers} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import UploadImageCrop from '../../common/UploadImageCrop'
import Image from '../../common/image'
import {useSelector} from 'react-redux'

function VouchersUpdatePage({location, history}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])
  const [form, setForm] = useState({...DEFAULT_CREATE_VOUCHER})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  useEffect(() => {
    ;(async () => {
      const res = await getProduct(id)
      if (res.ok)
        setForm({
          ...DEFAULT_CREATE_VOUCHER,
          ...res.data,
          payload: res.data.payload ? JSON.parse(res.data.payload) : DEFAULT_CREATE_VOUCHER.payload,
        })
    })()
  }, [])

  const changeForm = (event) => setForm((oldState) => updateForm(event, oldState))

  const saveVoucher = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      const res = await updateVouchers(id, form)
      if (res.ok) {
        history.push({pathname: ROUTES.VOUCHER})
      }
    }
  }

  return (
    <CCard>
      <CCardBody>
        <CRow>
          <CCol xs={12} md={8}>
            <Review disabled={!bepozVersion} form={form} updateForm={changeForm} error={error} />
          </CCol>
          <CCol xs={12} md={4}>
            <UploadImageCrop aspect={1} id="image" onConfirm={changeForm} className="w-100">
              <Image blob={form?.image} className="w-100" />
            </UploadImageCrop>
          </CCol>
        </CRow>
        <CButton
          disabled={!bepozVersion}
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={saveVoucher}>
          UPDATE AND EXIT
        </CButton>
      </CCardBody>
    </CCard>
  )
}

export default VouchersUpdatePage
