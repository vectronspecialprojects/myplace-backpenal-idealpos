const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.name.length === 0) error.name = 'The name is required'
    if (form.desc_short.length === 0) error.desc_short = 'The short description is required'
    if (form.unit_price < 0) error.unit_price = 'The unit price must be greater than or equal to 0'
    if (form.point_price < 0) error.point_price = 'The point price must be greater than or equal to 0'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
