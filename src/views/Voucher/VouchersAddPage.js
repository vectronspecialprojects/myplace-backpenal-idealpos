import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CLabel,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTabs,
} from '@coreui/react'
import UploadImageCrop from '../../common/UploadImageCrop'
import {DEFAULT_CREATE_VOUCHER} from './constant'
import helpers from './helpers'
import MainInformation from './components/MainInformation'
import PriceReward from './components/PriceReward'
import BepozVoucher from './components/BepozVoucher'
import Review from './components/Review'
import Image from '../../common/image'
import {createVouchers} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import {updateForm} from '../../helpers'
import {useSelector} from 'react-redux'

function VouchersAddPage({history}) {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const [step, setStep] = useState(0)
  const [form, setForm] = useState({...DEFAULT_CREATE_VOUCHER})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const changeForm = (event) => setForm((oldState) => updateForm(event, oldState))

  const isReview = true

  const clickNext = async () => {
    const validator = helpers.validateForm(form)
    setError(validator.error)
    if (validator.valid) {
      if (isReview) {
        const res = await createVouchers(form)
        if (res.ok) {
          history.push({pathname: ROUTES.VOUCHER})
        }
      } else {
        setStep(Math.min(step + 1, 3))
      }
    }
  }

  return (
    <CCard>
      <CCardBody>
        <CRow>
          <CCol xs={12} md={8}>
            <Review disabled={!bepozVersion} form={form} error={error} updateForm={changeForm} />
          </CCol>
          <CCol xs={12} md={4}>
            <span>Image</span>
            <UploadImageCrop aspect={1} id="image" onConfirm={changeForm} className="w-100">
              <Image blob={form?.image} className="w-100" />
            </UploadImageCrop>
          </CCol>
          <CCol xs={12}>
            <CButton
              disabled={!bepozVersion}
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={clickNext}>
              {isReview ? 'SAVE AND EXIT' : 'NEXT STEP'}
            </CButton>
          </CCol>
        </CRow>
      </CCardBody>
    </CCard>
  )
}

export default VouchersAddPage
