import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CRow, CTooltip} from '@coreui/react'
import {
  changeProductStatus,
  duplicateProduct,
  getPagingVouchers,
  postVoucherSetup,
} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TableFilter from '../../common/TableFilter'
import ROUTES from '../../constants/routes'
import MyTable from '../../common/Table'
import StatusToggle from '../../common/StatusToggle'
import {setModal} from '../../reusable/Modal'

const fields = [
  {key: 'id', label: '#'},
  'name',
  'status',
  {key: 'unit_price', label: 'Price'},
  {key: 'point_price', label: 'Point'},
  'option',
]

function VouchersPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [searchText, setSearchText] = useState('')
  const [sort, setSort] = useState({column: 'id', asc: false})

  const handleGetData = async () => {
    const res = await getPagingVouchers(
      currentPage,
      itemPerPage,
      searchText,
      showInactive,
      sort?.asc ? 'asc' : 'desc',
      sort.column,
    )
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, searchText, sort, showInactive])

  async function handleUpdateStatus(id, value) {
    await changeProductStatus(id, value ? 'active' : 'inactive')
  }

  function handleDuplicate(item) {
    setModal({
      title: `Would you like to duplicate the selected voucher (#${item.id})`,
      message: 'This action may take a few minutes.',
      primaryBtnClick: async () => {
        const res = await duplicateProduct(item.id)
        if (res?.data?.id) {
          history.push({pathname: ROUTES.VOUCHER_UPDATE.replace(':id', res.data.id)})
          setModal({show: false})
        }
      },
    })
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <CCard>
      <CCardHeader>
        <TableFilter
          params={{searchText, showInactive, itemPerPage}}
          title="Vouchers"
          onClickCreate={() => history.push(ROUTES.VOUCHER_CREATE)}
          total={data.total}
          onParamChange={handleChangeParam}
          actions={
            <>
              <CButton onClick={() => postVoucherSetup()}>
                <CTooltip content="Refresh Voucher Setup">
                  <CIcon content={freeSet.cilSettings} style={{width: 25, height: 25}} />
                </CTooltip>
              </CButton>
            </>
          }
        />
      </CCardHeader>
      <CCardBody>
        <MyTable
          data={data?.data}
          params={{itemPerPage, page: currentPage, total: data?.total}}
          fields={fields}
          onPageChange={setCurrentPage}
          onSort={setSort}
          size={'sm'}
          onRowClick={(item) => {
            history.push(ROUTES.VOUCHER_UPDATE.replace(':id', item.id))
          }}
          scopedSlots={{
            status: (item) => (
              <td>
                <StatusToggle
                  active={item.status === 'active'}
                  onChange={(value) => handleUpdateStatus(item.id, value)}
                  size={'sm'}
                />
              </td>
            ),
            option: (item) => (
              <td>
                <CButton onClick={() => history.push(ROUTES.VOUCHER_UPDATE.replace(':id', item.id))} className="p-0 pr-2">
                  <CTooltip content="Edit">
                    <CIcon content={freeSet.cilPencil} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => {}} className="p-0 pr-2">
                  <CTooltip content="Duplicate">
                    <CIcon content={freeSet.cilCopy} onClick={() => handleDuplicate(item)} />
                  </CTooltip>
                </CButton>
              </td>
            ),
          }}
        />
      </CCardBody>
    </CCard>
  )
}

export default VouchersPage
