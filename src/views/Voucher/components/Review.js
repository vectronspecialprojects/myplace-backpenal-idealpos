import React from 'react'
import MainInformation from './MainInformation'
import PriceReward from './PriceReward'
import BepozVoucher from './BepozVoucher'
import {CCol, CLabel, CRow} from '@coreui/react'
import NotificationSetting from '../../../common/NotificationSettting'

const Review = ({form, error, updateForm, disabled}) => {
  return (
    <>
      <BepozVoucher disabled={disabled} form={form} error={error} updateForm={updateForm} />
      <MainInformation form={form} error={error} updateForm={updateForm} />
      <PriceReward form={form} error={error} updateForm={updateForm} />
      <CLabel className="h6 font-weight-bold d-block mt-2">Notification</CLabel>
      <NotificationSetting
        disabled={disabled}
        setting={form.payload?.[0]}
        id="payload.0"
        onUpdate={updateForm}
      />
    </>
  )
}

export default Review
