import {CCol, CFormGroup, CInput, CLabel, CRow} from '@coreui/react'
import React from 'react'
import FormInput from '../../../common/FormInput'
import FormCheckbox from '../../../common/FormCheckbox'

const PriceReward = ({form, error, updateForm}) => {
  return (
    <>
      <CRow>
        <CCol xs={12} md={4}>
          <FormInput
            label="Price($AUD)"
            id="unit_price"
            value={form.unit_price}
            onChange={updateForm}
            type="number"
            error={error.unit_price}
            required
          />
        </CCol>
        <CCol xs={12} md={4}>
          <FormInput
            label="Point Price"
            id="point_price"
            value={form.point_price}
            onChange={updateForm}
            type="number"
            error={error.point_price}
            required
          />
        </CCol>

        <CCol xs={12} md={4}>
          <FormCheckbox
            label="Free"
            id="is_free"
            checked={form.is_free === 1}
            checkValue={1}
            uncheckValue={0}
            onChange={updateForm}
          />
        </CCol>

        <CCol xs={12}>
          <FormCheckbox
            id="system_point_ratio"
            label="To calculate the reward. Do you want to use System's Point Ratio?"
            checked={form.system_point_ratio === 1}
            checkValue={1}
            uncheckValue={0}
            onChange={updateForm}
          />
        </CCol>

        <CCol xs="6" md="12">
          <CFormGroup>
            <CLabel htmlFor="point_member_get">
              How many points will a member get after they buy your item?
            </CLabel>
            <CInput
              id="point_get"
              disabled={form.system_point_ratio}
              value={form.system_point_ratio ? form.unit_price : form.point_get}
              onChange={updateForm}
            />
            <CLabel>
              {form.system_point_ratio
                ? "Use system's point ratio every AUD$ 1,00 spent = 1pts."
                : `every AUD$${parseFloat((+form.unit_price).toFixed(2)).toLocaleString()} spent = ${
                    form.point_get
                  } pts`}
            </CLabel>
          </CFormGroup>
        </CCol>
      </CRow>
    </>
  )
}

export default PriceReward
