import {CCol, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getVoucherSetup} from '../../../utilities/ApiManage'
import FormSelect from '../../../common/FormSelect'
import FormInput from '../../../common/FormInput'

const BepozVoucher = ({form, error, updateForm, disabled = false}) => {
  const [voucherSetups, setVoucherSetups] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await getVoucherSetup()
      if (res.ok) setVoucherSetups(res.data.map((_) => ({value: _.id, label: _.name})))
    })()
  }, [])

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <FormSelect
            disabled={disabled}
            label="Please select voucher"
            options={voucherSetups}
            id="bepoz_voucher_setup_id"
            value={form.bepoz_voucher_setup_id}
            onChange={updateForm}
            className="mb-2"
          />
        </CCol>
      </CRow>
    </>
  )
}

export default BepozVoucher
