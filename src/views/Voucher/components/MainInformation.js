import {CCol, CFormGroup, CLabel, CRow} from '@coreui/react'
import React from 'react'
import FormInput from '../../../common/FormInput'
import FormSelect from '../../../common/FormSelect'
import {VOUCHER_CATEGORIES} from '../constant'
import FormCheckbox from '../../../common/FormCheckbox'
import NotificationSetting from '../../../common/NotificationSettting'

const MainInformation = ({form, error, updateForm}) => {
  return (
    <>
      <CRow>
        <CCol xs={12}>
          <FormInput
            label="Voucher name"
            id="name"
            value={form?.name}
            onChange={updateForm}
            error={error?.name}
            maxLength={255}
            required
          />
        </CCol>
        <CCol xs={12}>
          <FormInput
            label="Short Description"
            id="desc_short"
            value={form?.desc_short}
            onChange={updateForm}
            error={error?.desc_short}
            maxLength={255}
            required
            desc="Summarized information displayed in the app"
          />
        </CCol>
        <CCol xs={12}>
          <CFormGroup>
            <FormCheckbox
              label="Activate this item now?"
              id="status"
              checked={form?.status === 'active'}
              checkValue="active"
              uncheckValue="inactive"
              onChange={updateForm}
            />
          </CFormGroup>
        </CCol>
      </CRow>
    </>
  )
}

export default MainInformation
