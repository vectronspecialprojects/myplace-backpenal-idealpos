import {DEFAULT_NOTIFY_CONTENT} from '../Listing/constant'

export const VOUCHER_CATEGORIES = [
  {label: 'Voucher', value: 'voucher'},
  {label: 'F&B Voucher', value: 'fb'},
]

export const DEFAULT_CREATE_VOUCHER = {
  image: 'http://placehold.it/500x500',
  system_point_ratio: 1,
  bepoz_voucher_setup_id: 0,
  point_get: 0,
  unit_price: 0,
  point_price: 0,
  is_hidden: 0,
  product_type_id: 'voucher',
  gift_value: 0,
  category: 'voucher',
  name: '',
  desc_short: '',
  is_free: 0,
  status: 'inactive',
  payload: [{triggerNotif: false, notifType: 'push', notifContent: DEFAULT_NOTIFY_CONTENT, notifTier: []}],
}
