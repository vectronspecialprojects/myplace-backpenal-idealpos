import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CLabel, CRow, CTooltip} from '@coreui/react'
import {
  getPagingMember,
  postResendConfirmation,
  postResendConfirmationMember,
} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import ROUTES from '../../constants/routes'
import moment from 'moment'
import {cilBullhorn, cilCheckCircle, cilSend} from '@coreui/icons'
import {toast} from 'react-toastify'
import PopupSendNotification from './components/PopupSendNotification'
import TableFilter from '../../common/TableFilter'
import StatusToggle from '../../common/StatusToggle'
import MyTable from '../../common/Table'
import {useSelector} from 'react-redux'

// const fields = [
//   {key: 'id', label: '#'},
//   {key: 'first_name', label: 'Name'},
//   {key: 'status'},
//   {key: 'tier'},
//   {key: 'dob', label: 'DOB'},
//   {key: 'mobile', label: 'phone'},
//   {key: 'points'},
//   {key: 'balance'},
//   {key: 'email'},
//   {key: 'option', sorder: false, label: ''},
// ]

function ListPage({history}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(0)
  const [searchText, setSearchText] = useState('')
  const [sort, setSort] = useState({})
  const [showSendNotification, setShowNotification] = useState(0)
  const [selectedMember, setSelectedMember] = useState(null)
  const setting = useSelector((state) => state.app.appSettings)

  const fieldBySetting = useMemo(() => {
    let data = JSON.parse(setting?.view_member?.extended_value)
    data = data.filter((item) => item.visible)
    return [...data, {key: 'option', sorder: false, label: ''}]
  }, [setting])

  async function handleGetData() {
    const res = await getPagingMember(
      currentPage,
      itemPerPage,
      showInactive,
      searchText,
      sort.asc === false ? 'desc' : 'asc',
      sort.column,
    )
    if (res.ok) {
      res.data.data = res.data.data.map((_) => ({
        ..._,
        tier: _.tiers?.[0]?.name || '',
        mobile: _.user?.mobile,
        email: _.user?.email,
        phone: !!_?.phone ? _?.phone : '',
      }))
      setData(res.data)
    }
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showInactive, searchText, sort])

  const handleCreate = async () => history.push({pathname: ROUTES.MEMBER_CREATE})

  const switchSendNotification = (show, id) => () => {
    setShowNotification(show || 0)
    setSelectedMember(id || null)
  }

  const handleResendConfirmation = async () => {
    const res = await postResendConfirmation()
    if (res.ok) toast('Resending Confirmation is successfully', {type: 'success'})
  }

  const handleResendConfirmationMember = async (item) => {
    const res = await postResendConfirmationMember({member_id: item.id})
    if (res.ok) toast('Resending Confirmation for this member is successfully', {type: 'success'})
  }

  const handleNavigateUpdate = (item) =>
    history.push({pathname: ROUTES.MEMBER_UPDATE.replace(':id', item.id)})

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setShowInactive(params.showInactive)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <>
      <CCard>
        <CCardHeader>
          <TableFilter
            params={{searchText, showInactive, itemPerPage}}
            title="Members"
            onClickCreate={null}
            total={data.total}
            size={'small'}
            onParamChange={handleChangeParam}
            actions={
              <>
                <CButton onClick={switchSendNotification(1)}>
                  <CTooltip content="Broadcast Push Notification">
                    <CIcon content={freeSet.cilBullhorn} style={{width: 25, height: 25}} />
                  </CTooltip>
                </CButton>
                <CButton onClick={handleResendConfirmation}>
                  <CTooltip content="Resend Confirmation">
                    <CIcon content={freeSet.cilSend} style={{width: 25, height: 25}} />
                  </CTooltip>
                </CButton>
                <CButton onClick={switchSendNotification(2)}>
                  <CTooltip content="Broadcast System Notification">
                    <CIcon style={{width: 25, height: 25}} content={freeSet.cilEnvelopeLetter} />
                  </CTooltip>
                </CButton>
              </>
            }
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fieldBySetting}
            onPageChange={setCurrentPage}
            onSort={setSort}
            size="sm"
            clickableRows={null}
            // onRowClick={(item) => {
            //   handleNavigateUpdate(item)
            // }}
            scopedSlots={{
              name: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      {`${item.first_name || ''} `}{' '}
                      <span className="font-weight-bold m-0">{item.last_name || ''}</span>
                    </CButton>
                  </CRow>
                  {/* <CLabel className="m-0">
                    {`${item.first_name || ''} `}
                    <CLabel className="font-weight-bold m-0">{item.last_name || ''}</CLabel>
                  </CLabel> */}
                </td>
              ),
              status: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      <span style={{color: item.status === 'active' ? 'green' : 'red'}}>
                        {item.status?.toUpperCase()}
                      </span>
                    </CButton>
                  </CRow>
                  {/* <span style={{color: item.status === 'active' ? 'green' : 'red'}}>
                    {item.status?.toUpperCase()}
                  </span> */}
                </td>
              ),
              tier: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      {item.tier}
                    </CButton>
                  </CRow>
                </td>
              ),
              points: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      {item.points}
                    </CButton>
                  </CRow>
                </td>
              ),
              balance: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      {item.balance}
                    </CButton>
                  </CRow>
                </td>
              ),
              dob: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      <span>{item.dob ? moment(item.dob).format('MMMM Do') : 'n.a'}</span>
                    </CButton>
                  </CRow>
                  {/* <span>{item.dob ? moment(item.dob).format('MMMM Do') : 'n.a'}</span> */}
                </td>
              ),
              mobile: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      <span>{item.mobile || 'n.a'}</span>
                    </CButton>
                  </CRow>
                  {/* <span>{item.mobile || 'n.a'}</span> */}
                </td>
              ),
              email: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      <span>{item.email || ''}</span>
                      {item.user?.email_confirmation === 1 && (
                        <CIcon content={cilCheckCircle} className="text-success ml-2" />
                      )}
                    </CButton>
                  </CRow>

                  {/* <span>{item.email || ''}</span>
                  {item.user?.email_confirmation === 1 && (
                    <CIcon content={cilCheckCircle} className="text-success ml-2" />
                  )} */}
                </td>
              ),
              date_expiry: (item) => (
                <td>
                  <CRow>
                    <CButton
                      style={{padding: 0, marginTop: -1, marginLeft: 15}}
                      onClick={() => {
                        handleNavigateUpdate(item)
                      }}>
                      <span>{item.date_expiry || ''}</span>
                    </CButton>
                  </CRow>
                </td>
              ),
              option: (item) => (
                <td>
                  <CButton onClick={switchSendNotification(1, item.id)} className="p-0 pr-2">
                    <CTooltip content="Send Push Notification">
                      <CIcon content={cilBullhorn} />
                    </CTooltip>
                  </CButton>
                  <CButton onClick={() => handleResendConfirmationMember(item)} className="p-0 pr-2">
                    <CTooltip content="Resend Confirmation For This Member">
                      <CIcon content={cilSend} />
                    </CTooltip>
                  </CButton>
                  <CButton onClick={switchSendNotification(2, item.id)} className="p-0 pr-2">
                    <CTooltip content="Send System Notification">
                      <CIcon name="cil-envelope-letter" />
                    </CTooltip>
                  </CButton>
                  <CButton onClick={() => handleNavigateUpdate(item)} className="p-0 pr-2">
                    <CTooltip content="Edit">
                      <CIcon content={freeSet.cilPen} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>

      <PopupSendNotification
        show={!!showSendNotification}
        onClose={switchSendNotification()}
        isSystem={showSendNotification === 2}
        memberId={selectedMember}
      />
    </>
  )
}

export default ListPage
