import React, {useEffect, useMemo, useState} from 'react'
import {CAlert, CCard, CCardBody, CCardHeader, CCol, CFormGroup, CRow} from '@coreui/react'
import helpers from './helpers'
import MemberForm from './components/MemberForm'
import {DEFAULT_MEMBER} from './constant'
import {updateForm} from '../../helpers'
import {getMember} from '../../utilities/ApiManage'
import MemberHistory from './components/MemberHistory'
import MemberClaimedVouchers from './components/MemberClaimedVouchers'

const UpdatePage = ({location}) => {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])
  const [form, setForm] = useState({...DEFAULT_MEMBER})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    ;(async () => {
      try {
        const res = await getMember(id)
        if (!res.ok) throw new Error(res.message)
        const data = {...form, ...res.data}
        setForm(data)
      } catch (e) {
        console.log(e)
      }
    })()
  }, [])

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const onChange = (event) => setForm((oldState) => updateForm(event, oldState))

  return (
    <CCol xs="12">
      <CCard>
        <CCardHeader>Edit Member</CCardHeader>
      </CCard>

      <CCard className="create-listing-container">
        <CCardBody>
          {/* <CAlert color="info">This will sync with your bepoz</CAlert> */}
          <div style={{flexDirection: 'row', display: 'flex'}}>
            <MemberForm form={form} onChange={onChange} error={error} viewOnly />
            <img style={{width: 200, height: 200, marginLeft: 20}} src={form.profile_img} />
          </div>

          {/* <CFormGroup>
            <h4>Addresses</h4>
            <CAlert color="info">No Address at the moment.</CAlert>
          </CFormGroup> */}

          <CFormGroup>
            <MemberHistory id={id} />
          </CFormGroup>

          <CFormGroup>
            <MemberClaimedVouchers claimedVouchers={form.claimed_vouchers} />
          </CFormGroup>

          {/* <CFormGroup>
            <h4>Card Stamps</h4>
            <CAlert color="info">No Card Stamps at the moment.</CAlert>
          </CFormGroup> */}

        </CCardBody>
      </CCard>
    </CCol>
  )
}

export default UpdatePage
