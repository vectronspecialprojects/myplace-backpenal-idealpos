import React, {useEffect, useState} from 'react'
import {CAlert, CButton, CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import helpers from './helpers'
import {updateForm} from '../../helpers'
import {DEFAULT_MEMBER} from './constant'
import {getCheckEmail, postMember} from '../../utilities/ApiManage'
import {toast} from 'react-toastify'
import ROUTES from '../../constants/routes'
import MemberForm from './components/MemberForm'

function CreatePage({history, location}) {
  const [form, setForm] = useState({...DEFAULT_MEMBER})
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  const onChange = (event) => setForm((oldState) => updateForm(event, oldState))

  const clickSave = async () => {
    const validator = helpers.validateForm(form)
    if (validator.valid) {
      const checkEmail = await getCheckEmail(form.email)
      if (checkEmail.status === 'error') {
        toast(checkEmail.message, {type: 'error'})
      } else {
        const res = await postMember('', form)
        if (res.ok) {
          history.push({pathname: ROUTES.MEMBER})
        }
      }
    }
    setError(validator.error)
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Add Member</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <CAlert color="info">This will sync with your bepoz</CAlert>
            <MemberForm form={form} onChange={onChange} error={error} />
            <CButton
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={clickSave}>
              SAVE AND EXIT
            </CButton>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
