import {validateEmail, validatePhoneNumber} from '../../helpers/validation'

const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.first_name.length === 0) error.first_name = 'The first name is required'
    if (form.last_name.length === 0) error.last_name = 'The last name is required'
    if (form.email.length === 0) error.email = 'The email is required'
    if (validateEmail(form.email)) error.email = 'The email is not valid'
    if (form.mobile.length === 0) error.mobile = 'The mobile is required'
    if (validatePhoneNumber(form.mobile)) error.mobile = 'The mobile is not valid'
    if (!form.tier_selected) error.tier_selected = 'The Tier is required'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
