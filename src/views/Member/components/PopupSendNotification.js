import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import {CCard} from '@coreui/react/es'
import FormCheckbox from '../../../common/FormCheckbox'
import {useSelector} from 'react-redux'
import FormInput from '../../../common/FormInput'
import {
  postBroadcastGroupNotification,
  postPushNotification,
  postSendBroadcastSystemNotification,
  postSendSystemNotification,
} from '../../../utilities/ApiManage'
import {toast} from 'react-toastify'
import SystemNotificationSelect from '../../../common/SystemNotificationSelect'

function PopupSendNotification({show, onClose, isSystem = false, memberId = null}) {
  const tiers = useSelector((state) => state?.app?.tier || [])
  const [notificationId, setNotificationId] = useState(null)
  const [tierId, setTierId] = useState([])
  const [message, setMessage] = useState('')
  const [error, setError] = useState({__checked: false})

  useEffect(() => {
    setTierId([])
    setMessage('')
    setNotificationId(null)
    setError({__checked: false})
  }, [show])

  const validateForm = () => {
    const error = {__checked: true}
    if (isSystem) {
      if (!notificationId) error.notificationId = 'The notification is required'
    } else if (message?.length === 0) error.message = 'The message is required'
    setError(error)
    return Object.keys(error).length === 1
  }

  useEffect(() => {
    if (error?.__checked) validateForm()
  }, [message, notificationId])

  const onChangeTierId =
    ({id} = {}) =>
    () => {
      if (id) {
        if (tierId.includes(id)) setTierId(tierId.filter((_) => _ !== id))
        else setTierId([...tierId, id])
      } else {
        if (tierId.length === tiers.length) setTierId([])
        else setTierId(tiers.map((_) => _.id))
      }
    }

  const submit = async () => {
    const isValid = validateForm()
    if (isValid) {
      if (isSystem) {
        if (memberId) {
          const res = await postSendSystemNotification({
            notification_id: +notificationId,
            member_id: memberId,
          })
          if (res.status === 'ok') {
            onClose()
            toast('Sending notification is successfully', {type: 'success'})
          }
        } else {
          const res = await postSendBroadcastSystemNotification({
            notification_id: +notificationId,
            tier_id: tierId,
            venues: tiers,
          })
          if (res.status === 'ok') {
            onClose()
            toast('Sending group notification is successfully', {type: 'success'})
          }
        }
      } else {
        if (memberId) {
          const res = await postPushNotification({message, id: memberId})
          if (res.status === 'ok') {
            onClose()
            toast('Sending notification is successfully', {type: 'success'})
          }
        } else {
          const res = await postBroadcastGroupNotification({
            message,
            tier_id: tierId,
            venues: tiers,
          })
          if (res.status === 'ok') {
            onClose()
            toast('Sending broadcast notification is successfully', {type: 'success'})
          }
        }
      }
    }
  }

  return (
    <>
      <CModal show={show} onClose={onClose}>
        <CModalHeader closeButton>
          <CModalTitle>
            {memberId
              ? 'Send Notification'
              : isSystem
              ? 'Send Notification to All'
              : 'Send Push Notification to All'}
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12}>
              {isSystem ? (
                <SystemNotificationSelect
                  label="Notification"
                  required
                  value={notificationId}
                  error={error.notificationId}
                  placeholder="-Please select-"
                  onChange={(e) => setNotificationId(e.target.value)}
                />
              ) : (
                <FormInput
                  label="Message"
                  required
                  error={error.message}
                  value={message}
                  maxLength={255}
                  onChange={(e) => setMessage(e.target.value)}
                />
              )}
            </CCol>

            {!memberId && (
              <CCol xs={12}>
                <CCard className="mt-3">
                  <CCardHeader>
                    <FormCheckbox
                      id="all-tiers"
                      checked={tierId.length === tiers.length}
                      onChange={onChangeTierId()}
                      label="Target"
                    />
                  </CCardHeader>
                  <CCardBody>
                    <CRow>
                      {tiers.map((_) => (
                        <CCol xs="6" key={_.id}>
                          <CFormGroup>
                            <FormCheckbox
                              id="types"
                              value={_.id}
                              checked={tierId.includes(_.id)}
                              onChange={onChangeTierId(_)}
                              label={_.name}
                            />
                          </CFormGroup>
                        </CCol>
                      ))}
                    </CRow>
                  </CCardBody>
                </CCard>
              </CCol>
            )}
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={submit}>
            SUBMIT
          </CButton>
          <CButton color="secondary" onClick={onClose}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupSendNotification
