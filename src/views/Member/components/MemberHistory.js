import {CAlert, CDataTable, CPagination} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getMemberLog} from '../../../utilities/ApiManage'

const fields = [
  {key: 'id', label: ''},
  {key: 'message', label: 'Message'},
  {key: 'created_at', label: 'Changed At'},
]

const MemberHistory = ({id}) => {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    ;(async () => {
      const res = await getMemberLog(id, currentPage)
      if (res.ok) setData(res.data)
    })()
  }, [currentPage])

  return (
    <>
      <h4>History</h4>

      {data?.data?.length ? (
        <>
          <CDataTable
            items={data?.data || []}
            fields={fields}
            striped
            scopedSlots={{
              id: (item) => (
                <td>
                  <span>{item.id}</span>
                </td>
              ),
            }}
          />
          <CPagination
            activePage={currentPage}
            pages={Math.ceil(data?.total / data.per_page || 10)}
            onActivePageChange={setCurrentPage}
          />
        </>
      ) : (
        <CAlert color="info">No history at the moment</CAlert>
      )}
    </>
  )
}

export default MemberHistory
