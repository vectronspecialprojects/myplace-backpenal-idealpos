import {CCol, CFormGroup, CRow} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import FormDate from '../../../common/FormDate'
import TierSelect from '../../../common/TierSelect'
import React, {useEffect, useMemo, useState} from 'react'
import FormCheckbox from '../../../common/FormCheckbox'
import FormDateTime from '../../../common/FormDateTime'
import {debounce} from '../../../utilities/utils'
import {bepozAccountGet} from '../../../utilities/ApiManage'

const MemberForm = ({form, onChange, error, viewOnly = false}) => {
  const [bepozAccNumber, setAccNumber] = useState(form.bepoz_account_number)
  const [bepozCardNumber, setCardNumber] = useState(form.bepoz_account_card_number)
  const [messages, setMessages] = useState({})

  useEffect(() => {
    setCardNumber(form?.bepoz_account_card_number)
    setAccNumber(form?.bepoz_account_number)
  }, [form])

  const memberTier = useMemo(() => {
    if (!!form?.tiers) return form?.tiers[0]?.name
    return ''
  }, [form])

  async function validAccNumber(text) {
    try {
      setMessages({
        ...messages,
        bepozAccNumber: 'Checking your bepoz',
      })
      const res = await bepozAccountGet(text)
      if (!res.data) {
        setMessages({
          ...messages,
          bepozAccNumber: 'Number is AVAILABLE',
        })
      }
    } catch (e) {}
  }

  async function validCardNumber(text) {
    try {
      setMessages({
        ...messages,
        bepozCardNumber: 'Checking your bepoz',
      })
      const res = await bepozAccountGet(null, text)
      if (!res.data) {
        setMessages({
          ...messages,
          bepozCardNumber: 'Number is AVAILABLE',
        })
      }
    } catch (e) {}
  }

  return (
    <CRow>
      <CCol xs={12} md={4}>
        <FormInput
          id="first_name"
          value={form?.first_name}
          disabled
          label="First Name"
          placeholder="Not Available"
          // id="first_name"
          // value={form?.first_name}
          // onChange={onChange}
          // disabled={form.share_info === 0}
          // error={error.first_name}
          // label="First Name"
          // required
        />
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput id="last_name" value={form?.last_name} disabled label="Last Name" placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput id="email" value={form?.user?.email} label="Email" disabled placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <CFormGroup>
          <FormDate id="birthday" value={form?.dob} label="D.O.B" disabled placeholder="Not Available"/>
        </CFormGroup>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput id="mobile" disabled value={form?.user?.mobile} label="Mobile" placeholder="Not Available"/>
      </CCol>

      {/* <CCol xs={12} md={4}>
        <TierSelect
          id="tier_selected"
          value={form?.tier_selected}
          onUpdate={onChange}
          label="Tier"
          placeholder="Please select"
          required
          error={error.tier_selected}
          disabled={viewOnly}
        />
      </CCol> */}
      <CCol xs={12} md={4}>
        <FormInput label="Tier" disabled value={memberTier} placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput label="Memb. ID" disabled value={form?.id} placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput label="Member Status" disabled value={form?.status} placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput label="Bepoz ID" disabled value={form?.bepoz_account_id} placeholder="Not Available"/>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput
          label="Bepoz Status"
          disabled
          placeholder="Not Available"
          value={form?.bepoz_account_status}
        />
      </CCol>

      <CCol xs={12} md={2}>
        {viewOnly ? (
          <div className="d-flex justify-content-between align-items-center">
            <FormInput label="Balance" disabled placeholder="Not Available" />
            {/* <div className="position-absolute" style={{top: 0, right: 20, zIndex: 99}}>
              <FormCheckbox checked={false} label="Use CALink" disabled />
            </div> */}
          </div>
        ) : (
          <FormInput label="Balance" disabled placeholder="Not Available" />
        )}
      </CCol>
      <CCol xs={12} md={2}>
        {viewOnly ? (
          <div className="d-flex justify-content-between align-items-center">
            <FormInput label="Points" disabled value={form.points} placeholder="Not Available"/>
            {/* <div className="position-absolute" style={{top: 0, right: 20, zIndex: 99}}>
              <FormCheckbox checked={false} label="Use CALink" disabled />
            </div> */}
          </div>
        ) : (
          <FormInput label="Points" disabled value={form.points} placeholder="Not Available"/>
        )}
      </CCol>

      <CCol xs={12} md={4}>
        <FormInput
          label="Bepoz Acc. Number"
          placeholder="Not Available"
          value={bepozAccNumber}
          message={messages.bepozAccNumber}
          disabled
          // onChangeText={(e) => {
          //   debounce(() => {
          //     validAccNumber(e)
          //   })
          //   setAccNumber(e)
          // }}
        />
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput
          label="Bepoz Card Number"
          placeholder="Not Available"
          value={bepozCardNumber}
          message={messages.bepozCardNumber}
          disabled
          // onChangeText={(e) => {
          //   debounce(() => {
          //     validCardNumber(e)
          //   })
          //   setCardNumber(e)
          // }}
        />
      </CCol>

      <CCol xs={12} md={4}>
        <CFormGroup>
          <FormDate label="Exp. Date" disabled placeholder="Not Available" onChange={onChange} />
        </CFormGroup>
      </CCol>
      <CCol xs={12} md={4}>
        <CFormGroup>
          <FormDateTime
            label="Created"
            disabled
            placeholder="Not Available"
            value={form?.created_at}
            showToday={false}
          />
        </CFormGroup>
      </CCol>
      <CCol xs={12} md={4}>
        <CFormGroup>
          <FormDateTime
            label="Modified"
            disabled
            placeholder="Not Available"
            value={form?.updated_at}
            showToday={false}
          />
        </CFormGroup>
      </CCol>
      <CCol xs={12} md={4}>
        <FormInput
          label="Status"
          placeholder="Not Available"
          value={form?.status}
          disabled
        />
      </CCol>
      
    </CRow>
  )
}

export default MemberForm
