import {CAlert, CDataTable} from '@coreui/react'
import React, {useMemo} from 'react'

const fields = [
  {key: 'index', label: ''},
  {key: 'name', label: 'Voucher'},
  {key: 'qty', label: 'Qty'},
]

const MemberClaimedVouchers = ({claimedVouchers = []}) => {
  const data = useMemo(() => {
    return Object.values(
      claimedVouchers.reduce((acc, item) => {
        const voucher = acc[item.voucher_setup.id] || {
          id: item.voucher_setup.id,
          name: item.voucher_setup?.name || '',
          qty: 0,
        }
        voucher.qty++
        return {...acc, [item.voucher_setup.id]: voucher}
      }, {}),
    )
  }, [claimedVouchers])

  return (
    <>
      <h4>Voucher</h4>

      {data?.length ? (
        <CDataTable
          items={data}
          fields={fields}
          striped
          scopedSlots={{
            index: (item, index) => (
              <td>
                <span>{index + 1}</span>
              </td>
            ),
          }}
        />
      ) : (
        <CAlert color="info">No Voucher at the moment.</CAlert>
      )}
    </>
  )
}

export default MemberClaimedVouchers
