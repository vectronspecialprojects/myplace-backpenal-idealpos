import React, {useEffect, useState} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CDataTable, CPagination, CRow} from '@coreui/react'
import moment from 'moment'
import {getGamingLog} from '../../utilities/ApiManage'
import '../../scss/_custom.scss'
import TableFilter from '../../common/TableFilter'
import FormSelect from '../../common/FormSelect'
import FormDate from '../../common/FormDate'

const fields = [
  {key: 'id', label: '#'},
  'time',
  {key: 'request', label: 'Source'},
  {key: 'request_type', label: 'Type'},
  {key: 'response', label: 'Readable error message'},
]

function GamingReport() {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [searchText, setSearchText] = useState('')
  const [startDate, setStartDate] = useState(moment().format('YYYY-MM-DD'))
  const [endDate, setEndDate] = useState(moment().add(1, 'day').format('YYYY-MM-DD'))
  const [logFilter, setLogFilter] = useState('*')

  useEffect(() => {
    handleGetData()
  }, [endDate, currentPage, itemPerPage, startDate, searchText, logFilter])

  async function handleGetData() {
    try {
      const res = await getGamingLog(
        currentPage,
        itemPerPage,
        searchText,
        startDate,
        endDate,
        logFilter,
      )
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
    setItemPerPage(params.itemPerPage)
  }

  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <TableFilter
                params={{searchText, itemPerPage}}
                title="Bepoz Logs"
                total={data.total}
                onParamChange={handleChangeParam}
              />
            </CCardHeader>

            <CCardBody>
              <CRow>
                <CCol xs={4}>
                  <FormDate label="Date From" value={startDate} onValueChange={setStartDate} />
                </CCol>
                <CCol xs={4}>
                  <FormDate label="End Date" value={endDate} onValueChange={setEndDate} />
                </CCol>
                <CCol xs={4}>
                  <FormSelect
                    label="Log Type"
                    value={logFilter}
                    options={LOG_FILTER}
                    onValueChange={setLogFilter}
                  />
                </CCol>
              </CRow>
              <CDataTable
                items={data?.data || []}
                fields={fields}
                striped
                itemsPerPage={itemPerPage}
                scopedSlots={{time: (item) => <td>{moment(item.created_at).format('LL')}</td>}}
              />
              <CPagination
                activePage={currentPage}
                pages={Math.ceil(data?.total / itemPerPage)}
                onActivePageChange={setCurrentPage}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default GamingReport

const LOG_FILTER = [
  {value: '*', label: 'All'},
  {value: 'errors', label: 'Errors'},
  {value: 'accounts', label: 'Accounts'},
  {value: 'Products', label: 'Products'},
  {value: 'vouchers', label: 'Vouchers'},
  {value: 'transactions', label: 'Transactions'},
  {value: 'general', label: 'general'},
]
