import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CDataTable, CRow} from '@coreui/react'
import FormDate from '../../common/FormDate'
import {postRegisteredMembersReport} from '../../utilities/ApiManage'
import moment from 'moment'

const fields = [
  {key: 'id', label: '#'},
  'first_name',
  'last_name',
  'mobile',
  'email',
  {key: 'confirm', label: 'Confirmed?'},
  'source',
  'joined_date',
  'member_id',
  'bepoz_account_card',
  'bepoz_id',
  'tier',
]

function MembersReport({location, history}) {
  const [startDate, setStartDate] = useState(moment().add(-7, 'day'))
  const [endDate, setEndDate] = useState(moment())
  const [data, setData] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await postRegisteredMembersReport({
        end_date: endDate.get('date'),
        end_month: endDate.get('month') + 1,
        end_year: endDate.get('year'),
        start_date: startDate.get('date'),
        start_month: startDate.get('month') + 1,
        start_year: startDate.get('year'),
      })
      if (res.ok) {
        setData(res.data)
      }
    })()
  }, [startDate, endDate])

  const print = () => {
    window.print()
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Registered Members</CCardHeader>
          <CCardBody>
            <CRow>
              <CCol xs={12} md={6}>
                <FormDate
                  label="Start Date"
                  value={startDate}
                  onValueChange={(value) => setStartDate(moment(value))}
                />
              </CCol>
              <CCol xs={12} md={6}>
                <FormDate label="End Date" value={endDate} onValueChange={(value) => setEndDate(value)} />
                <div>
                  <CButton className="bg-info text-light mr-3 mb-3" onClick={print}>
                    PRINT PDF
                  </CButton>
                  <CButton className="bg-primary text-light mb-3" onClick={print}>
                    PRINT CSV
                  </CButton>
                </div>
              </CCol>
            </CRow>

            <CDataTable
              items={data}
              fields={fields}
              striped
              scopedSlots={{
                id: (item, idx) => <td>{idx + 1}</td>,
                first_name: (item) => <td>{item?.member?.first_name}</td>,
                last_name: (item) => <td>{item?.member?.last_name}</td>,
                mobile: (item) => <td>{item?.mobile || ''}</td>,
                confirm: (item) => <td>{item?.email_confirmation ? 'Yes' : 'No'}</td>,
                source: () => <td>app</td>,
                joined_date: (item) => <td>{moment(item.created_at).format('LLL')}</td>,
                member_id: (item) => <td>{item?.member?.id || ''}</td>,
                bepoz_account_card: (item) => <td>{item?.member?.bepoz_account_card_number || ''}</td>,
                bepoz_id: (item) => <td>{item?.member?.bepoz_account_id || ''}</td>,
                tier: (item) => <td>{item?.member?.member_tier?.tier?.name || ''}</td>,
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default MembersReport
