import React, {useEffect, useState} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CDataTable, CPagination, CRow} from '@coreui/react'
import moment from 'moment'
import {getPagingSystemLogs} from '../../utilities/ApiManage'
import '../../scss/_custom.scss'
import TableFilter from '../../common/TableFilter'

const fields = [
  {key: 'id', label: '#'},
  'time',
  'source',
  'type',
  {key: 'humanized_message', label: 'Readable error message'},
]

function SystemLogPage({}) {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage])

  async function handleGetData() {
    try {
      const res = await getPagingSystemLogs(currentPage, itemPerPage)
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader>
              <TableFilter
                params={{itemPerPage}}
                title="System Logs"
                total={data.total}
                onParamChange={handleChangeParam}
              />
            </CCardHeader>

            <CCardBody>
              <CDataTable
                items={data?.data || []}
                fields={fields}
                striped
                itemsPerPage={itemPerPage}
                scopedSlots={{time: (item) => <td>{moment(item.created_at).format('LL')}</td>}}
              />
              <CPagination
                activePage={currentPage}
                pages={Math.ceil(data?.total / itemPerPage)}
                onActivePageChange={setCurrentPage}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default SystemLogPage
