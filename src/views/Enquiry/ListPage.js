import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CRow, CTooltip} from '@coreui/react'
import {deleteListingEnquiry, getPagingEnquiry} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import PopupShowEnquiry from './components/PopupShowEnquiry'
import {setModal} from '../../reusable/Modal'
import moment from 'moment'
import TableFilter from '../../common/TableFilter'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#'},
  'member',
  'subject',
  'listing',
  'received_at',
  'contract',
  'rating',
  'option',
]

function ListPage() {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showDone, setShowDone] = useState(false)
  const [selectedEnquiry, setSelectedEnquiry] = useState(null)

  const handleGetData = async () => {
    const res = await getPagingEnquiry(currentPage, itemPerPage, showDone)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, showDone])

  const handleDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected enquiry (#${item.id})`,
      message: 'Caution: deleting this item is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteListingEnquiry(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
    setShowDone(params.showDone)
  }

  return (
    <>
      <CCard>
        <CCardHeader>
          <TableFilter
            params={{itemPerPage, showDone}}
            title="Feedback and Enquiries"
            total={data.total}
            onParamChange={handleChangeParam}
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fields}
            size={'sm'}
            onPageChange={setCurrentPage}
            onRowClick={(item) => {
              setSelectedEnquiry(item)
            }}
            scopedSlots={{
              member: (item) => (
                <td>
                  {item.member?.first_name || ''} {item.member?.last_name || ''} ({item.member?.id || ''})
                </td>
              ),
              subject: (item) => <td>{item.subject || ''}</td>,
              listing: (item) => <td>{item.listing ? `${item.listing.name} (${item.listing.id})` : ''}</td>,
              received_at: (item) => <td>{moment(item.created_at).format('MMMM DoYYYY, HH:mma')}</td>,
              contract: (item) => <td>{item.contract_me || ''}</td>,
              rating: (item) => <td>{item.rating || ''}</td>,
              option: (item) => (
                <td>
                  <CButton onClick={() => setSelectedEnquiry(item)} className="p-0 pr-2">
                    <CTooltip content="Edit">
                      <CIcon content={freeSet.cilPen} />
                    </CTooltip>
                  </CButton>
                  <CButton onClick={() => handleDelete(item)} className="p-0 pr-2">
                    <CTooltip content="Delete">
                      <CIcon content={freeSet.cilTrash} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>

      <PopupShowEnquiry
        show={selectedEnquiry !== null}
        closeModal={() => setSelectedEnquiry(null)}
        selectedEnquiry={selectedEnquiry}
      />
    </>
  )
}

export default ListPage
