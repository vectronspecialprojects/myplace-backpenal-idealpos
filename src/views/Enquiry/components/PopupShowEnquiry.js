import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCol,
  CFormGroup,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import FormTextarea from '../../../common/FormTextarea'
import FormCheckbox from '../../../common/FormCheckbox'
import {postMarkItAsDone, postSaveComment} from '../../../utilities/ApiManage'

function PopupShowEnquiry({show, closeModal, selectedEnquiry}) {
  const {id, listing, subject, message, contract_me, rating, member, staff, is_answered} =
    selectedEnquiry || {}
  const [comment, setComment] = useState('')
  const [isAnswer, setIsAnswer] = useState(is_answered)

  useEffect(() => {
    setIsAnswer(is_answered)
    setComment(selectedEnquiry?.comment || '')
  }, [is_answered, selectedEnquiry?.comment])

  const markItDone = (value) => {
    selectedEnquiry.is_answered = value
    setIsAnswer(value)
    postMarkItAsDone({listing_id: id, is_answered: value})
  }

  const saveAndClose = async () => {
    selectedEnquiry.comment = comment
    await postSaveComment({comment, listing_id: id})
    closeModal()
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Enquiry</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12} md={4}>
              <FormInput
                disabled
                label="Member"
                value={`${member?.first_name || ''} ${member?.last_name || ''} ( ${member?.id || ''}) }`}
              />
              <FormInput disabled label="Subject" value={subject || ''} />
              <FormInput disabled label="Message" value={message || ''} />
              <FormInput disabled label="Member Request Contract" value={contract_me || ''} />
              <FormInput disabled label="Rating" value={rating || ''} />
            </CCol>
            <CCol xs={12} md={4}>
              <FormInput disabled label="Email" value={member?.user?.email || ''} />
              <FormInput disabled label="Mobile" value={member?.user?.mobile || ''} />
              <FormTextarea label="Comment" value={comment} onChangeText={setComment} />
            </CCol>
            <CCol xs={12} md={4}>
              <CFormGroup>
                <CLabel>Listing Information</CLabel>
                {listing?.image_banner && <img className="w-100" src={listing.image_banner} />}
              </CFormGroup>
              <FormInput disabled label="Heading" value={listing ? `${listing.name} (${listing.id})` : ''} />
              <CFormGroup>
                <FormCheckbox
                  label="Mark it as DONE"
                  checked={isAnswer === 1}
                  checkValue={1}
                  uncheckValue={0}
                  onValueChange={markItDone}
                />
              </CFormGroup>
              <FormInput
                disabled
                label="Commented by Staff"
                value={staff ? `${staff.first_name || ''} ${staff.last_name || ''}` : 'Not Signed Yet'}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupShowEnquiry
