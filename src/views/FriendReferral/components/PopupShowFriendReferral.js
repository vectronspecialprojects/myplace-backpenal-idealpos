import React, {useEffect, useState} from 'react'
import {
  CButton,
  CDataTable,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import {postShowFriendReferral} from '../../../utilities/ApiManage'
import moment from 'moment'
import {getPluralString} from '../../../helpers'

const fields = [
  {key: 'id', label: '#'},
  'member',
  'send_at',
  {key: 'accepted', label: 'Accepted?'},
  'reward',
  'extra_information',
]

function PopupShowFriendReferral({show, closeModal, selectedFriendReferral}) {
  const [data, setData] = useState([])

  useEffect(() => {
    if (selectedFriendReferral?.email)
      (async () => {
        const res = await postShowFriendReferral({email: selectedFriendReferral.email})
        if (!res.ok) throw new Error(res.message)
        setData(res.data.map((_) => ({..._, reminder_interval: JSON.parse(_.reminder_interval)})))
      })()
  }, [selectedFriendReferral])

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Referrers</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CDataTable
            items={data || []}
            fields={fields}
            striped
            scopedSlots={{
              id: (item) => <td>{item.id || ''}</td>,
              member: (item) => (
                <td>
                  <CLabel>
                    {item.member?.first_name || ''} {item.member?.last_name || ''} ({item.member?.id || ''})
                  </CLabel>
                  <CLabel>level: {(item.referrers_membership || '').replace(/\_/, ' ')}</CLabel>
                </td>
              ),
              send_at: (item) => (
                <td>
                  <CLabel>{moment(item.sent_at).format('MMMM Do YYYY, HH:mma')}</CLabel>
                  <CLabel>
                    Reminder:{' '}
                    <CLabel className="font-weight-bold">
                      {item.reminder_interval.map((_) => `${_}${getPluralString(_, 'day')}`).join(', ')}
                    </CLabel>
                  </CLabel>
                </td>
              ),
              accepted: (item) => (
                <td>
                  <CLabel>{item.is_accepted === 1 ? 'Yes' : 'No'}</CLabel>
                </td>
              ),
              reward: (item) => (
                <td>
                  <CLabel>
                    rewarded:{' '}
                    <CLabel className="font-weight-bold">
                      {(item.reward_option || '').replace(/_/, ' ')}
                    </CLabel>
                  </CLabel>
                </td>
              ),
              extra_information: (item) => (
                <td>
                  <CLabel>Has he/she made a purchase? {item.made_purchase_at ? 'Yes' : 'No'}</CLabel>
                  <CLabel>Voucher pending</CLabel>
                </td>
              ),
            }}
          />
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={closeModal}>
            CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupShowFriendReferral
