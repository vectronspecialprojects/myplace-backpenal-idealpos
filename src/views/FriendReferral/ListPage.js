import React, {useEffect, useState} from 'react'
import {CAlert, CButton, CCard, CCardBody, CCardHeader, CTooltip} from '@coreui/react'
import {getPagingFriendReferral} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import '../../scss/_custom.scss'
import PopupShowFriendReferral from './components/PopupShowFriendReferral'
import TableFilter from '../../common/TableFilter'
import MyTable from '../../common/Table'

const fields = [
  {key: 'id', label: '#'},
  {key: 'email', label: 'Recipient'},
  'recipient_status',
  'referral_count',
  'joined_at',
  {key: 'option', label: ''},
]

function ListPage() {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [selectedFriendReferral, setSelectedFriendReferral] = useState(null)
  const [showModalCreate, setShowModalCreate] = useState(false)

  const handleGetData = async () => {
    const res = await getPagingFriendReferral(currentPage, itemPerPage)
    if (res.ok) setData(res.data)
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage])

  const clickShow = (item) => {
    setShowModalCreate(true)
    setSelectedFriendReferral(item)
  }

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
  }

  return (
    <>
      <CCard>
        <CAlert color="info">Please change friend referral reward from setting page</CAlert>
        <CCardHeader>
          <TableFilter
            params={{itemPerPage}}
            title="Friend Referral"
            total={data.total}
            onParamChange={handleChangeParam}
          />
        </CCardHeader>
        <CCardBody>
          <MyTable
            data={data?.data}
            params={{itemPerPage, page: currentPage, total: data?.total}}
            fields={fields}
            size={'sm'}
            onPageChange={setCurrentPage}
            onRowClick={(item) => {
              clickShow(item)
            }}
            scopedSlots={{
              id: (item) => <td>{item.id || ''}</td>,
              recipient_status: () => <td>hasn't joined</td>,
              joined_at: () => <td>Not yet</td>,
              option: (item) => (
                <td>
                  <CButton onClick={() => clickShow(item)} className="p-0 pr-2">
                    <CTooltip content="Show">
                      <CIcon content={freeSet.cilFindInPage} />
                    </CTooltip>
                  </CButton>
                </td>
              ),
            }}
          />
        </CCardBody>
      </CCard>

      <PopupShowFriendReferral
        show={showModalCreate}
        closeModal={() => setShowModalCreate(false)}
        selectedFriendReferral={selectedFriendReferral}
        onOk={handleGetData}
      />
    </>
  )
}

export default ListPage
