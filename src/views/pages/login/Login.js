import React, {useRef, useState} from 'react'
import {Link} from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {useDispatch} from 'react-redux'
import {userLogin} from '../../../redux/actions/user'
import logo from '../../../assets/images/bepoz_logo.png'
const Login = () => {
  const dispatch = useDispatch()
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const timeout = useRef()
  const count = useRef(0)

  function handleUserLogin() {
    dispatch(userLogin(username, password))
  }

  const handleSetPassword = () => {
    clearTimeout(timeout.current)
    count.current++
    timeout.current = setTimeout(() => {
      if (count.current >= 10) {
        setUsername('admin@vectron.com.au')
        setPassword('secret')
      }
      count.current = 0
    }, 500)
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="Username"
                        value={username}
                        autoComplete="username"
                        onChange={(e) => {
                          setUsername(e.target.value)
                        }}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon
                            name="cil-lock-locked"
                            onClick={() => {
                              handleSetPassword()
                            }}
                          />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        value={password}
                        placeholder="Password"
                        autoComplete="current-password"
                        onChange={(e) => {
                          setPassword(e.target.value)
                        }}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton
                          color="primary"
                          className="px-4"
                          onClick={() => {
                            handleUserLogin()
                          }}>
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-gradient-dark py-5 d-md-down-none" style={{width: '44%'}}>
                <CCardBody className="text-center">
                  <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                    <img src={logo} style={{width: 250, height: 80}} />
                    <span>Welcome to Backpanel</span>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
