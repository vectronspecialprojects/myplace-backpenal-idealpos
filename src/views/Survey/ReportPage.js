import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CCol, CFormGroup, CRow} from '@coreui/react'
import moment from 'moment'
import {getSurveyDetail} from '../../utilities/ApiManage'
import {CChartPie} from '@coreui/react-chartjs'
import ROUTES from '../../constants/routes'

function ReportPage({location, history}) {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])

  const [data, setData] = useState(null)

  const surveyAnswer = useMemo(() => {
    return (data?.survey_answers || []).reduce(
      (acc, item) => {
        return {
          total: {
            ...acc.total,
            [item.answer_id]: {name: item.answer.answer, count: (acc?.[item.id]?.count || 0) + 1},
          },
          question: {
            ...acc.question,
            [item.question_id]: {
              ...(acc.question?.[item.question_id] || {}),
              [item.answer_id]: (acc.question?.[item.question_id]?.[item.answer_id] || 0) + 1,
            },
          },
        }
      },
      {total: {}, question: {}},
    )
  }, [data])

  useEffect(() => {
    ;(async () => {
      const res = await getSurveyDetail(id)
      if (res.ok) {
        setData(res.data)
      }
    })()
  }, [])

  const print = () => {
    window.print()
  }

  const generateReport = () => {
    history.push(ROUTES.SURVEY_REPORT_GENERATE.replace(':id', id))
  }

  const renderTotalChart = () => {
    const value = Object.values(surveyAnswer.total)
    return (
      <CChartPie
        datasets={[{backgroundColor: CHART_COLORS, data: value.map((_) => _.count)}]}
        labels={value.map((_) => _.name)}
        options={{tooltips: {enabled: true}}}
      />
    )
  }

  const renderQuestionChart = (question) => {
    const value = question.answers.map((answer) => ({
      name: answer.answer,
      count: surveyAnswer.question?.[question.id]?.[answer.id] || 0,
    }))
    return (
      <CChartPie
        datasets={[{backgroundColor: CHART_COLORS, data: value.map((_) => _.count)}]}
        labels={value.map((_) => _.name)}
        options={{tooltips: {enabled: true}}}
      />
    )
  }

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Survey Detail</CCardHeader>
        </CCard>

        {data && (
          <>
            <CCard className="create-listing-container">
              <CCardBody>
                <CRow>
                  <CCol xs={12} md={6}>
                    {renderTotalChart()}
                  </CCol>
                  <CCol xs={12} md={6}>
                    <CFormGroup>
                      <h5>Title: {data.title}</h5>
                    </CFormGroup>
                    <CFormGroup>
                      <h5>User Groups:</h5>
                      {data.tiers.map((tier) => (
                        <li key={tier.id}>{tier.name}</li>
                      ))}
                    </CFormGroup>
                    <CFormGroup>
                      <h5>Expired?</h5>
                      <li>{data.never_expired ? 'Never' : moment(data.date_expiry).format('LL')}</li>
                    </CFormGroup>

                    <CFormGroup>
                      <h5>A List of Questions:</h5>
                      {data.questions.map((item) => (
                        <li key={item.id}>{item.question}</li>
                      ))}
                    </CFormGroup>
                    <CButton className="bg-primary text-light " onClick={print}>
                      PRINT PDF
                    </CButton>
                    <CButton className="bg-warning text-light mx-2" onClick={generateReport}>
                      GENERATE REPORT
                    </CButton>
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
            {data.questions.map((item, index) => (
              <CCard key={item.id} className="create-listing-container">
                <CCardBody>
                  <CRow>
                    <CCol xs={12} md={6}>
                      <CFormGroup>
                        <h5>
                          {index + 1}: {item.question}
                        </h5>
                        {item.answers.map((answer) => (
                          <li key={answer.id}>
                            {answer.answer} -{' '}
                            <span className="font-weight-bold">
                              {surveyAnswer.question?.[item.id]?.[answer.id] || 0}
                            </span>
                          </li>
                        ))}
                      </CFormGroup>
                    </CCol>
                    <CCol xs={12} md={6}>
                      {renderQuestionChart(item)}
                    </CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            ))}
          </>
        )}
      </CCol>
    </CRow>
  )
}

export default ReportPage
const CHART_COLORS = [
  '#41B883',
  '#E46651',
  '#00D8FF',
  '#DD1B16',
  '#3366CC',
  '#DC3912',
  '#FF9900',
  '#109618',
  '#990099',
  '#3B3EAC',
  '#0099C6',
  '#DD4477',
  '#66AA00',
  '#B82E2E',
  '#316395',
  '#994499',
  '#22AA99',
  '#AAAA11',
  '#6633CC',
  '#E67300',
  '#8B0707',
  '#329262',
  '#5574A6',
  '#3B3EAC',
]
