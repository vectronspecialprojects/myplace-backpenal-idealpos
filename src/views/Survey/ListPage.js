import React, {useEffect, useState} from 'react'
import {CButton, CCard, CCardBody, CCardHeader, CRow, CTooltip} from '@coreui/react'
import {deleteSurvey, getPagingSurvey} from '../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import {useHistory} from 'react-router-dom'
import TableFilter from '../../common/TableFilter'
import ROUTES from '../../constants/routes'
import {setModal} from '../../reusable/Modal'
import MyTable from '../../common/Table'

const fields = [{key: 'id', label: '#'}, 'title', 'status', {key: 'option', label: ''}]

function ListPage() {
  const history = useHistory()
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [searchText, setSearchText] = useState('')

  async function handleGetData() {
    try {
      const res = await getPagingSurvey(currentPage, itemPerPage, searchText)
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    handleGetData()
  }, [currentPage, itemPerPage, searchText])

  const navigateToQuestion = () => history.push(ROUTES.QUESTION)

  const navigateToAnswer = () => history.push(ROUTES.ANSWER)

  const clickReport = (item) => history.push(ROUTES.SURVEY_REPORT.replace(':id', item.id))

  const clickDelete = (item) => {
    setModal({
      title: `Would you like to delete the selected survey (#${item.id})`,
      message: 'Caution: deleting this ticket is not reversible and may have negative effects.',
      primaryBtnClick: async () => {
        const res = await deleteSurvey(item.id)
        if (res?.status === 'ok') {
          handleGetData()
          setModal({show: false})
        }
      },
    })
  }

  const clickUpdate = (item) => history.push(ROUTES.SURVEY_UPDATE.replace(':id', item.id))

  const clickCreate = () => history.push(ROUTES.SURVEY_CREATE)

  const handleChangeParam = (params) => {
    setItemPerPage(params.itemPerPage)
    setSearchText(params.searchText)
  }

  return (
    <CCard>
      <CCardHeader>
        <TableFilter
          params={{searchText}}
          title="Surveys"
          total={data.total}
          onParamChange={handleChangeParam}
          onClickCreate={clickCreate}
          actions={
            <>
              <CButton onClick={navigateToQuestion}>
                <CTooltip content="Show all question">
                  <CIcon content={freeSet.cilListNumbered} style={{width: 25, height: 25}} />
                </CTooltip>
              </CButton>
              <CButton onClick={navigateToAnswer}>
                <CTooltip content="Show all answer">
                  <CIcon content={freeSet.cilCommentBubble} style={{width: 25, height: 25}} />
                </CTooltip>
              </CButton>
            </>
          }
        />
      </CCardHeader>
      <CCardBody>
        <MyTable
          data={data?.data}
          params={{itemPerPage, page: currentPage, total: data?.total}}
          fields={fields}
          size={'sm'}
          onPageChange={setCurrentPage}
          onRowClick={(item) => {
            clickUpdate(item)
          }}
          scopedSlots={{
            option: (item) => (
              <td>
                <CButton onClick={() => clickReport(item)} className="p-0 pr-2">
                  <CTooltip content="Show report">
                    <CIcon content={freeSet.cilBarChart} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => clickUpdate(item)} className="p-0 pr-2">
                  <CTooltip content="Edit">
                    <CIcon content={freeSet.cilPen} />
                  </CTooltip>
                </CButton>
                <CButton onClick={() => clickDelete(item)} className="p-0 pr-2">
                  <CTooltip content="Delete">
                    <CIcon content={freeSet.cilTrash} />
                  </CTooltip>
                </CButton>
              </td>
            ),
          }}
        />
      </CCardBody>
    </CCard>
  )
}

export default ListPage
