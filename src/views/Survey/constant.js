import moment from 'moment-timezone'

export const SURVEY_REWARD = {
  POINT: {value: 'point', label: 'by Point'},
  VOUCHER: {value: 'voucher', label: 'by Voucher'},
}

export const SURVEY_REWARDS = Object.values(SURVEY_REWARD)

export const DEFAULT_SURVEY = {
  date_timezone: moment.tz.guess(),
  description: '',
  never_expired: 0,
  point_reward: 0,
  questions: [],
  reward_type: SURVEY_REWARD.POINT.value,
  status: 'active',
  tiers: [],
  title: '',
  voucher_setup_reward: undefined,
}
