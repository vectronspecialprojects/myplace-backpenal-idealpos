import React, {useState} from 'react'
import {CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'
import QuestionForm from '../../Question/components/QuestionForm'
import {postQuestion} from '../../../utilities/ApiManage'
import {MODE} from '../../../constants'

function PopupAddQuestion({show, onClose, onSubmit, selectedQuestion}) {
  const [value, setValue] = useState({form: {}, validator: {}})

  const handleSave = async () => {
    if (value?.validator?.valid) {
      const res = await postQuestion(selectedQuestion?.id, value.form)
      if (res.ok) {
        if (selectedQuestion) {
          onSubmit({...selectedQuestion, ...res.data})
        } else {
          onSubmit({...res.data, mode: MODE.NEW, question_order: 0})
        }
      }
    }
  }

  return (
    <>
      <CModal show={show} onClose={onClose} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>{selectedQuestion ? 'Create' : 'Update'} Question</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {show && (
            <QuestionForm showSaveButton={false} onFormChange={setValue} questionId={selectedQuestion?.id} />
          )}
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleSave}>
            SUBMIT
          </CButton>
          <CButton color="secondary" onClick={onClose}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupAddQuestion
