import {CButton, CCol, CDataTable, CFormGroup, CRow, CSelect} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import React, {useEffect, useState} from 'react'
import {updateForm} from '../../../helpers'
import {getSurvey, postCheckSurveyQuestion, postSurvey} from '../../../utilities/ApiManage'
import ROUTES from '../../../constants/routes'
import helpers from '../helpers'
import {DEFAULT_SURVEY, SURVEY_REWARD, SURVEY_REWARDS} from '../constant'
import {useHistory} from 'react-router-dom'
import FormCheckbox from '../../../common/FormCheckbox'
import FormTextarea from '../../../common/FormTextarea'
import FormDate from '../../../common/FormDate'
import VoucherSetupSelect from '../../../common/VoucherSetupSelect'
import FormItem from '../../../common/FormItem'
import TierCheckbox from '../../../common/TierCheckbox'
import moment from 'moment'
import PopupAddQuestion from './PopupAddQuestion'
import CIcon from '@coreui/icons-react'
import {MODE} from '../../../constants'
import {GIFT_CERTIFICATE_MODE} from '../../Listing/constant'
import {cilActionRedo} from '@coreui/icons'
import {freeSet} from '@coreui/icons/js/free'

const SurveyForm = ({surveyId}) => {
  const history = useHistory()
  const [form, setForm] = useState({...DEFAULT_SURVEY})
  const [error, setError] = useState({__checked: false})
  const [showAddQuestion, setShowAddQuestion] = useState(false)
  const [selectedQuestion, setSelectedQuestion] = useState(null)

  useEffect(() => {
    if (error?.__checked) {
      const validator = helpers.validateForm(form)
      setError(validator.error)
    }
  }, [form])

  useEffect(() => {
    if (surveyId)
      (async () => {
        try {
          const res = await getSurvey(surveyId)
          if (!res.ok) throw new Error(res.message)
          const data = {
            ...form,
            ...res.data,
            questions: res.data.questions.map((_) => ({
              ..._,
              question_order: _.pivot.question_order,
              mode: MODE.NOT_MODIFIED,
            })),
          }
          setForm(data)
        } catch (e) {
          console.log(e)
        }
      })()
  }, [surveyId])

  const onChange = (event) => setForm((oldState) => updateForm(event, oldState))

  const clickSave = async () => {
    const validator = helpers.validateForm(form)
    if (validator.valid) {
      const res = await postSurvey(surveyId, form)
      if (res.ok) {
        history.push({pathname: ROUTES.SURVEY})
      }
    }
    setError(validator.error)
  }

  const handleUpdateQuestion = (index) => {
    setSelectedQuestion(index)
    setShowAddQuestion(true)
  }

  const handleSaveQuestion = (item) => {
    if (selectedQuestion !== null) {
      const questions = [...form.questions]
      questions[selectedQuestion] = item
      setForm((f) => ({...f, questions}))
    } else {
      setForm((f) => ({...f, questions: [...(f?.questions || []), item]}))
    }
    setShowAddQuestion(false)
    setSelectedQuestion(null)
  }

  const handleRemoveQuestion = async (item, index) => {
    if (surveyId) {
      await postCheckSurveyQuestion({survey_id: surveyId, question_id: item.id})
      setForm({
        ...form,
        questions: form.questions.map((_, i) =>
          i === index ? {..._, mode: item.mode === MODE.DELETED ? MODE.NOT_MODIFIED : MODE.DELETED} : _,
        ),
      })
    } else {
      setForm({...form, questions: form.questions.filter((_) => _.id !== item.id)})
    }
  }

  return (
    <CRow>
      <CCol xs={12} md={8}>
        <FormInput
          id="title"
          value={form.title}
          onChange={onChange}
          error={error.title}
          label="Title"
          required
        />
      </CCol>
      <CCol xs={12} md={4}>
        <FormCheckbox
          id="status"
          onChange={onChange}
          label="Active"
          checked={form.status === 'active'}
          checkValue="active"
          uncheckValue="inactive"
        />
      </CCol>
      <CCol xs={12} md={8}>
        <FormTextarea
          id="description"
          value={form.description}
          onChange={onChange}
          error={error.description}
          label="Description"
          required
        />
      </CCol>
      <CCol xs={12} md={4}>
        <CFormGroup>
          <FormCheckbox
            id="never_expired"
            onChange={onChange}
            label="Never Expired"
            checked={form.never_expired === 1}
            checkValue={1}
            uncheckValue={0}
          />
        </CFormGroup>
        {form.never_expired === 0 && (
          <FormDate
            id="date_expiry"
            onChange={onChange}
            value={moment(form.date_expiry).format('YYYY-MM-DD')}
            label="Expiry Date"
          />
        )}
      </CCol>

      <CCol xs={12} md={6}>
        <FormItem id="reward_type" label="Set Survey Reward">
          <CSelect custom value={form.reward_type} id="reward_type" onChange={onChange}>
            {SURVEY_REWARDS.map((_) => (
              <option key={_.value} value={_.value}>
                {_.label}
              </option>
            ))}
          </CSelect>
        </FormItem>
        {form.reward_type === SURVEY_REWARD.VOUCHER.value ? (
          <FormItem id="voucher_setup_reward" label="Voucher Reward">
            <VoucherSetupSelect
              id="voucher_setup_reward"
              value={form.voucher_setup_reward}
              onChange={onChange}
            />
          </FormItem>
        ) : (
          <FormInput
            id="point_reward"
            label="Point Reward"
            onChange={onChange}
            value={form.point_reward}
            error={error.point_reward}
          />
        )}
      </CCol>

      <CCol xs={12} md={6}>
        <TierCheckbox id="tiers" onChange={onChange} value={form.tiers} />
      </CCol>

      <CCol xs={12}>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={() => setShowAddQuestion(true)}>
          ADD QUESTION
        </CButton>
        <PopupAddQuestion
          show={showAddQuestion}
          onClose={() => {
            setShowAddQuestion(false)
            setSelectedQuestion(null)
          }}
          selectedQuestion={form?.questions[selectedQuestion]}
          onSubmit={handleSaveQuestion}
        />

        {form.questions?.length ? (
          <CDataTable
            items={form.questions || []}
            fields={QUESTION_FIELD}
            striped
            scopedSlots={{
              question_order: (item, index) => (
                <td>
                  <FormInput
                    id={`questions.${index}.question_order`}
                    value={item.question_order}
                    onChange={onChange}
                  />
                </td>
              ),
              multiple_choice: (item) => (
                <td>{item.multiple_choice === 1 || item.multiple_choice === true ? 'Yes' : 'No'}</td>
              ),
              option: (item, index) => (
                <td>
                  <CRow>
                    <CButton onClick={() => handleUpdateQuestion(index)}>
                      <CIcon content={freeSet.cilPen} />
                    </CButton>
                    <CButton onClick={() => handleRemoveQuestion(item, index)}>
                      {item.mode === GIFT_CERTIFICATE_MODE.DELETED ? (
                        <CIcon content={cilActionRedo} />
                      ) : (
                        <CIcon name="cil-trash" />
                      )}
                    </CButton>
                  </CRow>
                </td>
              ),
            }}
          />
        ) : null}
      </CCol>

      <CCol xs={12}>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          SAVE AND EXIT
        </CButton>
      </CCol>
    </CRow>
  )
}

export default SurveyForm

const QUESTION_FIELD = [
  {key: 'id', label: '#'},
  'question',
  'question_order',
  'multiple_choice',
  {key: 'option', label: ''},
]
