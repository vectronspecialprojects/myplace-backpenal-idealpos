const helpers = {
  validateForm: (form) => {
    const error = {__checked: true}
    if (form.title.length === 0) error.title = 'The title is required'
    if (form.description.length === 0) error.description = 'The description is required'
    if (!form.point_reward) error.point_reward = 'The point reward is required'

    return {valid: Object.keys(error).length === 1, error}
  },
}
export default helpers
