import React from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import SurveyForm from './components/SurveyForm'

function CreatePage() {
  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Add Survey</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <SurveyForm />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default CreatePage
