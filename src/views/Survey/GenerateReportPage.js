import React, {useEffect, useMemo, useState} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CLabel, CRow} from '@coreui/react'
import {postSurveyGenerateReport} from '../../utilities/ApiManage'
import FormDate from '../../common/FormDate'

function GenerateReportPage({location}) {
  const id = useMemo(() => location.pathname.split('/')[3], [location.pathname])
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date(new Date().setDate(new Date().getDate() + 7)))
  const [data, setData] = useState(null)

  const generateReport = async () => {
    const res = await postSurveyGenerateReport({
      end_date: endDate.getDate(),
      end_month: endDate.getMonth() + 1,
      end_year: endDate.getFullYear(),
      start_date: startDate.getDate(),
      start_month: startDate.getMonth() + 1,
      start_year: startDate.getFullYear(),
      survey_id: id,
    })
    if (res.ok) {
      setData(res.data)
    }
  }

  useEffect(() => {
    generateReport()
  }, [startDate, endDate])

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Survey Generate Report</CCardHeader>
        </CCard>

        {data && (
          <>
            <CCard className="create-listing-container">
              <CCardBody>
                <CRow>
                  <CCol xs={12} md={6}>
                    <FormDate
                      value={startDate}
                      label="Start Date"
                      onChange={(e) => setStartDate(new Date(e.target.value))}
                    />
                  </CCol>
                  <CCol xs={12} md={6}>
                    <FormDate
                      value={endDate}
                      label="End Date"
                      onChange={(e) => setEndDate(new Date(e.target.value))}
                    />
                  </CCol>
                  <CCol xs={12}>
                    <CLabel>
                      Survey Title: <CLabel className="font-weight-bold"> {data.title}</CLabel>
                    </CLabel>
                    {data.questions.map((question) => (
                      <li>{question.question}</li>
                    ))}
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </>
        )}
      </CCol>
    </CRow>
  )
}

export default GenerateReportPage
