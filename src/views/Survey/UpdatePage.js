import React, {useMemo} from 'react'
import {CCard, CCardBody, CCardHeader, CCol, CRow} from '@coreui/react'
import SurveyForm from './components/SurveyForm'

function UpdatePage({location}) {
  const id = useMemo(() => location.pathname.split('/').pop(), [location.pathname])

  return (
    <CRow>
      <CCol xs="12">
        <CCard>
          <CCardHeader>Update Survey</CCardHeader>
        </CCard>

        <CCard className="create-listing-container">
          <CCardBody>
            <SurveyForm surveyId={id} />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default UpdatePage
