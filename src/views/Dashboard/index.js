import React, {useEffect, useMemo, useState} from 'react'
import {CButton, CCard, CCardBody, CCol, CLabel, CLink, CRow} from '@coreui/react'
import {postNewDashboard} from '../../utilities/ApiManage'
import ROUTES from '../../constants/routes'
import {LISTING} from '../Listing/constant'
import PopupSetCustomDate from './components/PopupSetCustomDate'
import moment from 'moment'
import RetrievePromotion from './components/RetrievePromotion'
import JoinedMembers from './components/JoinedMembers'
import TotalClick from './components/TotalClick'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons'
import {useSelector} from 'react-redux';
import {isTrue} from '../../helpers';

const TYPES = [
  {label: 'LAST MONTH', value: 'last_month', color: 'success'},
  {label: 'LAST WEEK', value: 'last_week', color: 'success'},
  {label: 'YESTERDAY', value: 'yesterday', color: 'success'},
  {label: 'TODAY', value: 'today', color: 'primary'},
  {label: 'THIS WEEK', value: 'this_week', color: 'warning'},
  {label: 'THIS MONTH', value: 'this_month', color: 'warning'},
  {label: 'THIS YEAR', value: 'this_year', color: 'warning'},
  {label: 'CUSTOM', value: 'custom', color: 'primary'},
]

const Dashboard = () => {
  const [type, setType] = useState('today')
  const [venueId, setVenueId] = useState('0')
  const [custom, setCustom] = useState({})
  const [customPopupVisible, setCustomPopupVisible] = useState(false)
  const extra = useMemo(() => {
    if (custom.startDate && custom.endDate && type === 'custom') {
      return JSON.stringify({date_start: custom.startDate, date_end: custom.endDate})
    }
    return undefined
  }, [custom, type])

  const settings = useSelector(state => state.app.appSettings)

  const [newDashboard, setNewDashboard] = useState({
    gift_certificate_count: 0,
    gift_certificate_total_value: 0,
    members_last_seven_days: 0,
    ticket_count: 0,
    ticket_total_paid: 0,
    total_friend_referral: 0,
    total_friend_referral_joined: 0,
    total_members: 0,
    total_promotions: 0,
    total_transactions: 0,
    transactions_total_paid: 0,
    used_promotions: 0,
  })

  const fetchNewDashboard = async () => {
    const res = await postNewDashboard({type, venue_id: venueId, extra})
    if (res.ok) {
      setNewDashboard(res.data)
    }
  }

  useEffect(() => {
    fetchNewDashboard()
  }, [type, venueId, extra])

  const handleClickButton = (item) => () => {
    if (item.value === 'custom') {
      setCustomPopupVisible(true)
    }
    setType(item.value)
  }

  const dashboards = [
    {
      color: 'gradient-primary',
      header: 'MEMBER',
      value: newDashboard.total_members,
      sub: `${newDashboard.members_last_seven_days} (LAST 7 DAYS)`,
      detail: ROUTES.MEMBER,
      icon: freeSet.cilGroup,
      visible: true
    },
    {
      color: 'gradient-info',
      header: 'PROMOTIONS',
      value: newDashboard.total_promotions,
      sub: `${newDashboard.used_promotions} SCANNED`,
      detail: ROUTES.LISTING.replace(':id', LISTING.PROMOTION),
      icon: freeSet.cilDiamond,
      visible: true
    },
    {
      color: 'gradient-warning',
      header: 'GIFT CERTIFICATE',
      value: newDashboard.gift_certificate_count,
      sub: `$${newDashboard.gift_certificate_total_value}`,
      detail: ROUTES.LISTING.replace(':id', LISTING.GIFT_CERTIFICATE),
      icon: freeSet.cilGift,
      visible: isTrue(settings?.gift_certificate?.value)
    },
    {
      color: 'gradient-danger',
      header: 'TICKETS',
      value: newDashboard.ticket_count,
      sub: `$${newDashboard.ticket_total_paid}`,
      detail: ROUTES.TICKET,
      icon: freeSet.cilCash,
      visible: isTrue(settings?.ticket?.value)
    },
    {
      color: 'gradient-info',
      header: 'REFER A FRIEND',
      value: newDashboard.total_friend_referral,
      sub: `${newDashboard.total_friend_referral_joined} SIGNED UP`,
      detail: ROUTES.FRIEND_REFERRAL,
      icon: freeSet.cilObjectGroup,
      visible: isTrue(settings?.refer_a_friend?.value)
    },
    {
      color: 'gradient-success',
      header: 'TRANSACTION',
      value: newDashboard.total_transactions,
      sub: `$${newDashboard.transactions_total_paid}`,
      detail: ROUTES.TRANSACTION,
      icon: freeSet.cilDollar,
      visible: isTrue(settings?.transaction?.value)
    },
  ]

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <div className="d-flex justify-content-end mb-3">
            {TYPES.map((item) => (
              <CButton
                key={item.value}
                className={`btn btn${type === item.value ? '-outline' : ''}-${item.color} mx-1`}
                onClick={handleClickButton(item)}>
                {item.value === 'custom' && type === 'custom' && custom?.startDate && custom?.endDate
                  ? `${moment(custom?.startDate).format('LL')} - ${moment(custom?.endDate).format('LL')}`
                  : item.label}
              </CButton>
            ))}
          </div>
        </CCol>
        {dashboards.map((item, index) => {
          if (!item.visible) return  null
          return <CCol key={index}>
            <div className={`bg-${item.color} mb-3 pt-2`}>
              <h6 className="text-value-sm text-right pr-2">{item.header}</h6>
              <div className="d-flex justify-content-center align-items-center px-2">
                <CIcon content={item.icon} width={40} height={40} />
                <div className="text-right flex-fill">
                  <div className="pl-1">
                    <h4>{item.value}</h4>
                    <CLabel className="text-value-sm">{item.sub}</CLabel>
                  </div>
                </div>
              </div>
              <div className="bg-white">
                <CLink className="text-dark px-3" href={item.detail}>
                  View Detail
                </CLink>
              </div>
            </div>
          </CCol>
        })}
      </CRow>

      <CCard>
        <CCardBody>
          <CRow>
            <CCol xs={12} md={9}>
              <CRow>
                <CCol xs={12} md={6}>
                  <JoinedMembers type={type} extra={extra} />
                </CCol>
                <CCol xs={12} md={6}>
                  <TotalClick type={type} extra={extra} />
                </CCol>
              </CRow>
            </CCol>
            <CCol xs={12} md={3}>
              <RetrievePromotion />
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>

      <PopupSetCustomDate
        show={customPopupVisible}
        onClose={() => setCustomPopupVisible(false)}
        onSubmit={setCustom}
      />
    </>
  )
}

export default Dashboard
