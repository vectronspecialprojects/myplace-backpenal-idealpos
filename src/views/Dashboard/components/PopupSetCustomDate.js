import React, {useState} from 'react'
import {CButton, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow} from '@coreui/react'
import FormDate from '../../../common/FormDate'

function PopupSetCustomDate({show, onClose, onSubmit}) {
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())

  const submit = async () => {
    onSubmit({startDate, endDate})
    onClose()
  }

  return (
    <>
      <CModal show={show} onClose={onClose}>
        <CModalHeader closeButton>
          <CModalTitle>Custom Date Range</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12}>
              <FormDate label="From" value={startDate} onValueChange={setStartDate} showToday />
            </CCol>
            <CCol xs={12}>
              <FormDate label="Util" value={endDate} onValueChange={setEndDate} showToday />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={submit}>
            SUBMIT
          </CButton>
          <CButton color="secondary" onClick={onClose}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupSetCustomDate
