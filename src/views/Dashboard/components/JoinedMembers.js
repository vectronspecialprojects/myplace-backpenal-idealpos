import React, {useEffect, useState} from 'react'
import {CChartBar} from '@coreui/react-chartjs'
import {getJoinedMembers} from '../../../utilities/ApiManage'
import {CButton, CLabel, CLink} from '@coreui/react'
import ROUTES from '../../../constants/routes'

const BACKGROUND_COLOR = ['#f87979', '#00D8FF']

const JoinedMembers = ({type, extra}) => {
  const [data, setData] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await getJoinedMembers({type, extra})
      if (res.ok) {
        setData(res.data)
      }
    })()
  }, [type, extra])

  const datasets = data.map((_, i) => ({
    label: _.key,
    backgroundColor: BACKGROUND_COLOR[i],
    data: _.values.map((value) => value.value),
  }))
  const labels = data?.[0]?.values?.map((_) => _.label)

  return (
    <div>
      <div className="d-flex">
        <div className="flex-fill">
          <h5 className="text-info">JOINED MEMBERS</h5>
          <CLink className="p-0 text-dark" href={ROUTES.MEMBER_REPORT}>
            View Report
          </CLink>
        </div>
        <CLabel className="">
          Display: <CLabel className="font-weight-bold">{type.replace('_', ' ').toUpperCase()}</CLabel>
        </CLabel>
      </div>
      <CChartBar datasets={datasets} labels={labels} options={{tooltips: {enabled: true}}} />
    </div>
  )
}
export default JoinedMembers
