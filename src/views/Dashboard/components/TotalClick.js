import React, {useEffect, useMemo, useState} from 'react'
import {CChartBar, CChartHorizontalBar} from '@coreui/react-chartjs'
import {getListingCounts} from '../../../utilities/ApiManage'
import {CButton, CLabel} from '@coreui/react'
import FormSelect from '../../../common/FormSelect'
import {API_URI} from '../../../constants/constants'
import {getAccessToken} from '../../../utilities/NetworkingAuth'

const BACKGROUND_COLOR = ['#f87979', '#00D8FF']

const TotalClick = ({type, extra}) => {
  const display = useMemo(() => type.replace('_', ' ').toUpperCase(), [type])
  return (
    <div>
      <div className="d-flex">
        <div className="flex-fill">
          <h5 className="text-info">TOTAL CLICKS</h5>
        </div>
        <CLabel className="">
          Display: <CLabel className="font-weight-bold">{display}</CLabel>
        </CLabel>
      </div>
      <Chart display={display} extra={extra} />
      <Chart display={display} extra={extra} />
    </div>
  )
}

const TEXT_EXPORT = {
  '*': 'EXPORT ALL',
  1: 'EXPORT SPECIAL EVENT',
  2: 'EXPORT REGULAR EVENT',
}
const ID = [
  {value: '*', label: 'ALL'},
  {value: '1', label: 'Special Event'},
  {value: '2', label: 'Regular Event'},
]

const Chart = ({display, extra}) => {
  const [id, setId] = useState(ID[0].value)
  const [data, setData] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await getListingCounts({id, display, extra})
      if (res.ok) {
        setData(res.data)
      }
    })()
  }, [id, display, extra])

  const datasets = data.map((_, i) => ({
    label: _.key,
    backgroundColor: BACKGROUND_COLOR[i],
    data: _.values.map((value) => value.value),
  }))
  const labels = data?.[0]?.values?.map((_) => _.label)

  const exportReport = () => {
    window.open(`${API_URI}admin/getExportReport?id=${id}&token=${getAccessToken()}`, '_blank')
  }

  return (
    <div className="mb-3">
      <div className="d-flex justify-content-between">
        <FormSelect options={ID} value={id} onValueChange={setId} />
        <CButton className="btn btn-success text-value-sm mb-3" onClick={exportReport}>
          {TEXT_EXPORT[id]}
        </CButton>
      </div>
      <CChartHorizontalBar
        datasets={datasets}
        labels={labels}
        options={{
          tooltips: {enabled: true},
          scales: {
            yAxes: [
              {
                afterFit: function (scaleInstance) {
                  scaleInstance.width = 100 // sets the width to 100px
                },
              },
            ],
          },
        }}
      />
    </div>
  )
}

export default TotalClick
