import {useEffect, useState} from 'react'
import {getRetrievePromotions} from '../../../utilities/ApiManage'
import {CButton} from '@coreui/react'
import {API_URI} from '../../../constants/constants'
import {getAccessToken} from '../../../utilities/NetworkingAuth'

const RetrievePromotion = () => {
  const [data, setData] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await getRetrievePromotions()
      if (res.ok) {
        setData(res.data.sort((a, b) => a.display_order - b.display_order))
      }
    })()
  }, [])

  const exportActivityReport = () => {
    window.open(`${API_URI}admin/getExportPromotions?token=${getAccessToken()}`, '_blank')
  }

  return (
    <div>
      <h4 className="text-primary">PROMOTIONS</h4>
      <CButton className="btn btn-success" onClick={exportActivityReport}>
        EXPORT ACTIVITY REPORT
      </CButton>
      {data.map((item, index) => (
        <div key={item.id || item.heading} className="d-flex my-2 align-items-center">
          <div
            className="d-flex justify-content-center align-items-center border rounded-circle border-info text-info"
            style={{width: 40, height: 40}}>
            {index + 1}
          </div>
          <div className="ml-2 d-flex flex-column">
            <span>{item.heading}</span>
            <span className="text-muted">Redeemed: {item.order_details_count}</span>
            <span className="text-muted">Scanned: {item.scanned}</span>
          </div>
        </div>
      ))}
    </div>
  )
}

export default RetrievePromotion
