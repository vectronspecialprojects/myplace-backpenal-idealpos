import React, {useEffect, useState} from 'react'
import {CAlert, CButton, CCard, CCardBody, CCardHeader, CCol, CLabel, CLink, CRow} from '@coreui/react'
import '../../scss/_custom.scss'
import AppLayout from './Component/AppLayout/AppLayout'
import {getSetting} from '../../utilities/ApiManage'
import PointRatioAndRewards from './Component/PointRatioAndRewards'
import FriendReferral from './Component/FriendReferral'
import Reminder from './Component/Reminder'
import EmailNotification from './Component/EmailNotifications'
import AboutAndInstruction from './Component/AboutAndInstruction'
import Legals from './Component/Legals'
import BepozHQ from './Component/BepozHQ'
import Venues from './Component/Venues/index'
import AppDesign from './Component/AppDesign'
import CompanyProfileAndInvoice from './Component/CompanyProfileAndInvoice'
import TierAndMember from './Component/TierAndMember'
import Security from './Component/Security'
import Integration from './Component/Intergration'
import PopupUnlockSetting from './Component/PopupUnlockSetting'
import {useDispatch, useSelector} from 'react-redux'
import {removeUnlockSetting} from '../../redux/actions/app'
import moment from 'moment'
import FeatureControl from './Component/FeatureControl/FeatureControl'
import {setModal} from '../../reusable/Modal'

function Setting({location, history}) {
  const dispatch = useDispatch()
  const unlock = useSelector((state) => ({
    settingUnlockExpiredTime: state?.app?.settingUnlockExpiredTime,
    settingUnlock: state?.app?.settingUnlock,
  }))
  const [unlockVisible, setUnlockVisible] = useState(false)
  const setting = useSelector((state) => state.app.appSettings)
  const bepozVersion = useSelector((state) => state.app.bepozVersion)

  useEffect(() => {
    if (moment(unlock.settingUnlockExpiredTime) < moment() || !unlock.settingUnlockExpiredTime) {
      removeSession()
    }
  }, [unlock.settingUnlockExpiredTime])

  useEffect(() => {
    if (!bepozVersion) {
      setModal({
        title: `No Bepoz Connection`,
        message: `Please connect to bepoz before configuring the app`,
        primaryBtnClick: () => {
          setModal({show: false})
        },
        isShowSecondaryBtn: false,
        closeButton: false,
      })
    }
  }, [bepozVersion])

  const removeSession = () => {
    dispatch(removeUnlockSetting())
  }

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CCard>
            <CCardHeader className="d-flex justify-content-between">
              <h5>Setting</h5>
              {/* <CAlert color="warning">
                Warning: Settings for Bepoz Staff Only. Misconfiguration May Cause Instability of The App.
              </CAlert> */}
              <div>
                <CButton
                  className="btn btn-secondary"
                  onClick={() => setUnlockVisible(true)}
                  disabled={unlock.settingUnlock}>
                  Unlock Internal Settings
                </CButton>
                {unlock.settingUnlockExpiredTime && (
                  <CLabel className="d-block">
                    Unlock til 23:59 <CLink onClick={removeSession}>Remove Session?</CLink>
                  </CLabel>
                )}
              </div>
            </CCardHeader>
            {setting && (
              <CCardBody>
                <BepozHQ setting={setting} bepozVersion={bepozVersion} />
                <Integration setting={setting} bepozVersion={bepozVersion} />
                {unlock.settingUnlock && <AppDesign setting={setting} bepozVersion={bepozVersion} />}
                <AppLayout setting={setting} bepozVersion={bepozVersion} />
                <Venues setting={setting} bepozVersion={bepozVersion} />
                <TierAndMember setting={setting} bepozVersion={bepozVersion} />
                <CompanyProfileAndInvoice setting={setting} bepozVersion={bepozVersion} />
                <PointRatioAndRewards setting={setting} bepozVersion={bepozVersion} />
                <Reminder setting={setting} bepozVersion={bepozVersion} />
                <FriendReferral setting={setting} bepozVersion={bepozVersion} />
                <EmailNotification setting={setting} bepozVersion={bepozVersion} />
                <AboutAndInstruction setting={setting} bepozVersion={bepozVersion} />
                <Legals setting={setting} bepozVersion={bepozVersion} />
                <Security setting={setting} bepozVersion={bepozVersion} />
                {unlock.settingUnlock && <FeatureControl setting={setting} bepozVersion={bepozVersion} />}
              </CCardBody>
            )}
          </CCard>
          <PopupUnlockSetting
            show={unlockVisible}
            closeModal={() => setUnlockVisible(false)}
            onSubmit={() => {}}
          />
        </CCol>
      </CRow>
    </>
  )
}

export default Setting
