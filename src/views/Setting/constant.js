import {LISTING, LISTING_INFO} from '../Listing/constant'

export const SETTING_GALLERY_SLOTS = [
  {label: '1 Slot', value: '1'},
  {label: '2 Slots', value: '2'},
  {label: '3 Slots', value: '3'},
  {label: '4 Slots', value: '4'},
  {label: '5 Slots', value: '5'},
  {label: '6 Slots', value: '6'},
  {label: '7 Slots', value: '7'},
  {label: '8 Slots', value: '8'},
]

export const SETTING_WELCOME_GALLERY_STYLES = [
  {label: 'Classic', value: 'classic'},
  {label: 'Gallery', value: 'gallery'},
]

export const SETTING_GALLERY_CONTENT_OPTION = [
  {label: 'Text', value: 'text'},
  {label: 'Total Pledged To Date', value: 'totalPledgedToDate'},
]

export const SETTING_PAGE_LAYOUT = [
  {label: 'Single View', value: 'single_view'},
  {label: 'Tab View', value: 'tab_view'},
  {label: 'Special View', value: 'special_view'},
]

export const PAGE_LAYOUT = {
  single_view: 'single_view',
  tab_view: 'tab_view',
  special_view: 'special_view',
}

export const SETTING_PAGE_TOTAL = [
  {label: '1', value: '1'},
  {label: '2', value: '2'},
  {label: '3', value: '3'},
]

export const SETTING_PAGE_LAYOUT_TAB = [
  {label: "What's On", value: 'what_on'},
  {label: 'Stamp Card', value: 'stamp_card'},
]

export const VIEW_TAB = {
  what_on: [
    {label: 'Regular Event', category: 'information', value: LISTING.REGULAR_EVENT},
    {label: 'Special Event', category: 'information', value: LISTING.SPECIAL_EVENT},
  ],
  stamp_card: [
    {label: 'Stamp Card', category: 'information', value: LISTING.STAMP_CARD},
    {label: 'Stamp Card Won', category: 'information', value: LISTING.STAMP_CARD_WON},
  ],
}

export const SETTING_ON_OFF = [
  {label: 'On', value: 'true'},
  {label: 'Off', value: 'false'},
]

export const APP_LAYOUT = [
  {label: 'Legacy Design', value: 'true'},
  {label: 'Classic Design', value: 'false'},
]

export const INPUT_FIELDS_STYLE = [
  {label: 'Rounded', value: 'rounded'},
  {label: 'Rectangle', value: 'rectangle'},
]

export const SETTING_HIDE_OPTION = [
  {label: 'On', value: 'false'},
  {label: 'Off', value: 'true'},
]

export const SETTING_SIGN_UP_REWARD = [
  {value: 'point', label: 'by Point'},
  {value: 'voucher', label: 'by Voucher'},
]

export const SETTING_REWARD_OPTION = [
  {value: 'after_purchase', label: 'After Purchase'},
  {value: 'after_join', label: 'After Join'},
]

export const SETTING_MULTIPLE = [
  {value: '1', label: '1x'},
  {value: '2', label: '2x'},
  {value: '3', label: '3x'},
]

export const SETTING_DAY_OF_WEEK = {
  0: 'Sunday',
  1: 'Monday',
  2: 'Tuesday',
  3: 'Wednesday',
  4: 'Thursday',
  5: 'Friday',
  6: 'Saturday',
}

export const SETTING_SINGLE_VIEW = Object.values(LISTING).map((_) => ({
  value: _,
  label: LISTING_INFO[_].label,
}))

export const TABS_BUTTON = [
  {label: 'None', value: 'none', page_layout: 'special_view', navigation: 'none'},
  {label: 'About', value: 'about', page_layout: 'special_view', navigation: 'AboutNavigator'},
  {label: 'Barcode', value: 'barcode', page_layout: 'special_view', navigation: 'BarcodeNavigator'},
  {label: 'FAQ', value: 'faq', page_layout: 'special_view', navigation: 'FaqNavigator'},
  {label: 'Feedback', value: 'feedbacks', page_layout: 'special_view', navigation: 'FeedbackNavigator'},
  {label: 'Gaming', value: 'gaming', page_layout: 'special_view', navigation: 'GamingNavigator'},
  {
    label: 'Gift Certificate',
    category: 'information',
    value: LISTING.GIFT_CERTIFICATE,
    page_layout: 'single_view',
    navigation: 'GiftCertificateScreen',
  },
  {label: 'Legals', value: 'legals', page_layout: 'special_view', navigation: 'LegalsNavigator'},
  {label: 'Location', value: 'time-location', page_layout: 'special_view', navigation: 'LocationsNavigator'},
  {
    label: 'Membership',
    value: LISTING.MEMBERSHIP_TIER,
    page_layout: 'single_view',
    navigation: 'MembershipNavigator',
  },
  {
    label: 'Our Team',
    category: 'information',
    value: LISTING.OUR_TEAM,
    page_layout: 'single_view',
    navigation: 'OurTeamScreen',
  },
  {label: 'Profile', value: 'profile', page_layout: 'special_view', navigation: 'ProfileNavigator'},
  {
    label: 'Promotion',
    category: 'promotion',
    value: LISTING.PROMOTION,
    page_layout: 'single_view',
    navigation: 'OffersNavigator',
  },
  {
    label: 'Refer a Friend',
    value: 'refer',
    page_layout: 'special_view',
    navigation: 'ReferNavigator',
  },
  {label: 'Scan QR Code', value: 'camera', page_layout: 'special_view', navigation: 'CameraNavigator'},
  {label: 'Shops', value: LISTING.SHOPS, page_layout: 'single_view', navigation: 'ShopsNavigator'},
  {label: 'Stamp Card', value: 'stamp_card', page_layout: 'tab_view', navigation: 'StampcardNavigator'},
  {label: 'Survey', value: 'surveys', page_layout: 'special_view', navigation: 'SurveysNavigator'},
  {label: 'Ticket', value: 'tickets', page_layout: 'special_view', navigation: 'TicketsNavigator'},
  {label: 'Voucher', value: 'vouchers', page_layout: 'special_view', navigation: 'VouchersNavigator'},
  {label: 'Website', value: 'website', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: "What's On", value: 'what_on', page_layout: 'tab_view', navigation: 'WhatsonNavigator'},
  {label: 'Yourorder', value: 'yourorder', page_layout: 'special_view', navigation: 'YourorderNavigator'},
  {label: 'Link 1', value: 'link1', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: 'Link 2', value: 'link2', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: 'Link 3', value: 'link3', page_layout: 'special_view', navigation: 'WebviewNavigator'},
]

export const HOME_BUTTONS = [
  {label: 'None', value: 'none', page_layout: 'special_view', navigation: 'none'},
  {label: 'Label', value: 'label', page_layout: 'special_view', navigation: 'none'},
  {label: 'About', value: 'about', page_layout: 'special_view', navigation: 'AboutNavigator'},
  {label: 'Balance', value: 'balance', page_layout: 'special_view', navigation: 'none'},
  {label: 'Barcode', value: 'barcode', page_layout: 'special_view', navigation: 'BarcodeScreen'},
  {label: 'FAQ', value: 'faq', page_layout: 'special_view', navigation: 'FaqScreen'},
  {label: 'Feedback', value: 'feedbacks', page_layout: 'special_view', navigation: 'FeedbackScreen'},
  {label: 'Gaming', value: 'gaming', page_layout: 'special_view', navigation: 'GamingScreen'},
  {
    label: 'Gift Certificate',
    category: 'information',
    value: LISTING.GIFT_CERTIFICATE,
    page_layout: 'single_view',
    navigation: 'GiftCertificateScreen',
  },
  {label: 'Legals', value: 'legals', page_layout: 'special_view', navigation: 'LegalsScreen'},
  {label: 'Location', value: 'time-location', page_layout: 'special_view', navigation: 'LocationsScreen'},
  {
    label: 'Membership',
    value: LISTING.MEMBERSHIP_TIER,
    page_layout: 'single_view',
    navigation: 'MembershipScreen',
  },
  {
    label: 'Our Team',
    category: 'information',
    value: LISTING.OUR_TEAM,
    page_layout: 'single_view',
    navigation: 'OurTeamScreen',
  },
  {label: 'Points', value: 'points', page_layout: 'special_view', navigation: 'none'},
  {label: 'Profile', value: 'profile', page_layout: 'special_view', navigation: 'ProfileScreen'},
  {
    label: 'Promotion',
    category: 'promotion',
    value: LISTING.PROMOTION,
    page_layout: 'single_view',
    navigation: 'OffersScreen',
  },
  {
    label: 'Refer a Friend',
    value: 'refer',
    page_layout: 'special_view',
    navigation: 'ReferScreen',
  },
  {label: 'Saved', value: 'saved', page_layout: 'special_view', navigation: 'none'},
  {label: 'Scan QR Code', value: 'camera', page_layout: 'special_view', navigation: 'CameraScreen'},
  {label: 'Shops', value: LISTING.SHOPS, page_layout: 'single_view', navigation: 'ShopsScreen'},
  {label: 'Stamp Card', value: 'stamp_card', page_layout: 'tab_view', navigation: 'StampcardScreen'},
  {label: 'Survey', value: 'surveys', page_layout: 'special_view', navigation: 'SurveysScreen'},
  {label: 'Ticket', value: 'tickets', page_layout: 'special_view', navigation: 'TicketsScreen'},
  {label: 'Voucher', value: 'vouchers', page_layout: 'special_view', navigation: 'VouchersScreen'},
  {label: 'Website', value: 'website', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: "What's On", value: 'what_on', page_layout: 'tab_view', navigation: 'WhatsonScreen'},
  {label: 'Yourorder', value: 'yourorder', page_layout: 'special_view', navigation: 'YourorderScreen'},
  {label: 'Link 1', value: 'link1', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: 'Link 2', value: 'link2', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: 'Link 3', value: 'link3', page_layout: 'special_view', navigation: 'WebviewScreen'},
]

export const PROFILE_BUTTONS = [
  {label: 'None', value: 'none', page_layout: 'special_view', navigation: 'none'},
  {label: 'About', value: 'about', page_layout: 'special_view', navigation: 'AboutScreen'},
  {label: 'Barcode', value: 'barcode', page_layout: 'special_view', navigation: 'BarcodeScreen'},
  {label: 'FAQ', value: 'faq', page_layout: 'special_view', navigation: 'FaqScreen'},
  {label: 'Feedback', value: 'feedbacks', page_layout: 'special_view', navigation: 'FeedbackScreen'},
  {label: 'Gaming', value: 'gaming', page_layout: 'special_view', navigation: 'GamingScreen'},
  {
    label: 'Gift Certificate',
    category: 'information',
    value: LISTING.GIFT_CERTIFICATE,
    page_layout: 'single_view',
    navigation: 'GiftCertificateScreen',
  },
  {label: 'Legals', value: 'legals', page_layout: 'special_view', navigation: 'LegalsScreen'},
  {label: 'Location', value: 'time-location', page_layout: 'special_view', navigation: 'LocationsScreen'},
  {
    label: 'Membership',
    value: LISTING.MEMBERSHIP_TIER,
    page_layout: 'single_view',
    navigation: 'MembershipScreen',
  },
  {
    label: 'Our Team',
    category: 'information',
    value: LISTING.OUR_TEAM,
    page_layout: 'single_view',
    navigation: 'OurTeamScreen',
  },
  {
    label: 'Promotion',
    category: 'promotion',
    value: LISTING.PROMOTION,
    page_layout: 'single_view',
    navigation: 'OffersScreen',
  },
  {
    label: 'Refer a Friend',
    value: 'refer',
    page_layout: 'special_view',
    navigation: 'ReferScreen',
  },
  {label: 'Scan QR Code', value: 'camera', page_layout: 'special_view', navigation: 'CameraScreen'},
  {label: 'Shops', value: LISTING.SHOPS, page_layout: 'single_view', navigation: 'ShopsScreen'},
  {label: 'Stamp Card', value: 'stamp_card', page_layout: 'tab_view', navigation: 'StampcardScreen'},
  {label: 'Survey', value: 'surveys', page_layout: 'special_view', navigation: 'SurveysScreen'},
  {label: 'Ticket', value: 'tickets', page_layout: 'special_view', navigation: 'TicketsScreen'},
  {label: 'Voucher', value: 'vouchers', page_layout: 'special_view', navigation: 'VouchersScreen'},
  {label: 'Website', value: 'website', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: "What's On", value: 'what_on', page_layout: 'tab_view', navigation: 'WhatsonScreen'},
  {label: 'Yourorder', value: 'yourorder', page_layout: 'special_view', navigation: 'YourorderScreen'},
  {label: 'Link 1', value: 'link1', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: 'Link 2', value: 'link2', page_layout: 'special_view', navigation: 'WebviewScreen'},
  {label: 'Link 3', value: 'link3', page_layout: 'special_view', navigation: 'WebviewScreen'},
]

export const POND_HOPPERS_HOME = [
  {
    label: 'Community Point',
    value: 'community-points',
    page_layout: 'special_view',
    navigation: 'CommunityPointScreen',
  },
  {label: 'Pledge', value: 'pledge', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {label: 'Nominate', value: 'nominate', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {label: 'Perks', value: 'perks', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {label: 'Contact', value: 'contact', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {label: 'Impact', value: 'impact', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {label: 'Our Story', value: 'ourstory', page_layout: 'special_view', navigation: 'PondHoppersScreen'},
  {
    label: 'Pond Hoppers FAQS',
    value: 'faqs',
    page_layout: 'special_view',
    navigation: 'PondHoppersScreen',
  },
]

export const POND_HOPPERS_SIDE_BAR = [
  {
    label: 'Community Point',
    value: 'community-points',
    page_layout: 'special_view',
    navigation: 'CommunityPointNavigator',
  },
  {label: 'Pledge', value: 'pledge', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Nominate', value: 'nominate', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Perks', value: 'perks', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Contact', value: 'contact', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Impact', value: 'impact', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Our Story', value: 'ourstory', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {
    label: 'Pond Hoppers FAQS',
    value: 'faqs',
    page_layout: 'special_view',
    navigation: 'PondHoppersNavigator',
  },
]

export const POND_HOPPERS_SIDE_BAR_BEFORE_LOGIN = [
  {label: 'Pledge', value: 'pledge', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Nominate', value: 'nominate', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Perks', value: 'perks', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Contact', value: 'contact', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Impact', value: 'impact', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {label: 'Our Story', value: 'ourstory', page_layout: 'special_view', navigation: 'PondHoppersNavigator'},
  {
    label: 'Pond Hoppers FAQS',
    value: 'faqs',
    page_layout: 'special_view',
    navigation: 'PondHoppersNavigator',
  },
]

export const SIDE_BAR_BEFORE_LOGIN = [
  {label: 'About', value: 'about', page_layout: 'special_view', navigation: 'AboutNavigator'},
  {label: 'Legals', value: 'legals', page_layout: 'special_view', navigation: 'LegalsNavigator'},
  {label: 'Location', value: 'time-location', page_layout: 'special_view', navigation: 'LocationsNavigator'},
  {label: 'Website', value: 'website', page_layout: 'special_view', navigation: 'WebviewNavigator'},
]

export const SIDE_BAR_AFTER_LOGIN = [
  {label: 'None', value: 'none', page_layout: 'special_view', navigation: 'none'},
  {label: 'About', value: 'about', page_layout: 'special_view', navigation: 'AboutNavigator'},
  {label: 'Barcode', value: 'barcode', page_layout: 'special_view', navigation: 'BarcodeNavigator'},
  {label: 'FAQ', value: 'faq', page_layout: 'special_view', navigation: 'FaqNavigator'},
  {label: 'Feedback', value: 'feedbacks', page_layout: 'special_view', navigation: 'FeedbackNavigator'},
  {label: 'Gaming', value: 'gaming', page_layout: 'special_view', navigation: 'GamingNavigator'},
  {
    label: 'Gift Certificate',
    category: 'information',
    value: LISTING.GIFT_CERTIFICATE,
    page_layout: 'single_view',
    navigation: 'GiftCertificateNavigator',
  },
  {label: 'Legals', value: 'legals', page_layout: 'special_view', navigation: 'LegalsNavigator'},
  {label: 'Location', value: 'time-location', page_layout: 'special_view', navigation: 'LocationsNavigator'},
  {
    label: 'Membership',
    value: LISTING.MEMBERSHIP_TIER,
    page_layout: 'single_view',
    navigation: 'MembershipNavigator',
  },
  {
    label: 'Our Team',
    category: 'information',
    value: LISTING.OUR_TEAM,
    page_layout: 'single_view',
    navigation: 'OurTeamNavigator',
  },
  {label: 'Profile', value: 'profile', page_layout: 'special_view', navigation: 'ProfileNavigator'},
  {
    label: 'Promotion',
    category: 'promotion',
    value: LISTING.PROMOTION,
    page_layout: 'single_view',
    navigation: 'OffersNavigator',
  },
  {
    label: 'Refer a Friend',
    value: 'refer',
    page_layout: 'special_view',
    navigation: 'ReferNavigator',
  },
  {label: 'Scan QR Code', value: 'camera', page_layout: 'special_view', navigation: 'CameraNavigator'},
  {label: 'Shops', value: LISTING.SHOPS, page_layout: 'single_view', navigation: 'ShopsNavigator'},
  {label: 'Stamp Card', value: 'stamp_card', page_layout: 'tab_view', navigation: 'StampcardNavigator'},
  {label: 'Survey', value: 'surveys', page_layout: 'special_view', navigation: 'SurveysNavigator'},
  {label: 'Ticket', value: 'tickets', page_layout: 'special_view', navigation: 'TicketsNavigator'},
  {label: 'Voucher', value: 'vouchers', page_layout: 'special_view', navigation: 'VouchersNavigator'},
  {label: 'Website', value: 'website', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: "What's On", value: 'what_on', page_layout: 'tab_view', navigation: 'WhatsonNavigator'},
  {label: 'Yourorder', value: 'yourorder', page_layout: 'special_view', navigation: 'YourorderNavigator'},
  {label: 'Link 1', value: 'link1', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: 'Link 2', value: 'link2', page_layout: 'special_view', navigation: 'WebviewNavigator'},
  {label: 'Link 3', value: 'link3', page_layout: 'special_view', navigation: 'WebviewNavigator'},
]

export const NAVIGATION_KEY = [
  {label: 'None', value: 'none'},
  {label: 'AboutNavigator', value: 'AboutNavigator'},
  {label: 'EnquiriesNavigator', value: 'EnquiriesNavigator'},
  {label: 'FaqNavigator', value: 'FaqNavigator'},
  {label: 'FeedbackNavigator', value: 'FeedbackNavigator'},
  {label: 'FrontpageNavigator', value: 'FrontpageNavigator'},
  {label: 'GiftCertificateNavigator', value: 'GiftCertificateNavigator'},
  {label: 'HomeNavigator', value: 'HomeNavigator'},
  {label: 'TabNavigator', value: 'TabNavigator'},
  {label: 'LegalsNavigator', value: 'LegalsNavigator'},
  {label: 'LocationsNavigator', value: 'LocationsNavigator'},
  {label: 'OffersNavigator', value: 'OffersNavigator'},
  {label: 'OurTeamNavigator', value: 'OurTeamNavigator'},
  {label: 'ReferNavigator', value: 'ReferNavigator'},
  {label: 'StampcardNavigator', value: 'StampcardNavigator'},
  {label: 'SurveysNavigator', value: 'SurveysNavigator'},
  {label: 'TicketsNavigator', value: 'TicketsNavigator'},
  {label: 'VouchersNavigator', value: 'VouchersNavigator'},
  {label: 'WebviewNavigator', value: 'WebviewNavigator'},
  {label: 'YourorderNavigator', value: 'YourorderNavigator'},
  {label: 'WhatsonNavigator', value: 'WhatsonNavigator'},
  {label: 'GamingNavigator', value: 'GamingNavigator'},
  {label: 'ShopsNavigator', value: 'ShopsNavigator'},
  {label: 'MembershipNavigator', value: 'MembershipNavigator'},
  {label: 'CameraNavigator', value: 'CameraNavigator'},
  {label: 'BarcodeNavigator', value: 'BarcodeNavigator'},
  {label: 'CommunityPointNavigator', value: 'CommunityPointNavigator'},
  {label: 'PondHoppersNavigator', value: 'PondHoppersNavigator'},
  {label: 'ProfileNavigator', value: 'ProfileNavigator'},
  //Screen
  {label: 'LoginScreen', value: 'LoginScreen'},
  {label: 'SignupScreen', value: 'SignupScreen'},
  {label: 'ForgotPasswordScreen', value: 'ForgotPasswordScreen'},
  {label: 'PreferredVenueScreen', value: 'PreferredVenueScreen'},
  {label: 'MapScreen', value: 'MapScreen'},
  {label: 'ProfileScreen', value: 'ProfileScreen'},
  {label: 'HistoryScreen', value: 'HistoryScreen'},
  {label: 'FavoriteScreen', value: 'FavoriteScreen'},
  {label: 'StartupScreen', value: 'StartupScreen'},
  {label: 'AboutScreen', value: 'AboutScreen'},
  {label: 'WhatsonScreen', value: 'WhatsonScreen'},
  {label: 'WhatsonDetailsScreen', value: 'WhatsonDetailsScreen'},
  {label: 'ReferScreen', value: 'ReferScreen'},
  {label: 'TicketsScreen', value: 'TicketsScreen'},
  {label: 'GiftCertificateScreen', value: 'GiftCertificateScreen'},
  {label: 'SurveysScreen', value: 'SurveysScreen'},
  {label: 'LocationsScreen', value: 'LocationsScreen'},
  {label: 'StampcardScreen', value: 'StampcardScreen'},
  {label: 'FaqScreen', value: 'FaqScreen'},
  {label: 'FeedbackScreen', value: 'FeedbackScreen'},
  {label: 'VouchersScreen', value: 'VouchersScreen'},
  {label: 'VoucherDetailScreen', value: 'VoucherDetailScreen'},
  {label: 'OffersScreen', value: 'OffersScreen'},
  {label: 'EnquiriesScreen', value: 'EnquiriesScreen'},
  {label: 'LegalsScreen', value: 'LegalsScreen'},
  {label: 'HomeScreen', value: 'HomeScreen'},
  {label: 'WebviewScreen', value: 'WebviewScreen'},
  {label: 'YourorderScreen', value: 'YourorderScreen'},
  {label: 'ResetPasswordScreen', value: 'ResetPasswordScreen'},
  {label: 'ChangeEmailScreen', value: 'ChangeEmailScreen'},
  {label: 'SurveyDetailsScreen', value: 'SurveyDetailsScreen'},
  {label: 'OurTeamScreen', value: 'OurTeamScreen'},
  {label: 'TicketDetailScreen', value: 'TicketDetailScreen'},
  {label: 'FavoriteDetailScreen', value: 'FavoriteDetailScreen'},
  {label: 'ProductListScreen', value: 'ProductListScreen'},
  {label: 'BuyGiftCertificateScreen', value: 'BuyGiftCertificateScreen'},
  {label: 'MatchAccountScreen', value: 'MatchAccountScreen'},
  {label: 'MatchAccountInfo', value: 'MatchAccountInfo'},
  {label: 'GamingScreen', value: 'GamingScreen'},
  {label: 'ShopsScreen', value: 'ShopsScreen'},
  {label: 'MembershipScreen', value: 'MembershipScreen'},
  {label: 'CameraScreen', value: 'CameraScreen'},
  {label: 'BarcodeScreen', value: 'BarcodeScreen'},
  {label: 'CommunityPointScreen', value: 'CommunityPointScreen'},
  {label: 'PondHoppersScreen', value: 'PondHoppersScreen'},
]

export const SETTING_SIDE_BUTTON_BEFORE_LOGIN = [
  // {label: '0 Buttons', value: '0'},
  {label: '1 Buttons', value: '1'},
  {label: '2 Buttons', value: '2'},
  {label: '3 Buttons', value: '3'},
  {label: '4 Buttons', value: '4'},
  {label: '5 Buttons', value: '5'},
  {label: '6 Buttons', value: '6'},
  {label: '7 Buttons', value: '7'},
  {label: '8 Buttons', value: '8'},
  {label: '9 Buttons', value: '9'},
  {label: '10 Buttons', value: '10'},
]

export const SETTING_TABS_BUTTON = [
  {label: '3 Buttons', value: '3'},
  {label: '4 Buttons', value: '4'},
  {label: '5 Buttons', value: '5'},
]

export const SETTING_SIDE_BUTTON = [
  // {label: '0 Buttons', value: '0'},
  // {label: '1 Buttons', value: '1'},
  {label: '2 Buttons', value: '2'},
  {label: '3 Buttons', value: '3'},
  {label: '4 Buttons', value: '4'},
  {label: '5 Buttons', value: '5'},
  {label: '6 Buttons', value: '6'},
  {label: '7 Buttons', value: '7'},
  {label: '8 Buttons', value: '8'},
  {label: '9 Buttons', value: '9'},
  {label: '10 Buttons', value: '10'},
  {label: '11 Buttons', value: '11'},
  {label: '12 Buttons', value: '12'},
  {label: '10 Buttons', value: '13'},
  {label: '14 Buttons', value: '14'},
  {label: '15 Buttons', value: '15'},
  {label: '16 Buttons', value: '16'},
  {label: '17 Buttons', value: '17'},
  {label: '18 Buttons', value: '18'},
  {label: '19 Buttons', value: '19'},
  {label: '20 Buttons', value: '20'},
]

export const PROFILE_BUTTON = [
  {label: '0 Buttons', value: '0'},
  {label: '1 Buttons', value: '1'},
  {label: '2 Buttons', value: '2'},
  {label: '3 Buttons', value: '3'},
  {label: '4 Buttons', value: '4'},
  {label: '5 Buttons', value: '5'},
  {label: '6 Buttons', value: '6'},
  {label: '7 Buttons', value: '7'},
  {label: '8 Buttons', value: '8'},
  {label: '9 Buttons', value: '9'},
  {label: '10 Buttons', value: '10'},
  {label: '11 Buttons', value: '11'},
  {label: '12 Buttons', value: '12'},
  {label: '10 Buttons', value: '13'},
  {label: '14 Buttons', value: '14'},
  {label: '15 Buttons', value: '15'},
  {label: '16 Buttons', value: '16'},
  {label: '17 Buttons', value: '17'},
  {label: '18 Buttons', value: '18'},
  {label: '19 Buttons', value: '19'},
  {label: '20 Buttons', value: '20'},
]

export const FIRST_TIME_SIGN_IN = [
  {label: '2 SLots', value: '2'},
  {label: '3 SLots', value: '3'},
  {label: '4 SLots', value: '4'},
  {label: '5 SLots', value: '5'},
  {label: '6 SLots', value: '6'},
  {label: '7 SLots', value: '7'},
  {label: '8 SLots', value: '8'},
  {label: '9 SLots', value: '9'},
  {label: '10 SLots', value: '10'},
]

export const SETTING_MAP_LABEL = {
  ticket: 'Ticket',
  refer_a_friend: 'Refer A Friend',
  transaction: 'Transaction',
  gift_certificate: 'Gift Certificate',
  stripe_integration_show: 'Stripe',
  gaming_integration_show: 'Gaming',
  third_party_integration_show: 'Third Party',
  pond_hoppers_enable: 'Pond Hoppers',
  burst_sms_integration_show: 'Burst SMS',
}

export const SETTING_STRIPE_FEE_OPTION = [
  {value: 'fixed', label: 'Fixed'},
  {value: 'dynamic', label: 'Dynamic'},
]

export const SETTING_LISTING_CATEGORY = [
  {value: 'information', label: 'Information'},
  {value: 'promotion', label: 'Promotion'},
  {value: 'other', label: 'Other'},
]

export const SETTING_CARD_TRACK = [
  {value: 'track_1', label: 'Track 1(%)'},
  {value: 'track_2', label: 'Track 2(;)'},
  {value: 'track_3', label: 'Track 3(+)'},
  {value: 'no_track', label: 'No Track()'},
]

export const FIELD_TYPE = [
  {
    value: 'Text',
    label: 'Text',
    code: 3,
  },
  {
    value: 'Number',
    label: 'Number',
    code: 2,
  },
  {
    value: 'Date',
    label: 'Date',
    code: 1,
  },
  {
    value: 'Dropdown',
    label: 'Dropdown',
    code: 3,
  },
  {
    value: 'Checkbox',
    label: 'Checkbox',
    code: 0,
  },
  {
    value: 'Toggle',
    label: 'Toggle',
    code: 0,
  },
  {
    value: 'Venue',
    label: 'Venue',
    code: 3,
  },
  {
    value: 'VenueTag',
    label: 'Venue Tag',
    code: 3,
  },
  {
    value: 'Password',
    label: 'Password',
    code: 3,
  },
  {
    value: 'Tier',
    label: 'Tier',
    code: 3,
  },
]

export const FIELD_NAME = {
  0: [
    {value: 1, label: 'CFlag1', type: 'bool'},
    {value: 2, label: 'CFlag2', type: 'bool'},
    {value: 3, label: 'CFlag3', type: 'bool'},
    {value: 4, label: 'CFlag4', type: 'bool'},
    {value: 5, label: 'CFlag5', type: 'bool'},
    {value: 6, label: 'CFlag6', type: 'bool'},
    {value: 7, label: 'CFlag7', type: 'bool'},
    {value: 8, label: 'CFlag8', type: 'bool'},
    {value: 9, label: 'CFlag9', type: 'bool'},
    {value: 10, label: 'CFlag10', type: 'bool'},
    {value: 11, label: 'DoNotPost', type: 'bool', param: 'do_not_post'},
    {value: 12, label: 'DoNotEmail', type: 'bool', param: 'do_not_email'},
    {value: 13, label: 'DoNotSMS', type: 'bool', param: 'do_not_sms'},
    {value: 14, label: 'DoNotPhone', type: 'bool', param: 'do_not_phone'},
  ],
  1: [
    {value: 101, label: 'CDate1', type: 'date'},
    {value: 102, label: 'CDate2', type: 'date'},
    {value: 103, label: 'CDate3', type: 'date'},
    {value: 104, label: 'CDate4', type: 'date'},
    {value: 105, label: 'CDate5', type: 'date'},
    {value: 106, label: 'DateBirth', type: 'date', param: 'dob'},
  ],
  2: [
    {value: 201, label: 'CNum1', type: 'number'},
    {value: 202, label: 'CNum2', type: 'number'},
    {value: 203, label: 'CNum3', type: 'number'},
    {value: 204, label: 'CNum4', type: 'number'},
    {value: 205, label: 'CNum5', type: 'number'},
  ],
  3: [
    {value: 301, label: 'CText1', type: 'text'},
    {value: 302, label: 'CText2', type: 'text'},
    {value: 303, label: 'CText3', type: 'text'},
    {value: 304, label: 'CText4', type: 'text'},
    {value: 305, label: 'CText5', type: 'text'},
    {value: 306, label: 'CText6', type: 'text'},
    {value: 307, label: 'CText7', type: 'text'},
    {value: 308, label: 'CText8', type: 'text'},
    {value: 309, label: 'CText9', type: 'text'},
    {value: 310, label: 'CText10', type: 'text'},
    {value: 311, label: 'CText11', type: 'text'},
    {value: 312, label: 'CText12', type: 'text'},
    {value: 313, label: 'CText13', type: 'text'},
    {value: 314, label: 'CText14', type: 'text'},
    {value: 315, label: 'CText15', type: 'text'},
    {value: 316, label: 'CText16', type: 'text'},
    {value: 317, label: 'CText17', type: 'text'},
    {value: 318, label: 'CText18', type: 'text'},
    {value: 319, label: 'CText19', type: 'text'},
    {value: 320, label: 'CText20', type: 'text'},
    {value: 321, label: 'Title', type: 'text', param: 'title'},
    {value: 322, label: 'FirstName', type: 'text', param: 'first_name'},
    {value: 323, label: 'LastName', type: 'text', param: 'last_name'},
    {value: 324, label: 'Street1', type: 'text', param: 'street'},
    {value: 325, label: 'City', type: 'text', param: 'city'},
    {value: 326, label: 'State', type: 'text', param: 'state'},
    {value: 327, label: 'PCode', type: 'text', param: 'post_code'},
    {value: 328, label: 'Mobile', type: 'text', param: 'mobile'},
    {value: 329, label: 'Email1st', type: 'text', param: 'email'},
    {value: 330, label: 'Gender', type: 'text', param: 'gender'},
    {value: 331, label: 'Tier', type: 'text', param: 'tier_id'},
    {value: '', label: 'not send cfield', type: 'text', param: ''},
  ],
}

// export const FIELD_NAME = {
//   Checkbox: [
//     {value: 1, label: 'CFlag1', type: 'bool'},
//     {value: 2, label: 'CFlag2', type: 'bool'},
//     {value: 3, label: 'CFlag3', type: 'bool'},
//     {value: 4, label: 'CFlag4', type: 'bool'},
//     {value: 5, label: 'CFlag5', type: 'bool'},
//     {value: 6, label: 'CFlag6', type: 'bool'},
//     {value: 7, label: 'CFlag7', type: 'bool'},
//     {value: 8, label: 'CFlag8', type: 'bool'},
//     {value: 9, label: 'CFlag9', type: 'bool'},
//     {value: 10, label: 'CFlag10', type: 'bool'},
//     {value: 11, label: 'DoNotPost', type: 'bool', param: 'do_not_post'},
//     {value: 12, label: 'DoNotEmail', type: 'bool', param: 'do_not_email'},
//     {value: 13, label: 'DoNotSMS', type: 'bool', param: 'do_not_sms'},
//     {value: 14, label: 'DoNotPhone', type: 'bool', param: 'do_not_phone'},
//   ],
//   Toggle: [
//     {value: 1, label: 'CFlag1', type: 'bool'},
//     {value: 2, label: 'CFlag2', type: 'bool'},
//     {value: 3, label: 'CFlag3', type: 'bool'},
//     {value: 4, label: 'CFlag4', type: 'bool'},
//     {value: 5, label: 'CFlag5', type: 'bool'},
//     {value: 6, label: 'CFlag6', type: 'bool'},
//     {value: 7, label: 'CFlag7', type: 'bool'},
//     {value: 8, label: 'CFlag8', type: 'bool'},
//     {value: 9, label: 'CFlag9', type: 'bool'},
//     {value: 10, label: 'CFlag10', type: 'bool'},
//     {value: 11, label: 'DoNotPost', type: 'bool', param: 'do_not_post'},
//     {value: 12, label: 'DoNotEmail', type: 'bool', param: 'do_not_email'},
//     {value: 13, label: 'DoNotSMS', type: 'bool', param: 'do_not_sms'},
//     {value: 14, label: 'DoNotPhone', type: 'bool', param: 'do_not_phone'},
//   ],
//   Date: [
//     {value: 101, label: 'CDate1', type: 'date'},
//     {value: 102, label: 'CDate2', type: 'date'},
//     {value: 103, label: 'CDate3', type: 'date'},
//     {value: 104, label: 'CDate4', type: 'date'},
//     {value: 105, label: 'CDate5', type: 'date'},
//     {value: 106, label: 'DateBirth', type: 'date', param: 'dob'},
//   ],
//   Number: [
//     {value: 201, label: 'CNum1', type: 'number'},
//     {value: 202, label: 'CNum2', type: 'number'},
//     {value: 203, label: 'CNum3', type: 'number'},
//     {value: 204, label: 'CNum4', type: 'number'},
//     {value: 205, label: 'CNum5', type: 'number'},
//   ],
//   Dropdown: [
//     {value: 301, label: 'CText1', type: 'text'},
//     {value: 302, label: 'CText2', type: 'text'},
//     {value: 303, label: 'CText3', type: 'text'},
//     {value: 304, label: 'CText4', type: 'text'},
//     {value: 305, label: 'CText5', type: 'text'},
//     {value: 306, label: 'CText6', type: 'text'},
//     {value: 307, label: 'CText7', type: 'text'},
//     {value: 308, label: 'CText8', type: 'text'},
//     {value: 309, label: 'CText9', type: 'text'},
//     {value: 310, label: 'CText10', type: 'text'},
//     {value: 311, label: 'CText11', type: 'text'},
//     {value: 312, label: 'CText12', type: 'text'},
//     {value: 313, label: 'CText13', type: 'text'},
//     {value: 314, label: 'CText14', type: 'text'},
//     {value: 315, label: 'CText15', type: 'text'},
//     {value: 316, label: 'CText16', type: 'text'},
//     {value: 317, label: 'CText17', type: 'text'},
//     {value: 318, label: 'CText18', type: 'text'},
//     {value: 319, label: 'CText19', type: 'text'},
//     {value: 320, label: 'CText20', type: 'text'},
//     {value: 321, label: 'Title', type: 'text', param: 'title'},
//     {value: 322, label: 'FirstName', type: 'text', param: 'first_name'},
//     {value: 323, label: 'LastName', type: 'text', param: 'last_name'},
//     {value: 324, label: 'Street1', type: 'text', param: 'street'},
//     {value: 325, label: 'City', type: 'text', param: 'city'},
//     {value: 326, label: 'State', type: 'text', param: 'state'},
//     {value: 327, label: 'PCode', type: 'text', param: 'post_code'},
//     {value: 328, label: 'Mobile', type: 'text', param: 'mobile'},
//     {value: 329, label: 'Email1st', type: 'text', param: 'email'},
//     {value: 330, label: 'Gender', type: 'text', param: 'gender'},
//     {value: 331, label: 'Password', type: 'text', param: 'password'},
//     {value: 332, label: 'Tier', type: 'text', param: 'tier'},
//   ],
//   Text: [
//     {value: 301, label: 'CText1', type: 'text'},
//     {value: 302, label: 'CText2', type: 'text'},
//     {value: 303, label: 'CText3', type: 'text'},
//     {value: 304, label: 'CText4', type: 'text'},
//     {value: 305, label: 'CText5', type: 'text'},
//     {value: 306, label: 'CText6', type: 'text'},
//     {value: 307, label: 'CText7', type: 'text'},
//     {value: 308, label: 'CText8', type: 'text'},
//     {value: 309, label: 'CText9', type: 'text'},
//     {value: 310, label: 'CText10', type: 'text'},
//     {value: 311, label: 'CText11', type: 'text'},
//     {value: 312, label: 'CText12', type: 'text'},
//     {value: 313, label: 'CText13', type: 'text'},
//     {value: 314, label: 'CText14', type: 'text'},
//     {value: 315, label: 'CText15', type: 'text'},
//     {value: 316, label: 'CText16', type: 'text'},
//     {value: 317, label: 'CText17', type: 'text'},
//     {value: 318, label: 'CText18', type: 'text'},
//     {value: 319, label: 'CText19', type: 'text'},
//     {value: 320, label: 'CText20', type: 'text'},
//     {value: 321, label: 'Title', type: 'text', param: 'title'},
//     {value: 322, label: 'FirstName', type: 'text', param: 'first_name'},
//     {value: 323, label: 'LastName', type: 'text', param: 'last_name'},
//     {value: 324, label: 'Street1', type: 'text', param: 'street'},
//     {value: 325, label: 'City', type: 'text', param: 'city'},
//     {value: 326, label: 'State', type: 'text', param: 'state'},
//     {value: 327, label: 'PCode', type: 'text', param: 'post_code'},
//     {value: 328, label: 'Mobile', type: 'text', param: 'mobile'},
//     {value: 329, label: 'Email1st', type: 'text', param: 'email'},
//     {value: 330, label: 'Gender', type: 'text', param: 'gender'},
//     {value: 331, label: 'Password', type: 'text', param: 'password'},
//     {value: 332, label: 'Tier', type: 'text', param: 'tier'},
//   ],
// }

export const KEYBOARD_TYPE = [
  {value: 'default', label: 'Text'},
  {value: 'number-pad', label: 'Number'},
  {value: 'decimal-pad', label: 'Decimal'},
  {value: 'numeric', label: 'Numeric'},
  {value: 'email-address', label: 'Email'},
  {value: 'phone-pad', label: 'Phone'},
]

export const PRODUCT_TYPE = [
  {
    value: 'product',
    label: 'Product',
  },
]

export const PRICE_OPTION = [
  {
    value: 'dollar_price',
    label: 'Dollar Price',
  },
  {
    value: 'point_price',
    label: 'Point Price',
  },
  {
    value: 'free',
    label: 'Free',
  },
]

export const TIERS_EXPIRY_OPTIONS = [
  {value: 'never_expired', label: 'Never Expired'},
  {value: 'annual', label: 'Annually'},
  {value: 'months', label: 'Months'},
  {value: 'days', label: 'Days'},
  {value: 'specific_date', label: 'Specific Date'},
]

export const MEMBER_DEFAULT_TIER = [
  {value: 'no_default', label: 'No Default'},
  {value: 'by_venue', label: 'By Venue'},
  {value: 'by_venue_tag', label: 'By Venue Tag/State'},
  {value: 'specific_tier', label: 'Specific Tier'},
]

export const APP_FLAG_BEPOZ = [
  {value: false, label: 'Disable'},
  {value: 1, label: 'CFlag1'},
  {value: 2, label: 'CFlag2'},
  {value: 3, label: 'CFlag3'},
  {value: 4, label: 'CFlag4'},
  {value: 5, label: 'CFlag5'},
  {value: 6, label: 'CFlag6'},
  {value: 7, label: 'CFlag7'},
  {value: 8, label: 'CFlag8'},
  {value: 9, label: 'CFlag9'},
  {value: 10, label: 'CFlag10'},
]

export const COMMUNITY_OPTIONS = [
  {value: 'text', label: 'Text'},
  {value: 'totalPledgedToDate', label: 'Total Pledged To Date'},
]

export const POND_HOPPERS_FEATURES = [
  {value: 'pledge', label: 'Pledge'},
  {value: 'nominate', label: 'Nominate'},
  {value: 'perks', label: 'Perks'},
  {value: 'contact', label: 'Contact'},
  {value: 'impact', label: 'Impact'},
  {value: 'ourstory', label: 'Ourstory'},
  {value: 'faqs', label: 'Faqs'},
]
