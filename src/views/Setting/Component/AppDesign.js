import React, {useState} from 'react'
import {CButton, CCol, CNav, CNavItem, CNavLink, CRow, CTabContent, CTabPane, CTabs} from '@coreui/react'
import StatusToggle from '../../../common/StatusToggle'
import FormSelect from '../../../common/FormSelect'
import FormCheckbox from '../../../common/FormCheckbox'
import Collapse from '../../../common/Collapse'
import Layout from './components/Layout'
import FormInput from '../../../common/FormInput'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import {SETTING_ON_OFF, APP_LAYOUT, INPUT_FIELDS_STYLE} from '../../Setting/constant'
import {isTrue} from 'src/helpers'

const AppDesign = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    use_legacy_design: setting.use_legacy_design, // boolean
    form_input_style: setting.form_input_style, // rounded or rectangle
    display_venue_selection: setting.display_venue_selection, //boolean
    welcome_instruction_enable: setting.welcome_instruction_enable, //boolean
    side_menu_show_profile: setting.side_menu_show_profile, // boolean
    app_name: setting.app_name,
    app_default_website: setting.app_default_website,
    app_is_show_logo: setting.app_is_show_logo,
    app_account_match: setting.app_account_match,
    app_is_show_star_bar: setting.app_is_show_star_bar,
    app_show_tier_below_name: setting.app_show_tier_below_name,
    app_show_points_below_name: setting.app_show_points_below_name,
    app_is_show_preferred_venue: setting.app_is_show_preferred_venue,
    app_show_tier_name: setting.app_show_tier_name,
    app_on_boarding_enable: setting.app_on_boarding_enable,
    app_is_group_filter: setting.app_is_group_filter,
    app_is_component_shadowed: setting.app_is_component_shadowed,
    app_sub_header_align_left: setting.app_sub_header_align_left,
  })

  console.log('setting', state.app_is_show_preferred_venue.value)

  const changeValue = (keys) => (value) => {
    setState((prevState) => ({...prevState, [keys]: {...prevState[keys], value}}))
  }

  const clickSave = () => {
    dispatch(updateAppSettings({settings: JSON.stringify([...Object.values(state)])}))
  }

  return (
    <Collapse title="App Design">
      <CRow>
        <CCol>
          <Layout contentWidth={4} title="App Layout">
            <FormSelect
              options={APP_LAYOUT}
              value={state.use_legacy_design.value}
              onValueChange={changeValue('use_legacy_design')}
            />
          </Layout>

          <Layout contentWidth={4} title="Input Fields & Buttons Style">
            <FormSelect
              options={INPUT_FIELDS_STYLE}
              value={state.form_input_style.value}
              onValueChange={changeValue('form_input_style')}
            />
          </Layout>

          <Layout contentWidth={4} title="Display Venue Selector">
            <FormSelect
              options={SETTING_ON_OFF}
              value={state.display_venue_selection.value}
              onValueChange={changeValue('display_venue_selection')}
            />
          </Layout>

          <Layout contentWidth={4} title="Welcome Instructions">
            <FormSelect
              options={SETTING_ON_OFF}
              value={state.welcome_instruction_enable.value}
              onValueChange={changeValue('welcome_instruction_enable')}
            />
          </Layout>

          <Layout contentWidth={4} title="Show Profile on Side Drawer">
            <FormSelect
              options={SETTING_ON_OFF}
              value={state.side_menu_show_profile.value}
              onValueChange={changeValue('side_menu_show_profile')}
            />
          </Layout>
        </CCol>
        <CCol>
          <Layout contentWidth={4} title="app name">
            <FormInput value={state.app_name.value} onChangeText={changeValue('app_name')} />
          </Layout>

          <Layout contentWidth={4} title="app default website" desc="ex:https://www.vectron.com.au">
            <FormInput
              value={state.app_default_website.value}
              onChangeText={changeValue('app_default_website')}
            />
          </Layout>

          <Layout contentWidth={4} title="show logo on background image" desc="for splash and first screen">
            <FormCheckbox
              checked={state.app_is_show_logo.value}
              onValueChange={changeValue('app_is_show_logo')}
            />
          </Layout>

          <Layout contentWidth={4} title="enable account matching" desc="for signing up">
            <FormCheckbox
              checked={state.app_account_match.value}
              onValueChange={changeValue('app_account_match')}
            />
          </Layout>

          <Layout contentWidth={4} title="enable on boarding" desc="app welcoming feature">
            <FormCheckbox
              checked={state.app_on_boarding_enable.value}
              onValueChange={changeValue('app_on_boarding_enable')}
            />
          </Layout>

          <Layout contentWidth={4} title="group venues on listing filter">
            <FormCheckbox
              checked={state.app_is_group_filter.value}
              onValueChange={changeValue('app_is_group_filter')}
            />
          </Layout>

          <Layout contentWidth={4} title="align sub header title to left">
            <FormCheckbox
              checked={state.app_sub_header_align_left.value}
              onValueChange={changeValue('app_sub_header_align_left')}
            />
          </Layout>

          {isTrue(state.use_legacy_design.value) && (
            <Layout contentWidth={4} title="show star bar on home page">
              <FormCheckbox
                checked={state.app_is_show_star_bar.value}
                onValueChange={changeValue('app_is_show_star_bar')}
              />
            </Layout>
          )}

          {isTrue(state.use_legacy_design.value) && (
            <Layout contentWidth={4} title="show tier text below name on home page">
              <FormCheckbox
                checked={state.app_show_tier_below_name.value}
                onValueChange={changeValue('app_show_tier_below_name')}
              />
            </Layout>
          )}

          {isTrue(state.use_legacy_design.value) && (
            <Layout contentWidth={4} title="show points text below name on home page">
              <FormCheckbox
                checked={state.app_show_points_below_name.value}
                onValueChange={changeValue('app_show_points_below_name')}
              />
            </Layout>
          )}

          {isTrue(state.use_legacy_design.value) && (
            <Layout
              contentWidth={4}
              title="show venues selector on home page"
              desc="untick this will enable tier bar instead">
              <FormCheckbox
                checked={state.app_is_show_preferred_venue.value}
                onValueChange={changeValue('app_is_show_preferred_venue')}
              />
            </Layout>
          )}

          {isTrue(state.use_legacy_design.value) && !isTrue(state.app_is_show_preferred_venue.value) && (
            <Layout contentWidth={4} title="show tier name on tier bar">
              <FormCheckbox
                checked={state.app_show_tier_name.value}
                onValueChange={changeValue('app_show_tier_name')}
              />
            </Layout>
          )}

          {isTrue(state.use_legacy_design.value) && (
            <Layout contentWidth={4} title="shadowed home components">
              <FormCheckbox
                checked={state.app_is_component_shadowed.value}
                onValueChange={changeValue('app_is_component_shadowed')}
              />
            </Layout>
          )}
        </CCol>
      </CRow>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </Collapse>
  )
}

export default AppDesign
