import Collapse from '../../../common/Collapse'
import {CButton, CCol, CDataTable, CRow} from '@coreui/react'
import React, {useState} from 'react'
import {SETTING_DAY_OF_WEEK, SETTING_MULTIPLE, SETTING_ON_OFF, SETTING_SIGN_UP_REWARD} from '../constant'
import FormInput from '../../../common/FormInput'
import FormSelect from '../../../common/FormSelect'
import VoucherSetupSelect from '../../../common/VoucherSetupSelect'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import Layout from './components/Layout';

const fields = [{key: 'day', label: 'Day of Week'}, 'multiple']

const PointRatioAndRewards = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    reward_after_signup_option: setting.reward_after_signup_option,
    reward_after_signup_type: setting.reward_after_signup_type,
    reward_after_signup_point: setting.reward_after_signup_point,
    facebook_point_reward: 0,
    google_point_reward: 0,
    // facebook_point_reward: setting.facebook_point_reward,
    // google_point_reward: setting.google_point_reward,
    reward_after_signup_voucher_setups_id: setting.reward_after_signup_voucher_setups_id,
    redeem_point_ratio: setting.redeem_point_ratio,
    reward_point_ratio: setting.reward_point_ratio,
    reward_multiply_rules: {
      ...setting.reward_multiply_rules,
      extended_value: JSON.parse(setting.reward_multiply_rules.extended_value),
    },
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const changeMultiple = (value, index) => {
    const newMultiple = [...state.reward_multiply_rules.extended_value]
    newMultiple[index].multiply = value
    setState({...state, reward_multiply_rules: {...state.reward_multiply_rules, extended_value: newMultiple}})
  }

  return (
    <Collapse title="Point Ratio And Rewards">
      <CRow>
        <CCol xs={12} md={6}>
          <Layout title="Reward After SignUp" desc="Enable this if you want to give a reward">
            <FormSelect
              options={SETTING_ON_OFF}
              value={state.reward_after_signup_option.value}
              onValueChange={changeValue('reward_after_signup_option')}
            />
          </Layout>
          <Layout title="Set SignUp Reward" desc="Choose what kind of reward we offer">
            <FormSelect
              options={SETTING_SIGN_UP_REWARD}
              value={state.reward_after_signup_type.value}
              onValueChange={changeValue('reward_after_signup_type')}
            />
          </Layout>
          {state.reward_after_signup_type.value === 'point' && (
            <Layout title="Set Point Reward" desc="Set point rewards for new member using signup.">
              <FormInput
                value={state.reward_after_signup_point.value}
                onChangeText={changeValue('reward_after_signup_point')}
              />
            </Layout>
          )}
          {state.reward_after_signup_type.value === 'voucher' && (
            <Layout title="Set Reward (Bepoz Voucher Setups)" desc="Bepoz Voucher as a reward">
              <VoucherSetupSelect
                value={state.reward_after_signup_voucher_setups_id.value}
                onValueChange={changeValue('reward_after_signup_voucher_setups_id')}
              />
            </Layout>
          )}
          <Layout title="Redeem Point(s)" desc="100 point(s) -> equals to AUD$ 1">
            <FormInput
              value={state.redeem_point_ratio.value}
              onChangeText={changeValue('redeem_point_ratio')}
            />
          </Layout>
          <Layout title="Reward Point(s)" desc="If members spend AUD$ 1 -> They receive 1 point(s)">
            <FormInput
              value={state.reward_point_ratio.value}
              onChangeText={changeValue('reward_point_ratio')}
            />
          </Layout>
        </CCol>

        <CCol xs={12} md={6}>
          <CDataTable
            items={state?.reward_multiply_rules?.extended_value || []}
            fields={fields}
            striped
            size={'small'}
            scopedSlots={{
              day: (item) => <td>{SETTING_DAY_OF_WEEK[item.dayofweek]}</td>,
              multiple: (item, index) => (
                <td className="p-1">
                  <FormSelect
                    options={SETTING_MULTIPLE}
                    value={item.multiply}
                    onValueChange={(value) => changeMultiple(value, index)}
                  />
                </td>
              ),
            }}
          />
        </CCol>
      </CRow>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </Collapse>
  )
}


export default PointRatioAndRewards
