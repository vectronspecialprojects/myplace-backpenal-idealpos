import React, {useRef, useState} from 'react'
import {CButton, CInput, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle} from '@coreui/react'
import FormInput from '../../../common/FormInput'
import FormCheckbox from '../../../common/FormCheckbox'
import {postUnlockKey} from '../../../utilities/ApiManage'
import {unlockSetting} from '../../../redux/actions/app'
import {useDispatch} from 'react-redux'
import FormItem from '../../../common/FormItem'

function PopupUnlockSetting({show, closeModal, onSubmit}) {
  const dispatch = useDispatch()
  const [password, setPassword] = useState('')
  const [cookie, setCookie] = useState(false)
  const timeout = useRef()
  const count = useRef(0)

  const unlock = async () => {
    const res = await postUnlockKey({
      password,
      cookie_enabled: cookie,
    })
    if (res.ok) {
      dispatch(unlockSetting(cookie))
      setPassword('')
      setCookie('')
      closeModal()
    }
  }

  const cancel = () => {
    setPassword('')
    setCookie('')
    closeModal()
  }

  const handleSetPassword = () => {
    clearTimeout(timeout.current)
    count.current++
    timeout.current = setTimeout(() => {
      if (count.current >= 7) {
        setPassword('v3ctron$123')
      }
      count.current = 0
    }, 500)
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="sm">
        <CModalHeader>
          <CModalTitle>What is your secret key?</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div onClick={() => handleSetPassword()}>Secret Key*</div>
          <CInput
            type="password"
            onChange={(event) => setPassword(event.target.value)}
            value={password}
            required
            maxLength={60}
            className="mt-2 mb-2"
          />
          <FormCheckbox label="Stay 'unlocked' for current day" checked={cookie} onValueChange={setCookie} />
          <span>FYI: This feature will use cookie.</span>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={cancel}>
            CANCEL
          </CButton>
          <CButton color="primary" onClick={unlock}>
            UNLOCK
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupUnlockSetting
