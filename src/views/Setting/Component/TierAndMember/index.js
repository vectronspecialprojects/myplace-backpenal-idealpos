import Collapse from '../../../../common/Collapse'
import {CNav, CNavItem, CNavLink, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import Tier from './Tier'
import Flags from './Flags'
import {useSelector} from 'react-redux'
import SignupField from './SignupField'
import ImportMember from './ImportMember'
import {getBepozCustomFieldsMetaGet} from '../../../../utilities/ApiManage'

const TierAndMember = ({setting, bepozVersion}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const [tab, setTab] = useState(0)
  const [bepozMetaData, setBepozMetaData] = useState([])

  useEffect(() => {
    ;(async () => {
      const res = await getBepozCustomFieldsMetaGet()
      if (res.ok) {
        const data = res?.data?.ArrayOfCustomFieldMeta?.CustomFieldMeta.sort(
          (a, b) => +a.FieldType - +b.FieldType,
        )
        setBepozMetaData(data)
      }
    })()
  }, [])

  return (
    <Collapse title="Sign Up ,Tiers and Membership Setup" onOpen={() => setTab(0)}>
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>Tiers</CNavLink>
          </CNavItem>
          {unlock && (
            <CNavItem>
              <CNavLink>Signup Field</CNavLink>
            </CNavItem>
          )}
          {/* {unlock && (
            <CNavItem>
              <CNavLink>MEMBER</CNavLink>
            </CNavItem>
          )} */}
          <CNavItem>
            <CNavLink>Flags</CNavLink>
          </CNavItem>
          {unlock && (
            <CNavItem>
              <CNavLink>Import Members</CNavLink>
            </CNavItem>
          )}
        </CNav>

        <CTabContent style={{marginTop: 20}}>
          <CTabPane>
            <Tier bepozMetaData={bepozMetaData} setting={setting} bepozVersion={bepozVersion} />
          </CTabPane>
          {unlock && (
            <CTabPane>
              <SignupField bepozMetaData={bepozMetaData} bepozVersion={bepozVersion} />
            </CTabPane>
          )}
          {/* {unlock && (
            <CTabPane>
              <Member setting={setting} />
            </CTabPane>
          )} */}
          <CTabPane>
            <Flags setting={setting} bepozMetaData={bepozMetaData} bepozVersion={bepozVersion} />
          </CTabPane>
          {unlock && (
            <CTabPane>
              <ImportMember />
            </CTabPane>
          )}
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}
export default TierAndMember
