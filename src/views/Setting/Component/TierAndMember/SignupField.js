import React, {useState, useEffect, useMemo} from 'react'
import {
  CButton,
  CLabel,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CDataTable,
  CTooltip,
  CModal,
  CModalBody,
  CModalFooter,
} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import FormCheckbox from '../../../../common/FormCheckbox'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TagInput from '../../../../common/TagInput'
import FormSelect from '../../../../common/FormSelect'
import {FIELD_NAME, FIELD_TYPE, KEYBOARD_TYPE} from '../../constant'
import SortableComponent from '../../../../common/SortableList'
import StatusToggle from '../../../../common/StatusToggle'
import {updateAppSettings} from '../../../../redux/actions/app'
import {useDispatch, useSelector} from 'react-redux'
import {isTrue} from '../../../../helpers'
import {signupFields} from './tableField'
import {setModal} from '../../../../reusable/Modal'

const CustomField = ({bepozVersion, bepozMetaData}) => {
  const dispatch = useDispatch()
  const [show, setShow] = useState(false)
  const tier = useSelector((state) => state.app.tier)
  const settings = useSelector((state) => state.app.appSettings)
  const [state, setState] = useState({
    ...settings.bepozcustomfield,
    extended_value: JSON.parse(settings.bepozcustomfield?.extended_value),
  })

  // console.log('state', state)

  const reset = () => {
    setState({
      ...settings.bepozcustomfield,
      extended_value: JSON.parse(settings.bepozcustomfield?.extended_value),
    })
  }

  const usedCflag = useMemo(() => {
    let usedCflag = []
    tier.forEach((tier) => {
      if (!!tier.precreated_account_custom_flag) usedCflag.push(+tier.precreated_account_custom_flag)
    })

    if (!!+settings?.app_flag_id_bepoz?.value) {
      usedCflag.push(+settings?.app_flag_id_bepoz?.value)
    }

    return usedCflag
  }, [tier, settings])

  // console.log('usedCflag', usedCflag)

  const [isCustomFieldEnabled, setIsCustomFieldEnabled] = useState(settings.bepozcustomfield_enable)

  const handleDelete = (data, index) => {
    const extendedValue = [...state.extended_value]
    const missingDisplayOrder = extendedValue[index].displayOrder
    extendedValue.splice(index, 1)
    const newExtendedValue = extendedValue.map((item) =>
      item.displayOrder > missingDisplayOrder ? {...item, displayOrder: item.displayOrder - 1} : {...item},
    )
    const newData = {
      ...state,
      extended_value: newExtendedValue,
    }
    // newData.extended_value.splice(index, 1)
    setState(newData)
  }

  const handleAddItem = () => {
    const newData = {...state}
    newData.extended_value.push({
      bepozFieldName: '',
      bepozFieldNum: '',
      defaultValue: '',
      displayOrder: state.extended_value.length + 1,
      displayInApp: true,
      fieldDisplayTitle: '',
      fieldName: '',
      fieldType: '',
      id: '',
      keyboardType: 'default',
      maxLength: '',
      minLength: '',
      multiValues: [],
      required: false,
      shownOnProfile: true,
      toggle_value: false,
    })
    setState(newData)
  }

  const onOrderChange = (e) => {
    const newExtendedValue = e.map((item, index) => ({...item, displayOrder: index + 1}))
    setState((prevState) => ({...prevState, extended_value: newExtendedValue}))
  }

  const changeValue = (key, index) => (value) => {
    const newData = {...state, extended_value: [...state.extended_value]}
    newData.extended_value[index][key] = value
    setState(newData)
  }

  const customFieldEnabledHandle = () => (value) => {
    setIsCustomFieldEnabled((prevState) => ({...prevState, value: value.toString()}))
  }

  const handleSave = () => {
    let fieldNum = []
    state.extended_value.forEach((item) => {
      if (item.bepozFieldNum !== '') fieldNum.push(item.bepozFieldNum)
    })
    if (new Set(fieldNum).size !== fieldNum.length) {
      setModal({
        title: `Duplicate Bepoz field Name!`,
        message: `Bepoz Field Name cannot be duplicated as they will be sent to bepoz custom fields`,
        primaryBtnClick: () => {
          setModal({show: false})
        },
        isShowSecondaryBtn: false,
        closeButton: false,
      })
      return
    }
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([state, isCustomFieldEnabled]),
      }),
    )
  }

  return (
    <>
      <CRow className="mb-2 ml-1">
        <div className="row pl-3">
          <span>Send Signup Field To Bepoz</span>
          <StatusToggle
            color="primary"
            disabled={!bepozVersion}
            active={isTrue(isCustomFieldEnabled.value)}
            className="ml-3 mr-3"
            onChange={customFieldEnabledHandle()}
          />
        </div>
      </CRow>
      <CRow className="mb-2 ml-1">
        <h4>Signup Field Setting</h4>
        <CButton
          disabled={!bepozVersion}
          className="border ml-2"
          onClick={() => {
            handleAddItem()
          }}>
          +
        </CButton>
        <CIcon
          content={freeSet.cilListNumbered}
          height="30"
          color={'red'}
          alt={'order'}
          onClick={() => {
            if (bepozVersion) {
              setShow(true)
            }
          }}
          style={{marginLeft: 50}}
        />
        <CIcon
          content={freeSet.cilReload}
          height="30"
          color={'red'}
          alt={'go back'}
          onClick={reset}
          style={{marginLeft: 50}}
        />
      </CRow>
      <CDataTable
        items={state.extended_value}
        fields={signupFields}
        striped
        size={'ms'}
        scopedSlots={{
          [signupFields[0].key]: (item, index) => (
            <td className="p-1">
              <FormInput value={item[signupFields[0].key]} disabled />
            </td>
          ),
          [signupFields[1].key]: (item, index) => (
            <td className="p-1">
              <FormInput disabled={true} value={item[signupFields[1].key]} />
            </td>
          ),
          [signupFields[2].key]: (item, index) => {
            return (
              <td className="p-1">
                {item[signupFields[2].key] !== '' && (
                  <FormInput disabled={true} value={item[signupFields[2].key]} />
                )}
              </td>
            )
          },
          [signupFields[3].key]: (item, index) => (
            <td className="p-1">
              <FormSelect
                disabled={!bepozVersion}
                options={FIELD_TYPE}
                value={item[signupFields[3].key]}
                onValueChange={(e) => {
                  changeValue(signupFields[3].key, index)(e)
                  if (e === 'Password') {
                    changeValue(signupFields[1].key, index)('password')
                    changeValue(signupFields[2].key, index)('')
                  }
                  if (e === 'Venue') {
                    changeValue(signupFields[1].key, index)('venue_id')
                    changeValue(signupFields[2].key, index)('')
                  }
                  if (e === 'VenueTag') {
                    changeValue(signupFields[1].key, index)('venue_tag')
                    changeValue(signupFields[2].key, index)('')
                  }
                  if (e === 'Tier') {
                    changeValue(signupFields[4].key, index)('331')
                  } else {
                    changeValue(signupFields[4].key, index)('')
                  }
                }}
              />
            </td>
          ),
          [signupFields[4].key]: (item, index) => {
            let listData = []
            if (item[signupFields[3].key] !== '') {
              const code = FIELD_TYPE.filter((type) => type.value === item[signupFields[3].key])[0].code
              let textFieldNum = []
              if (!!bepozMetaData) {
                bepozMetaData.forEach((data) => {
                  if (+data.FieldType === code) {
                    textFieldNum.push(+data.FieldType * 100 + +data.FieldNum)
                  }
                })
              }

              if (item.fieldType === 'Venue' || item.fieldType === 'VenueTag') {
                listData = FIELD_NAME[code]?.filter((a) => textFieldNum.includes(a.value) || a.value === '')
              } else {
                listData = FIELD_NAME[code]?.filter(
                  (a) =>
                    (textFieldNum.includes(a.value) || typeof a?.param === 'string') &&
                    !usedCflag.includes(a.value),
                )
              }
            }
            return (
              <td className="p-1">
                {item[signupFields[3].key] !== 'Password' && (
                  <FormSelect
                    disabled={
                      item[signupFields[3].key] === 'Password' ||
                      item[signupFields[3].key] === 'Tier' ||
                      !bepozVersion ||
                      item[signupFields[3].key] === ''
                    }
                    options={listData || []}
                    value={+item[signupFields[4].key]}
                    onValueChange={(e) => {
                      console.log('e', e)
                      changeValue(signupFields[4].key, index)(e)
                      if (e === '330') changeValue(signupFields[11].key, index)('0')
                      else changeValue(signupFields[11].key, index)('1')

                      if (+e >= 301 && +e <= 320) changeValue(signupFields[12].key, index)('60')
                      if (+e === 321) changeValue(signupFields[12].key, index)('8')
                      if (+e >= 322 && +e <= 325) changeValue(signupFields[12].key, index)('40')
                      if (+e >= 326 && +e <= 327) changeValue(signupFields[12].key, index)('20')
                      if (+e === 328) changeValue(signupFields[12].key, index)('30')
                      if (+e === 329) changeValue(signupFields[12].key, index)('50')
                      if (+e === 330) changeValue(signupFields[12].key, index)('2')
                      if (+e === 331 || '') changeValue(signupFields[12].key, index)('60')

                      if (e === '') {
                        changeValue('fieldName', index)('')
                      } else {
                        const value = bepozMetaData.filter(
                          (item) =>
                            item.FieldType === Math.floor(+e / 100).toString() &&
                            item.FieldNum === (+e % 100).toString(),
                        )
                        if (value.length === 1) {
                          changeValue('fieldName', index)(value[0]?.FieldName)
                          if (!(item.fieldType === 'Venue' || item.fieldType === 'VenueTag')) {
                            changeValue('id', index)(value[0]?.FieldName.toLowerCase().replace(/\s/g, '_'))
                          }
                        } else {
                          changeValue('fieldName', index)('')
                          if (!(item.fieldType === 'Venue' || item.fieldType === 'VenueTag')) {
                            changeValue('id', index)(listData.filter((item) => item.value == e)[0]?.param)
                          }
                        }
                      }
                    }}
                  />
                )}
              </td>
            )
          },
          [signupFields[5].key]: (item, index) => (
            <td className="p-1">
              <FormInput
                disabled={!bepozVersion}
                value={item[signupFields[5].key]}
                onChangeText={changeValue(signupFields[5].key, index)}
              />
            </td>
          ),
          [signupFields[6].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox
                disabled={!bepozVersion}
                checked={item[signupFields[6].key]}
                onValueChange={(e) => {
                  changeValue(signupFields[6].key, index)(e)
                  if (!e) changeValue(signupFields[7].key, index)(false)
                }}
              />
            </td>
          ),
          [signupFields[7].key]: (item, index) => {
            return (
              <td className="p-1 text-center">
                <FormCheckbox
                  disabled={!bepozVersion || !isTrue(item[signupFields[6].key])}
                  checked={item[signupFields[7].key]}
                  onValueChange={(e) => {
                    changeValue(signupFields[7].key, index)(e)
                    if (!e) changeValue(signupFields[7].key, index)(false)
                  }}
                />
              </td>
            )
          },
          [signupFields[8].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox
                disabled={!bepozVersion}
                checked={item[signupFields[8].key]}
                onValueChange={(e) => {
                  changeValue(signupFields[8].key, index)(e)
                  if (!e) changeValue(signupFields[8].key, index)(false)
                }}
              />
            </td>
          ),
          [signupFields[9].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox
                disabled={!bepozVersion}
                checked={item[signupFields[9].key]}
                onValueChange={(e) => {
                  changeValue(signupFields[9].key, index)(e)
                  if (!e) changeValue(signupFields[9].key, index)(false)
                }}
              />
            </td>
          ),
          [signupFields[10].key]: (item, index) => (
            <td className="p-1">
              {(item[signupFields[3].key] === 'Text' || item[signupFields[3].key] === 'Number') && (
                <FormSelect
                  disabled={!bepozVersion}
                  options={KEYBOARD_TYPE}
                  value={item[signupFields[10].key]}
                  onValueChange={changeValue(signupFields[10].key, index)}
                />
              )}
            </td>
          ),
          [signupFields[11].key]: (item, index) => {
            // console.log('item[signupFields[4].key]', item[signupFields[4].key])
            return (
              <td className="p-1">
                {(item[signupFields[3].key] === 'Text' || item[signupFields[3].key] === 'Number') && (
                  <FormInput
                    disabled={
                      !bepozVersion ||
                      !(item[signupFields[3].key] === 'Text' || item[signupFields[3].key] === 'Number')
                    }
                    type="number"
                    min={item[signupFields[4].key] !== '330' ? '1' : '0'}
                    value={item[signupFields[11].key]}
                    onChangeText={changeValue(signupFields[11].key, index)}
                  />
                )}
              </td>
            )
          },
          [signupFields[12].key]: (item, index) => {
            // console.log('item[signupFields[3].key]', item[signupFields[3].key])
            let max = ''
            if (!!item[signupFields[4].key] && item[signupFields[3].key] === 'Text') {
              const value = +item[signupFields[4].key]
              if (value >= 301 && value <= 320) max = '60'
              if (value === 321) max = '8'
              if (value >= 322 && value <= 325) max = '40'
              if (value >= 326 && value <= 327) max = '20'
              if (value === 328) max = '30'
              if (value === 329) max = '50'
              if (value === 330) max = '2'
            }
            return (
              <td className="p-1">
                {(item[signupFields[3].key] === 'Text' || item[signupFields[3].key] === 'Number') && (
                  <FormInput
                    disabled={
                      !bepozVersion ||
                      !(item[signupFields[3].key] === 'Text' || item[signupFields[3].key] === 'Number')
                    }
                    type="number"
                    min="0"
                    max={max}
                    value={item[signupFields[12].key]}
                    onChangeText={changeValue(signupFields[12].key, index)}
                  />
                )}
              </td>
            )
          },
          [signupFields[13].key]: (item, index) => (
            <td className="p-1">
              {item[signupFields[3].key] === 'Dropdown' && (
                <TagInput
                  disabled={item[signupFields[3].key] !== 'Dropdown' || !bepozVersion}
                  value={item[signupFields[13].key]}
                  onValueChange={changeValue(signupFields[13].key, index)}
                />
              )}
            </td>
          ),
          [signupFields[14].key]: (item, index) => (
            <td className="p-1">
              <CButton disabled={!bepozVersion} onClick={() => handleDelete(item, index)}>
                <CTooltip content="Delete">
                  <CIcon content={freeSet.cilTrash} />
                </CTooltip>
              </CButton>
            </td>
          ),
        }}
      />
      <CButton
        variant="outline"
        disabled={!bepozVersion}
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={handleSave}>
        Save Changes
      </CButton>

      <CModal show={show} onClose={() => setShow(false)} size="xl">
        <CModalBody>
          <CRow>
            <CCol xs="12">
              <CCard>
                <CCardHeader>
                  <CRow className="px-3">
                    <CLabel className="flex-fill m-0 h4">Signup Fields</CLabel>
                    <CButton
                      className="btn btn-secondary"
                      onClick={() => {
                        handleSave()
                        setShow(false)
                      }}>
                      SAVE AND EXIT
                    </CButton>
                  </CRow>
                </CCardHeader>
                <CCardBody>
                  <SortableComponent
                    onChange={onOrderChange}
                    items={state.extended_value.sort((a, b) => a.displayOrder - b.displayOrder) || []}
                    renderItem={(_) => (
                      <CCard className="my-3 p-3 ">
                        <CLabel>{_.fieldDisplayTitle}</CLabel>
                      </CCard>
                    )}
                  />
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter></CModalFooter>
      </CModal>
    </>
  )
}

export default CustomField
