import React, {useState, useEffect, useMemo} from 'react'
import {CCol, CRow, CButton, CDataTable} from '@coreui/react'
import {useSelector, useDispatch} from 'react-redux'
import FormSelect from '../../../../common/FormSelect'
import TagInput from '../../../../common/TagInput'
import FormInput from '../../../../common/FormInput'
import {MEMBER_DEFAULT_TIER, APP_FLAG_BEPOZ, FIELD_NAME} from '../../constant'
import {updateAppSettings} from '../../../../redux/actions/app'
import {getTriggerAllMemberCustomField} from '../../../../utilities/ApiManage'
import Layout from '../components/Layout'
import {venueTagTierFields, venueTierFields} from './tableField'
import {toast} from 'react-toastify'
import {setModal} from '../../../../reusable/Modal'

const Flags = ({setting, bepozMetaData, bepozVersion}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const tiersOptions =
    useSelector((state) => state.app.tier).map((tier) => ({
      value: tier.id,
      label: `${tier.name} - ${tier.bepoz_group_id} - ${tier.bepoz_group_name}`,
    })) || []

  const tiers = useSelector((state) => state.app.tier)
  const venues = useSelector((state) => state.app.venues)
  const tags = useSelector((state) => state.app.tags)
  const activeVenues = venues?.filter((venue) => venue.active === true) || []
  const activeTags = tags?.filter((tag) => tag.status === 1) || []
  const [didMount, setDidMount] = useState(false)
  const [switchTableCheck, setSwitchTableCheck] = useState(setting.member_default_tier.value)
  const [venueExtendedValue, setVenueExtendedValue] = useState([])
  const [tagExtendedValue, setTagExtendedValue] = useState([])
  const [memberTier, setMembertier] = useState({
    ...setting.member_default_tier,
    extended_value: JSON.parse(setting.member_default_tier.extended_value),
  })
  const [state, setState] = useState({
    app_flag_id_bepoz: setting.app_flag_id_bepoz,
    app_flag_name_bepoz: setting.app_flag_name_bepoz,
    default_tier: setting.default_tier,
    email_not_allowed_domains: {
      ...setting.email_not_allowed_domains,
      extended_value: JSON.parse(setting.email_not_allowed_domains.extended_value),
    },
  })

  const cflag = useMemo(() => {
    let usedCflag = []
    JSON.parse(setting.bepozcustomfield.extended_value).forEach((item) => {
      if (item.bepozFieldNum.length >= 1 && item.bepozFieldNum.length <= 2) {
        usedCflag.push(+item.bepozFieldNum)
      }
    })
    tiers.forEach((item) => {
      if (!!+item.precreated_account_custom_flag) {
        usedCflag.push(+item.precreated_account_custom_flag)
      }
    })
    usedCflag = [...new Set(usedCflag)]
    if (!!bepozMetaData) {
      let cflagList = []
      bepozMetaData.forEach((item) => {
        if (item.FieldType === '0') {
          cflagList.push(+item.FieldNum)
        }
      })
      return FIELD_NAME[0]?.filter((a) => cflagList.includes(a.value) && !usedCflag.includes(a.value))
    }
    return []
  }, [bepozMetaData, setting, tiers])

  useEffect(() => {
    if (setting?.member_default_tier?.value === 'by_venue') {
      const extendedVenues = JSON.parse(setting?.member_default_tier?.extended_value) || []
      const activeVenuesId = activeVenues.map((venue) => venue.id) || []
      let newExtendedVenues =
        extendedVenues.filter((extendedVenue) => activeVenuesId.includes(extendedVenue.id)) || []
      const newExtendedVenuesId = newExtendedVenues.map((_) => _.id) || []
      activeVenues.forEach((venue) => {
        if (!newExtendedVenuesId.includes(venue?.id)) {
          newExtendedVenues.push({id: venue.id, name: venue.name, tier: 0})
        }
      })
      setVenueExtendedValue(newExtendedVenues)
      setTagExtendedValue(activeTags.map((tag) => ({id: tag.id, name: tag.name, tier: 0})))
    }

    if (setting?.member_default_tier?.value === 'by_venue_tag') {
      const extendedVenueTags = JSON.parse(setting?.member_default_tier?.extended_value) || []
      const activeVenueTagsId = activeTags.map((tag) => tag.id) || []
      let newExtendedVenueTags =
        extendedVenueTags.filter((extendedVenueTag) => activeVenueTagsId.includes(extendedVenueTag.id)) || []
      const newExtendedVenueTagsId = newExtendedVenueTags.map((_) => _.id) || []
      activeTags.forEach((tag) => {
        if (!newExtendedVenueTagsId.includes(tag?.id)) {
          newExtendedVenueTags.push({id: tag.id, name: tag.name, tier: 0})
        }
      })
      setTagExtendedValue(newExtendedVenueTags)
      setVenueExtendedValue(activeVenues.map((venue) => ({id: venue.id, name: venue.name, tier: 0})))
    }
  }, [setting, venues, tags])

  useEffect(() => {
    setDidMount(true)
  }, [])

  useEffect(() => {
    if (switchTableCheck === 'by_venue' && didMount) changeMemberTier('extended_value')(venueExtendedValue)
    if (switchTableCheck === 'by_venue_tag' && didMount) changeMemberTier('extended_value')(tagExtendedValue)
  }, [switchTableCheck])

  const changeValue = (keys) => (value) => {
    const [key1, key2] = keys.split('.')
    const newObject = {...state[key1], [key2]: value}
    setState((prevState) => ({...prevState, [key1]: newObject}))
  }

  const changeMemberTier = (key) => (value) => {
    if (key === 'value') setSwitchTableCheck(value)
    setMembertier((prevState) => ({...prevState, [key]: value}))
  }

  const changeTierValue = (keys, index) => (value) => {
    let newExtendedValue = []
    let newValue = {}
    if (keys === 'tagExtendedValue') {
      newExtendedValue = [...tagExtendedValue]
      newValue = {...tagExtendedValue[index]}
      newValue.tier = +value
      newExtendedValue[index] = newValue
      setTagExtendedValue(newExtendedValue)
    }
    if (keys === 'venueExtendedValue') {
      newExtendedValue = [...venueExtendedValue]
      newValue = {...venueExtendedValue[index]}
      newValue.tier = +value
      newExtendedValue[index] = newValue
      setVenueExtendedValue(newExtendedValue)
    }
    changeMemberTier('extended_value')(newExtendedValue)
  }

  const errHandler = (title, message) => {
    setModal({
      title,
      message,
      primaryBtnClick: () => {
        setModal({show: false})
      },
      isShowSecondaryBtn: false,
      closeButton: false,
    })
  }

  const handleSave = () => {
    if (
      (memberTier?.value === 'by_venue' || memberTier?.value === 'by_venue_tag') &&
      !!memberTier.extended_value
    ) {
      if (memberTier?.extended_value.filter((value) => value.tier == 0).length > 0) {
        errHandler(
          `Setup wasn't completed`,
          `Default tiers are needed for each ${memberTier?.value === 'by_venue' ? 'venues' : 'venue tags'}`,
        )
        return
      }
    }
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([...Object.values(state), memberTier]),
      }),
    )
  }

  const triggerHandle = async () => {
    const res = await getTriggerAllMemberCustomField()
    if (res.ok) {
      toast('Trigger sent', {type: 'success'})
    } else {
      toast('Trigger Failed', {type: 'error'})
    }
  }

  return (
    <CRow>
      <CCol>
        <Layout contentWidth={4} title="Member Default Tier Options">
          <FormSelect
            options={MEMBER_DEFAULT_TIER}
            value={memberTier.value}
            onValueChange={changeMemberTier('value')}
            disabled={!unlock || !bepozVersion}
          />
        </Layout>

        {memberTier.value === 'specific_tier' && (
          <Layout contentWidth={4} title="Set Default Sign up Tier">
            <FormSelect
              options={tiersOptions}
              value={state.default_tier.value}
              onValueChange={changeValue('default_tier.value')}
              disabled={!unlock || !bepozVersion}
            />
          </Layout>
        )}

        {memberTier.value === 'by_venue_tag' && tiersOptions.length >= 1 && (
          <CDataTable
            items={tagExtendedValue}
            fields={venueTagTierFields}
            striped
            scopedSlots={{
              venue_tags: (item, index) => <td>{item.name}</td>,
              default_tier: (item, index) => {
                return (
                  <td>
                    <FormSelect
                      options={tiersOptions}
                      value={item.tier}
                      onValueChange={changeTierValue('tagExtendedValue', index)}
                      disabled={!unlock || !bepozVersion}
                    />
                  </td>
                )
              },
            }}
          />
        )}

        {memberTier.value === 'by_venue' && tiersOptions.length >= 1 && (
          <CDataTable
            items={venueExtendedValue}
            fields={venueTierFields}
            striped
            scopedSlots={{
              venue: (item, index) => <td>{item.name}</td>,
              default_tier: (item, index) => (
                <td>
                  <FormSelect
                    options={tiersOptions}
                    value={item.tier}
                    onValueChange={changeTierValue('venueExtendedValue', index)}
                    disabled={!unlock || !bepozVersion}
                  />
                </td>
              ),
            }}
          />
        )}

        <Layout title="Email Not Allowed Domains">
          <TagInput
            disabled={!unlock || !bepozVersion}
            value={state.email_not_allowed_domains.extended_value}
            onValueChange={changeValue('email_not_allowed_domains.extended_value')}
          />
        </Layout>
        <CButton
          disabled={!unlock || !bepozVersion}
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={handleSave}>
          Save Changes
        </CButton>
      </CCol>
      <CCol>
        <Layout contentWidth={4} title="Update App Flag in Bepoz">
          <FormSelect
            options={cflag}
            value={state.app_flag_id_bepoz.value}
            onValueChange={(e) => {
              changeValue('app_flag_id_bepoz.value')(e)
              const cname =
                bepozMetaData.filter((_) => _.FieldType == '0' && _.FieldNum == e)[0]?.FieldName || ''
              changeValue('app_flag_name_bepoz.value')(cname)
            }}
            disabled={!unlock || !bepozVersion}
          />
        </Layout>
        <Layout contentWidth={4} title="Custom Field Name">
          <FormInput value={state.app_flag_name_bepoz.value} disabled={true} />
        </Layout>
        <CButton
          disabled={!unlock || !bepozVersion}
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={triggerHandle}>
          TRIGGER ALL MEMBER CUSTOM FIELD
        </CButton>
      </CCol>
    </CRow>
  )
}

export default Flags
