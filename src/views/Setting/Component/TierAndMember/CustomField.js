import React, {useState} from 'react'
import {CButton, CDataTable, CRow, CTooltip} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import FormCheckbox from '../../../../common/FormCheckbox'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TagInput from '../../../../common/TagInput'
import FormSelect from '../../../../common/FormSelect'
import {FIELD_NAME, FIELD_TYPE, SETTING_ON_OFF} from '../../constant'
import SettingLayout from '../SettingLayout'
import {updateAppSettings} from '../../../../redux/actions/app'
import {useDispatch} from 'react-redux'
import {isTrue} from '../../../../helpers'

const fields = [
  {
    key: 'id',
    label: '#',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'fieldName',
    label: 'Field Name',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'fieldType',
    label: 'Field Type',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'bepozFieldNum',
    label: 'Bepoz Field Name',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'fieldDisplayTitle',
    label: 'Field Display Title',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'required',
    label: 'Required',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'active',
    label: 'Active',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'displayInApp',
    label: 'Display In App',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'multiValues',
    label: 'Default Values',
    _style: {fontWeight: 'normal', fontSize: 12, width: '40%'},
  },
  {
    key: 'delete',
    label: 'Delete',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
]
const CustomField = ({settings}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    ...settings.bepozcustomfield,
    extended_value: JSON.parse(settings.bepozcustomfield?.extended_value),
  })

  const handleDelete = (data, index) => {
    const newData = {...state}
    newData.extended_value.splice(index, 1)
    setState(newData)
  }

  const handleAddItem = () => {
    const newData = {...state}
    newData.extended_value.push({
      active: false,
      bepozFieldName: '',
      bepozFieldNum: '',
      defaultValue: '',
      displayInApp: false,
      fieldName: '',
      fieldType: '',
      id: '',
      multiValues: [],
      required: false,
      toggle_value: false,
    })
    setState(newData)
  }

  const changeValue = (key, index) => (value) => {
    const newData = {...state, extended_value: [...state.extended_value]}
    newData.extended_value[index][key] = value
    setState(newData)
  }

  const handleSave = () => {
    let bepozcustomfield_enable = {...settings.bepozcustomfield_enable}
    bepozcustomfield_enable.value = 'false'
    state.extended_value.some((item) => {
      if (isTrue(item.active)) {
        bepozcustomfield_enable.value = 'true'
        return true
      }
    })
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([state, bepozcustomfield_enable]),
      }),
    )
  }

  return (
    <>
      <CRow className="mb-2 ml-1">
        <h4>Bepoz Custom Field Setting</h4>
        <CButton
          className="border ml-2"
          onClick={() => {
            handleAddItem()
          }}>
          +
        </CButton>
      </CRow>
      <CDataTable
        items={state.extended_value}
        fields={fields}
        striped
        size={'ms'}
        scopedSlots={{
          [fields[0].key]: (item, index) => (
            <td className="p-1">
              <FormInput value={item.id} onChangeText={changeValue(item.id, index)} />
            </td>
          ),
          [fields[1].key]: (item, index) => (
            <td className="p-1">
              <FormInput value={item[fields[1].key]} onChangeText={changeValue(fields[1].key, index)} />
            </td>
          ),
          [fields[2].key]: (item, index) => (
            <td className="p-1">
              <FormSelect
                options={FIELD_TYPE}
                value={item[fields[2].key]}
                onValueChange={changeValue(fields[2].key, index)}
              />
            </td>
          ),
          [fields[3].key]: (item, index) => {
            let listData = FIELD_NAME[item[fields[2].key]]?.filter(
              (item) => item.value != settings.preffered_venue_update_custom_field_enable?.value,
            )
            return (
              <td className="p-1">
                <FormSelect
                  options={listData || []}
                  value={+item[fields[3].key]}
                  onValueChange={changeValue(fields[3].key, index)}
                />
              </td>
            )
          },
          [fields[4].key]: (item, index) => (
            <td className="p-1">
              <FormInput value={item[fields[4].key]} onChangeText={changeValue(fields[4].key, index)} />
            </td>
          ),
          [fields[5].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox checked={item[fields[5].key]} onValueChange={changeValue(fields[5].key, index)} />
            </td>
          ),
          [fields[6].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox checked={item[fields[6].key]} onValueChange={changeValue(fields[6].key, index)} />
            </td>
          ),
          [fields[7].key]: (item, index) => (
            <td className="p-1 text-center">
              <FormCheckbox checked={item[fields[7].key]} onValueChange={changeValue(fields[7].key, index)} />
            </td>
          ),
          [fields[8].key]: (item, index) => (
            <td className="p-1">
              <TagInput value={item.multiValues} onValueChange={changeValue(fields[8].key, index)} />
            </td>
          ),
          [fields[9].key]: (item, index) => (
            <td className="p-1">
              <CButton onClick={() => handleDelete(item, index)}>
                <CTooltip content="Delete">
                  <CIcon content={freeSet.cilTrash} />
                </CTooltip>
              </CButton>
            </td>
          ),
        }}
      />
      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={handleSave}>
        Save Changes
      </CButton>
    </>
  )
}

export default CustomField
