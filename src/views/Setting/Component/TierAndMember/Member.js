import {CButton, CRow} from '@coreui/react'
import React, {useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import FormCheckbox from '../../../../common/FormCheckbox'
import {changeValueLoop} from '../../../../helpers'
import {updateAppSettings} from '../../../../redux/actions/app'

const Member = ({setting}) => {
  const [state, setState] = useState({
    // create_member: setting.create_member,
    view_member: {...setting.view_member, extended_value: JSON.parse(setting.view_member.extended_value)},
  })
  const dispatch = useDispatch()

  const clickSave = async () => {
    dispatch(updateAppSettings({settings: JSON.stringify(Object.values(state))}))
  }

  const changeValue = (key) => (value) => setState(changeValueLoop(key, value, state))

  return (
    <>
      <h4>Member page setting</h4>
      <h5 className="text-muted">Member View (Show/Hide columns index page)</h5>
      {state.view_member.extended_value.map((item, index) => (
        <FormCheckbox
          key={item.label}
          label={item.label}
          checked={item.visible}
          onValueChange={changeValue(`view_member.extended_value.${index}.visible`)}
        />
      ))}

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </>
  )
}
export default Member
