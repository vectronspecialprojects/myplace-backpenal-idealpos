export const signupFields = [
  {
    key: 'displayOrder',
    label: 'Display Order',
    _style: {fontWeight: 'normal', fontSize: 12, width: '4%'},
  },
  {
    key: 'id',
    label: 'Parameters',
    _style: {fontWeight: 'normal', fontSize: 12, width: '15%'},
  },
  {
    key: 'fieldName',
    label: 'Custom Field Name',
    _style: {fontWeight: 'normal', fontSize: 12, width: '15%'},
  },
  {
    key: 'fieldType',
    label: 'Field Type',
    _style: {fontWeight: 'normal', fontSize: 12, width: '15%'},
  },
  {
    key: 'bepozFieldNum',
    label: 'Bepoz Field Name',
    _style: {fontWeight: 'normal', fontSize: 12, width: '15%'},
  },
  {
    key: 'fieldDisplayTitle',
    label: 'Field Display Title',
    _style: {fontWeight: 'normal', fontSize: 12, width: '15%'},
  },
  {
    key: 'displayInApp',
    label: 'Display In App',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'required',
    label: 'Required',
    _style: {fontWeight: 'normal', fontSize: 12, width: '4%'},
  },
  {
    key: 'shownOnProfile',
    label: 'Show On Profile',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'editOnProfile',
    label: 'Editable',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
  {
    key: 'keyboardType',
    label: 'Keyboard',
    _style: {fontWeight: 'normal', fontSize: 12, width: '10%'},
  },
  // {
  //   key: 'disabled',
  //   label: 'Disabled',
  //   _style: {fontWeight: 'normal', fontSize: 12},
  // },
  // {
  //   key: 'useVenueTag',
  //   label: 'Use Venue Tag',
  //   _style: {fontWeight: 'normal', fontSize: 12},
  // },

  {
    key: 'minLength',
    label: 'Min Length',
    _style: {fontWeight: 'normal', fontSize: 12, width: '10%'},
  },
  {
    key: 'maxLength',
    label: 'Max Length',
    _style: {fontWeight: 'normal', fontSize: 12, width: '10%'},
  },

  {
    key: 'multiValues',
    label: 'Default Values',
    _style: {fontWeight: 'normal', fontSize: 12, width: '40%'},
  },
  {
    key: 'delete',
    label: 'Delete',
    _style: {fontWeight: 'normal', fontSize: 12},
  },
]

export const tierFields = [
  'tier',
  'bepoz_setting',
  {key: 'acc', label: 'Extra Features'},
  {key: 'bin', label: ''},
]

export const venueTagTierFields = ['venue_tags', 'default_tier']

export const venueTierFields = ['venue', 'default_tier']
