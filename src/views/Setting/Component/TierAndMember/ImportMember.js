import React, {useState} from 'react'
import {CButton, CCol} from '@coreui/react'
import {importMember} from '../../../../utilities/ApiManage'
import {toast} from 'react-toastify'

const ImportMember = () => {
  const [fileUrl, setFileUrl] = useState()

  const handleUploadFile = async () => {
    try {
      if (!fileUrl) throw new Error('Please select a file!')
      debugger
      const res = await importMember({csv_file: fileUrl})
      if (!res.ok) throw new Error(res.message)
      toast('File uploaded successfully!', {type: 'success'})
    } catch (e) {
      toast(e.message, {type: 'error'})
    }
  }

  return (
    <CCol>
      <h4>Import Member page setting</h4>
      <div className="mt-3 mb-2">
        <input type="file" accept="text/csv" onChange={(e) => setFileUrl(e.target.files[0])} />
      </div>
      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={handleUploadFile}>
        UPLOAD FILE
      </CButton>
    </CCol>
  )
}

export default ImportMember
