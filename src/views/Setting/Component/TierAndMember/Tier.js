import {CButton, CDataTable} from '@coreui/react'
import React, {useEffect, useMemo, useState} from 'react'
import {getBepozGroups, bepozAccountGet} from '../../../../utilities/ApiManage'
import {useDispatch, useSelector} from 'react-redux'
import FormInput from '../../../../common/FormInput'
import FormDate from '../../../../common/FormDate'
import FormItem from '../../../../common/FormItem'
import FormCheckbox from '../../../../common/FormCheckbox'
import FormSelect from '../../../../common/FormSelect'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons'
import {postUploadGalleryPhoto, getTier} from '../../../../utilities/ApiManage'
import {setModal} from '../../../../reusable/Modal'
import {tierFields} from './tableField'
import {TIERS_EXPIRY_OPTIONS, FIELD_NAME} from '../../constant'
import {updateTierSettings, updateAppSettings} from '../../../../redux/actions/app'
import StatusToggle from '../../../../common/StatusToggle'
import {isTrue} from '../../../../helpers'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import tips from '../../../../tips'

const Tier = ({bepozMetaData, bepozVersion}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const setting = useSelector((state) => state.app.appSettings)
  const [tiers, setTiers] = useState([])
  const [venueTags, setVenueTags] = useState([])
  const [state, setState] = useState({enable_tier_icon_image_colour: setting.enable_tier_icon_image_colour})
  const [reload, setReload] = useState(false)
  const [bepozGroup, setBepozGroup] = useState([])
  const [showAll, setShowAll] = useState(0)

  const dispatch = useDispatch()

  const cflag = useMemo(() => {
    let usedCflag = []
    JSON.parse(setting?.bepozcustomfield?.extended_value).forEach((item) => {
      if (item?.bepozFieldNum.length >= 1 && item?.bepozFieldNum.length <= 2) {
        usedCflag.push(+item?.bepozFieldNum)
      }
    })

    if (!!+setting?.app_flag_id_bepoz?.value) {
      usedCflag.push(+setting?.app_flag_id_bepoz?.value)
    }

    if (!!bepozMetaData) {
      let cflagList = []
      bepozMetaData.forEach((item) => {
        if (item.FieldType === '0') {
          cflagList.push(+item.FieldNum)
        }
      })
      return FIELD_NAME[0]?.filter((a) => cflagList.includes(a.value) && !usedCflag.includes(a.value))
    }
    return []
  }, [bepozMetaData, setting])

  const tiersOptions = tiers.map((tier) => ({value: tier.id, label: tier.name}))

  const fetchTier = async () => {
    const res = await getTier(showAll)
    if (res.ok) {
      setVenueTags(res.venueTags)
      setTiers(res.data)
    }
  }

  useEffect(() => {
    fetchTier()
  }, [showAll, reload])

  useEffect(() => {
    ;(async () => {
      const res = await getBepozGroups()
      if (res.ok) {
        setBepozGroup(res.data)
      }
    })()
  }, [])

  const addNewTier = async () => {
    setReload(false)
    const newTier = [...tiers, {...DEFAULT_TIER}]
    setTiers(newTier)
  }

  const removeTier = async (index) => {
    const newTier = [...tiers]
    newTier.splice(index, 1)
    setTiers(newTier)
  }

  const errHandler = (title, message) => {
    setModal({
      title,
      message,
      primaryBtnClick: () => {
        setModal({show: false})
      },
      isShowSecondaryBtn: false,
      closeButton: false,
    })
  }

  const clickSave = async () => {
    let validation = true
    tiers.forEach((tier) => {
      if (!isTrue(tier.name) || !isTrue(tier.bepoz_group_id)) validation = false
      if (!isTrue(tier.use_precreated_account)) {
        if (!isTrue(tier.bepoz_template_id) || !isTrue(tier.boundary_start) || !isTrue(tier.boundary_end)) {
          validation = false
        }
      }
      if (isTrue(tier.use_precreated_account) && !isTrue(tier.precreated_account_custom_flag))
        validation = false
    })
    if (!validation) {
      errHandler(`Setup wasn't completed`, `Please fill out all required fields indicated with *`)
    } else {
      dispatch(
        updateAppSettings({
          settings: JSON.stringify([...Object.values(state)]),
        }),
      )
      await dispatch(
        updateTierSettings({
          tiers: JSON.stringify(Object.values(tiers)),
        }),
      )
      fetchTier()
    }
  }

  const checkTemplateAcc = async (number) => {
    const res = await bepozAccountGet(number)
    if (!res.data) {
      errHandler('Cannot find the template', "The account template does't exist in Bepoz")
    } else {
      if (!res.data.AccountFull.LastName.toLowerCase().includes('template', 'temp')) {
        errHandler(
          'We need user verification',
          'We found the account but not sure if it is the template account, The template account should have "template" included in the LastName field',
        )
      }
    }
  }

  // const clickSave = async () => {
  //   await postTierSetting({
  //     tiers: JSON.stringify(Object.values(tiers)),
  //   })
  //   fetchTier()
  // }

  const uploadImage = async (e, key, index) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) changeTierValue(key, index)(res.data)
  }

  const changeTierValue = (key, index) => (value) => {
    const newTiers = [...tiers]
    newTiers[index][key] = key === 'color' ? value.target.value : value
    setTiers(newTiers)
  }

  const changeValue = (key) => (value) => {
    setState((prevState) => ({...prevState, [key]: {...prevState[key], value}}))
  }

  return (
    <>
      {unlock && (
        <div className="d-flex align-items-center mb-2">
          <CButton className="mr-3 btn btn-secondary" onClick={addNewTier} disabled={!bepozVersion}>
            ADD NEW TIER
          </CButton>
          <FormCheckbox
            disabled={!bepozVersion}
            label="Show hidden?"
            checked={showAll === 1}
            checkValue={1}
            uncheckValue={0}
            onValueChange={setShowAll}
          />
          <div className="ml-5">
            <FormCheckbox
              disabled={!bepozVersion}
              label="Enable Tiers' Image and Color"
              checked={isTrue(state.enable_tier_icon_image_colour.value)}
              checkValue={'true'}
              uncheckValue={'false'}
              onValueChange={changeValue('enable_tier_icon_image_colour')}
            />
          </div>
        </div>
      )}
      <CDataTable
        items={tiers}
        fields={tierFields}
        striped
        scopedSlots={{
          tier: (item, index) => (
            <td>
              <FormCheckbox
                label="Inactive this tier"
                disabled={!unlock || !bepozVersion}
                checked={item.is_active === false}
                checkValue={false}
                uncheckValue={true}
                onValueChange={changeTierValue('is_active', index)}
              />
              <FormInput
                toolTip={true}
                tipContent={tips.tier.name}
                label="Name *"
                disabled={!unlock || !bepozVersion}
                value={item.name}
                onChangeText={changeTierValue('name', index)}
              />
              <FormSelect
                disabled={!unlock || !bepozVersion}
                toolTip={true}
                tipContent={tips.tier.allowVenue}
                label="Allowed Venues *"
                options={venueTags}
                getValue={(_) => _.id.toString()}
                getLabel={(_) => _.name}
                value={item.allowed_venues || '0'}
                onValueChange={changeTierValue('allowed_venues', index)}
              />
              {isTrue(state.enable_tier_icon_image_colour.value) && (
                <FormInput
                  label="Color"
                  id="venue_color"
                  value={item.color}
                  onChange={changeTierValue('color', index)}
                  type={'color'}
                  style={{width: 70}}
                  disabled={!bepozVersion}
                />
              )}
              {isTrue(state.enable_tier_icon_image_colour.value) && (
                <FormItem label="Tier Icon">
                  <div style={{width: '100px'}}>
                    <UploadImageCrop
                      aspect={1}
                      onConfirm={(e) => uploadImage(e, 'icon', index)}
                      className="w-100"
                      hideUndo>
                      <Image blob={item.icon} className="w-100" />
                    </UploadImageCrop>
                  </div>
                </FormItem>
              )}
            </td>
          ),
          bepoz_setting: (item, index) => {
            return (
              <td>
                <div className="row pl-3">
                  <span>Use Templace Acc</span>
                  <StatusToggle
                    disabled={!bepozVersion}
                    active={isTrue(item.use_precreated_account)}
                    className="ml-3 mr-3"
                    onChange={(e) => {
                      changeTierValue('use_precreated_account', index)(e)
                      changeTierValue('precreated_account_custom_flag', index)('')
                      changeTierValue('precreated_account_custom_flag_name', index)('')
                    }}
                  />
                  <span>Use Pre-created Acc</span>
                </div>

                <FormSelect
                  disabled={!unlock || !bepozVersion}
                  toolTip={true}
                  tipContent={tips.tier.groupId}
                  label="Group ID *"
                  options={bepozGroup}
                  getValue={(_) => _.id}
                  getLabel={(_) => _.name}
                  value={item.bepoz_group_id}
                  onValueChange={changeTierValue('bepoz_group_id', index)}
                />

                {!isTrue(item.use_precreated_account) && (
                  <FormInput
                    label="Template Acc. Number *"
                    onBlur={() => {
                      checkTemplateAcc(item.bepoz_template_id)
                    }}
                    disabled={!unlock || !bepozVersion}
                    value={item.bepoz_template_id}
                    onChangeText={changeTierValue('bepoz_template_id', index)}
                  />
                )}
                {!isTrue(item.use_precreated_account) && (
                  <FormInput
                    label="From *"
                    disabled={!unlock || item.maual_entry || !bepozVersion}
                    value={item.boundary_start}
                    onChangeText={changeTierValue('boundary_start', index)}
                  />
                )}
                {!isTrue(item.use_precreated_account) && (
                  <FormInput
                    label="To *"
                    disabled={!unlock || item.maual_entry || !bepozVersion}
                    value={item.boundary_end}
                    onChangeText={changeTierValue('boundary_end', index)}
                  />
                )}
                {isTrue(item.use_precreated_account) && (
                  <div>
                    <FormSelect
                      toolTip={true}
                      tipContent={tips.tier.selectCustomField}
                      disabled={!unlock || !bepozVersion}
                      label="Select a Custom Field *"
                      options={cflag}
                      value={item.precreated_account_custom_flag}
                      onValueChange={(e) => {
                        changeTierValue('precreated_account_custom_flag', index)(e)
                        const cname =
                          bepozMetaData.filter((_) => _.FieldType == '0' && _.FieldNum == e)[0]?.FieldName ||
                          ''
                        changeTierValue('precreated_account_custom_flag_name', index)(cname)
                      }}
                    />
                    <FormInput
                      label="Custom Field Name"
                      disabled={true}
                      value={item.precreated_account_custom_flag_name}
                    />
                  </div>
                )}
              </td>
            )
          },
          acc: (item, index) => {
            const isShowExpiryInput =
              item.expiry_date_option === 'months' || item.expiry_date_option === 'days' ? true : false
            const label = item.expiry_date_option === 'months' ? 'Months' : 'Days'
            return (
              <td>
                <FormSelect
                  style={{marginBottom: 10}}
                  disabled={!unlock || !bepozVersion}
                  label="Expiry Options"
                  options={TIERS_EXPIRY_OPTIONS}
                  value={item.expiry_date_option || 'never_expired'}
                  onValueChange={changeTierValue('expiry_date_option', index)}
                />
                {isShowExpiryInput && (
                  <FormInput
                    label={label}
                    disabled={!unlock || !isShowExpiryInput || !bepozVersion}
                    value={item[item.expiry_date_option]}
                    onChangeText={changeTierValue(item.expiry_date_option, index)}
                  />
                )}
                {item.expiry_date_option === 'specific_date' && (
                  <FormDate
                    label="Specific Date"
                    disabled={!bepozVersion}
                    value={typeof item?.specific_date === 'string' ? item?.specific_date.split(' ')[0] : ''}
                    onChange={(e) =>
                      changeTierValue(
                        'specific_date',
                        index,
                      )(`${e.target.value} ${item?.specific_date.split(' ')[1]}`)
                    }
                  />
                )}
                {item.expiry_date_option !== 'never_expired' && item.expiry_date_option !== null && (
                  <FormSelect
                    disabled={!unlock || !bepozVersion}
                    label="Downgrade Group ID:"
                    options={tiersOptions}
                    value={item?.downgrade_tier || item?.id}
                    onValueChange={changeTierValue('downgrade_tier', index)}
                  />
                )}
                <FormCheckbox
                  label="Manual Entry"
                  disabled={!unlock || !bepozVersion}
                  checked={item.maual_entry}
                  onValueChange={changeTierValue('maual_entry', index)}
                />
                <FormCheckbox
                  label="Force Card Range"
                  disabled={!unlock || !bepozVersion}
                  checked={item.force_card_range}
                  onValueChange={changeTierValue('force_card_range', index)}
                />
                <FormCheckbox
                  label="Add to Tiers List"
                  disabled={!unlock || !bepozVersion}
                  checked={item.display === 1}
                  checkValue={1}
                  uncheckValue={0}
                  onValueChange={changeTierValue('display', index)}
                />
                <FormCheckbox
                  label="Use App Member Details"
                  disabled={!unlock || !bepozVersion}
                  checked={item.dont_update_from_bepoz}
                  checkValue={true}
                  uncheckValue={false}
                  onValueChange={changeTierValue('dont_update_from_bepoz', index)}
                />
              </td>
            )
          },
          bin: (item, index) => (
            <td>
              {!item.id && (
                <CButton onClick={removeTier.bind(this, index)} disabled={!unlock || !bepozVersion}>
                  <CIcon content={freeSet.cilTrash} />
                </CButton>
              )}
            </td>
          ),
        }}
      />

      <CButton
        disabled={!unlock || !bepozVersion}
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </>
  )
}
export default Tier

const DEFAULT_TIER = {
  allowed_venues: '0',
  bepoz_group_id: '',
  bepoz_group_name: '',
  bepoz_template_id: '',
  boundary_end: '',
  boundary_start: '',
  color: null,
  display: 0,
  dont_update_from_bepoz: false,
  downgrade_tier: null,
  expiry_date_option: 'never_expired',
  force_card_range: false,
  icon: 'https://via.placeholder.com/125x125',
  id: null,
  is_active: true,
  months: 0,
  days: 0,
  name: '',
  precreated_account_custom_flag: '',
  precreated_account_custom_flag_name: '',
  specific_date: null,
  use_precreated_account: 0,
  is_an_expiry_tier: true,
  is_default_tier: true,
  is_purchasable: true,
  key: '',
  downgrade_group_id: null,
}
