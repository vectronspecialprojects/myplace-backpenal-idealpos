import Collapse from '../../../common/Collapse'
import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import {SETTING_REWARD_OPTION, SETTING_SIGN_UP_REWARD} from '../constant'
import {postUploadGalleryPhoto} from '../../../utilities/ApiManage'
import FormInput from '../../../common/FormInput'
import FormSelect from '../../../common/FormSelect'
import VoucherSetupSelect from '../../../common/VoucherSetupSelect'
import UploadImageCrop from '../../../common/UploadImageCrop'
import Image from '../../../common/image'
import TagInput from '../../../common/TagInput'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import Layout from './components/Layout';

const FriendReferral = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    friend_referral_reward: setting.friend_referral_reward,
    friend_referral_point: setting.friend_referral_point,
    friend_referral_voucher_setups_id: setting.friend_referral_voucher_setups_id,
    friend_referral_reward_option: setting.friend_referral_reward_option,
    friend_referral_voucher_background: setting.friend_referral_voucher_background,
    friend_referral_message: setting.friend_referral_message,
    friend_referral_reminder_interval: {
      ...setting.friend_referral_reminder_interval,
      extended_value: JSON.parse(setting.friend_referral_reminder_interval.extended_value),
    },
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const uploadImage = async (e) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) {
      setState({
        ...state,
        friend_referral_voucher_background: {...state.friend_referral_voucher_background, value: res.data},
      })
    }
  }

  return (
    <Collapse title="Friend Referral">
      <CCol>
        <Layout title="Set Friend Referral Reward" desc="Choose what kind of reward to offer" contentWidth={4}>
          <FormSelect
            options={SETTING_SIGN_UP_REWARD}
            value={state.friend_referral_reward.value}
            onValueChange={changeValue('friend_referral_reward')}
          />
        </Layout>
        {state.friend_referral_reward.value === 'point' && (
          <Layout title="Set Point Reward" desc="Set point rewards for new member using friend referral" contentWidth={4}>
            <FormInput
              value={state.friend_referral_point.value}
              onChangeText={changeValue('friend_referral_point')}
            />
          </Layout>
        )}

        {state.friend_referral_reward.value === 'voucher' && (
          <Layout title="Set Reward (Bepoz Voucher Setups)" desc="Bepoz Voucher as a reward" contentWidth={4}>
            <VoucherSetupSelect
              noVoucher
              value={state.friend_referral_voucher_setups_id.value}
              onValueChange={changeValue('friend_referral_voucher_setups_id')}
            />
          </Layout>
        )}

        <Layout title="Friend Referral Reminder Interval" desc='Set how many days'>
          <TagInput
            value={state.friend_referral_reminder_interval.extended_value}
            int
            onValueChange={(value) => {
              setState({
                ...state,
                friend_referral_reminder_interval: {
                  ...setting.friend_referral_reminder_interval,
                  extended_value: value,
                },
              })
            }}
          />
        </Layout>

        <Layout title="Friend Referral Message Shown in App" contentWidth={4}>
          <FormInput
            value={state.friend_referral_message.value}
            onChangeText={changeValue('friend_referral_message')}
          />
        </Layout>
        <Layout title="Background Image For Voucher Reward">
          <CRow>
            <CCol xs={12} md={6}>
              <UploadImageCrop aspect={1} onConfirm={uploadImage} className="w-50" hideUndo>
                <Image blob={state.friend_referral_voucher_background.value} className="w-50" />
              </UploadImageCrop>
            </CCol>
          </CRow>
        </Layout>
      </CCol>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </Collapse>
  )
}



export default FriendReferral
