import Collapse from '../../../../common/Collapse'
import {
  CButton,
  CCol,
  CDataTable,
  CLabel,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CSelect,
  CTabContent,
  CTabPane,
  CTabs,
  CTextarea,
} from '@coreui/react'
import React, {useEffect, useState, useMemo} from 'react'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import * as C from '../../constant'
import {postUploadGalleryPhoto} from '../../../../utilities/ApiManage'
import FormInput from '../../../../common/FormInput'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import {useDispatch, useSelector} from 'react-redux'
import FormSelect from '../../../../common/FormSelect'
import PageTable from './PageTable'
import {updateAppSettings} from '../../../../redux/actions/app'
import DefaultImage375x150 from '../../../../assets/images/375x150.png'
import {fields, appLayoutTabs} from './constant'
import {isTrue} from 'src/helpers'
import PageTableHeader from './PageTableHeader'
import DisplayOrder from './DisplayOrder'

const AppLayout = ({setting}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [show, setShow] = useState(false)
  const [toReOrder, setToReOrder] = useState('')
  const [unlockedPledge, setUnlockedPledge] = useState(false)
  const [slot, setSlot] = useState(setting?.gallery_total_slots)
  const [gallery, setGallery] = useState({
    ...setting?.galleries,
    extended_value: JSON.parse(setting?.galleries?.extended_value),
  })

  const [welcomeGallerySlot, setWelcomeGallerySlot] = useState(setting?.welcome_instruction_total_slots)
  const [welcomeGallery, setWelcomeGallery] = useState({
    ...setting?.welcome_instruction,
    extended_value: JSON.parse(setting?.welcome_instruction?.extended_value),
  })

  const [communityGallerySlot, setCommunityGallerySlot] = useState(
    setting?.community_impact_gallery_total_slots,
  )
  const [communityGallery, setCommunityGallery] = useState({
    ...setting?.community_impact_gallery,
    extended_value: JSON.parse(setting?.community_impact_gallery?.extended_value),
  })

  const [pledgeURL, setPledgeURL] = useState(setting?.pond_hoppers_url?.value || '')
  const [isLegacyStyle, setIsLegacyStyle] = useState(false)
  const [isWelcomeOn, setIsWelcomeOn] = useState(true)
  const [isPondHoppersOn, setIsPondHoppersOn] = useState(false)
  const [state, setState] = useState({
    tab_layout_total_buttons: setting?.tab_layout_total_buttons,
    tab_layout: {...setting?.tab_layout, extended_value: JSON.parse(setting?.tab_layout?.extended_value)},
    front_menu_total_buttons: setting?.front_menu_total_buttons,
    front_menu_buttons: {
      ...setting?.front_menu_buttons,
      extended_value: JSON.parse(setting?.front_menu_buttons?.extended_value),
    },
    community_impact_button_option: setting?.community_impact_button_option,
    community_impact_content_option: setting.community_impact_content_option,
    community_impact_content_text: setting.community_impact_content_text,
    side_menu_total_buttons: setting.side_menu_total_buttons,
    side_menu_buttons: {
      ...setting.side_menu_buttons,
      extended_value: JSON.parse(setting.side_menu_buttons.extended_value),
    },
    side_menu_before_login_total_buttons: setting.side_menu_before_login_total_buttons,
    side_menu_before_login_buttons: {
      ...setting.side_menu_before_login_buttons,
      extended_value: JSON.parse(setting.side_menu_before_login_buttons.extended_value),
    },
    profile_menu_total_buttons: setting.profile_menu_total_buttons,
    profile_menu_buttons: {
      ...setting.profile_menu_buttons,
      extended_value: JSON.parse(setting.profile_menu_buttons.extended_value),
    },
  })

  const modalItem = useMemo(() => {
    if (toReOrder === 'tab_layout') {
      return state.tab_layout.extended_value.slice(1, +state.tab_layout_total_buttons.value)
    }
    if (toReOrder === 'front_menu_buttons') {
      return isLegacyStyle
        ? state.front_menu_buttons.extended_value.slice(0, 4)
        : state.front_menu_buttons.extended_value.slice(0, +state.front_menu_total_buttons.value)
    }
    if (toReOrder === 'side_menu_before_login_buttons') {
      return state.side_menu_before_login_buttons.extended_value.slice(
        1,
        +state.side_menu_before_login_total_buttons.value,
      )
    }
    if (toReOrder === 'side_menu_buttons') {
      return state.side_menu_buttons.extended_value.slice(1, +state.side_menu_total_buttons.value - 1)
    }
    if (toReOrder === 'profile_menu_buttons') {
      return state.profile_menu_buttons.extended_value.slice(0, +state.profile_menu_total_buttons.value)
    }
    return []
  }, [show, toReOrder, state])

  useEffect(() => {
    setIsLegacyStyle(isTrue(setting.use_legacy_design.value))
    setIsWelcomeOn(isTrue(setting.welcome_instruction_enable.value))
    setIsPondHoppersOn(isTrue(setting.pond_hoppers_enable.value))
  }, [setting])

  const homePageOptions = useMemo(() => {
    if (isPondHoppersOn) return [...C.HOME_BUTTONS, ...C.POND_HOPPERS_HOME]
    return C.HOME_BUTTONS
  }, [isPondHoppersOn])

  const pageOptions = useMemo(() => {
    if (isPondHoppersOn) return [...C.SIDE_BAR_AFTER_LOGIN, ...C.POND_HOPPERS_SIDE_BAR]
    return C.SIDE_BAR_AFTER_LOGIN
  }, [isPondHoppersOn])

  const sideBarBeforePageOptions = useMemo(() => {
    if (isPondHoppersOn) return [...C.SIDE_BAR_BEFORE_LOGIN, ...C.POND_HOPPERS_SIDE_BAR_BEFORE_LOGIN]
    return C.SIDE_BAR_BEFORE_LOGIN
  }, [isPondHoppersOn])

  const profilePageOptions = useMemo(() => {
    if (isPondHoppersOn) return [...C.PROFILE_BUTTONS, ...C.POND_HOPPERS_HOME]
    return C.PROFILE_BUTTONS
  }, [isPondHoppersOn])

  useEffect(() => {
    setUnlockedPledge(false)
    state.front_menu_buttons.extended_value
      .slice(0, +state.front_menu_total_buttons.value)
      .forEach((button) => {
        if (button?.page_layout === 'special_view' && button?.special_page === 'community-points') {
          setUnlockedPledge(true)
        }
      })
  }, [state])

  const [tab, setTab] = useState(0)
  useEffect(() => {
    setTab(0)
  }, [unlock])

  const changeValue = (key) => (value) => {
    const newState = {...state}
    const updateDataRecursive = (data, keys) => {
      if (keys.length === 1) return (data[keys[0]] = value)
      const newData = data[keys[0]]
      keys.shift()
      return updateDataRecursive(newData, keys)
    }
    updateDataRecursive(newState, key.split('.'))
    setState(newState)
  }

  const uploadImage = (index, state, key) => async (e) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) {
      let newExtendedValue = []
      switch (state) {
        case 'galleries':
          newExtendedValue = [...gallery.extended_value]
          newExtendedValue[index][key] = res.data
          setGallery((prevState) => ({...prevState, extended_value: newExtendedValue}))
          break
        case 'welcome_instruction':
          newExtendedValue = [...welcomeGallery.extended_value]
          newExtendedValue[index][key] = res.data
          setWelcomeGallery((prevState) => ({...prevState, extended_value: newExtendedValue}))
          break
        case 'community_impact_gallery':
          newExtendedValue = [...communityGallery.extended_value]
          newExtendedValue[index][key] = res.data
          setCommunityGallery((prevState) => ({...prevState, extended_value: newExtendedValue}))
          break
        default:
          changeValue(`${state}.extended_value.${index}.${key}`)(res.data)
      }
    }
  }

  const changeTabPageTotal = (key, value, index, total) => {
    const newState = {...state}
    const item = {listing_type_id: 'none', title: 'Title ###'}
    const pages = [...value.pages, item, item, item].splice(0, +total)
    newState[key].extended_value[index].pages = pages
    setState(newState)
  }

  const changeSlot = (e) => {
    setSlot((prevState) => ({...prevState, value: e.target.value}))
  }

  const changeWelcomeSlot = (e) => {
    setWelcomeGallerySlot((prevState) => ({...prevState, value: e.target.value}))
  }

  const changeCommunitySlot = (e) => {
    setCommunityGallerySlot((prevState) => ({...prevState, value: e.target.value}))
  }

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([
          slot,
          gallery,
          welcomeGallerySlot,
          welcomeGallery,
          communityGallerySlot,
          communityGallery,
          ...Object.values(state),
        ]),
      }),
    )
  }

  const changeImageInput = (value, index, key) => {
    const newExtendedValue = [...welcomeGallery.extended_value]
    newExtendedValue[index][key] = value
    setWelcomeGallery((prevState) => ({...prevState, extended_value: newExtendedValue}))
  }

  const onOrderChange = (e) => {
    let reOrderedValue
    if (toReOrder !== 'front_menu_buttons' || toReOrder !== 'profile_menu_buttons') {
      reOrderedValue = [
        {...state[toReOrder].extended_value[0]},
        ...e.map((item, index) => ({...item, id: index + 2})),
      ]
    }
    if (toReOrder === 'front_menu_buttons' || toReOrder === 'profile_menu_buttons') {
      reOrderedValue = [...e.map((item, index) => ({...item, id: index + 1}))]
    }
    if (toReOrder === 'side_menu_buttons') {
      reOrderedValue = [
        ...reOrderedValue,
        {...state[toReOrder].extended_value[state.side_menu_total_buttons.value - 1]},
      ]
    }
    const newExtendedValue = [
      ...reOrderedValue,
      ...state[toReOrder].extended_value.slice(reOrderedValue.length),
    ]
    changeValue(`${toReOrder}.extended_value`)(newExtendedValue)
  }

  return (
    <Collapse title="App Layout">
      <>
        <CTabs activeTab={tab} onActiveTabChange={setTab}>
          <CNav variant="tabs">
            {appLayoutTabs.map((item, index) => {
              if (
                index == 0 ||
                (index == 1 && isWelcomeOn) ||
                (index == 7 && unlockedPledge && isPondHoppersOn) ||
                (index != 0 && index != 1 && index != 7 && unlock)
              ) {
                return (
                  <CNavItem key={index}>
                    <CNavLink>{item}</CNavLink>
                  </CNavItem>
                )
              }
            })}
          </CNav>
          <CTabContent style={{marginTop: 20}}>
            {/* Gallery */}
            <CTabPane>
              <div style={{width: '10%'}}>
                <CLabel>Number of Slots</CLabel>
                <CSelect custom value={slot?.value} onChange={changeSlot}>
                  {C.SETTING_GALLERY_SLOTS.map((_) => (
                    <option key={_.value} value={_.value}>
                      {_.label}
                    </option>
                  ))}
                </CSelect>
              </div>

              <CDataTable
                items={gallery?.extended_value?.slice(0, slot?.value) || []}
                fields={fields}
                striped={false}
                scopedSlots={{
                  slot: (item, index) => <td>{index + 1}</td>,
                  images: (item, index) => (
                    <td>
                      <UploadImageCrop
                        aspect={1.25}
                        onConfirm={uploadImage(index, 'galleries', 'url')}
                        className="w-700"
                        hideUndo>
                        <Image blob={item.url} className="w-700" />
                      </UploadImageCrop>
                    </td>
                  ),
                }}
              />
            </CTabPane>
            {/* Welcome Gallery */}
            {isWelcomeOn && (
              <CTabPane>
                <div style={{width: '10%'}}>
                  <CLabel>Number of Slots</CLabel>
                  <CSelect custom value={welcomeGallerySlot?.value} onChange={changeWelcomeSlot}>
                    {C.SETTING_GALLERY_SLOTS.map((_) => (
                      <option key={_.value} value={_.value}>
                        {_.label}
                      </option>
                    ))}
                  </CSelect>
                </div>
                <CDataTable
                  items={welcomeGallery?.extended_value?.slice(0, welcomeGallerySlot?.value) || []}
                  fields={fields}
                  striped={false}
                  scopedSlots={{
                    slot: (item, index) => <td>{index + 1}</td>,
                    images: (item, index) => {
                      return (
                        <td>
                          <UploadImageCrop
                            aspect={1.25}
                            onConfirm={uploadImage(index, 'welcome_instruction', 'image_icon')}
                            className="w-700"
                            hideUndo>
                            <Image blob={item.image_icon} className="w-700" />
                          </UploadImageCrop>

                          {/* <FormInput
                        label="URL"
                        value={item.image_icon}
                        onChangeText={(value) => changeImageInput(value, index, 'image_icon')}
                      /> */}

                          <FormInput
                            label="Title"
                            value={item.title}
                            onChangeText={(value) => changeImageInput(value, index, 'title')}
                          />

                          <FormInput
                            label="Content"
                            value={item.content}
                            onChangeText={(value) => changeImageInput(value, index, 'content')}
                          />
                        </td>
                      )
                    },
                  }}
                />
              </CTabPane>
            )}
            {unlock && (
              <>
                {/* Tab*/}
                <CTabPane>
                  <div>
                    <PageTableHeader
                      labelS={'Tabs Buttons'}
                      valueS={state.tab_layout_total_buttons.value}
                      optionsS={C.SETTING_TABS_BUTTON}
                      onValueChangeS={changeValue('tab_layout_total_buttons.value')}
                      onClickIcon={() => {
                        setShow(true)
                        setToReOrder('tab_layout')
                      }}
                    />
                    <PageTable
                      label="Tab"
                      field="tab_layout"
                      value={state.tab_layout.extended_value.slice(0, +state.tab_layout_total_buttons.value)}
                      changeValue={changeValue}
                      changeTabPageTotal={changeTabPageTotal}
                      onImageIconChange={uploadImage}
                      viewOptions={pageOptions}
                    />
                  </div>
                </CTabPane>
                {/* Home*/}
                <CTabPane>
                  <div>
                    <PageTableHeader
                      labelS={'Home Buttons'}
                      valueS={state.front_menu_total_buttons.value}
                      optionsS={C.SETTING_SIDE_BUTTON_BEFORE_LOGIN}
                      onValueChangeS={(e) => {
                        if (isLegacyStyle) {
                          changeValue('front_menu_total_buttons.value')('4')
                        } else {
                          changeValue('front_menu_total_buttons.value')(e)
                        }
                      }}
                      unlockSelect={!isLegacyStyle}
                      onClickIcon={() => {
                        setShow(true)
                        setToReOrder('front_menu_buttons')
                      }}
                    />

                    <PageTable
                      label="Menu"
                      field="front_menu_buttons"
                      allowIconColor={true}
                      value={
                        isLegacyStyle
                          ? state.front_menu_buttons.extended_value.slice(0, 4)
                          : state.front_menu_buttons.extended_value.slice(
                              0,
                              +state.front_menu_total_buttons.value,
                            )
                      }
                      changeValue={changeValue}
                      changeTabPageTotal={changeTabPageTotal}
                      onImageIconChange={uploadImage}
                      viewOptions={homePageOptions}
                    />
                  </div>
                </CTabPane>
                {/* Drawer Before Login */}
                <CTabPane>
                  <div>
                    <PageTableHeader
                      labelS={'Drawer Buttons'}
                      valueS={state.side_menu_before_login_total_buttons.value}
                      optionsS={C.SETTING_SIDE_BUTTON_BEFORE_LOGIN}
                      onValueChangeS={changeValue('side_menu_before_login_total_buttons.value')}
                      onClickIcon={() => {
                        setShow(true)
                        setToReOrder('side_menu_before_login_buttons')
                      }}
                    />
                    <PageTable
                      label="Menu"
                      field="side_menu_before_login_buttons"
                      value={state.side_menu_before_login_buttons.extended_value.slice(
                        0,
                        +state.side_menu_before_login_total_buttons.value,
                      )}
                      changeValue={changeValue}
                      changeTabPageTotal={changeTabPageTotal}
                      onImageIconChange={uploadImage}
                      viewOptions={sideBarBeforePageOptions}
                    />
                  </div>
                </CTabPane>
                {/* Drawer After Login */}
                <CTabPane>
                  <div>
                    <PageTableHeader
                      labelS={'Drawer Buttons'}
                      valueS={state.side_menu_total_buttons.value}
                      optionsS={C.SETTING_SIDE_BUTTON}
                      onValueChangeS={changeValue('side_menu_total_buttons.value')}
                      onClickIcon={() => {
                        setShow(true)
                        setToReOrder('side_menu_buttons')
                      }}
                    />

                    <PageTable
                      label="Menu"
                      field="side_menu_buttons"
                      value={state.side_menu_buttons.extended_value.slice(
                        0,
                        +state.side_menu_total_buttons.value,
                      )}
                      changeValue={changeValue}
                      changeTabPageTotal={changeTabPageTotal}
                      onImageIconChange={uploadImage}
                      viewOptions={pageOptions}
                    />
                  </div>
                </CTabPane>
                {/* Profile*/}
                <CTabPane>
                  <div>
                    <PageTableHeader
                      labelS={'Profile Buttons'}
                      valueS={state.profile_menu_total_buttons.value}
                      optionsS={C.PROFILE_BUTTON}
                      onValueChangeS={changeValue('profile_menu_total_buttons.value')}
                      onClickIcon={() => {
                        setShow(true)
                        setToReOrder('profile_menu_buttons')
                      }}
                    />

                    <PageTable
                      label="Menu"
                      field="profile_menu_buttons"
                      value={state.profile_menu_buttons.extended_value.slice(
                        0,
                        +state.profile_menu_total_buttons.value,
                      )}
                      changeValue={changeValue}
                      changeTabPageTotal={changeTabPageTotal}
                      onImageIconChange={uploadImage}
                      viewOptions={profilePageOptions}
                    />
                  </div>
                </CTabPane>
              </>
            )}
            {/* Community Points */}
            {unlockedPledge && isPondHoppersOn && (
              <CTabPane>
                <CRow>
                  <CCol>
                    <div style={{width: '21%'}}>
                      <CLabel>Number of Slots</CLabel>
                      <CSelect custom value={communityGallerySlot?.value} onChange={changeCommunitySlot}>
                        {C.SETTING_GALLERY_SLOTS.map((_) => (
                          <option key={_.value} value={_.value}>
                            {_.label}
                          </option>
                        ))}
                      </CSelect>
                    </div>

                    <CDataTable
                      items={communityGallery?.extended_value?.slice(0, communityGallerySlot?.value) || []}
                      fields={fields}
                      striped={false}
                      scopedSlots={{
                        slot: (item, index) => <td>{index + 1}</td>,
                        images: (item, index) => (
                          <td>
                            <UploadImageCrop
                              aspect={2.6}
                              onConfirm={uploadImage(index, 'community_impact_gallery', 'url')}
                              className="w-700"
                              hideUndo>
                              <Image blob={item.url || DefaultImage375x150} className="w-700" />
                            </UploadImageCrop>
                          </td>
                        ),
                      }}
                    />
                  </CCol>
                  <CCol md={6}>
                    <h4 className="mb-5">Community impact</h4>
                    <div style={{width: '335px', marginBottom: '20px'}}>
                      <FormSelect
                        label="Community Points Options"
                        style={{width: '335px'}}
                        value={state.community_impact_content_option.value}
                        options={C.COMMUNITY_OPTIONS}
                        onValueChange={changeValue('community_impact_content_option.value')}
                      />
                    </div>

                    <div
                      style={{
                        width: '335px',
                        height: '60px',
                        borderRadius: '10px',
                        backgroundColor: '#fff',
                        boxShadow: '3px 3px 3px #ccc',
                      }}
                      className="card ">
                      <div style={{padding: '5px'}}>
                        <h5 style={{color: 'red', marginBottom: '0px'}}>Community Points</h5>
                        <span>4792 Community Points ready to pledge</span>
                      </div>
                    </div>

                    <div
                      style={{
                        width: '335px',
                        minHeight: '400px',
                        borderRadius: '10px',
                        backgroundColor: '#fff',
                        boxShadow: '3px 3px 3px #ccc',
                        overflow: 'hidden',
                      }}
                      className="card ">
                      <Image
                        blob={communityGallery.extended_value[0].url || DefaultImage375x150}
                        className="w-700"
                      />

                      <div style={{padding: '20px'}}>
                        <span style={{color: 'red', fontWeight: 'bold'}}>$547,449.17</span>

                        {state.community_impact_content_option.value === 'text' && (
                          <CTextarea
                            placeholder={'Please add content here.'}
                            rows="5"
                            value={state.community_impact_content_text.value}
                            style={{
                              maxWidth: '335px',
                              borderWidth: '0px',
                              height: '100px',
                              padding: 0,
                              marginTop: '15px',
                            }}
                            onChange={(e) =>
                              changeValue('community_impact_content_text.value')(e.target.value)
                            }
                          />
                        )}

                        {state.community_impact_content_option.value !== 'text' && pledgeURL !== '' && (
                          <div className="mt-4">
                            <span>The Content will come from Pledge platform.</span>
                          </div>
                        )}

                        {state.community_impact_content_option.value !== 'text' && pledgeURL == '' && (
                          <div className="mt-4">
                            <span>Please setup Pledge API in Integration Section.</span>
                          </div>
                        )}

                        <div
                          style={{
                            width: '100px',
                            height: '35px',
                            borderRadius: '20px',
                            padding: '0px',
                            justifyItems: 'center',
                            lineHeight: 2,
                            alignItems: 'center',
                            color: 'red',
                            borderColor: 'red',
                            paddingTop: '2px',
                            marginTop: '30px',
                          }}
                          className={'card'}>
                          Read Now
                        </div>

                        <FormSelect
                          label="please assign button feature."
                          style={{width: '200px'}}
                          value={state.community_impact_button_option.value}
                          options={C.POND_HOPPERS_FEATURES}
                          onValueChange={changeValue('community_impact_button_option.value')}
                        />
                      </div>
                    </div>
                  </CCol>
                </CRow>
              </CTabPane>
            )}
          </CTabContent>
        </CTabs>
      </>
      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>

      <DisplayOrder
        show={show}
        onClose={() => setShow(false)}
        items={modalItem}
        onOrderChange={onOrderChange}
        onSave={() => {
          clickSave()
          setShow(false)
        }}
      />
    </Collapse>
  )
}

export default AppLayout
