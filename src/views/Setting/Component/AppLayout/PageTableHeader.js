import React from 'react'
import {CRow} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import FormSelect from '../../../../common/FormSelect'

const PageTableHeader = ({labelS, valueS, optionsS, onValueChangeS, onClickIcon, unlockSelect = true, ...props}) => {
  return (
    <CRow xs={12} md={6}>
      {unlockSelect && (
        <div style={{width: '10%', marginLeft: 15}}>
          <FormSelect label={labelS} value={valueS} options={optionsS} onValueChange={onValueChangeS} />
        </div>
      )}
      <CIcon
        content={freeSet.cilListNumbered}
        height="30"
        style={unlockSelect?{marginTop: '1.75rem', marginLeft: '50px'}:{ marginLeft: '50px'}}
        color={'red'}
        alt={'order'}
        onClick={onClickIcon}
      />
    </CRow>
  )
}

export default PageTableHeader
