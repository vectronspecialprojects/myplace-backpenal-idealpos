import React from 'react'
import {
  CModal,
  CModalBody,
  CRow,
  CCol,
  CCard,
  CCardHeader,
  CCardBody,
  CLabel,
  CButton,
  CModalFooter,
} from '@coreui/react'
import SortableList from '../../../../common/SortableList'

const DisplayOrder = ({show, onClose, label, onSave, onOrderChange, items}) => {
  return (
    <CModal show={show} onClose={onClose} size="xl">
      <CModalBody>
        <CRow>
          <CCol xs="12">
            <CCard>
              <CCardHeader>
                <CRow className="px-3">
                  <CLabel className="flex-fill m-0 h4">{label}</CLabel>
                  <CButton className="btn btn-secondary" onClick={onSave}>
                    SAVE AND EXIT
                  </CButton>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <SortableList
                  onChange={onOrderChange}
                  items={items?.sort((a, b) => a.id - b.id) || []}
                  renderItem={(_) => (
                    <CCard className="my-3 p-3 ">
                      <CLabel>{_.page_name}</CLabel>
                    </CCard>
                  )}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CModalBody>
    </CModal>
  )
}

export default DisplayOrder
