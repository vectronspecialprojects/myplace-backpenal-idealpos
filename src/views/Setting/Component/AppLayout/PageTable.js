import React, {useEffect} from 'react'
import * as C from '../../constant'
import FormInput from '../../../../common/FormInput'
import FormSelect from '../../../../common/FormSelect'
import {CDataTable} from '@coreui/react'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import StatusToggle from '../../../../common/StatusToggle'

const PageTable = ({
  label,
  field,
  changeValue,
  changeTabPageTotal,
  value,
  onImageIconChange,
  viewOptions,
  allowIconColor = false,
}) => {
  const tabFields = [
    {key: label, _style: {width: '2%'}},
    {key: 'page_name', _style: {width: '21%'}},
    {key: 'icon', _style: {width: '13%'}},
    {key: 'selector', _style: {width: '2%'}},
    {key: 'image', _style: {width: '2%'}},
    {key: 'view', _style: {width: '21%'}},
    {key: 'navigation', _style: {width: '21%'}},
  ]

  const iconColorTabFields = [
    {key: label, _style: {width: '2%'}},
    {key: 'page_name', _style: {width: '19%'}},
    {key: 'icon', _style: {width: '13%'}},
    {key: 'selector', _style: {width: '2%'}},
    {key: 'image', _style: {width: '2%'}},
    {key: 'icon_color', _style: {width: '10%'}},
    {key: 'view', _style: {width: '19%'}},
    {key: 'navigation', _style: {width: '19%'}},
  ]

  const imageIconChangeHandler = (state, index, key) => (e) => {
    onImageIconChange(index, state, key)(e)
  }

  function getTabSelected(item) {
    let selectedTab = item.layoutTab
    if (!selectedTab && item.page_layout === 'tab_view') {
      if (item.pages?.[0]?.listing_type_id == 1 || item.pages?.[0]?.listing_type_id == 2) {
        selectedTab = 'what_on'
      } else {
        selectedTab = 'stamp_card'
      }
    }

    return selectedTab
  }

  let selectOption = []
  switch (field) {
    case 'tab_layout':
      selectOption = [
        {
          label: 'Home',
          value: 'home',
          navigation: 'HomeNavigator',
        },
      ]
      break
    case 'side_menu_before_login_buttons':
      selectOption = [
        {
          label: 'Signup',
          value: 'signup',
          navigation: 'FrontpageNavigator',
        },
      ]
      break
    case 'side_menu_buttons':
      selectOption = [
        {
          label: 'Home',
          value: 'tab',
          navigation: 'TabNavigator',
        },
      ]
      break
  }

  useEffect(() => {
    // console.log('value', value)
    if (
      field === 'tab_layout' ||
      field === 'side_menu_before_login_buttons' ||
      field === 'side_menu_buttons'
    ) {
      changeValue(`${field}.extended_value.${0}.page_layout`)('special_view')
      changeValue(`${field}.extended_value.${0}.state`)(selectOption[0]?.navigation)
      changeValue(`${field}.extended_value.${0}.special_page`)(selectOption[0]?.value)
    }
    if (field === 'side_menu_buttons') {
      changeValue(`${field}.extended_value.${value.length - 1}.page_layout`)('')
      changeValue(`${field}.extended_value.${value.length - 1}.state`)('logout')
      changeValue(`${field}.extended_value.${value.length - 1}.special_page`)('')
    }
  }, [])

  function getValuePage(item) {
    switch (item.page_layout) {
      case C.PAGE_LAYOUT.single_view:
        return +item.pages?.[0]?.listing_type_id
      case C.PAGE_LAYOUT.special_view:
        return item.special_page
      case C.PAGE_LAYOUT.tab_view:
        return getTabSelected(item)
    }
  }

  return (
    <CDataTable
      items={value || []}
      fields={allowIconColor ? iconColorTabFields : tabFields}
      striped
      size={'sm'}
      scopedSlots={{
        [label]: (item) => <td>#{item.id}</td>,
        page_name: (item, index) => (
          <td>
            <FormInput
              formClassName="mb-0"
              value={item.page_name}
              onChangeText={changeValue(`${field}.extended_value.${index}.page_name`)}
            />
          </td>
        ),
        icon: (item, index) => (
          <td className="p-1 h-auto">
            <FormInput
              disabled={item.icon_selector === 'image'}
              formClassName="mb-0"
              value={item.icon}
              onChangeText={changeValue(`${field}.extended_value.${index}.icon`)}
            />
          </td>
        ),
        selector: (item, index) => (
          <td className="p-1 h-auto">
            <StatusToggle
              active={item.icon_selector === 'image'}
              className="ml-3 mr-3"
              onChange={(value) => {
                if (value) {
                  changeValue(`${field}.extended_value.${index}.icon_selector`)('image')
                } else {
                  changeValue(`${field}.extended_value.${index}.icon_selector`)('icon')
                }
              }}
            />
          </td>
        ),

        image: (item, index) => (
          <td className="p-1 h-auto">
            <div style={{marginTop: 0}}>
              <UploadImageCrop
                disabled={item.icon_selector === 'icon'}
                aspect={1}
                onConfirm={imageIconChangeHandler(field, index, 'image_icon')}
                className="w-100"
                hideUndo>
                <Image blob={item.image_icon} className="w-100" />
              </UploadImageCrop>
            </div>
            {/* <FormInput
              formClassName="mb-0"
              value={item.image_icon}
              onChangeText={changeValue(`${field}.extended_value.${index}.image_icon`)}
            /> */}
          </td>
        ),
        icon_color: (item, index) => (
          <td className="p-1 h-auto">
            <FormInput
              className="w-100"
              value={item?.icon_color}
              onChange={(e) => changeValue(`${field}.extended_value.${index}.icon_color`)(e.target.value)}
              type={'color'}
            />
          </td>
        ),
        view: (item, index) => {
          let selectedValue = getValuePage(item)
          return (
            <td className="p-1 h-auto">
              <>
                {index === 0 &&
                  (field === 'tab_layout' ||
                    field === 'side_menu_before_login_buttons' ||
                    field === 'side_menu_buttons') && (
                    <FormSelect
                      disabled={true}
                      value={selectedValue}
                      options={selectOption}
                      isGetObject={true}
                    />
                  )}
                {index === value.length - 1 && field === 'side_menu_buttons' && <div></div>}

                {((index !== 0 && index !== value.length - 1) ||
                  (index === 0 && (field === 'front_menu_buttons' || field === 'profile_menu_buttons')) ||
                  (index === value.length - 1 && field !== 'side_menu_buttons')) && (
                  <FormSelect
                    value={selectedValue}
                    options={viewOptions}
                    isGetObject={true}
                    onValueChange={(value) => {
                      changeValue(`${field}.extended_value.${index}.page_layout`)(value?.page_layout)
                      changeValue(`${field}.extended_value.${index}.state`)(value?.navigation)
                      switch (value?.page_layout) {
                        case C.PAGE_LAYOUT.single_view:
                          changeValue(`${field}.extended_value.${index}.pages.0.listing_type_id`)(
                            value?.value,
                          )
                          break
                        case C.PAGE_LAYOUT.special_view:
                          changeValue(`${field}.extended_value.${index}.special_page`)(value?.value)
                          break
                        case C.PAGE_LAYOUT.tab_view:
                          changeValue(`${field}.extended_value.${index}.layoutTab`)(value?.value)

                          changeTabPageTotal(field, item, index, 2)
                          setTimeout(() => {
                            changeValue(`${field}.extended_value.${index}.pages.${0}.listing_type_id`)(
                              C.VIEW_TAB[value?.value]?.[0]?.value,
                            )
                            changeValue(`${field}.extended_value.${index}.pages.${1}.listing_type_id`)(
                              C.VIEW_TAB[value?.value]?.[1]?.value,
                            )
                          }, 500)

                          break
                      }
                    }}
                  />
                )}
                {['yourorder', 'website'].includes(selectedValue) && (
                  <FormInput
                    label={'URI'}
                    value={item.special_link}
                    onChangeText={changeValue(`${field}.extended_value.${index}.special_link`)}
                    formClassName="mb-0"
                  />
                )}
                {['what_on', 'stamp_card'].includes(selectedValue) &&
                  item.pages.map((_, i) => {
                    return (
                      <div key={i} className="d-flex">
                        <FormSelect
                          label={`Tab #${i + 1}`}
                          disabled={i === 1}
                          value={_?.listing_type_id}
                          options={C.VIEW_TAB[selectedValue]}
                          onValueChange={(value) => {
                            changeValue(`${field}.extended_value.${index}.pages.${i}.listing_type_id`)(value)
                            if (C.VIEW_TAB[selectedValue]?.[0]?.value === +value) {
                              changeValue(`${field}.extended_value.${index}.pages.${1}.listing_type_id`)(
                                C.VIEW_TAB[selectedValue]?.[1]?.value,
                              )
                            } else {
                              changeValue(`${field}.extended_value.${index}.pages.${1}.listing_type_id`)(
                                C.VIEW_TAB[selectedValue]?.[0]?.value,
                              )
                            }
                          }}
                        />
                        <div className="mr-1" />
                        <FormInput
                          formClassName="mb-0"
                          label="Title"
                          value={_?.title}
                          onChangeText={changeValue(`${field}.extended_value.${index}.pages.${i}.title`)}
                        />
                      </div>
                    )
                  })}
              </>
            </td>
          )
        },
        navigation: (item, index) => {
          return (
            <td className="p-1 h-auto">
              {index === value.length - 1 && field === 'side_menu_buttons' && <div></div>}
              {!(index === value.length - 1 && field === 'side_menu_buttons') && (
                <FormSelect
                  disabled={true}
                  value={item.state}
                  options={C.NAVIGATION_KEY}
                  onValueChange={changeValue(`${field}.extended_value.${index}.state`)}
                />
              )}
            </td>
          )
        },
      }}
    />
  )
}

export default PageTable
