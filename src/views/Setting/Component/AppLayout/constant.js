export const fields = [
    {key: 'slot', _style: {width: '2%'}},
    {key: 'images', _style: {width: '95%'}},
  ]

export const appLayoutTabs = [
    'Gallery',
    'Welcome Screen Gallery',
    'Tabs',
    'Home Buttons',
    'Side Bar Before Login',
    'Side Bar',
    'Profile Buttons',
    'Community Point',
  ]