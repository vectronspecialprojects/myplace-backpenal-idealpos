import Collapse from '../../../common/Collapse'
import {CButton, CCol, CNav, CNavItem, CNavLink, CRow, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import RichEditor from '../../../common/RichEditor'
import StatusToggle from '../../../common/StatusToggle'
import FormInput from '../../../common/FormInput'
import Layout from './components/Layout'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import Tooltip from '../../../common/Tooltip'
import tips from '../../../tips'
import {isTrue} from '../../../helpers'
import {setModal} from '../../../reusable/Modal'

const Legals = ({setting}) => {
  const [tab, setTab] = useState(0)

  return (
    <Collapse title="Legals">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>Terms and Conditions</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Privacy Policy</CNavLink>
          </CNavItem>
        </CNav>
        <CTabContent>
          <CTabPane>
            <Item setting={setting} />
          </CTabPane>
          <CTabPane>
            <Item
              fieldInput="privacy_policy_tab_title"
              fieldTextarea="privacy_policy"
              setting={setting}
              webViewToggle="policy_webview_enable"
              fieldURL="policy_webview_url"
            />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

const Item = ({
  webViewToggle = 'term_webview_enable',
  fieldURL = 'term_webview_url',
  fieldInput = 'terms_and_conditions_tab_title',
  fieldTextarea = 'terms_and_conditions',
  setting,
}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    [webViewToggle]: setting[webViewToggle],
    [fieldURL]: setting[fieldURL],
    [fieldInput]: setting[fieldInput],
    [fieldTextarea]: setting[fieldTextarea],
  })

  const clickSave = async () => {
    if (checkUrl()) {
      dispatch(updateAppSettings({settings: JSON.stringify(Object.values(state))}))
    }
  }

  const checkUrl = () => {
    if (
      isTrue(state[webViewToggle].value) &&
      !/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/.test(
        state[fieldURL].value,
      )
    ) {
      setModal({
        title: 'URL Invalid Format',
        message: 'URL must be in this format ex https://www.bepoz.com.au',
        primaryBtnClick: () => {
          setModal({show: false})
        },
        isShowSecondaryBtn: false,
        closeButton: false,
      })
      return false
    }
    return true
  }

  const changeText = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const updateForm = async (e) => {
    state[fieldTextarea].extended_value = e.target.value
    setState({...state})
  }

  return (
    <>
      <CRow>
        <CCol xs={12} md={8}>
          <FormInput
            label="Tab Title"
            value={state[fieldInput].value || ''}
            onChangeText={changeText(fieldInput)}
          />
        </CCol>
        <CCol xs={12} md={8}>
          <div className="row pl-3 mb-3">
            <span className="mt-1">
              Enable Webview Navigator&nbsp;&nbsp; <Tooltip tipContent={tips.legals.termWebviewEnable} />
            </span>

            <StatusToggle
              color="primary"
              active={isTrue(state[webViewToggle].value)}
              className="ml-3 mr-3 mt-1"
              onChange={changeText(webViewToggle)}
            />
            <span className="mr-2 mt-1">URL:</span>

            {isTrue(state[webViewToggle].value) && (
              <FormInput
                style={{marginBottom: -22}}
                onBlur={checkUrl}
                className="w-50"
                placeholder="https://www.bepoz.com.au"
                value={state[fieldURL].value || ''}
                onChangeText={changeText(fieldURL)}
              />
            )}
          </div>
        </CCol>
        {!isTrue(state[webViewToggle].value) && (
          <CCol xs={12} md={8}>
            <RichEditor initValue={state[fieldTextarea].extended_value || ''} onChange={updateForm} />
          </CCol>
        )}

        <CCol xs={12}>
          <CButton
            variant="outline"
            color="secondary"
            className="text-dark font-weight-bold mt-3"
            onClick={clickSave}>
            Save Changes
          </CButton>
        </CCol>
      </CRow>
    </>
  )
}
export default Legals
