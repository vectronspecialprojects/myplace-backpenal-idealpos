import {CCol} from '@coreui/react'
import React from 'react'

const SettingLayout = ({children, title, desc, className = 'mb-3', titleWidth = 4}) => {
  return (
    <>
      <CCol xs={12} md={titleWidth} className={className}>
        <h6 className="text-capitalize">{title}</h6>
        <span className="text-muted text-capitalize">{desc}</span>
      </CCol>
      <CCol xs={12} md={12 - titleWidth} className={className}>
        {children}
      </CCol>
    </>
  )
}

export default SettingLayout
