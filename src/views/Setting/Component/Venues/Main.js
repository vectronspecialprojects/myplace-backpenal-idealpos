import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import Layout from '../components/Layout'
import StatusToggle from '../../../../common/StatusToggle'
import {isTrue} from '../../../../helpers'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import HourAndLocation from './HourAndLocation'

const MultiVenue = ({setting}) => {
  
  const [state, setState] = useState({
    venue_number: {...setting.venue_number, extended_value: JSON.parse(setting.venue_number.extended_value)},
    google_map_apikey: setting.google_map_apikey,
    is_preffered_venue_color: setting.is_preffered_venue_color,
    android_link: setting.android_link,
    ios_link: setting.ios_link,
    distance_order: setting.distance_order,
  })
  
  const dispatch = useDispatch()

  const clickSave = () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <CCol>
      <CRow>
        <CCol xs={12} className="mb-3 ">
          <div className="row pl-3">
            <h3 className="text-capitalize">Single Venue</h3>
            <StatusToggle
              active={state.venue_number.value === 'multiple'}
              className="ml-3 mr-3"
              onChange={(value) => {
                if (value) {
                  changeValue('venue_number')('multiple')
                } else {
                  changeValue('venue_number')('single')
                }
              }}
            />
            <h3 className="text-capitalize">Multi Venue</h3>
          </div>
        </CCol>
      </CRow>

      <Layout
        title="Android Download Link"
        contentWidth={state.venue_number.value === 'single' ? 10 : 8}
        titleWidth={state.venue_number.value === 'single' ? 2 : 4}>
        <FormInput value={state.android_link?.value} onChangeText={changeValue('android_link')} />
      </Layout>

      <Layout
        title="IOS Download Link"
        contentWidth={state.venue_number.value === 'single' ? 10 : 8}
        titleWidth={state.venue_number.value === 'single' ? 2 : 4}>
        <FormInput value={state.ios_link?.value} onChangeText={changeValue('ios_link')} />
      </Layout>

      {state.venue_number.value === 'single' && (
        <CRow>
          <HourAndLocation setting={setting} onSave={clickSave} />
        </CRow>
      )}

      {state.venue_number.value === 'multiple' && (
        <>
          <Layout
            title="Google Map API key"
            desc="Type the Google Map APi key ID here. e.g. 1"
            contentWidth={8}>
            <FormInput
              value={state.google_map_apikey.value}
              onChangeText={changeValue('google_map_apikey')}
            />
          </Layout>

          <Layout title="Preffered Venue Color" contentWidth={8}>
            <StatusToggle
              color="primary"
              active={isTrue(state.is_preffered_venue_color.value)}
              className="mt-0"
              onChange={changeValue('is_preffered_venue_color')}
            />
          </Layout>

          <Layout title="Distance Order	" contentWidth={8} >
            <StatusToggle
              color="primary"
              active={isTrue(state.distance_order.value)}
              className="mt-0"
              onChange={changeValue('distance_order')}
            />
          </Layout>

          <CRow>
            <CCol xs={12}>
              <CButton
                variant="outline"
                color="secondary"
                className="text-dark font-weight-bold mt-3"
                onClick={clickSave}>
                Save Changes
              </CButton>
            </CCol>
          </CRow>
        </>
      )}
    </CCol>
  )
}

export default MultiVenue
