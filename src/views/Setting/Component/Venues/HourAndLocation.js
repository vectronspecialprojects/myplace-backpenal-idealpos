import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import SettingLayout from '../SettingLayout'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_HIDE_OPTION} from '../../constant'
import {isTrue} from '../../../../helpers'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import StatusToggle from '../../../../common/StatusToggle'

const HourAndLocation = ({setting, onSave}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    address: setting.address,
    email: setting.email,
    telp: setting.telp,
    fax: setting.fax,
    android_link: setting.android_link,
    ios_link: setting.ios_link,
    social_links: {...setting.social_links, extended_value: JSON.parse(setting.social_links.extended_value)},
    menu_links: {...setting.menu_links, extended_value: JSON.parse(setting.menu_links.extended_value)},
    hide_opening_hours_info: setting.hide_opening_hours_info,
    open_and_close_hours: {
      ...setting.open_and_close_hours,
      extended_value: JSON.parse(setting.open_and_close_hours.extended_value),
    },
    hide_delivery_info: setting.hide_delivery_info,
    pickup_and_delivery_hours: {
      ...setting.pickup_and_delivery_hours,
      extended_value: JSON.parse(setting.pickup_and_delivery_hours.extended_value),
    },
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
    onSave?.()
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const changeExtendedValue = (key, index, field) => (extended_value) => {
    if (index !== undefined) {
      state[key].extended_value[index][field] = extended_value
      setState({...state})
    } else {
      setState({...state, [key]: {...state[key], extended_value}})
    }
  }

  return (
    <>
      <SettingLayout title="Address" titleWidth={2} className="mb-0">
        <FormInput value={state.address.extended_value} onChangeText={changeExtendedValue('address')} />
      </SettingLayout>
      <SettingLayout title="Email" titleWidth={2} className="mb-0">
        <FormInput value={state.email.extended_value} onChangeText={changeExtendedValue('email')} />
      </SettingLayout>
      <SettingLayout title="Telephone" titleWidth={2} className="mb-0">
        <FormInput value={state.telp.extended_value} onChangeText={changeExtendedValue('telp')} />
      </SettingLayout>
      <SettingLayout title="Fax" titleWidth={2} className="mb-0">
        <FormInput value={state.fax.extended_value} onChangeText={changeExtendedValue('fax')} />
      </SettingLayout>
      <CCol xs={12}>
        <CRow>
          <CCol xs={12} md={6}>
            <h5 className="mb-4">Social Links</h5>
            <CRow>
              {state.social_links.extended_value.map((item, index) => (
                <SettingLayout title={item.platform} className="m-0" key={item.platform}>
                  <FormInput
                    value={item.url}
                    onChangeText={changeExtendedValue('social_links', index, 'url')}
                  />
                </SettingLayout>
              ))}
            </CRow>
          </CCol>

          <CCol xs={12} md={6}>
            <h5 className="mb-4">Menu Links</h5>
            <CRow>
              {state.menu_links.extended_value.map((item, index) => (
                <SettingLayout title={item.platform} className="m-0" key={item.platform}>
                  <FormInput
                    value={item.url}
                    onChangeText={changeExtendedValue('menu_links', index, 'url')}
                  />
                </SettingLayout>
              ))}
            </CRow>
          </CCol>
        </CRow>
      </CCol>

      <SettingLayout title="Open and Close Hours" titleWidth={2}>
        {/* <FormSelect
          options={SETTING_HIDE_OPTION}
          value={state.hide_opening_hours_info.value}
          onValueChange={changeValue('hide_opening_hours_info')}
        /> */}

        <StatusToggle
          color="primary"
          active={!isTrue(state.hide_opening_hours_info.value)}
          className="mt-0"
          onChange={(e) => {
            if (e) changeValue('hide_opening_hours_info')('false')
            if (!e) changeValue('hide_opening_hours_info')('true')
            
          }}
        />
      </SettingLayout>

      {!isTrue(state.hide_opening_hours_info.value) && (
        <CCol xs={12}>
          {state.open_and_close_hours.extended_value.map((item, index) => (
            <CRow key={item.day}>
              <CCol xs={2} />
              <CCol xs={10}>
                <CRow>
                  <SettingLayout title={item.day} className="m-0" titleWidth={1}>
                    <FormInput
                      value={item.time}
                      onChangeText={changeExtendedValue('open_and_close_hours', index, 'time')}
                    />
                  </SettingLayout>
                </CRow>
              </CCol>
            </CRow>
          ))}
        </CCol>
      )}

      <SettingLayout title="Pickup and Delivery Hours" titleWidth={2}>
        {/* <FormSelect
          options={SETTING_HIDE_OPTION}
          value={state.hide_delivery_info.value}
          onValueChange={changeValue('hide_delivery_info')}
        /> */}
        <StatusToggle
          color="primary"
          active={!isTrue(state.hide_delivery_info.value)}
          className="mt-0"
          onChange={(e) => {
            if (e) changeValue('hide_delivery_info')('false')
            if (!e) changeValue('hide_delivery_info')('true')
            
          }}
        />
      </SettingLayout>

      {!isTrue(state.hide_delivery_info.value) && (
        <CCol xs={12}>
          {state.pickup_and_delivery_hours.extended_value.map((item, index) => (
            <CRow>
              <CCol xs={2} />
              <CCol xs={10}>
                <CRow>
                  <SettingLayout title={item.day} className="m-0" titleWidth={1}>
                    <FormInput
                      value={item.time}
                      onChangeText={changeExtendedValue('pickup_and_delivery_hours', index, 'time')}
                    />
                  </SettingLayout>
                </CRow>
              </CCol>
            </CRow>
          ))}
        </CCol>
      )}
      <CCol xs={12}>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default HourAndLocation
