import React, {useEffect, useState, useMemo} from 'react'
import {
  CButton,
  CCol,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTabs,
} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import FormSelect from '../../../../common/FormSelect'
import StatusToggle from '../../../../common/StatusToggle'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import Tooltip from '../../../../common/Tooltip'
import tips from '../../../../tips'
import helpers from '../../../Listing/helpers'
import FormCheckbox from '../../../../common/FormCheckbox'
import FormItem from '../../../../common/FormItem'
import {
  postUploadGalleryPhoto,
  postVenue,
  updateVenue,
  getBepozVenueName,
} from '../../../../utilities/ApiManage'
import SettingLayout from '../SettingLayout'
import VenueTagSelect from '../../../../common/VenueTagSelect'
import {isTrue} from '../../../../helpers'
import {useSelector} from 'react-redux'
import {setModal} from '../../../../reusable/Modal'

import AddressField from '../../../../common/AddressField'

function PopupCreateVenue({show, closeModal, selectedVenue, onSubmit}) {
  const [form, setForm] = useState({...DEFAULT_VENUE})
  const [tab, setTab] = useState(0)
  const [bepozVenueNames, setBepozVenueNames] = useState([])

  const changeForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))
  const appSettings = useSelector((state) => state.app.appSettings)

  const getBVenueName = async () => {
    const res = await getBepozVenueName()
    if (res.ok) {
      const bepozVenues = res.data.map((bepozVenue) => {
        return {label: `id: ${bepozVenue?.id} & name: ${bepozVenue?.name}`, value: +bepozVenue?.id}
      })
      setBepozVenueNames(bepozVenues)
    }
  }

  useEffect(() => {
    getBVenueName()
  }, [show])

  const links = useMemo(() => {
    return [
      {label: 'link1', url: form?.link1 || ''},
      {label: 'link2', url: form?.link2 || ''},
      {label: 'link3', url: form?.link3 || ''},
    ]
  }, [form])

  useEffect(() => {
    setForm({
      ...DEFAULT_VENUE,
      ...(selectedVenue
        ? {
            ...JSON.parse(JSON.stringify(selectedVenue)),
            menu_links: JSON.parse(selectedVenue?.menu_links),
            open_and_close_hours: JSON.parse(selectedVenue?.open_and_close_hours),
            social_links: JSON.parse(selectedVenue?.social_links),
            pickup_and_delivery_hours: JSON.parse(selectedVenue?.pickup_and_delivery_hours),
          }
        : {}),
    })
    setTab(0)
  }, [show, selectedVenue])

  const saveAndClose = async () => {
    if (checkUrl()) {
      const res = await postVenue(form)
      if (res.ok) {
        closeModal()
        onSubmit()
      }
    }
  }

  const updateAndClose = async () => {
    if (checkUrl()) {
      try {
        const res = await updateVenue(form.id, form)
        if (res.ok) {
          closeModal()
          onSubmit()
        }
      } catch (e) {}
    }
  }

  const uploadImage = async (e) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) {
      const newForm = {...form}
      newForm.image = res.data
      setForm({...newForm})
    }
  }

  const checkUrl = () => {
    if (
      isTrue(form?.your_order_link) &&
      isTrue(form?.your_order_integration) &&
      !/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/.test(
        form?.your_order_link,
      )
    ) {
      setModal({
        title: 'URL Invalid Format',
        message: 'URL must be in this format ex https://www.yourorder.io',
        primaryBtnClick: () => {
          setModal({show: false})
        },
        isShowSecondaryBtn: false,
        closeButton: false,
      })
      return false
    }
    return true
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>{!!selectedVenue ? 'Update Venue' : 'Create Venue'}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CTabs activeTab={tab} onActiveTabChange={setTab}>
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink>MAIN INFO</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>EXTERNAL LINKS</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>VENUE TAGS</CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink>BEPOZ SETTINGS</CNavLink>
              </CNavItem>
              {!!selectedVenue && (
                <CNavItem>
                  <CNavLink>GAMING SETTINGS</CNavLink>
                </CNavItem>
              )}
            </CNav>
            <CTabContent style={{marginTop: 20}}>
              <CTabPane>
                <CRow>
                  <CCol xs={12} md={6}>
                    {selectedVenue && (
                      <FormCheckbox
                        label="Allow Signup for Existing Customer Only"
                        id="allow_signup_for_existing_customer_only"
                        checked={form?.allow_signup_for_existing_customer_only}
                        onChange={changeForm}
                        desc="Toggle Allow Signup For Existing Customer Only status(active/inactive)"
                      />
                    )}

                    <FormInput
                      label="Venue Name"
                      value={form?.name}
                      id="name"
                      onChange={changeForm}
                      desc="Set a name for the venue"
                    />
                    <FormSelect
                      label="Bepoz Venue ID"
                      value={form?.bepoz_venue_id}
                      id="bepoz_venue_id"
                      options={bepozVenueNames}
                      onChange={changeForm}
                    />
                    <FormInput
                      label="Venue Address"
                      id="address"
                      value={form?.address}
                      onChange={changeForm}
                    />

                    {/* <AddressField/> */}

                    {isTrue(appSettings?.is_preffered_venue_color?.value) && (
                      <FormInput
                        label="Color"
                        id="color"
                        value={form?.color}
                        onChange={changeForm}
                        desc="Set the color for the venue"
                        type={'color'}
                        style={{width: 70}}
                      />
                    )}
                    <FormInput
                      label="Venue Email"
                      id="email"
                      value={form?.email}
                      onChange={changeForm}
                      desc="Set the email address for the venue"
                    />
                    <FormInput
                      label="Venue Contract Number"
                      id="telp"
                      value={form?.telp}
                      onChange={changeForm}
                      desc="Set the contract number for the venue"
                    />

                    <FormCheckbox
                      label="Active"
                      id="active"
                      checked={isTrue(form?.active)}
                      onChange={changeForm}
                      checkValue={true}
                      uncheckValue={false}
                    />
                    <FormCheckbox
                      label="Delivery Info"
                      id="hide_delivery_info"
                      checked={!isTrue(form?.hide_delivery_info)}
                      onChange={changeForm}
                      checkValue={false}
                      uncheckValue={true}
                    />
                    <FormCheckbox
                      label="Opening Hours Info"
                      id="hide_opening_hours_info"
                      checked={!isTrue(form?.hide_opening_hours_info)}
                      onChange={changeForm}
                      checkValue={false}
                      uncheckValue={true}
                    />
                    <FormItem label="Image/Logo" className="mt-3">
                      <div style={{width: '100px'}}>
                        <UploadImageCrop
                          sizeLimited={true}
                          imgWidth={100}
                          imgHeight={100}
                          cropWidth={100}
                          aspect={1}
                          onConfirm={(e) => uploadImage(e)}
                          className="w-100"
                          hideUndo>
                          <Image blob={form?.image} className="w-100" />
                        </UploadImageCrop>
                        {/* <FormInput value={form?.image || ''} id="image" onChange={changeForm} /> */}
                      </div>
                    </FormItem>
                  </CCol>
                  <CCol xs={12} md={6}>
                    {!form?.hide_delivery_info && (
                      <>
                        <h5>Pickup and Delivery Hours</h5>
                        {form?.pickup_and_delivery_hours.map((item, index) => (
                          <CRow key={item.day}>
                            <SettingLayout title={item.day} className="m-0">
                              <FormInput
                                id={`pickup_and_delivery_hours.${index}.time`}
                                value={item.time}
                                onChange={changeForm}
                              />
                            </SettingLayout>
                          </CRow>
                        ))}
                      </>
                    )}
                    {!form?.hide_opening_hours_info && (
                      <>
                        <h5>Open and Close Hours</h5>
                        {form?.open_and_close_hours.map((item, index) => (
                          <CRow key={item.day}>
                            <SettingLayout title={item.day} className="m-0">
                              <FormInput
                                id={`open_and_close_hours.${index}.time`}
                                value={item.time}
                                onChange={changeForm}
                              />
                            </SettingLayout>
                          </CRow>
                        ))}
                      </>
                    )}
                  </CCol>
                </CRow>
              </CTabPane>
              <CTabPane>
                <div>
                  <h5>Social Links</h5>
                  {form?.social_links.map((item, index) => (
                    <CRow key={item?.platform}>
                      <SettingLayout title={item?.platform} className="m-0">
                        <FormInput id={`social_links.${index}.url`} value={item?.url} onChange={changeForm} />
                      </SettingLayout>
                    </CRow>
                  ))}
                  <h5>Menu Links</h5>
                  {form?.menu_links.map((item, index) => (
                    <CRow key={item?.platform}>
                      <SettingLayout title={item?.platform} className="m-0">
                        <FormInput id={`menu_links.${index}.url`} value={item?.url} onChange={changeForm} />
                      </SettingLayout>
                    </CRow>
                  ))}
                  <h5>Links</h5>
                  {links.map((item, index) => (
                    <CRow key={item?.label}>
                      <SettingLayout title={item?.label} className="m-0">
                        <FormInput id={`${item?.label}`} value={item?.url} onChange={changeForm} />
                      </SettingLayout>
                    </CRow>
                  ))}
                </div>
              </CTabPane>
              <CTabPane>
                <CRow>
                  <CCol xs={12}>
                    <FormItem>
                      <VenueTagSelect onChange={console.log} />
                    </FormItem>
                  </CCol>
                </CRow>
              </CTabPane>
              <CTabPane>
                <CRow>
                  <CCol xs={12}>
                    <CFormGroup>
                      <FormCheckbox
                        label="Gaming System Point (Ebet)"
                        desc="Enable this to grab point from gaming System like Ebet, Max"
                        id="gaming_system_point"
                        checked={form?.gaming_system_point}
                        onChange={changeForm}
                      />
                    </CFormGroup>

                    <FormInput
                      label="Bepoz Server Public IP and Port Number for XML API"
                      placeholder="xxx.xxx.xxx.xxx:xxxx"
                      id="bepoz_api"
                      value={form?.bepoz_api}
                      onChange={changeForm}
                    />
                    <FormInput
                      label="Bepoz MAC Key"
                      id="bepoz_mac_key"
                      value={form?.bepoz_mac_key}
                      onChange={changeForm}
                    />

                    {/* <CFormGroup>
                      <FormCheckbox
                        label="Enable Your Order Integration"
                        id="your_order_integration"
                        checked={form?.your_order_integration}
                        onChange={(e) => {
                          console.log('e', e)
                          changeForm(e)
                        }}
                      />
                    </CFormGroup> */}

                    <CCol xs={12} md={8}>
                      <div className="row mb-3">
                        <span className="mt-1">
                          Enable Your Order Integration&nbsp;&nbsp;{' '}
                          <Tooltip tipContent={tips.venue.yourorderIntegration} />
                        </span>
                        <StatusToggle
                          color="primary"
                          active={isTrue(form?.your_order_integration)}
                          id="your_order_integration"
                          className="ml-3 mr-3 mt-1"
                          onChange={(e) => {
                            changeForm({target: {checked: e, id: 'your_order_integration', value: e}})
                          }}
                        />
                      </div>
                    </CCol>

                    {/* <FormInput
                      label="Your Order API URL https://api.vecport.net/webstore/"
                      id="your_order_api"
                      value={form?.your_order_api}
                      onChange={changeForm}
                      desc="Set Your Order API URL here"
                    />
                    <FormInput
                      label="Your Order API KEY"
                      id="your_order_key"
                      value={form?.your_order_key}
                      onChange={changeForm}
                      desc="Set Your Order API KEY here"
                    /> */}
                    {isTrue(form?.your_order_integration) && (
                      <FormInput
                        label="Your Order Link"
                        id="your_order_link"
                        value={form?.your_order_link}
                        onChange={changeForm}
                        disabled={!isTrue(form?.your_order_integration)}
                        onBlur={checkUrl}
                      />
                    )}
                    {isTrue(form?.your_order_integration) && (
                      <FormInput
                        label="Your Order Location Parameter(Optional)"
                        id="your_order_location"
                        value={form?.your_order_location}
                        onChange={changeForm}
                        disabled={!isTrue(form?.your_order_integration)}
                      />
                    )}
                  </CCol>
                </CRow>
              </CTabPane>
              <CTabPane>
                <CRow>
                  <CCol xs={12}>
                    <FormItem>
                      <FormInput
                        label="Custom Field Value"
                        value={form?.odyssey_custom_field_id}
                        id="odyssey_custom_field_id"
                        onChange={changeForm}
                        desc="Set Custom Field value here."
                      />
                    </FormItem>
                  </CCol>
                </CRow>
              </CTabPane>
            </CTabContent>
          </CTabs>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={!!selectedVenue ? updateAndClose : saveAndClose}>
            {!!selectedVenue ? 'UPDATE' : 'SAVE'} & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

const DEFAULT_VENUE = {
  active: true,
  address: null,
  allow_signup_for_existing_customer_only: false,
  android_link: null,
  bepoz_api: null,
  bepoz_mac_key: null,
  bepoz_operator_id: null,
  bepoz_payment_name: null,
  bepoz_till_id: null,
  bepoz_venue_id: null,
  email: null,
  gaming_system_point: false,
  hide_delivery_info: 1,
  hide_opening_hours_info: 1,
  image: 'https://via.placeholder.com/100x100',
  ios_link: null,
  link1: null,
  link2: null,
  link3: null,
  menu_links: [
    {platform: 'food', url: ''},
    {platform: 'drink', url: ''},
  ],
  name: '',
  open_and_close_hours: [
    {day: 'Mon', time: '9AM to 5PM'},
    {day: 'Tue', time: '9AM to 5PM'},
    {day: 'Wed', time: '9AM to 5PM'},
    {day: 'Thu', time: '9AM to 5PM'},
    {day: 'Fri', time: '9AM to 5PM'},
    {day: 'Sat', time: '9AM to 5PM'},
    {day: 'Sun', time: '9AM to 5PM'},
  ],
  pickup_and_delivery_hours: [
    {day: 'Mon', time: '9AM to 5PM'},
    {day: 'Tue', time: '9AM to 5PM'},
    {day: 'Wed', time: '9AM to 5PM'},
    {day: 'Thu', time: '9AM to 5PM'},
    {day: 'Fri', time: '9AM to 5PM'},
    {day: 'Sat', time: '9AM to 5PM'},
    {day: 'Sun', time: '9AM to 5PM'},
  ],
  social_links: [
    {platform: 'facebook', url: 'www.facebook.com'},
    {platform: 'twitter', url: 'www.twitter.com'},
    {platform: 'instagram', url: 'www.instagram.com'},
    {platform: 'google-plus', url: 'www.googleplus.com'},
    {platform: 'website', url: 'www.example.com'},
  ],
  tagSelected: 0,
  telp: null,
  // your_order_api: null,
  your_order_integration: false,
  // your_order_key: null,
  your_order_link: null,
}

export default PopupCreateVenue
