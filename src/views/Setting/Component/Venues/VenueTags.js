import {CButton, CCol, CDataTable, CPagination, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {
  getPaginateVenueTag,
  postChangeVenueTagStatus,
  postChangeVenueTagHideInApp,
} from '../../../../utilities/ApiManage'
import StatusToggle from '../../../../common/StatusToggle'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import TableFilter from '../../../../common/TableFilter'
import PopupCreateTag from './PopupCreateTag'
import {useDispatch, useSelector} from 'react-redux'
import {updateAppSettings, getTags} from '../../../../redux/actions/app'
import Layout from '../components/Layout'
import {isTrue} from '../../../../helpers'
import {setModal} from '../../../../reusable/Modal'

const fields = [{key: 'id', label: '#'}, 'name', 'hide_in_app', 'active', 'display_order', {key: 'option'}]

const VenueTag = ({setting}) => {
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const tiers = useSelector((state) => state.app.tier)
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [searchText, setSearchText] = useState('')
  const [createTagVisible, setCreateTagVisible] = useState(false)
  const [selectedTag, setSelectedTag] = useState(null)
  const [state, setState] = useState({
    venue_tags_enable: setting.venue_tags_enable,
  })

  const dispatch = useDispatch()

  const fetchVenueTag = async () => {
    const res = await getPaginateVenueTag(currentPage, itemPerPage, searchText)
    if (!res.ok) throw new Error(res.message)
    setData(res.data)
  }

  useEffect(() => {
    fetchVenueTag()
  }, [currentPage, itemPerPage, searchText])

  const changeValue = (key) => (value) => {
    const newState = {...state, [key]: {...state[key], value}}
    setState(newState)
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(newState)),
      }),
    )
  }

  const changeTableValue = (key, index) => async (value) => {
    const isTierInUsed = tiers.filter((tier) => +tier?.allowed_venues === data?.data[index]?.id)
    if (key === 'status' && value === 0 && isTierInUsed.length > 0) {
      setModal({
        title: 'WARNING !',
        message: `This venue tag is currrently associate with ${isTierInUsed.length} tiers. Please disassociate this venue tag with all tiers before disabling it.`,
        primaryBtnText: 'OK',
        primaryBtnClick: () => {
          setModal({show: false})
          return
        },
        isShowSecondaryBtn: false,
        closeButton: true,
      })
    } else {
      const newData = {...data, data: [...data.data]}
      newData.data[index][key] = value
      const res =
        key === 'status'
          ? await postChangeVenueTagStatus({id: newData.data[index].id, [key]: value})
          : await postChangeVenueTagHideInApp({id: newData.data[index].id, [key]: value})
      if (res.ok) {
        setData(newData)
        if (key === 'status') dispatch(getTags())
      }
    }
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
  }

  return (
    <>
      <CCol>
        <Layout title="Venue Tags Enable" contentWidth={4}>
          <StatusToggle
            disabled={!bepozVersion}
            color="primary"
            id="venue_tags_enable"
            active={isTrue(state.venue_tags_enable.value)}
            className="ml-3 mr-3"
            onChange={(e) => {
              if (!!e) changeValue('venue_tags_enable')('true')
              else changeValue('venue_tags_enable')('false')
            }}
          />
        </Layout>

        {isTrue(state.venue_tags_enable.value) && (
          <TableFilter
            disabled={!bepozVersion}
            params={{searchText}}
            title="Tags"
            onClickCreate={() => setCreateTagVisible(true)}
            total={data.total}
            onParamChange={handleChangeParam}
          />
        )}
        {isTrue(state.venue_tags_enable.value) && (
          <CDataTable
            items={data?.data || []}
            fields={fields}
            striped
            itemsPerPage={itemPerPage}
            scopedSlots={{
              name: (item, index) => (
                <td>
                  <CRow>
                    <CButton
                      disabled={!bepozVersion}
                      style={{marginTop: -7}}
                      onClick={() => {
                        setSelectedTag(item.id)
                        setCreateTagVisible(true)
                      }}>
                      {item.name}
                    </CButton>
                  </CRow>
                </td>
              ),
              hide_in_app: (item, index) => (
                <td>
                  <StatusToggle
                    disabled={!bepozVersion}
                    color="primary"
                    active={isTrue(item.hide_in_app)}
                    onChange={(e) => {
                      if (!!e) changeTableValue('hide_in_app', index)(1)
                      else changeTableValue('hide_in_app', index)(0)
                    }}
                  />
                </td>
              ),
              active: (item, index) => (
                <td>
                  <StatusToggle
                    autoSwitch={false}
                    disabled={!bepozVersion}
                    color="primary"
                    active={isTrue(item.status)}
                    onChange={(e) => {
                      if (!!e) changeTableValue('status', index)(1)
                      else changeTableValue('status', index)(0)
                    }}
                  />
                </td>
              ),
              option: (item) => (
                <td>
                  <CRow>
                    <CButton
                      disabled={!bepozVersion}
                      onClick={() => {
                        setSelectedTag(item.id)
                        setCreateTagVisible(true)
                      }}>
                      <CIcon content={freeSet.cilPen} />
                    </CButton>
                  </CRow>
                </td>
              ),
            }}
          />
        )}
        {isTrue(state.venue_tags_enable.value) && (
          <CPagination
            activePage={currentPage}
            pages={Math.ceil(data?.total / itemPerPage)}
            onActivePageChange={setCurrentPage}
          />
        )}

        <PopupCreateTag
          show={createTagVisible}
          onSubmit={fetchVenueTag}
          closeModal={() => {
            setSelectedTag(null)
            setCreateTagVisible(false)
          }}
          selectedTagId={selectedTag}
        />
      </CCol>
    </>
  )
}

export default VenueTag
