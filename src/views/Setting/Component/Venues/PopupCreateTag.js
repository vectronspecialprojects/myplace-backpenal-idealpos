import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCard,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import helpers from '../../../Listing/helpers'
import FormItem from '../../../../common/FormItem'
import {getVenueTag, postVenueTag, putVenueTag} from '../../../../utilities/ApiManage'
import FormSelect from '../../../../common/FormSelect'
import VenueSelect from '../../../../common/VenueSelect'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons'
import {MODE} from '../../../../constants'
import {CLabel} from '@coreui/react/es'
import SortableComponent from '../../../../common/SortableList'
import StatusToggle from '../../../../common/StatusToggle'
import {isTrue} from '../../../../helpers'

function PopupCreateTag({show, closeModal, selectedTagId, onSubmit}) {
  const [form, setForm] = useState({...DEFAULT_TAG, venuesPivot: []})
  const [selectedVenue, setSelectedVenue] = useState(null)
  const changeForm = (event) => {
    setForm((oldState) => helpers.updateForm(event, oldState))
  }

  useEffect(() => {
    if (selectedTagId) {
      ;(async () => {
        const res = await getVenueTag(selectedTagId)
        if (res.ok) {
          setForm({
            ...DEFAULT_TAG,
            ...res.data,
            venuesPivot: res?.data?.venues?.map((_) => ({..._, mode: MODE.NOT_MODIFIED})) || [],
          })
        }
      })()
    } else {
      setForm({...DEFAULT_TAG, venuesPivot: []})
    }
    setSelectedVenue(null)
  }, [show, selectedTagId])

  const addVenue = () => {
    const newForm = {...form}
    if (!newForm.venuesPivot.find((_) => _.id === selectedVenue.id) && selectedVenue) {
      newForm.venuesPivot.push({
        id: selectedVenue.id,
        name: selectedVenue.name,
        gaming_system_point: selectedVenue.gaming_system_point,
        allow_signup_for_existing_customer_only: selectedVenue.allow_signup_for_existing_customer_only,
        bepoz_venue_id: selectedVenue.bepoz_venue_id,
        bepoz_api: selectedVenue.bepoz_api,
        bepoz_mac_key: selectedVenue.bepoz_mac_key,
        bepoz_secondary_api: selectedVenue.bepoz_secondary_api,
        bepoz_training_mode: selectedVenue.bepoz_training_mode,
        default_card_number_prefix: selectedVenue.default_card_number_prefix,
        default_card_track: selectedVenue.default_card_track,
        color: selectedVenue.color,
        state_id: selectedVenue.state_id,
        link1: selectedVenue.link1,
        link2: selectedVenue.link2,
        link3: selectedVenue.link3,
        your_order_link: selectedVenue.your_order_link,
        your_order_integration: selectedVenue.your_order_integration,
        your_order_api: selectedVenue.your_order_api,
        your_order_key: selectedVenue.your_order_key,
        odyssey_custom_field_id: selectedVenue.odyssey_custom_field_id,
        tier: selectedVenue.tier,
        mode: MODE.NEW,
      })
    } else {
      newForm.venuesPivot.forEach((venue, index) => {
        if (venue.id === selectedVenue.id && venue.mode === MODE.DELETED) {
          newForm.venuesPivot[index].mode = MODE.NOT_MODIFIED
        }
      })
    }
    setForm(newForm)
  }

  const removeVenue = (item, index) => {
    const newForm = {...form}
    if (item.mode === MODE.NEW) {
      newForm.venuesPivot = newForm.venuesPivot.filter((_) => _.id !== item.id)
    } else {
      newForm.venuesPivot[index].mode = MODE.DELETED
    }
    // if (item.mode === MODE.NOT_MODIFIED) {
    //   newForm.venuesPivot[index].mode = MODE.DELETED
    // } else {
    //   newForm.venuesPivot = newForm.venuesPivot.filter((_) => _.id !== item.id)
    // }
    setForm(newForm)
  }

  const saveAndClose = async () => {
    const res = await (selectedTagId ? putVenueTag(selectedTagId, form) : postVenueTag(form))
    setForm({...DEFAULT_TAG, venuesPivot: []})
    if (res.ok) {
      closeModal()
      onSubmit()
    }
  }

  const sortData = (data) => {
    const newForm = {...form}
    newForm.venuesPivot = data
    setForm(newForm)
  }
  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Venue Tag Maintenance</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <FormInput label="Tag Name" value={form.name} id="name" onChange={changeForm} />
          <CCol>
            <CRow>
              <FormSelect
                className={'w-50'}
                label="Venue Sorting"
                id="display_order_type"
                value={form.display_order_type}
                onChange={changeForm}
                options={DISPLAY_ORDER}
              />
              <div className="row pl-3" style={{marginLeft: 10, marginTop: 32}}>
                <span>Hide In App</span>
                <StatusToggle
                  color="primary"
                  id="hide_in_app"
                  active={isTrue(form.hide_in_app)}
                  className="ml-3 mr-3"
                  getEvent
                  onChange={changeForm}
                />
              </div>
            </CRow>
          </CCol>

          <div className="d-flex">
            <FormItem label="Venue" desc="Add the venues that belong to this tag" className="flex-fill">
              <VenueSelect value={selectedVenue} onVenueChange={setSelectedVenue} defaultText="Select.." />
            </FormItem>
            <CButton className="btn  ml-5" onClick={addVenue}>
              Add
            </CButton>
          </div>
          <SortableComponent
            onChange={(data) => sortData(data)}
            items={form.venuesPivot}
            renderItem={(item, index) => {
              if (item.mode === MODE.DELETED) return null
              return (
                <CRow key={item.id}>
                  <CCol xs={9}>{item.name}</CCol>
                  <CCol xs={3}>
                    <CButton onClick={() => removeVenue(item, index)}>
                      <CIcon style={{pointerEvents: 'none'}} content={freeSet.cilTrash} />
                    </CButton>
                  </CCol>
                </CRow>
              )
            }}
          />
          {/*{form.venuesPivot*/}
          {/*  .filter((_) => _.mode !== MODE.DELETED)*/}
          {/*  .map((item, index) => (*/}
          {/*    <CRow key={item.id}>*/}
          {/*      <CCol xs={9}>{item.name}</CCol>*/}
          {/*      <CCol xs={3}>*/}
          {/*        <CButton onClick={() => removeVenue(item, index)}>*/}
          {/*          <CIcon content={freeSet.cilTrash} />*/}
          {/*        </CButton>*/}
          {/*      </CCol>*/}
          {/*    </CRow>*/}
          {/*  ))}*/}
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

const DISPLAY_ORDER = [
  {value: 'manual', label: 'Manual'},
  {value: 'byDistance', label: 'By Distance'},
  {value: 'alphabetically', label: 'Alphabetically'},
]

const DEFAULT_TAG = {
  display_order_type: DISPLAY_ORDER[2].value,
  name: '',
  status: 1,
  hide_in_app: 0,
  venuesPivot: [],
}

export default PopupCreateTag
