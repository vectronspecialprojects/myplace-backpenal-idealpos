import Collapse from '../../../../common/Collapse'
import {CNav, CNavItem, CNavLink, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import MultiVenue from './MultiVenue'
import Main from './Main'
import VenueTag from './VenueTags'

const Venues = ({setting}) => {
  const [tab, setTab] = useState(0)
  return (
    <Collapse title="Venues">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>Main</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Venue List</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Venue Tags</CNavLink>
          </CNavItem>
        </CNav>
        <CTabContent style={{marginTop: 20}}>
          <CTabPane>
            <Main setting={setting} />
          </CTabPane>
          <CTabPane>
            <MultiVenue setting={setting} />
          </CTabPane>
          <CTabPane>
            <VenueTag setting={setting} />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

export default Venues
