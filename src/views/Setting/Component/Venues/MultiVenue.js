import {CButton, CCol, CDataTable, CPagination, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {getPagingVenue, postChangeVenueStatus} from '../../../../utilities/ApiManage'
import {getVenueList} from '../../../../redux/actions/app'
import StatusToggle from '../../../../common/StatusToggle'
import TableFilter from '../../../../common/TableFilter'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import {isTrue} from '../../../../helpers'
import PopupCreateVenue from './PopupCreateVenue'
import {setModal} from '../../../../reusable/Modal'

const fields = [{key: 'id', label: '#'}, {key: 'name'}, {key: 'status'}, {key: 'option'}]
const MultiVenue = () => {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [showInactive, setShowInactive] = useState(1)
  const [searchText, setSearchText] = useState('')
  const [createVenueVisible, setCreateVenueVisible] = useState(false)
  const [selectedVenue, setSelectedVenue] = useState(null)
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const dispatch = useDispatch()

  const fetchVenue = async () => {
    const res = await getPagingVenue(currentPage, itemPerPage, showInactive, searchText)
    if (!res.ok) throw new Error(res.message)
    setData(res.data)
  }

  useEffect(() => {
    fetchVenue()
  }, [currentPage, itemPerPage, changeVanueStatus, searchText])

  const changeVanueStatus = (id, index) => (value) => {
    if (value == true) {
      postData(id, index, value)
      return
    }
    setModal({
      title: 'WARNING !',
      message: `Disable the venue will terminate everything that this venue attached to such as events, promotions and ect. This venue will also be removed from venue tags that it's attached to`,
      primaryBtnText: 'Cancel',
      primaryBtnClick: () => {
        setModal({show: false})
      },
      isShowSecondaryBtn: true,
      secondaryBtnText: 'OK',
      secondaryBtnClick: () => {
        postData(id, index, value)
        setModal({show: false})
      },
      closeButton: true,
    })
  }

  const postData = async (id, index, value) => {
    const newState = {...data}
    newState.data[index].active = value
    setData(newState)
    const res = await postChangeVenueStatus({id: +id, active: value})
    if (res.ok) {
      fetchVenue()
      dispatch(getVenueList())
    }
  }

  const handleChangeParam = (params) => {
    setSearchText(params.searchText)
  }

  return (
    <>
      <CCol>
        <TableFilter
          disabled={!bepozVersion}
          params={{searchText}}
          title="Tags"
          onClickCreate={() => setCreateVenueVisible(true)}
          total={data.total}
          onParamChange={handleChangeParam}
        />
        <CDataTable
          items={data?.data || []}
          fields={fields}
          striped
          itemsPerPage={itemPerPage}
          hover={true}
          scopedSlots={{
            name: (item) => (
              <td>
                <CRow>
                  <CButton
                    disabled={!bepozVersion}
                    style={{marginTop: -7}}
                    onClick={() => {
                      setSelectedVenue(item)
                      setCreateVenueVisible(true)
                    }}>
                    {item.name}
                  </CButton>
                </CRow>
              </td>
            ),
            status: (item, index) => {
              return (
                <td>
                  <StatusToggle
                    autoSwitch={false}
                    disabled={!bepozVersion}
                    color="primary"
                    active={isTrue(item.active)}
                    onChange={changeVanueStatus(item.id, index)}
                  />
                </td>
              )
            },
            option: (item) => (
              <td>
                <CRow>
                  <CButton
                    disabled={!bepozVersion}
                    onClick={() => {
                      setSelectedVenue(item)
                      setCreateVenueVisible(true)
                    }}>
                    <CIcon content={freeSet.cilPen} />
                  </CButton>
                </CRow>
              </td>
            ),
          }}
        />
        <CPagination
          activePage={currentPage}
          pages={Math.ceil(data?.total / itemPerPage)}
          onActivePageChange={setCurrentPage}
        />

        <PopupCreateVenue
          show={createVenueVisible}
          closeModal={() => {
            setCreateVenueVisible(false)
            setSelectedVenue(null)
          }}
          selectedVenue={selectedVenue}
          onSubmit={fetchVenue}
        />
      </CCol>
    </>
  )
}

export default MultiVenue
