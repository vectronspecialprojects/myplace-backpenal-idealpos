import Collapse from '../../../common/Collapse'
import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import {SETTING_HIDE_OPTION} from '../constant'
import {postUploadGalleryPhoto} from '../../../utilities/ApiManage'
import FormInput from '../../../common/FormInput'
import FormSelect from '../../../common/FormSelect'
import UploadImageCrop from '../../../common/UploadImageCrop'
import Image from '../../../common/image'
import SettingLayout from './SettingLayout'
import {useDispatch} from 'react-redux';
import {updateAppSettings} from '../../../redux/actions/app';

const CompanyProfileAndInvoice = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    company_name: setting.company_name,
    company_street_number: setting.company_street_number,
    company_street_name: setting.company_street_name,
    company_suburb: setting.company_suburb,
    company_postcode: setting.company_postcode,
    company_state: setting.company_state,
    company_email: setting.company_email,
    company_phone: setting.company_phone,
    company_fax: setting.company_fax,
    company_abn: setting.company_abn,
    invoice_logo: setting.invoice_logo,
    use_invoice_logo_as_default_image: setting.use_invoice_logo_as_default_image,
    use_bepoz_header_as_default_header: setting.use_bepoz_header_as_default_header,
    invoice_header_logo: setting.invoice_header_logo,
  })

  const clickSave = async () => {
    dispatch(updateAppSettings({
      settings: JSON.stringify(Object.values(state)),
    }))
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const uploadImage = (key) => async (e) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) {
      setState({...state, [key]: {...state[key], value: res.data}})
    }
  }

  return (
    <Collapse title="Company Profile and Invoice">
      <CRow>
        <SettingLayout title="Company Name" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_name.value} onChangeText={changeValue('company_name')} />
        </SettingLayout>
        <SettingLayout title="Street Number" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput
            value={state.company_street_number.value}
            onChangeText={changeValue('company_street_number')}
          />
        </SettingLayout>
        <SettingLayout title="Street Name" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput
            value={state.company_street_name.value}
            onChangeText={changeValue('company_street_name')}
          />
        </SettingLayout>
        <SettingLayout title="Suburb" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_suburb.value} onChangeText={changeValue('company_suburb')} />
        </SettingLayout>
        <SettingLayout title="PostCode" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_postcode.value} onChangeText={changeValue('company_postcode')} />
        </SettingLayout>
        <SettingLayout title="State" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_state.value} onChangeText={changeValue('company_state')} />
        </SettingLayout>
        <SettingLayout title="Company Email" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_email.value} onChangeText={changeValue('company_email')} />
        </SettingLayout>
        <SettingLayout title="Company Phone" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_phone.value} onChangeText={changeValue('company_phone')} />
        </SettingLayout>
        <SettingLayout title="Company Fax" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_fax.value} onChangeText={changeValue('company_fax')} />
        </SettingLayout>
        <SettingLayout title="ABN" desc="It'll be displayed on eReceipt/eInvoice.">
          <FormInput value={state.company_abn.value} onChangeText={changeValue('company_abn')} />
        </SettingLayout>
        <SettingLayout title="Logo for invoice/receipt">
          <CRow>
            <CCol xs={12} md={4}>
              <UploadImageCrop aspect={1} onConfirm={uploadImage('invoice_logo')} className="w-100" hideUndo>
                <Image blob={state.invoice_logo.value} className="w-100" />
              </UploadImageCrop>
            </CCol>
          </CRow>
        </SettingLayout>
        <SettingLayout
          title="Use invoice logo as default image"
          desc="It'll be displayed on eReceipt/eInvoice.">
          <FormSelect
            options={SETTING_HIDE_OPTION}
            value={state.use_invoice_logo_as_default_image.value}
            onValueChange={changeValue('use_invoice_logo_as_default_image')}
          />
        </SettingLayout>
        <SettingLayout
          title="Use Bepoz header as default header"
          desc="It'll be displayed on eReceipt/eInvoice.">
          <FormSelect
            options={SETTING_HIDE_OPTION}
            value={state.use_bepoz_header_as_default_header.value}
            onValueChange={changeValue('use_bepoz_header_as_default_header')}
          />
        </SettingLayout>
        <SettingLayout title="Logo for invoice/receipt">
          <CRow>
            <CCol xs={12} md={4}>
              <UploadImageCrop
                aspect={2.5}
                onConfirm={uploadImage('invoice_header_logo')}
                className="w-100"
                hideUndo>
                <Image blob={state.invoice_header_logo.value} className="w-100" />
              </UploadImageCrop>
            </CCol>
          </CRow>
        </SettingLayout>
      </CRow>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </Collapse>
  )
}
export default CompanyProfileAndInvoice
