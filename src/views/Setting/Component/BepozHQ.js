import Collapse from '../../../common/Collapse'
import {CAlert, CButton, CCol, CRow} from '@coreui/react'
import React, {useState, useEffect} from 'react'
import {SETTING_CARD_TRACK, SETTING_ON_OFF} from '../constant'
import FormInput from '../../../common/FormInput'
import FormSelect from '../../../common/FormSelect'
import {useDispatch, useSelector} from 'react-redux'
import SettingLayout from './SettingLayout'
import FormDateTime from '../../../common/FormDateTime'
import {updateAppSettings, getBepozVersion} from '../../../redux/actions/app'
import Layout from './components/Layout'
import {getOperatorId, getTillId} from '../../../utilities/ApiManage'

const BepozHQ = ({setting}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [tillIdList, setTillIdList] = useState([])
  const [operatorIdList, setOperatorIdList] = useState([])
  const [state, setState] = useState({
    bepoz_training_mode: setting.bepoz_training_mode,
    bepoz_api: setting.bepoz_api,
    bepoz_mac_key: setting.bepoz_mac_key,
    default_card_number_prefix: setting.default_card_number_prefix,
    default_card_track: setting.default_card_track,
    bepoz_secondary_api: setting.bepoz_secondary_api,
    bepoz_operator_id: setting.bepoz_operator_id,
    bepoz_till_id: setting.bepoz_till_id,
    bepoz_payment_name: setting.bepoz_payment_name,
    send_image_bepoz: setting.send_image_bepoz,
    bepoz_account_last_successful_execution_time: setting.bepoz_account_last_successful_execution_time,
    poll_account_total_saved: setting.poll_account_total_saved,
    bepoz_account_total_saved_last_successful_execution_time:
      setting.bepoz_account_total_saved_last_successful_execution_time,
  })

  const getData = async () => {
    const res = await getOperatorId()
    const resTillId = await getTillId()
    if (res.ok) {
      setOperatorIdList(res?.data?.map(op => ({value: op?.OperatorID, label: `${op?.OperatorID} & ${op?.FirstName} ${op?.LastName}`})))
    }
    if (resTillId.ok) {
      setTillIdList(resTillId?.data?.map(till => ({value: till?.WorkstationID, label: `${till?.WorkstationID} & ${till?.Name}`})))
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const changeValue = (key) => (value) => {
    const newState = {...state}
    const updateDataRecursive = (data, keys) => {
      if (keys.length === 1) return (data[keys[0]] = value)
      const newData = data[keys[0]]
      keys.shift()
      return updateDataRecursive(newData, keys)
    }
    updateDataRecursive(newState, key.split('.'))
    setState(newState)
  }

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
    dispatch(getBepozVersion())
  }

  return (
    <Collapse title="Bepoz HQ">
      <CCol>
        <Layout
          contentWidth={4}
          title="Bepoz Traning Mode"
          desc="Enable this for transaction testing purpose">
          <FormSelect
            options={SETTING_ON_OFF}
            value={state.bepoz_training_mode.value}
            onValueChange={changeValue('bepoz_training_mode.value')}
            disabled={!unlock}
          />
        </Layout>
        <Layout
          contentWidth={4}
          title="Bepoz API (XML API)"
          desc="Type it without 'http://'. e.g. 192.168.10.10:9993">
          <FormInput
            value={state.bepoz_api.value}
            disabled={!unlock}
            onChangeText={changeValue('bepoz_api.value')}
          />
        </Layout>

        <Layout contentWidth={4} title="Bepoz MAC Key" desc="Type the mac key here. e.g. secret123">
          <FormInput
            value={state.bepoz_mac_key.value}
            disabled={!unlock}
            onChangeText={changeValue('bepoz_mac_key.value')}
          />
        </Layout>

        <Layout contentWidth={4} title="Set Bepoz Card Number Prefix" desc="Type the prefix here. e.g. VEC">
          <FormInput
            value={state.default_card_number_prefix.value}
            onChangeText={changeValue('default_card_number_prefix.value')}
            disabled={!unlock}
          />
        </Layout>

        <Layout contentWidth={4} title="Set Bepoz Card Track" desc="Setting For Member Barcode Reader.">
          <FormSelect
            value={state.default_card_track.value}
            options={SETTING_CARD_TRACK}
            onValueChange={changeValue('default_card_track.value')}
            disabled={!unlock}
          />
        </Layout>

        <Layout
          contentWidth={4}
          title="Bepoz Secondary API"
          desc="(Stored Procedure API) Type it without 'http://'. e.g. 192.168.10.10:9993">
          <FormInput
            value={state.bepoz_secondary_api.value}
            onChangeText={changeValue('bepoz_secondary_api.value')}
            disabled={!unlock}
          />
        </Layout>

        <Layout contentWidth={4} title="Bepoz Operator ID" desc="Type the operator ID here. e.g. 1">
        <FormSelect
            options={operatorIdList}
            value={state.bepoz_operator_id.value}
            onValueChange={changeValue('bepoz_operator_id.value')}
            disabled={!unlock || operatorIdList.length === 0}
          />
        </Layout>

        {/* <Layout contentWidth={4} title="Bepoz Operator ID" desc="Type the operator ID here. e.g. 1">
          <FormInput
            value={state.bepoz_operator_id.value}
            onChangeText={changeValue('bepoz_operator_id.value')}
            disabled={!unlock}
          />
        </Layout> */}

        <Layout contentWidth={4} title="Bepoz Till ID" desc="Type the till ID here. e.g. 1">
        <FormSelect
            options={tillIdList}
            value={state.bepoz_till_id.value}
            onValueChange={changeValue('bepoz_till_id.value')}
            disabled={!unlock || tillIdList.length === 0}
          />
        </Layout>

        {/* <Layout contentWidth={4} title="Bepoz Till ID" desc="Type the till ID here. e.g. 1">
          <FormInput
            value={state.bepoz_till_id.value}
            onChangeText={changeValue('bepoz_till_id.value')}
            disabled={!unlock}
          />
        </Layout> */}

        <Layout
          contentWidth={4}
          title="Bepoz Payment Name"
          desc="Type the payment name here. e.g. Online Payment">
          <FormInput
            value={state.bepoz_payment_name.value}
            onChangeText={changeValue('bepoz_payment_name.value')}
            disabled={!unlock}
          />
        </Layout>
        {unlock && (
          <>
            <Layout
              contentWidth={4}
              title="Bepoz Receive Image From Member Upload"
              desc="Set Image From Member Upload Send To Bepoz">
              <FormSelect
                value={state.send_image_bepoz.value}
                options={SETTING_ON_OFF}
                onValueChange={changeValue('send_image_bepoz.value')}
                disabled={!unlock}
              />
            </Layout>

            <Layout
              contentWidth={4}
              title="Bepoz Account Last Successful Execution Time"
              desc="Change Bepoz Account Last Successful Execution Time here.
In case cloud data not sync, try to change this setting to date before bepoz data updated">
              <FormDateTime
                value={state.bepoz_account_last_successful_execution_time.value}
                onValueChange={changeValue('bepoz_account_last_successful_execution_time.value')}
                disabled={!unlock}
              />
            </Layout>

            <Layout
              contentWidth={4}
              title="Bepoz Poll Account Total Saved"
              desc="Change Bepoz Poll Account Total Saved active/inactive. Total Saved will show in app.">
              <FormSelect
                value={state.poll_account_total_saved.value}
                options={SETTING_ON_OFF}
                onValueChange={changeValue('poll_account_total_saved.value')}
                disabled={!unlock}
              />
            </Layout>

            <Layout
              contentWidth={4}
              title="Bepoz Account Total Saved Last Successful Execution Time"
              desc="Change Bepoz Account Total Saved Last Successful Execution Time here.
In case cloud data not sync, try to change this setting to date before bepoz data updated">
              <FormDateTime
                value={state.bepoz_account_total_saved_last_successful_execution_time.value}
                onValueChange={changeValue('bepoz_account_total_saved_last_successful_execution_time.value')}
                disabled={!unlock}
              />
            </Layout>

            <CButton
              variant="outline"
              color="secondary"
              className="text-dark font-weight-bold mt-3"
              onClick={clickSave}>
              Save Changes
            </CButton>
          </>
        )}
      </CCol>
    </Collapse>
  )
}

export default BepozHQ
