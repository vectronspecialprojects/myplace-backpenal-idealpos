export const SETTING_FEATURES = [
  'Dashboard',
  'Tickets',
  'Vouchers',
  'Products',
  'Members',
  'Staff',
  'Notifications',
  'Enquiries',
  'FAQs',
  'Friend Referrals',
  'Transactions',
  'Survey',
]

export const INTEGRATION = ['Stripe', 'Gaming', 'Third Party', 'SSO', 'Pond Hoppers', 'Burst SMS']
