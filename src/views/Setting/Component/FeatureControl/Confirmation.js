import React, {useState} from 'react'
import {CRow, CCol, CLabel, CButton} from '@coreui/react'
import {useSelector, useDispatch} from 'react-redux'
import Layout from '../components/Layout'
import FormInput from '../../../../common/FormInput'
import FormSelect from '../../../../common/FormSelect'
import UploadImageCrop from '../../../../common/UploadImageCrop'
import Image from '../../../../common/image'
import RichEditor from '../../../../common/RichEditor'
import {SETTING_ON_OFF} from '../../constant'
import {postUploadGalleryPhoto} from '../../../../utilities/ApiManage'
import {updateAppSettings} from '../../../../redux/actions/app'

const Confirmation = ({setting}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [state, setState] = useState({
    mobile_confirmation_enable: setting.mobile_confirmation_enable,

    mobile_confirmation_successful_button_font_color:
      setting.mobile_confirmation_successful_button_font_color,
    mobile_confirmation_successful_button_border_color:
      setting.mobile_confirmation_successful_button_border_color,
    mobile_confirmation_successful_button_bg_color: setting.mobile_confirmation_successful_button_bg_color,
    mobile_confirmation_successful_font_color: setting.mobile_confirmation_successful_font_color,
    mobile_confirmation_successful_bg_color: setting.mobile_confirmation_successful_bg_color,
    mobile_confirmation_successful_image: setting.mobile_confirmation_successful_image,
    mobile_confirmation_successful_message: setting.mobile_confirmation_successful_message,

    mobile_confirmation_unsuccessful_button_font_color:
      setting.mobile_confirmation_unsuccessful_button_font_color,
    mobile_confirmation_unsuccessful_button_border_color:
      setting.mobile_confirmation_unsuccessful_button_border_color,
    mobile_confirmation_unsuccessful_button_bg_color:
      setting.mobile_confirmation_unsuccessful_button_bg_color,
    mobile_confirmation_unsuccessful_font_color: setting.mobile_confirmation_unsuccessful_font_color,
    mobile_confirmation_unsuccessful_bg_color: setting.mobile_confirmation_unsuccessful_bg_color,
    mobile_confirmation_unsuccessful_image: setting.mobile_confirmation_unsuccessful_image,
    mobile_confirmation_unsuccessful_message: setting.mobile_confirmation_unsuccessful_message,

    email_confirmation_successful_button_font_color: setting.email_confirmation_successful_button_font_color,
    email_confirmation_successful_button_border_color:
      setting.email_confirmation_successful_button_border_color,
    email_confirmation_successful_button_bg_color: setting.email_confirmation_successful_button_bg_color,
    email_confirmation_successful_font_color: setting.email_confirmation_successful_font_color,
    email_confirmation_successful_bg_color: setting.email_confirmation_successful_bg_color,
    email_confirmation_successful_image: setting.email_confirmation_successful_image,
    email_confirmation_successful_message: setting.email_confirmation_successful_message,

    email_confirmation_unsuccessful_button_font_color:
      setting.email_confirmation_unsuccessful_button_font_color,
    email_confirmation_unsuccessful_button_border_color:
      setting.email_confirmation_unsuccessful_button_border_color,
    email_confirmation_unsuccessful_button_bg_color: setting.email_confirmation_unsuccessful_button_bg_color,
    email_confirmation_unsuccessful_font_color: setting.email_confirmation_unsuccessful_font_color,
    email_confirmation_unsuccessful_bg_color: setting.email_confirmation_unsuccessful_bg_color,
    email_confirmation_unsuccessful_image: setting.email_confirmation_unsuccessful_image,
    email_confirmation_unsuccessful_message: setting.email_confirmation_unsuccessful_message,
  })

  const changeValue = (key) => (value) => {
    setState((prevState) => ({...prevState, [key]: {...prevState[key], value}}))
  }

  const onTextChange = (key, e) => {
    changeValue(key)(e.target.value)
  }

  const uploadImage = (key) => async (e) => {
    const res = await postUploadGalleryPhoto({image: e.target.value})
    if (res.ok) setState((prevState) => ({...prevState, [key]: {...prevState[key], value: res.data}}))
  }

  const saveHandle = () => {
    dispatch(updateAppSettings({settings: JSON.stringify(Object.values(state))}))
  }

  return (
    <CRow>
      <CCol>
        <h3 className="text-capitalize mb-5">Mobile Confirmation</h3>
        <Layout contentWidth={4} title="Mobile Confirmation	">
          <FormSelect
            options={SETTING_ON_OFF}
            value={state.mobile_confirmation_enable.value}
            onValueChange={changeValue('mobile_confirmation_enable')}
            disabled={!unlock}
          />
        </Layout>
        <h4 className="text-capitalize mb-4 font-weight-bold">Button Successful Styling</h4>
        <Layout contentWidth={4} title="Button Font Color">
          <FormInput
            value={state.mobile_confirmation_successful_button_font_color.value}
            onChange={changeValue('mobile_confirmation_successful_button_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Border Color">
          <FormInput
            value={state.mobile_confirmation_successful_button_border_color.value}
            onChange={changeValue('mobile_confirmation_successful_button_border_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Background Color">
          <FormInput
            value={state.mobile_confirmation_successful_button_bg_color.value}
            onChange={changeValue('mobile_confirmation_successful_button_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Font Color">
          <FormInput
            value={state.mobile_confirmation_successful_font_color.value}
            onChange={changeValue('mobile_confirmation_successful_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Background Color">
          <FormInput
            value={state.mobile_confirmation_successful_bg_color.value}
            onChange={changeValue('mobile_confirmation_successful_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Image Logo">
          <UploadImageCrop
            aspect={1}
            onConfirm={uploadImage('mobile_confirmation_successful_image')}
            className="w-100"
            hideUndo>
            <Image blob={state.mobile_confirmation_successful_image.value} className="w-100" />
          </UploadImageCrop>
        </Layout>
        <CLabel className="h5">Message</CLabel>
        <RichEditor
          initValue={state.mobile_confirmation_successful_message.value || ''}
          onChange={(e) => onTextChange('mobile_confirmation_successful_message', e)}
        />

        <h4 className="text-capitalize mt-5 mb-4 font-weight-bold">Button Unsuccessful Styling</h4>
        <Layout contentWidth={4} title="Button Font Color">
          <FormInput
            value={state.mobile_confirmation_unsuccessful_button_font_color.value}
            onChange={changeValue('mobile_confirmation_unsuccessful_button_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Border Color">
          <FormInput
            value={state.mobile_confirmation_unsuccessful_button_border_color.value}
            onChange={changeValue('mobile_confirmation_unsuccessful_button_border_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Background Color">
          <FormInput
            value={state.mobile_confirmation_unsuccessful_button_bg_color.value}
            onChange={changeValue('mobile_confirmation_unsuccessful_button_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Font Color">
          <FormInput
            value={state.mobile_confirmation_unsuccessful_font_color.value}
            onChange={changeValue('mobile_confirmation_unsuccessful_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Background Color">
          <FormInput
            value={state.mobile_confirmation_unsuccessful_bg_color.value}
            onChange={changeValue('mobile_confirmation_unsuccessful_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Image Logo">
          <UploadImageCrop
            aspect={1}
            onConfirm={uploadImage('mobile_confirmation_unsuccessful_image')}
            className="w-100"
            hideUndo>
            <Image blob={state.mobile_confirmation_unsuccessful_image.value} className="w-100" />
          </UploadImageCrop>
        </Layout>
        <CLabel className="h5">Message</CLabel>
        <RichEditor
          initValue={state.mobile_confirmation_unsuccessful_message.value || ''}
          onChange={(e) => onTextChange('mobile_confirmation_unsuccessful_message', e)}
        />
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={saveHandle}>
          Save Changes
        </CButton>
      </CCol>

      <CCol>
        <h3 className="text-capitalize mb-5">Email Confirmation</h3>
        <Layout contentWidth={4} title="Email Confirmation">
          <FormSelect
            options={SETTING_ON_OFF}
            value={'true'}
            onValueChange={changeValue('')}
            disabled={true}
          />
        </Layout>
        <h4 className="text-capitalize mb-4 font-weight-bold">Button Successful Styling</h4>
        <Layout contentWidth={4} title="Button Font Color">
          <FormInput
            value={state.email_confirmation_successful_button_font_color.value}
            onChange={changeValue('email_confirmation_successful_button_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Border Color">
          <FormInput
            value={state.email_confirmation_successful_button_border_color.value}
            onChange={changeValue('email_confirmation_successful_button_border_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Background Color">
          <FormInput
            value={state.email_confirmation_successful_button_bg_color.value}
            onChange={changeValue('email_confirmation_successful_button_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Font Color">
          <FormInput
            value={state.email_confirmation_successful_font_color.value}
            onChange={changeValue('email_confirmation_successful_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Background Color">
          <FormInput
            value={state.email_confirmation_successful_bg_color.value}
            onChange={changeValue('email_confirmation_successful_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Image Logo">
          <UploadImageCrop
            aspect={1}
            onConfirm={uploadImage('email_confirmation_successful_image')}
            className="w-100"
            hideUndo>
            <Image blob={state.email_confirmation_successful_image.value} className="w-100" />
          </UploadImageCrop>
        </Layout>
        <CLabel className="h5">Message</CLabel>
        <RichEditor
          initValue={state.email_confirmation_successful_message.value || ''}
          onChange={(e) => onTextChange('email_confirmation_successful_message', e)}
        />
        <h4 className="text-capitalize mt-5 mb-4 font-weight-bold">Button Unsuccessful Styling</h4>
        <Layout contentWidth={4} title="Button Font Color">
          <FormInput
            value={state.email_confirmation_unsuccessful_button_font_color.value}
            onChange={changeValue('email_confirmation_unsuccessful_button_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Border Color">
          <FormInput
            value={state.email_confirmation_unsuccessful_button_border_color.value}
            onChange={changeValue('email_confirmation_unsuccessful_button_border_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Button Background Color">
          <FormInput
            value={state.email_confirmation_unsuccessful_button_bg_color.value}
            onChange={changeValue('email_confirmation_unsuccessful_button_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Font Color">
          <FormInput
            value={state.email_confirmation_unsuccessful_font_color.value}
            onChange={changeValue('email_confirmation_unsuccessful_font_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Background Color">
          <FormInput
            value={state.email_confirmation_unsuccessful_bg_color.value}
            onChange={changeValue('email_confirmation_unsuccessful_bg_color')}
            type={'color'}
          />
        </Layout>
        <Layout contentWidth={4} title="Image Logo">
          <UploadImageCrop
            aspect={1}
            onConfirm={uploadImage('email_confirmation_unsuccessful_image')}
            className="w-100"
            hideUndo>
            <Image blob={state.email_confirmation_unsuccessful_image.value} className="w-100" />
          </UploadImageCrop>
        </Layout>
        <CLabel className="h5">Message</CLabel>
        <RichEditor
          initValue={state.email_confirmation_unsuccessful_message.value || ''}
          onChange={(e) => onTextChange('email_confirmation_unsuccessful_message', e)}
        />
      </CCol>
    </CRow>
  )
}

export default Confirmation
