import Collapse from '../../../../common/Collapse'
import {
  CAlert,
  CButton,
  CCol,
  CDataTable,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTabs,
} from '@coreui/react'
import React, {useEffect, useMemo, useState} from 'react'
import Layout from '../components/Layout'
import StatusToggle from '../../../../common/StatusToggle'
import {isTrue} from '../../../../helpers'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import {listingTypeHide} from '../../../../utilities/ApiManage'
import FormCheckbox from '../../../../common/FormCheckbox'
import {SETTING_MAP_LABEL} from '../../constant'
import PopupUpdateElementGroup from '../PopupUpdateElementGroup'
import {useDispatch, useSelector} from 'react-redux'
import {getListingType, updateAppSettings, updateListingType} from '../../../../redux/actions/app'
import {toast} from 'react-toastify'
import {setModal} from '../../../../reusable/Modal'
import Confirmation from './Confirmation'
import {SETTING_FEATURES, INTEGRATION} from './constant'

const MENU = ['Dashboard', 'Side Nav', 'Integration', 'Extra Feature', 'Confirmation']

const FeatureControl = ({setting, bepozVersion}) => {
  const dispatch = useDispatch()
  const [tab, setTab] = useState(0)
  const [state, setState] = useState({
    sidenavigationbar: {
      ...setting.sidenavigationbar,
      extended_value: JSON.parse(setting.sidenavigationbar.extended_value).filter(
        (item) => !['booking_ticket', 'venues', 'roles'].includes(item.key),
      ),
    },
  })
  const [dashboard, setDashboard] = useState([
    setting.ticket,
    setting.refer_a_friend,
    setting.transaction,
    setting.gift_certificate,
  ])
  const [signUpMatching, setSignUpMatching] = useState({
    signup_matching_bepoz_group_venue_tier: setting.signup_matching_bepoz_group_venue_tier,
    gaming_account_lowest_enable: setting.gaming_account_lowest_enable,
  })

  const [integration, setIntegration] = useState([
    setting.stripe_integration_show,
    setting.gaming_integration_show,
    setting.third_party_integration_show,
    setting.pond_hoppers_enable,
    setting.burst_sms_integration_show,
  ])

  const listingType = useSelector((state) => state.app.listing)
  const [showAllListing, setShowAllListing] = useState(false)
  const [selectedListing, setSelectedListing] = useState(null)

  const changeValue = (key, index) => (value) => {
    const newState = {...state}
    const updateDataRecursive = (data, keys) => {
      if (keys.length === 1) return (data[keys[0]] = value)
      const newData = data[keys[0]]
      keys.shift()
      return updateDataRecursive(newData, keys)
    }
    updateDataRecursive(newState, key.split('.'))
    setState(newState)
    setModal({
      title: `${SETTING_FEATURES[index]} will ${value ? 'appear' : 'be hidden'} from the side menu.`,
      message: 'Caution: It takes a while...',
      primaryBtnClick: async () => {
        await dispatch(
          updateAppSettings({
            settings: JSON.stringify(Object.values(state)),
          }),
        )
        setModal({show: false})
      },
      isShowSecondaryBtn: false,
      closeButton: false,
    })
  }

  const fetchListingType = async () => {
    dispatch(getListingType())
  }

  // const clickSave = async () => {
  //   dispatch(
  //     updateAppSettings({
  //       settings: JSON.stringify(Object.values(state)),
  //     }),
  //   )
  // }

  const changeDashboardActive = (index) => async (value) => {
    const newDashboard = [...dashboard]
    newDashboard[index].value = value.toString()
    setDashboard(newDashboard)
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(newDashboard),
      }),
    )
  }

  const changeIntegrationActive = (index) => (value) => {
    // console.log(index, value)
    const newIntegration = [...integration]
    newIntegration[index].value = value.toString()
    setIntegration(newIntegration)
    setModal({
      title: `This will only ${value ? 'show' : 'hind'} ${
        INTEGRATION[index]
      } controller from the back panel. ${
        INTEGRATION[index]
      } will need to be reconfigured to change the app feature`,
      message: `This back panel will need to be reconfigured based on the change you just made.`,
      primaryBtnClick: async () => {
        await dispatch(
          updateAppSettings({
            settings: JSON.stringify(newIntegration),
          }),
        )
        setModal({show: false})
      },
      isShowSecondaryBtn: false,
      closeButton: false,
    })
  }

  const changeSignUpMatching = (index) => async (value) => {
    const data = {...signUpMatching}
    data[index].value = value.toString()
    setSignUpMatching(data)
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(data)),
      }),
    )
  }

  const handleToggleNavMenu = (item, value) => {
    setModal({
      // title: `Would you like to ${value ? 'un hide' : 'hide'} the selected listing type (#${id})?`,
      title: `${item.name} will ${value ? 'appear' : 'be hidden'} from the side menu.`,
      message: 'Caution: It takes a while...',
      primaryBtnClick: async () => {
        try {
          const res = await listingTypeHide(item.id, value)
          if (!res.ok) throw new Error(res.message)
          dispatch(updateListingType(res.data))
        } catch (e) {
          toast(e.message, {type: 'error'})
        } finally {
          setModal({show: false})
        }
      },
      isShowSecondaryBtn: false,
      closeButton: false,
    })
  }

  return (
    <Collapse title="Feature Control">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          {MENU.map((item, index) => {
            return (
              <CNavItem key={index.toString()}>
                <CNavLink>{item}</CNavLink>
              </CNavItem>
            )
          })}
        </CNav>
        <CTabContent className="mt-3">
          <CTabPane>
            <CDataTable
              items={dashboard || []}
              fields={[{key: 'index', label: '#'}, 'name', 'active']}
              striped
              scopedSlots={{
                index: (item, index) => <td>{index + 1}</td>,
                name: (item) => <td>{SETTING_MAP_LABEL[item.key]}</td>,
                active: (item, index) => (
                  <td>
                    <StatusToggle
                      color="primary"
                      active={isTrue(item.value)}
                      onChange={changeDashboardActive(index)}
                    />
                  </td>
                ),
              }}
            />
          </CTabPane>
          <CTabPane>
            <FormCheckbox label="Show hidden?" checked={showAllListing} onValueChange={setShowAllListing} />
            <CDataTable
              items={(showAllListing ? listingType : listingType.filter((_) => !_.key?.hide_this)) || []}
              fields={[
                {key: 'index', label: '#', _style: {width: 50}},
                {key: 'name', label: 'Name', _style: {width: '50%'}},
                'active',
                'options',
              ]}
              striped
              scopedSlots={{
                index: (item, index) => <td>{index + 1}</td>,
                name: (item) => (
                  <td>
                    <CRow>
                      <CButton style={{marginTop: -7}} onClick={() => setSelectedListing(item)}>
                        {item.name}
                      </CButton>
                    </CRow>
                  </td>
                ),
                active: (item, index) => (
                  <td>
                    <StatusToggle
                      color="primary"
                      active={!isTrue(item.key?.hide_this)}
                      onChange={(value) => handleToggleNavMenu(item, value)}
                    />
                  </td>
                ),
                options: (item) => {
                  return (
                    <td>
                      <CButton onClick={() => setSelectedListing(item)}>
                        <CIcon content={freeSet.cilPen} />
                      </CButton>
                    </td>
                  )
                },
              }}
            />

            {/* <CAlert color="warning">
              Don't forget to click "Save Changes" button below to get effect on side navigation{' '}
            </CAlert> */}
            <CDataTable
              items={state.sidenavigationbar?.extended_value}
              fields={[
                {key: 'index', label: '#', _style: {width: 50}},
                {key: 'label', label: 'Name', _style: {width: '50%'}},
                'active',
              ]}
              striped
              scopedSlots={{
                index: (item, index) => <td>{index + 1}</td>,
                name: (item) => <td>{item.name}</td>,
                active: (item, index) => (
                  <td>
                    <StatusToggle
                      color="primary"
                      active={isTrue(item.visible)}
                      onChange={changeValue(`sidenavigationbar.extended_value.${index}.visible`, index)}
                    />
                  </td>
                ),
              }}
            />
            <PopupUpdateElementGroup
              show={!!selectedListing}
              closeModal={() => setSelectedListing(null)}
              selectedListing={selectedListing}
              onSubmit={fetchListingType}
            />
            {/* <CCol xs={12}>
              <CButton
                variant="outline"
                color="secondary"
                className="text-dark font-weight-bold mt-3"
                onClick={clickSave}>
                Save Changes
              </CButton>
            </CCol> */}
          </CTabPane>
          <CTabPane>
            <CDataTable
              items={integration || []}
              fields={[{key: 'index', label: '#'}, 'name', 'active']}
              striped
              scopedSlots={{
                index: (item, index) => <td>{index + 1}</td>,
                name: (item) => <td>{SETTING_MAP_LABEL[item.key]}</td>,
                active: (item, index) => (
                  <td>
                    <StatusToggle
                      color="primary"
                      active={isTrue(item.value)}
                      onChange={changeIntegrationActive(index)}
                    />
                  </td>
                ),
              }}
            />
          </CTabPane>
          <CTabPane>
            <Layout contentWidth={4} title="Signup Matching Bepoz Group Venue Tier" desc="">
              <StatusToggle
                disabled={!bepozVersion}
                color="primary"
                active={isTrue(signUpMatching.signup_matching_bepoz_group_venue_tier?.value)}
                onChange={changeSignUpMatching('signup_matching_bepoz_group_venue_tier')}
              />
            </Layout>
            <Layout
              contentWidth={4}
              title="Gaming Account Lowest"
              desc="Enable searching Gaming Account Lowest in Bepoz">
              <StatusToggle
                disabled={!bepozVersion}
                color="primary"
                active={isTrue(signUpMatching.gaming_account_lowest_enable?.value)}
                onChange={changeSignUpMatching('gaming_account_lowest_enable')}
              />
            </Layout>
          </CTabPane>

          <CTabPane>
            <Confirmation setting={setting} />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}
export default FeatureControl
