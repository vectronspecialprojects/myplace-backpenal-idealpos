import {CCol, CRow} from '@coreui/react'
import React from 'react'

const Layout = ({children, title, desc, className = 'mb-3', titleWidth = 4, contentWidth = 8}) => {
  return (
    <CRow className={className}>
      <CCol xs={12} md={titleWidth} className="align-items-center">
        <h5 className="text-capitalize">{title}</h5>
        {desc && <h6 className="text-muted text-lowercase">{desc}</h6>}
      </CCol>
      <CCol xs={12} md={contentWidth}>
        {children}
      </CCol>
    </CRow>
  )
}

export default Layout
