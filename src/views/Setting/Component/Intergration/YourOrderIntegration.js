import {CButton, CCol} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getPagingVenue, getWebStore} from '../../../../utilities/ApiManage'
import FormInput from '../../../../common/FormInput'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_ON_OFF} from '../../constant'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'
import {isTrue} from '../../../../helpers'
import {setModal} from '../../../../reusable/Modal'

const YourOrderIntegration = ({setting}) => {
  const [data, setData] = useState({})
  const [state, setState] = useState({
    your_order_api: setting.your_order_api,
    your_order_key: setting.your_order_key,
  })
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const dispatch = useDispatch()

  useEffect(() => {
    ;(async () => {
      const res = await getPagingVenue(currentPage, itemPerPage)
      if (!res.ok) throw new Error(res.message)
      setData(res.data)
    })()
  }, [currentPage, itemPerPage])

  const clickSave = async () => {
    if (checkUrl()) {
      dispatch(
        updateAppSettings({
          settings: JSON.stringify(Object.values(state)),
        }),
      )
    }
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const clickOption = async (index) => {
    await getWebStore()
    const newData = {...data, data: [...data.data]}
    newData.data[index].checked = true
    setData(newData)
  }

  const checkUrl = () => {
    if (
      isTrue(state.your_order_api.value) &&
      !/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/.test(
        state.your_order_api.value,
      )
    ) {
      setModal({
        title: 'URL Invalid Format',
        message: 'URL must be in this format ex https://www.api.vecport.net/sso/api',
        primaryBtnClick: () => {
          setModal({show: false})
        },
        isShowSecondaryBtn: false,
        closeButton: false,
      })
      return false
    }
    return true
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="SSO api url" desc="">
          <FormInput
            onBlur={checkUrl}
            value={state.your_order_api.value}
            onChangeText={changeValue('your_order_api')}
          />
        </Layout>
        <Layout contentWidth={4} title="SSO api key" desc="">
          <FormInput value={state.your_order_key.value} onChangeText={changeValue('your_order_key')} />
        </Layout>

        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default YourOrderIntegration
