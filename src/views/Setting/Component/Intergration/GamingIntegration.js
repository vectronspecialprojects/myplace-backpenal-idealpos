import {
  CButton,
  CCol,
  CDataTable,
  CFormGroup,
  CInputCheckbox,
  CNav,
  CNavItem,
  CNavLink,
  CRow,
  CTabContent,
  CTabPane,
  CTabs,
} from '@coreui/react'
import React, {useMemo, useState} from 'react'
import SettingLayout from '../SettingLayout'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_ON_OFF} from '../../constant'
import {useDispatch, useSelector} from 'react-redux'
import {getAppSettings, updateAppSettings} from '../../../../redux/actions/app'
import FormInput from '../../../../common/FormInput'
import {freeSet} from '@coreui/icons/js/free'
import CIcon from '@coreui/icons-react'
import StatusToggle from '../../../../common/StatusToggle'
import {isTrue} from '../../../../helpers'
import {
  getSetting,
  refreshGamingSettingIGT,
  refreshGamingSettingOdy,
  restoreGaming,
  testCustomFieldOdy,
} from '../../../../utilities/ApiManage'

const fields = [
  '#',
  'Field Name',
  'Field Type',
  'Field Display Title',
  'Required',
  'Active',
  'Display in app',
  'Default Values',
  'Delete',
]
const GamingIntegration = () => {
  const dispatch = useDispatch()
  const [tab, setTab] = useState(0)
  const setting = useSelector((state) => state.app.appSettings)
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const [state, setState] = useState({
    gaming_system_enable: setting.gaming_system_enable,
    gaming_system: {
      ...setting.gaming_system,
      extended_value: JSON.parse(setting.gaming_system.extended_value),
    },
    odyssey_url: setting.odyssey_url,
    odyssey_from_system: setting.odyssey_from_system,
    odyssey_to_system: setting.odyssey_to_system,
    odyssey_message_id: setting.odyssey_message_id,
    odyssey_datetime_sent: setting.odyssey_datetime_sent,
    odyssey_vendor_code: setting.odyssey_vendor_code,
    odyssey_device_code: setting.odyssey_device_code,
    odyssey_shared_key: setting.odyssey_shared_key,
    odyssey_custom_field_id: setting.odyssey_custom_field_id,
    gaming_mandatory_field: setting.gaming_mandatory_field,
    gaming_username: setting.gaming_username,
    gaming_system_url: setting.gaming_system_url,
    gaming_password: setting.gaming_password,
    gaming_site_id: setting.gaming_site_id,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const toggleEnable = (value) => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([
          {
            ...setting.gaming_system_enable,
            value: value.toString(),
          },
        ]),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const listData = useMemo(() => {
    return JSON.parse(state.gaming_mandatory_field.extended_value) || []
  }, [setting])

  const isIGT = state.gaming_system.value === 'igt'

  const handleButtonPress = async (type, igt) => {
    try {
      switch (type) {
        case 'restore':
          await restoreGaming()
          break
        case 'refresh':
          if (igt) {
            await refreshGamingSettingIGT()
          } else {
            await refreshGamingSettingOdy()
          }
          break
        case 'test':
          await testCustomFieldOdy()
          break
      }
      dispatch(getAppSettings())
    } catch (e) {}
  }

  return (
    <>
      <CRow>
        <SettingLayout title="Gaming System Enable" desc="">
          <StatusToggle
            color="primary"
            active={isTrue(setting.gaming_system_enable.value)}
            onChange={toggleEnable}
            disabled={!bepozVersion}
          />
        </SettingLayout>
        {isTrue(setting.gaming_system_enable.value) && (
          <SettingLayout title="Gaming System" desc="Select a gaming system">
            <CRow>
              <CCol md={4}>
                <FormSelect
                  options={state.gaming_system.extended_value}
                  value={state.gaming_system.value}
                  getValue={(_) => _.key}
                  onValueChange={changeValue('gaming_system')}
                  disabled={!bepozVersion}
                />
              </CCol>
              <CButton
                variant="outline"
                color="secondary"
                className="text-dark font-weight-bold"
                onClick={clickSave}
                disabled={!bepozVersion}>
                SAVE
              </CButton>
            </CRow>
          </SettingLayout>
        )}
      </CRow>
      {isTrue(setting.gaming_system_enable.value) && (
        <CTabs activeTab={tab} onActiveTabChange={setTab} className="mt-3">
          <CNav variant="tabs">
            <CNavItem>
              <CNavLink>GAMING SYSTEM SETUP</CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink>MEMBER DETAIL</CNavLink>
            </CNavItem>
          </CNav>
          <CTabContent className="mt-3">
            <CTabPane>
              {isIGT ? (
                <CRow>
                  <SettingLayout title="Gaming System URL" desc="">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.gaming_system_url?.value}
                      onChangeText={changeValue('gaming_system_url')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Gaming System Username" desc="">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.gaming_username?.value}
                      onChangeText={changeValue('gaming_username')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Gaming System Password" desc="">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.gaming_password?.value}
                      onChangeText={changeValue('gaming_password')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Gaming System Site ID" desc="">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.gaming_site_id?.value}
                      onChangeText={changeValue('gaming_site_id')}
                    />
                  </SettingLayout>
                </CRow>
              ) : (
                <CRow>
                  <SettingLayout title="Odyssey URL" desc="Set Odyssey URL">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_url?.value}
                      onChangeText={changeValue('odyssey_url')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey From System BePoz" desc="Odyssey From System">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_from_system?.value}
                      onChangeText={changeValue('odyssey_from_system')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey To System" desc="Set Odyssey To System">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_to_system?.value}
                      onChangeText={changeValue('odyssey_to_system')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey Message ID" desc="Set Odyssey Message ID">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_message_id?.value}
                      onChangeText={changeValue('odyssey_message_id')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey Datetime Sent" desc="Set Odyssey Datetime Sent">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_datetime_sent?.value}
                      onChangeText={changeValue('odyssey_datetime_sent')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey Vendor Code" desc="Set Odyssey Vendor Code">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_vendor_code?.value}
                      onChangeText={changeValue('odyssey_vendor_code')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey Device Code" desc="Set Odyssey Device Code">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_device_code?.value}
                      onChangeText={changeValue('odyssey_device_code')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey Shared Key" desc="Set Odyssey Shared Key">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_shared_key?.value}
                      onChangeText={changeValue('odyssey_shared_key')}
                    />
                  </SettingLayout>
                  <SettingLayout title="Odyssey CustomFieldID" desc="Set Odyssey CustomFieldID">
                    <FormInput
                      disabled={!bepozVersion}
                      value={state.odyssey_custom_field_id?.value}
                      onChangeText={changeValue('odyssey_custom_field_id')}
                    />
                  </SettingLayout>
                </CRow>
              )}
              <CCol xs={12}>
                <CButton
                  disabled={!bepozVersion}
                  variant="outline"
                  color="secondary"
                  className="text-dark font-weight-bold"
                  onClick={clickSave}>
                  Save Changes
                </CButton>
              </CCol>
            </CTabPane>
            <CTabPane>
              <CCol>
                <CRow>
                  <CButton
                    disabled={!bepozVersion}
                    variant="outline"
                    color="secondary"
                    className="text-dark font-weight-bold mr-2"
                    onClick={() => handleButtonPress('restore')}>
                    RESTORE DEFAULT
                  </CButton>
                  <CButton
                    disabled={!bepozVersion}
                    variant="outline"
                    color="secondary"
                    className="text-dark font-weight-bold mr-2"
                    onClick={() => handleButtonPress('refresh', isIGT)}>
                    SYNC FIELD {isIGT ? 'IGT' : 'ODYSSEY'}
                  </CButton>
                  {!isIGT && (
                    <CButton
                      disabled={!bepozVersion}
                      variant="outline"
                      color="secondary"
                      className="text-dark font-weight-bold mr-2"
                      onClick={() => handleButtonPress('test')}>
                      TEST CUSTOMER FIELD ODYSSEY
                    </CButton>
                  )}
                </CRow>
                <h2 className="mt-3">Fields for Gaming System (view only)</h2>
                <CCol>
                  <CDataTable
                    items={listData}
                    fields={fields}
                    scopedSlots={{
                      [fields[0]]: (item) => (
                        <td>
                          <FormInput disabled={!bepozVersion} value={item.id} disabled={true} />
                        </td>
                      ),
                      [fields[1]]: (item) => (
                        <td>
                          <FormInput disabled={!bepozVersion} value={item.fieldName} disabled={true} />
                        </td>
                      ),
                      [fields[2]]: (item) => (
                        <td>
                          <FormInput disabled={!bepozVersion} value={item.fieldType} disabled={true} />
                        </td>
                      ),
                      [fields[3]]: (item) => (
                        <td>
                          <FormInput
                            disabled={!bepozVersion}
                            value={item.fieldDisplayTitle}
                            disabled={true}
                          />
                        </td>
                      ),
                      [fields[4]]: (item) => (
                        <td className="pl-5">
                          <CInputCheckbox disabled={!bepozVersion} checked={item.required} />
                        </td>
                      ),
                      [fields[5]]: (item) => (
                        <td className="pl-5">
                          <CInputCheckbox disabled={!bepozVersion} checked={item.active} />
                        </td>
                      ),
                      [fields[6]]: (item) => (
                        <td className="pl-5 justify-content-center">
                          <CInputCheckbox disabled={!bepozVersion} checked={item.displayInApp} />
                        </td>
                      ),
                      [fields[7]]: (item) => (
                        <td>
                          <CRow>
                            {!!item.multiValues &&
                              item.multiValues?.map((item) => {
                                return (
                                  <div className="bg-gray-100 mr-2 pl-2 pr-2 rounded mb-1">
                                    <span className="text-dark">{item}</span>
                                  </div>
                                )
                              })}
                          </CRow>
                        </td>
                      ),
                      [fields[8]]: (item) => (
                        <td>
                          <CIcon content={freeSet.cilTrash} />
                        </td>
                      ),
                    }}
                  />
                </CCol>
              </CCol>
            </CTabPane>
          </CTabContent>
        </CTabs>
      )}
    </>
  )
}

export default GamingIntegration
