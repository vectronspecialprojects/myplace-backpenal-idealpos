import {CButton, CCol, CRow} from '@coreui/react'
import SettingLayout from '../SettingLayout'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import {useDispatch, useSelector} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
const ThirdPartyIntegration = ({setting}) => {
  const [state, setState] = useState(setting?.third_party_url?.value)
  const bepozVersion = useSelector((state) => state.app.bepozVersion)
  const dispatch = useDispatch()
  function handleSave() {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify([
          {
            ...setting?.third_party_url,
            value: state,
          },
        ]),
      }),
    )
  }

  return (
    <>
      <CRow>
        <SettingLayout
          title="URL Call service"
          desc="Type it with complete url. e.g. https://192.168.10.10:9993/a/a">
          <FormInput disabled={!bepozVersion} value={state} onChangeText={setState} />
        </SettingLayout>
        <CCol xs={12}>
          <CButton
            disabled={!bepozVersion}
            variant="outline"
            color="secondary"
            className="text-dark font-weight-bold mt-3"
            onClick={() => {
              handleSave()
            }}>
            Save Changes
          </CButton>
        </CCol>
      </CRow>
    </>
  )
}
export default ThirdPartyIntegration
