import {CButton, CCol} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'
import {postWithTimeout} from '../../../../utilities/networking'

const PondHoppers = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    pond_hoppers_url: setting.pond_hoppers_url,
    pond_hoppers_unique_key: setting.pond_hoppers_unique_key,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Pledge URI" desc="ex: https://www.bepoz.com.au">
          <FormInput value={state.pond_hoppers_url.value} onChangeText={changeValue('pond_hoppers_url')} />
        </Layout>
        <Layout contentWidth={4} title="Pledge Unique Key" desc="">
          <FormInput
            value={state.pond_hoppers_unique_key.value}
            onChangeText={changeValue('pond_hoppers_unique_key')}
          />
        </Layout>

        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default PondHoppers
