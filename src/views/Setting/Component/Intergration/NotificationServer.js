import {CButton, CCol} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'

const NotificationServer = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    onesignal_app_id: setting.onesignal_app_id,
    onesignal_rest_api_key: setting.onesignal_rest_api_key,
    onesignal_user_auth_key: setting.onesignal_user_auth_key,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Onesignal App ID" desc="">
          <FormInput value={state.onesignal_app_id.value} onChangeText={changeValue('onesignal_app_id')} />
        </Layout>
        <Layout contentWidth={4} title="Onesignal Rest Api key" desc="">
          <FormInput
            value={state.onesignal_rest_api_key.value}
            onChangeText={changeValue('onesignal_rest_api_key')}
          />
        </Layout>
        <Layout contentWidth={4} title="Onesignal User Auth Key" desc="">
          <FormInput
            value={state.onesignal_user_auth_key.value}
            onChangeText={changeValue('onesignal_user_auth_key')}
          />
        </Layout>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default NotificationServer
