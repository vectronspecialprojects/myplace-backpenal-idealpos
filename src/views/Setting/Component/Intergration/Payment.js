import {CButton, CCol, CLink, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import SettingLayout from '../SettingLayout'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_ON_OFF, SETTING_STRIPE_FEE_OPTION} from '../../constant'
import {useDispatch, useSelector} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'

const Payment = ({setting}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [state, setState] = useState({
    stripe_testmode: setting.stripe_testmode,
    stripe_connected_account_id: setting.stripe_connected_account_id,
    stripe_testkey: setting.stripe_testkey,
    stripe_test_publishable_key: setting.stripe_test_publishable_key,
    stripe_test_application_fee_option: setting.stripe_test_application_fee_option,
    stripe_test_fixed_application_fee: setting.stripe_test_fixed_application_fee,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Stripe Test Mode" desc="Enable this for transaction testing purpose.">
          <FormSelect
            options={SETTING_ON_OFF}
            value={state.stripe_testmode.value}
            onValueChange={changeValue('stripe_testmode')}
          />
        </Layout>
        <Layout contentWidth={4} title="Test" desc="Development">
          <CLink
            variant="outline"
            color="primary"
            className="text-dark font-weight-bold mt-3"
            target="_blank"
            href="https://connect.stripe.com/oauth/v2/authorize?response_type=code&client_id=ca_CrGWrM6cb3B9hch2FEIfjsFTIj9Bb0T4&scope=read_write">
            Connect with Strip (DEV)
          </CLink>
        </Layout>
        {unlock && (
          <>
            <Layout contentWidth={4} title="Stripe Secret Key" desc="Set stripe test secret key here">
              <FormInput value={state.stripe_testkey.value} onChangeText={changeValue('stripe_testkey')} />
            </Layout>
            <Layout
              contentWidth={4}
              title="Stripe Publishable Key"
              desc="Set stripe test publishable key here">
              <FormInput
                value={state.stripe_test_publishable_key.value}
                onChangeText={changeValue('stripe_test_publishable_key')}
              />
            </Layout>
            <Layout contentWidth={4}
              title="Stripe Application Fee Option"
              desc="Set the application fee to your client (test).">
              <FormSelect
                value={state.stripe_test_application_fee_option.value}
                options={SETTING_STRIPE_FEE_OPTION}
                onValueChange={changeValue('stripe_test_application_fee_option')}
              />
            </Layout>
            <Layout contentWidth={4}
              title="Stripe Fixed Application Fee (cents)"
              desc="Set stripe test fixed application fee here (number only). Exact amount in cents e.g: 5 (it means 5 cents)">
              <FormInput
                value={state.stripe_test_fixed_application_fee.value}
                onChangeText={changeValue('stripe_test_fixed_application_fee')}
              />
            </Layout>
          </>
        )}
        <Layout contentWidth={4}
          title="Stripe Connected Account ID"
          desc="Set stripe test connected account ID here. Please click blue button above to retrieve account ID">
          <FormInput
            value={state.stripe_connected_account_id.value}
            onChangeText={changeValue('stripe_connected_account_id')}
          />
        </Layout>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default Payment
