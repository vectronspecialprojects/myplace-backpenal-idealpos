import {CButton, CCol} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'

const smsParams = ['{{FirstName}}', '{{LastName}}', '{{AccountNumber}}', '{{Tier}}', '{{Link}}']

const Pledge = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    sms_from: setting.sms_from,
    sms_burst_key: setting.sms_burst_key,
    sms_burst_secret: setting.sms_burst_secret,
    sms_confirmation_message: setting.sms_confirmation_message,
    sms_mobile_change_confirmation_message: setting.sms_mobile_change_confirmation_message,
    sms_resend_details_message: setting.sms_resend_details_message,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Burst From" desc="">
          <FormInput value={state.sms_from.value} onChangeText={changeValue('sms_from')} />
        </Layout>
        <Layout contentWidth={4} title="Burst Key" desc="">
          <FormInput value={state.sms_burst_key.value} onChangeText={changeValue('sms_burst_key')} />
        </Layout>
        <Layout contentWidth={4} title="Burst Secret" desc="">
          <FormInput value={state.sms_burst_secret.value} onChangeText={changeValue('sms_burst_secret')} />
        </Layout>

        <div className="mb-5">
          <h3>Message</h3>
          <Layout
            contentWidth={4}
            title="Please use the following text pattern to create dynamic messages"
            desc="ex: Hi {{FirstName}} {{LastName}} {{AccountNumber}} {{Tier}}, you’re a click away from connecting - verify your mobile below {{Link}}"
          />
          {smsParams.map((param) => (
            <p>{param}</p>
          ))}
        </div>

        <Layout contentWidth={12} title="SMS Confirmation Message" desc="">
          <FormInput
            value={state.sms_confirmation_message.value}
            onChangeText={changeValue('sms_confirmation_message')}
          />
        </Layout>
        <Layout contentWidth={12} title="SMS Change Mobile Confirmation Message" desc="">
          <FormInput
            value={state.sms_mobile_change_confirmation_message.value}
            onChangeText={changeValue('sms_mobile_change_confirmation_message')}
          />
        </Layout>
        <Layout contentWidth={12} title="SMS Resend Details Message" desc="">
          <FormInput
            value={state.sms_resend_details_message.value}
            onChangeText={changeValue('sms_resend_details_message')}
          />
        </Layout>

        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default Pledge
