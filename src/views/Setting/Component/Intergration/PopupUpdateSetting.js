import React, {useEffect, useState} from 'react'
import {CButton, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import helpers from '../../../Listing/helpers'
import {putEmail} from '../../../../utilities/ApiManage'

function PopupUpdateSetting({show, closeModal, selectedEmail, onSubmit}) {
  const [form, setForm] = useState({})

  const changeForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  useEffect(() => {
    if (selectedEmail) {
      setForm(
        {...selectedEmail, settings: selectedEmail.settings.reduce((acc, _) => ({...acc, [_.key]: _}), {})} ||
          {},
      )
    }
  }, [show, selectedEmail])

  const saveAndClose = async () => {
    const res = await putEmail(selectedEmail.id, {
      ...form,
      settings: JSON.stringify(Object.values(form.settings)),
    })
    if (res.ok) {
      closeModal()
      onSubmit()
    }
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Update Setting</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12} md={6}>
              <FormInput label="Name" id="name" onChange={changeForm} value={form.name} />
              <FormInput
                label="Template"
                id="settings.template.value"
                onChange={changeForm}
                value={form.settings?.template?.value}
              />
              <FormInput
                label="Subject"
                id="settings.subject.value"
                onChange={changeForm}
                value={form.settings?.subject?.value}
              />
              <FormInput
                label="Template id"
                id="settings.template_id.value"
                onChange={changeForm}
                value={form.settings?.template_id?.value}
              />
            </CCol>
            <CCol xs={12} md={6}>
              <h5>If you use mailjet for sending email, please fill template ID field, unless it not work</h5>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupUpdateSetting
