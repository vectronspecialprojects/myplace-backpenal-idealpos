import Collapse from '../../../../common/Collapse'
import {CNav, CNavItem, CNavLink, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import EmailServer from './EmailServer'
import NotificationServer from './NotificationServer'
import Payment from './Payment'
import PondHoppers from './PondHoppers'
import GamingIntegration from './GamingIntegration'
import YourOrderIntegration from './YourOrderIntegration'
import SmsServer from './SmsServer'
import {useSelector} from 'react-redux'
import {isTrue} from '../../../../helpers'
import ThirdPartyIntegration from './ThirdPartyIntegration'

const Integration = ({setting}) => {
  const [tab, setTab] = useState(0)
  const appSettings = useSelector((state) => state.app.appSettings)

  return (
    <Collapse title="Integration">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>SSO Setup</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Mailchimp</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Onesignal</CNavLink>
          </CNavItem>
          {isTrue(appSettings?.stripe_integration_show?.value) && (
            <CNavItem>
              <CNavLink>Stripe</CNavLink>
            </CNavItem>
          )}

          {isTrue(appSettings?.gaming_integration_show?.value) && (
            <CNavItem>
              <CNavLink>Gaming Integration</CNavLink>
            </CNavItem>
          )}

          {isTrue(appSettings?.third_party_integration_show?.value) && (
            <CNavItem>
              <CNavLink>Third Party URI</CNavLink>
            </CNavItem>
          )}

          {isTrue(appSettings?.burst_sms_integration_show?.value) && (
            <CNavItem>
              <CNavLink>BURST SMS</CNavLink>
            </CNavItem>
          )}

          {isTrue(appSettings?.pond_hoppers_enable?.value) && (
            <CNavItem>
              <CNavLink>Pond Hoppers</CNavLink>
            </CNavItem>
          )}

        </CNav>
        <CTabContent style={{marginTop: 20}}>
          <CTabPane>
            <YourOrderIntegration setting={setting} />
          </CTabPane>

          <CTabPane>
            <EmailServer setting={setting} />
          </CTabPane>
          <CTabPane>
            <NotificationServer setting={setting} />
          </CTabPane>
          {isTrue(appSettings?.stripe_integration_show?.value) && (
            <CTabPane>
              <Payment setting={setting} />
            </CTabPane>
          )}
          {isTrue(appSettings?.gaming_integration_show?.value) && (
            <CTabPane>
              <GamingIntegration setting={setting} />
            </CTabPane>
          )}
          {isTrue(appSettings?.third_party_integration_show?.value) && (
            <CTabPane>
              <ThirdPartyIntegration setting={setting} />
            </CTabPane>
          )}

          {isTrue(appSettings?.burst_sms_integration_show?.value) && (
            <CTabPane>
              <SmsServer setting={setting} />
            </CTabPane>
          )}
          {isTrue(appSettings?.pond_hoppers_enable?.value) && (
            <CTabPane>
              <PondHoppers setting={setting} />
            </CTabPane>
          )}
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

export default Integration
