import {CButton, CCol, CDataTable, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getEmail} from '../../../../utilities/ApiManage'
import FormInput from '../../../../common/FormInput'
import FormSelect from '../../../../common/FormSelect'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import PopupUpdateSetting from './PopupUpdateSetting'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'

const fields = [{key: 'id', label: '#'}, {key: 'name', label: 'Email'}, {key: 'option'}]

const EmailServer = ({setting}) => {
  const [data, setData] = useState([])
  const [state, setState] = useState({
    email_server: {...setting.email_server, extended_value: JSON.parse(setting.email_server.extended_value)},
    mandrill_key: setting.mandrill_key,
    mandrill_email: setting.mandrill_email,
    mandrill_sender_name: setting.mandrill_sender_name,
  })
  const [selectedEmail, setSelectedEmail] = useState(null)
  const dispatch = useDispatch()

  const fetchEmail = async () => {
    const res = await getEmail()
    if (!res.ok) throw new Error(res.message)
    setData(res.data)
  }

  useEffect(() => {
    fetchEmail()
  }, [])

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Email Server">
          <FormSelect
            options={state.email_server.extended_value}
            value={state.email_server.value}
            getValue={(_) => _}
            getLabel={(_) => _}
            onValueChange={changeValue('email_server')}
          />
        </Layout>
        <Layout contentWidth={4} title="Mandrill Secret" desc="">
          <FormInput value={state.mandrill_key.value} onChangeText={changeValue('mandrill_key')} />
        </Layout>
        <Layout contentWidth={4} title="Mandrill Sender Smail" desc="">
          <FormInput value={state.mandrill_email.value} onChangeText={changeValue('mandrill_email')} />
        </Layout>
        <Layout contentWidth={4} title="Mandrill Sender Name" desc="">
          <FormInput
            value={state.mandrill_sender_name.value}
            onChangeText={changeValue('mandrill_sender_name')}
          />
        </Layout>

        <CDataTable
          items={data || []}
          fields={fields}
          striped
          hover={true}
          clickableRows={true}
          onRowClick={(item) => setSelectedEmail(item)}
          scopedSlots={{
            option: (item) => (
              <td>
                <CRow>
                  <CButton onClick={() => setSelectedEmail(item)}>
                    <CIcon content={freeSet.cilPen} />
                  </CButton>
                  {/* <CButton onClick={() => {}} disabled>
                    <CIcon content={freeSet.cilTrash} />
                  </CButton> */}
                </CRow>
              </td>
            ),
          }}
        />

        <PopupUpdateSetting
          show={selectedEmail}
          closeModal={() => setSelectedEmail(null)}
          onSubmit={fetchEmail}
          selectedEmail={selectedEmail}
        />
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default EmailServer
