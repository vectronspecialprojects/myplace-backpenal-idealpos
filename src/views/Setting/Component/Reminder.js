import Collapse from '../../../common/Collapse'
import {CButton, CCol, CNav, CNavItem, CNavLink, CRow, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import {SETTING_ON_OFF} from '../constant'
import FormSelect from '../../../common/FormSelect'
import TagInput from '../../../common/TagInput'
import FormTime from '../../../common/FormTime'
import CIcon from '@coreui/icons-react'
import {useDispatch, useSelector} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import Layout from './components/Layout'

const Reminder = ({setting}) => {
  const [tab, setTab] = useState(0)

  return (
    <Collapse title="Reminder">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>Event Reminder</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Voucher Reminder</CNavLink>
          </CNavItem>
        </CNav>
        <CTabContent>
          <CTabPane>
            <Item setting={setting} title="Event Reminder" />
          </CTabPane>
          <CTabPane>
            <Item
              selectKey={'reminder_voucher'}
              dayKey={'reminder_voucher_interval'}
              timeKey={'reminder_voucher_time'}
              setting={setting}
              title="Voucher Reminder"
            />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

const Item = ({
  selectKey = 'reminder_ticket',
  dayKey = 'reminder_ticket_interval',
  timeKey = 'reminder_ticket_time',
  title,
  setting,
}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [state, setState] = useState({
    [selectKey]: setting[selectKey],
    [dayKey]: {...setting[dayKey], extended_value: JSON.parse(setting[dayKey].extended_value)},
    [timeKey]: {
      ...setting[timeKey],
      extended_value: JSON.parse(setting[timeKey].extended_value),
    },
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const addTime = async (e) => {
    if (e.target.value && !state[timeKey].extended_value.includes(e.target.value)) {
      state[timeKey].extended_value.push(e.target.value)
      setState({...state})
    }
  }

  const removeTime = async (index) => {
    state[timeKey].extended_value.splice(index, 1)
    setState({...state})
  }
  return (
    <>
      <CCol className="mt-2">
        <h4>{title}</h4>
        <Layout
          title={
            selectKey === 'reminder_ticket'
              ? 'Send Email for reminding before Special Event Date to whom bought ticket'
              : 'Send Email for reminding before voucher expired'
          }
          desc={
            selectKey === 'reminder_ticket'
              ? 'Set Send Email for reminding before Special Event Date to whom bought ticket'
              : 'Set Send Email for reminding before voucher expired'
          }
          contentWidth={4}>
          <FormSelect
            options={SETTING_ON_OFF}
            value={state[selectKey].value}
            disabled={!unlock}
            onValueChange={(value) => {
              setState({...state, [selectKey]: {...setting[selectKey], value}})
            }}
          />
        </Layout>

        <h6 className="text-danger">
          {selectKey === 'reminder_ticket'
            ? 'This is default setting for all events, you can customized each event in Regular Event or Special Event'
            : 'This is default setting for all voucher'}
        </h6>

        <Layout
          title={
            selectKey === 'reminder_ticket'
              ? 'Email Reminder Interval (days) before the Event'
              : 'Email Reminder Interval (days) before the expired'
          }
          desc={
            selectKey === 'reminder_ticket'
              ? 'Schedules for reminding before Special Event Date to whom bought ticket'
              : 'Schedules for reminding before voucher expired'
          }>
          <TagInput
            value={state[dayKey].extended_value}
            int
            onValueChange={(value) => {
              setState({...state, [dayKey]: {...setting[dayKey], extended_value: value}})
            }}
          />
        </Layout>

        <h6>At what time reminder should be sent?</h6>
        <Layout title="Select time here" contentWidth={4}>
          <FormTime onBlur={addTime} step="3600" />
        </Layout>
        <Layout>
          <div className="d-flex flex-wrap mt-1">
            {state[timeKey].extended_value.map((_, i) => (
              <div
                key={i}
                className="d-flex rounded-pill bg-secondary mr-1 mb-1 py-1 px-2 align-items-center">
                <span>{_}</span>
                <CButton className="ml-1 p-0" onClick={() => removeTime(i)}>
                  <CIcon name="cil-x" />
                </CButton>
              </div>
            ))}
          </div>
        </Layout>

        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default Reminder
