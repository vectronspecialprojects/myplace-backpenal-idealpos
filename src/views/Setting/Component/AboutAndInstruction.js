import Collapse from '../../../common/Collapse'
import {CButton, CCol, CNav, CNavItem, CNavLink, CRow, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import RichEditor from '../../../common/RichEditor'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'

const AboutAndInstruction = ({setting}) => {
  const [tab, setTab] = useState(0)

  return (
    <Collapse title="About & Instruction">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>About</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Match Email Instruction</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Match Bepoz Instruction</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Resend Details Instruction</CNavLink>
          </CNavItem>
        </CNav>
        <CTabContent>
          <CTabPane>
            <Item setting={setting} />
          </CTabPane>
          <CTabPane>
            <Item
              field="instruction_match_email_mobile"
              setting={setting}
              title="This Instruction Appears on Match Email Page."
            />
          </CTabPane>
          <CTabPane>
            <Item
              field="instruction_match_bepoz_id_number"
              setting={setting}
              title="This Instruction Appears on Match Bepoz Id & Number Page."
            />
          </CTabPane>
          <CTabPane>
            <Item
              field="instruction_resend_details"
              setting={setting}
              title="This Instruction Appears on Resend Details Page."
            />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

const Item = ({field = 'about', title, setting}) => {
  const [state, setState] = useState({[field]: {...setting[field]}})
  const dispatch = useDispatch()
  const clickSave = async () => {
    dispatch(updateAppSettings({settings: JSON.stringify(Object.values(state))}))
  }

  const updateForm = async (e) => {
    state[field].extended_value = e.target.value
    setState({...state})
  }
  return (
    <>
      {title && <h4 className="text-capitalize">{title}</h4>}

      <CRow>
        <CCol xs={12} md={8}>
          <RichEditor initValue={state[field].extended_value || ''} onChange={updateForm} />
        </CCol>

        <CCol xs={12}>
          <CButton
            variant="outline"
            color="secondary"
            className="text-dark font-weight-bold mt-3"
            onClick={clickSave}>
            Save Changes
          </CButton>
        </CCol>
      </CRow>
    </>
  )
}
export default AboutAndInstruction
