import Collapse from '../../../../common/Collapse'
import {CAlert, CNav, CNavItem, CNavLink, CTabContent, CTabPane, CTabs} from '@coreui/react'
import React, {useState} from 'react'
import Roles from './Roles'
import OTP from './OTP'
import GoogleRecaptcha from './GoogleRecaptcha'

const Security = ({setting}) => {
  const [tab, setTab] = useState(0)
  return (
    <Collapse title="Security">
      <CTabs activeTab={tab} onActiveTabChange={setTab}>
        <CNav variant="tabs">
          <CNavItem>
            <CNavLink>Roles</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>OTP</CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink>Google Recaptcha</CNavLink>
          </CNavItem>
        </CNav>
        <CTabContent style={{marginTop: 20}}>
          <CTabPane>
            <Roles setting={setting} />
          </CTabPane>
          <CTabPane>
            <OTP setting={setting} />
          </CTabPane>
          <CTabPane>
            <GoogleRecaptcha setting={setting} />
          </CTabPane>
        </CTabContent>
      </CTabs>
    </Collapse>
  )
}

export default Security
