import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import SettingLayout from '../SettingLayout'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_ON_OFF} from '../../constant'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout'

const OTP = ({setting}) => {
  const [state, setState] = useState({
    enable_otp: setting.enable_otp,
    sms_from: setting.sms_from,
    sms_burst_key: setting.sms_burst_key,
    sms_burst_secret: setting.sms_burst_secret,
  })
  const dispatch = useDispatch()

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout
          contentWidth={4}
          title="Enable OTP"
          desc="Enable one time password for 2-factor authentication">
          <FormSelect
            options={SETTING_ON_OFF}
            value={state.enable_otp.value}
            onValueChange={changeValue('enable_otp')}
          />
        </Layout>
        <Layout contentWidth={4} title="Burst SMS From" desc="">
          <FormInput value={state.sms_from.value} onChangeText={changeValue('sms_from')} />
        </Layout>
        <Layout contentWidth={4} title="Burst Key" desc="">
          <FormInput value={state.sms_burst_key.value} onChangeText={changeValue('sms_burst_key')} />
        </Layout>
        <Layout contentWidth={4} title="Burst Secret" desc="">
          <FormInput value={state.sms_burst_secret.value} onChangeText={changeValue('sms_burst_secret')} />
        </Layout>
        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default OTP
