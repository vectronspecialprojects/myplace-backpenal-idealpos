import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../../common/FormInput'
import SettingLayout from '../SettingLayout'
import FormSelect from '../../../../common/FormSelect'
import {SETTING_ON_OFF} from '../../constant'
import {useDispatch, useSelector} from 'react-redux'
import {updateAppSettings} from '../../../../redux/actions/app'
import Layout from '../components/Layout';

const GoogleRecaptcha = ({setting}) => {
  const unlock = useSelector((state) => state?.app?.settingUnlock)
  const dispatch = useDispatch()
  const [state, setState] = useState({
    recaptcha_enable: setting.recaptcha_enable,
    recaptcha_site_key: setting.recaptcha_site_key,
    recaptcha_server_key: setting.recaptcha_server_key,
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  return (
    <>
      <CCol>
        <Layout contentWidth={4} title="Use Google Recaptcha" desc="Enable google recaptcha on the back panel">
          <FormSelect
            options={SETTING_ON_OFF}
            value={state.recaptcha_enable.value}
            disabled={!unlock}
            onValueChange={changeValue('recaptcha_enable')}
          />
        </Layout>
        <Layout contentWidth={4} title="Google Recaptcha Site Key" desc="">
          <FormInput
            value={state.recaptcha_site_key.value}
            onChangeText={changeValue('recaptcha_site_key')}
          />
        </Layout>
        <Layout contentWidth={4} title="Google Recaptcha Sever Key" desc="">
          <FormInput
            value={state.recaptcha_server_key.value}
            onChangeText={changeValue('recaptcha_server_key')}
          />
        </Layout>

        <CButton
          variant="outline"
          color="secondary"
          className="text-dark font-weight-bold mt-3"
          onClick={clickSave}>
          Save Changes
        </CButton>
      </CCol>
    </>
  )
}

export default GoogleRecaptcha
