import {CButton, CCol, CDataTable, CPagination, CRow} from '@coreui/react'
import React, {useEffect, useState} from 'react'
import {getPaginateRole} from '../../../../utilities/ApiManage'
import CIcon from '@coreui/icons-react'
import {freeSet} from '@coreui/icons/js/free'
import PopupEditRole from './PopupEditRole'

const fields = [{key: 'id', label: '#'}, 'name', 'description', {key: 'option'}]

const Roles = ({setting}) => {
  const [data, setData] = useState({})
  const [currentPage, setCurrentPage] = useState(1)
  const [itemPerPage, setItemPerPage] = useState(10)
  const [selectedRole, setSelectedRole] = useState(null)

  const fetchData = async () => {
    const res = await getPaginateRole(currentPage, itemPerPage)
    if (!res.ok) throw new Error(res.message)
    setData(res.data)
  }

  useEffect(() => {
    fetchData()
  }, [currentPage, itemPerPage])

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <CDataTable
            items={data?.data || []}
            fields={fields}
            striped
            itemsPerPage={itemPerPage}
            hover={true}
            clickableRows={true}
            onRowClick={(item) => setSelectedRole(item)}
            scopedSlots={{
              option: (item) => (
                <td>
                  <CRow>
                    <CButton onClick={() => setSelectedRole(item)}>
                      <CIcon content={freeSet.cilPen} />
                    </CButton>
                  </CRow>
                </td>
              ),
            }}
          />
          <CPagination
            activePage={currentPage}
            pages={Math.ceil(data?.total / itemPerPage)}
            onActivePageChange={setCurrentPage}
          />
        </CCol>
        <PopupEditRole
          show={!!selectedRole}
          onSubmit={fetchData}
          closeModal={() => setSelectedRole(null)}
          selectedRole={selectedRole}
        />
      </CRow>
    </>
  )
}

export default Roles
