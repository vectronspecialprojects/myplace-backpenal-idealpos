import React, {useEffect, useState} from 'react'
import {CButton, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow} from '@coreui/react'
import FormInput from '../../../../common/FormInput'
import helpers from '../../../Listing/helpers'
import FormCheckbox from '../../../../common/FormCheckbox'
import {putRole} from '../../../../utilities/ApiManage'

function PopupEditRole({show, closeModal, selectedRole, onSubmit}) {
  const [form, setForm] = useState({})

  const changeForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  useEffect(() => {
    setForm(selectedRole || {})
  }, [show, selectedRole])

  const saveAndClose = async () => {
    const res = await putRole(selectedRole.id, {...form, control: JSON.stringify(form.control)})
    if (res.ok) {
      closeModal()
      onSubmit()
    }
  }

  const getCRUDRoles = (key) => {
    const role = [{key: 'create'}, {key: 'read'}, {key: 'update'}, {key: 'delete', label: '(SOFT) DELETE'}]
    return role.map((item) => {
      const field = `${key}_${item.key}`
      return (
        <FormCheckbox
          key={item.key}
          id={`control.${field}`}
          checked={form.control?.[field]}
          label={item.label || item.key.toUpperCase()}
          onChange={changeForm}
        />
      )
    })
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Update Role Controls</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12}>
              <FormInput
                label="Name"
                value={form.name}
                id="name"
                onChange={changeForm}
                maxLength="255"
                disabled
              />
              <FormInput
                label="Description"
                value={form.description}
                id="description"
                onChange={changeForm}
                maxLength="255"
                disabled
              />
            </CCol>

            <div className="d-flex w-100 p-3">
              <div className="flex-fill">
                <h5>General</h5>
                <FormCheckbox
                  id="control.root_access"
                  checked={form.control?.root_access}
                  label="ROOT ACCESS"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.backend_access"
                  checked={form.control?.backend_access}
                  label="BACKEND ACCESS"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.frontend_access"
                  checked={form.control?.frontend_access}
                  label="FRONTEND ACCESS"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.venue_management"
                  checked={form.control?.venue_management}
                  label="VENUE MANAGEMENT"
                  onChange={changeForm}
                />

                <div className="border-bottom my-2" />
                <h5>Role</h5>
                {getCRUDRoles('role')}

                <div className="border-bottom my-2" />
                <h5>Setting</h5>
                <FormCheckbox
                  id="control.setting_read"
                  checked={form.control?.setting_read}
                  label="READ"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.setting_update"
                  checked={form.control?.setting_update}
                  label="UPDATE"
                  onChange={changeForm}
                />
              </div>

              <div className="flex-fill">
                <h5>Venue</h5>
                {getCRUDRoles('venue')}

                <div className="border-bottom my-2" />
                <h5>User Role (Access)</h5>
                {getCRUDRoles('user_role')}

                <div className="border-bottom my-2" />
                <h5>Report</h5>
                <FormCheckbox
                  id="control.report_read"
                  checked={form.control?.report_read}
                  label="READ"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.report_export"
                  checked={form.control?.report_export}
                  label="EXPORT"
                  onChange={changeForm}
                />
              </div>
              <div className="flex-fill">
                <h5>Staff</h5>
                {getCRUDRoles('staff')}

                <div className="border-bottom my-2" />
                <h5>Member</h5>
                {getCRUDRoles('member')}

                <div className="border-bottom my-2" />
                <h5>Transaction</h5>
                <FormCheckbox
                  id="control.transaction_read"
                  checked={form.control?.transaction_read}
                  label="READ"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.transaction_export"
                  checked={form.control?.transaction_export}
                  label="EXPORT"
                  onChange={changeForm}
                />
              </div>
              <div className="flex-fill">
                <h5>Promo, Events, G.Cert</h5>
                {getCRUDRoles('listing')}

                <div className="border-bottom my-2" />
                <h5>Voucher</h5>
                {getCRUDRoles('voucher')}

                <div className="border-bottom my-2" />
                <h5>Log</h5>
                <FormCheckbox
                  id="control.log_read"
                  checked={form.control?.log_read}
                  label="READ"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.log_delete"
                  checked={form.control?.log_delete}
                  label="(SOFT) DELETE"
                  onChange={changeForm}
                />
              </div>
              <div className="flex-fill">
                <h5>Notification</h5>
                {getCRUDRoles('notification')}

                <div className="border-bottom my-2" />
                <h5>Survey</h5>
                {getCRUDRoles('survey')}

                <div className="border-bottom my-2" />
                <h5>Extra</h5>
                <FormCheckbox
                  id="control.resend_confirmation"
                  checked={form.control?.resend_confirmation}
                  label="RESEND_CONFIRMATION"
                  onChange={changeForm}
                />
                <FormCheckbox
                  id="control.send_broadcast"
                  checked={form.control?.send_broadcast}
                  label="SEND BROADCAST"
                  onChange={changeForm}
                />
              </div>
            </div>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupEditRole
