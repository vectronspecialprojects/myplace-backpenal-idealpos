import React, {useEffect, useState} from 'react'
import {
  CButton,
  CCol,
  CFormGroup,
  CInputRadio,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import {putListingType} from '../../../utilities/ApiManage'
import {SETTING_LISTING_CATEGORY} from '../constant'
import helpers from '../../Listing/helpers'
import FormCheckbox from '../../../common/FormCheckbox'
import FormInput from '../../../common/FormInput'

function PopupUpdateElementGroup({show, closeModal, selectedListing, onSubmit}) {
  const [form, setForm] = useState({})

  const changeForm = (event) => setForm((oldState) => helpers.updateForm(event, oldState))

  useEffect(() => {
    setForm(selectedListing || {})
  }, [show, selectedListing])

  const saveAndClose = async () => {
    const res = await putListingType(selectedListing.id, form)
    if (res.ok) {
      closeModal()
      onSubmit()
    }
  }

  return (
    <>
      <CModal show={show} onClose={closeModal} size="xl">
        <CModalHeader closeButton>
          <CModalTitle>Update Element Group</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xs={12}>
              <FormInput value={form.key?.name} required label="Name" onChange={changeForm} />
            </CCol>
            <CCol xs={3}>
              {SETTING_LISTING_CATEGORY.map((_) => (
                <CFormGroup variant="checkbox" key={_.value}>
                  <CInputRadio
                    className="form-check-input"
                    id="key.category"
                    checked={form.key?.category === _.value}
                    value={_.value}
                    onChange={changeForm}
                  />
                  <CLabel>{_.label}</CLabel>
                </CFormGroup>
              ))}
              <FormCheckbox
                id="key.hide_this"
                checked={form.key?.hide_this}
                label="Hide this element group"
                onChange={changeForm}
              />
            </CCol>
            <CCol xs={3}>
              <FormCheckbox
                id="key.feature_display_schedules"
                checked={form.key?.feature_display_schedules}
                label="Display-Schedule"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.feature_products"
                checked={form.key?.feature_products}
                label="Tickets"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.feature_vouchers"
                checked={form.key?.feature_vouchers}
                label="Vouchers"
                onChange={changeForm}
              />
            </CCol>
            <CCol xs={3}>
              <FormCheckbox
                id="key.setting_maximum_allocation"
                checked={form.key?.setting_maximum_allocation}
                label="Maximum Allocation"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_time_driven"
                checked={form.key?.setting_time_driven}
                label="Time-Driven Voucher"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_show_time"
                checked={form.key?.setting_show_time}
                label="Show Time"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_show_banner_image"
                checked={form.key?.setting_show_banner_image}
                label="Use Banner Image"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_show_square_image"
                checked={form.key?.setting_show_square_image}
                label="Use Square Image"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_show_long_description"
                checked={form.key?.setting_show_long_description}
                label="Use Long Description"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_show_venue_selection"
                checked={form.key?.setting_show_venue_selection}
                label="Show Venue Selection"
                onChange={changeForm}
              />
            </CCol>
            <CCol xs={3}>
              <FormCheckbox
                id="key.option_inquirable"
                checked={form.key?.option_inquirable}
                label="Allow Inquiry"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.option_purchasable"
                checked={form.key?.option_purchasable}
                label="Allow Purchase"
                onChange={changeForm}
              />
              <FormCheckbox
                disabled
                id="key.option_specific"
                checked={form.key?.option_specific}
                label="Specific Date"
                onChange={changeForm}
              />
              <FormCheckbox
                disabled
                id="key.option_regular"
                checked={form.key?.option_regular}
                label="Regular Time"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.setting_never_expired"
                checked={form.key?.setting_never_expired}
                label="Never Expired"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.non_bepoz_offer"
                checked={form.key?.non_bepoz_offer}
                label="Non Bepoz Offer"
                onChange={changeForm}
              />
              <FormCheckbox
                id="key.sales_report"
                checked={form.key?.sales_report}
                label="Sales Report"
                onChange={changeForm}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={saveAndClose}>
            SAVE & CLOSE
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default PopupUpdateElementGroup
