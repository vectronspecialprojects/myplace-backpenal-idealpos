import Collapse from '../../../common/Collapse'
import {CButton, CCol, CRow} from '@coreui/react'
import React, {useState} from 'react'
import FormInput from '../../../common/FormInput'
import TagInput from '../../../common/TagInput'
import {validateEmail} from '../../../helpers/validation'
import {toast} from 'react-toastify'
import {useDispatch} from 'react-redux'
import {updateAppSettings} from '../../../redux/actions/app'
import Layout from './components/Layout';

const EmailNotification = ({setting}) => {
  const dispatch = useDispatch()
  const [state, setState] = useState({
    admin_full_name: setting.admin_full_name,
    admin_email: {...setting.admin_email, extended_value: JSON.parse(setting.admin_email.extended_value)},
    enquiry_email: {
      ...setting.enquiry_email,
      extended_value: JSON.parse(setting.enquiry_email.extended_value),
    },
    signup_email: {...setting.signup_email, extended_value: JSON.parse(setting.signup_email.extended_value)},
    system_check_email: {
      ...setting.system_check_email,
      extended_value: JSON.parse(setting.system_check_email.extended_value),
    },
  })

  const clickSave = async () => {
    dispatch(
      updateAppSettings({
        settings: JSON.stringify(Object.values(state)),
      }),
    )
  }

  const changeValue = (key) => (value) => {
    setState({...state, [key]: {...state[key], value}})
  }

  const changeEmail = (key) => (value) => {
    if (!value?.length || !validateEmail(value[value.length - 1]))
      setState({...state, [key]: {...setting[key], extended_value: value}})
    else toast('Invalid email format', {type: 'warning'})
  }

  return (
    <Collapse title="Email Notifications">
      <CCol>
        <Layout title="Admin Name" desc="It'll be used for mailing purpose" contentWidth={4} >
          <FormInput value={state.admin_full_name.value} onChangeText={changeValue('admin_full_name')} />
        </Layout>
        <Layout title="Admin Email(s)" desc="Set email address that will receive un-specified notifications.">
          <TagInput value={state.admin_email.extended_value} onValueChange={changeEmail('admin_email')} />
        </Layout>
        <Layout title="Enquiry Email(s)" desc="Set email address that will receive all enquiries.">
          <TagInput value={state.enquiry_email.extended_value} onValueChange={changeEmail('enquiry_email')} />
        </Layout>
        <Layout title="Signup Email(s)" desc="Set email address that will receive all sign-up notifications.">
          <TagInput value={state.signup_email.extended_value} onValueChange={changeEmail('signup_email')} />
        </Layout>
        <Layout
          title="System Check Email(s)"
          desc="Set email address that will receive all system check notifications.">
          <TagInput
            value={state.system_check_email.extended_value}
            onValueChange={changeEmail('system_check_email')}
          />
        </Layout>
      </CCol>

      <CButton
        variant="outline"
        color="secondary"
        className="text-dark font-weight-bold mt-3"
        onClick={clickSave}>
        Save Changes
      </CButton>
    </Collapse>
  )
}


export default EmailNotification
